package com.oseasy.service.test;

import com.oseasy.xlxq.service.ServiceConfig;
import com.oseasy.xlxq.service.api.ServiceApiConfig;
import com.oseasy.xlxq.service.api.sys.model.TenantUserAddrs;
import com.oseasy.xlxq.service.api.sys.service.TenantUserAddrsService;
import com.oseasy.xlxq.service.cache.FrameworkCacheConfig;
import com.oseasy.xlxq.service.config.Constants;
import com.oseasy.xlxq.service.core.utils.HqlUtil;
import com.oseasy.xlxq.service.core.utils.JSONUtil;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.autoconfigure.EnableAutoConfiguration;
import org.springframework.boot.test.SpringApplicationConfiguration;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Import;
import org.springframework.data.jpa.repository.config.EnableJpaRepositories;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import java.util.List;

/**
 * Created by Tandy on 2016/7/14.
 */
@RunWith(SpringJUnit4ClassRunner.class)
@Import(value = {ServiceConfig.class, ServiceApiConfig.class, FrameworkCacheConfig.class})
@EnableJpaRepositories(value = {"com.oseasy"})
@EnableAutoConfiguration
@SpringApplicationConfiguration(UserAddressTest.class)
public class UserAddressTest {

    static {
        //将 spring boot 的默认配置文件设置为系统配置文件
        System.setProperty("spring.config.location", "classpath:" + Constants.DEFAULT_CONFIG_FILE);
    }


    @Bean
    public String getSystemId() {
        return "aa";
    }


    @Autowired
    private TenantUserAddrsService tenantUserAddrsService;

    @Test
    public void test001() {

        try {
            String hql = "from TenantUserAddrs obj ";
            hql = HqlUtil.addCondition(hql, "tenantUser.id", "2");
            List<TenantUserAddrs> list = tenantUserAddrsService.getList(hql);
            System.out.println(JSONUtil.objectToJson(list));

        } catch (Exception ex) {
            ex.printStackTrace();
        }



    }


}
