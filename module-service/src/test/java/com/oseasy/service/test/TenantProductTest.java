package com.oseasy.service.test;

import com.oseasy.xlxq.service.ServiceConfig;
import com.oseasy.xlxq.service.api.ServiceApiConfig;
import com.oseasy.xlxq.service.api.tenant.model.TenantProduct;
import com.oseasy.xlxq.service.api.tenant.service.TenantProductService;
import com.oseasy.xlxq.service.cache.FrameworkCacheConfig;
import com.oseasy.xlxq.service.config.Constants;
import com.oseasy.xlxq.service.core.utils.HqlUtil;
import com.oseasy.xlxq.service.core.utils.JSONUtil;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.autoconfigure.EnableAutoConfiguration;
import org.springframework.boot.test.SpringApplicationConfiguration;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Import;
import org.springframework.data.jpa.repository.config.EnableJpaRepositories;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import java.util.List;

/**
 * Created by heckman on 2017/6/13.
 */

@RunWith(SpringJUnit4ClassRunner.class)
@Import(value={ServiceConfig.class, ServiceApiConfig.class, FrameworkCacheConfig.class})
@EnableJpaRepositories(value = {"com.oseasy"})
@EnableAutoConfiguration
@SpringApplicationConfiguration(TenantProductTest.class)
public class TenantProductTest {

    static {
        //将 spring boot 的默认配置文件设置为系统配置文件
        System.setProperty("spring.config.location","classpath:"+ Constants.DEFAULT_CONFIG_FILE);
    }

    @Bean
    public String getSystemId(){
        return "aa";
    }

    @Autowired
    private TenantProductService service;

    @Test
    public void test(){
        String order_hql = "from OrderProduct obj ";
        order_hql = HqlUtil.addCondition(order_hql, "orderInfo.orderNum", "F201706101");
        List<TenantProduct> list=service.getList(order_hql);
        System.out.println(JSONUtil.listToJson(list, new String[]{
                "purchaseNum", "tenantProduct", "productName", "productType","distributionType"}));
    }

}
