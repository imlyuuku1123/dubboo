package com.oseasy.service.test;

import com.oseasy.xlxq.service.ServiceConfig;
import com.oseasy.xlxq.service.api.ServiceApiConfig;
import com.oseasy.xlxq.service.api.order.model.OrderLogistics;
import com.oseasy.xlxq.service.api.order.model.OrderProduct;
import com.oseasy.xlxq.service.api.order.model.OrderRefund;
import com.oseasy.xlxq.service.api.order.service.OrderInfoService;
import com.oseasy.xlxq.service.api.order.service.OrderLogisticsService;
import com.oseasy.xlxq.service.api.order.service.OrderProductService;
import com.oseasy.xlxq.service.api.order.service.OrderRefundService;
import com.oseasy.xlxq.service.api.tenant.model.TenantProduct;
import com.oseasy.xlxq.service.api.tenant.model.TenantScenicTicket;
import com.oseasy.xlxq.service.cache.FrameworkCacheConfig;
import com.oseasy.xlxq.service.config.Constants;
import com.oseasy.xlxq.service.core.utils.HqlUtil;
import com.oseasy.xlxq.service.core.utils.JSONUtil;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.autoconfigure.EnableAutoConfiguration;
import org.springframework.boot.test.SpringApplicationConfiguration;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Import;
import org.springframework.data.jpa.repository.config.EnableJpaRepositories;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

/**
 * Created by Tandy on 2016/7/14.
 */
@RunWith(SpringJUnit4ClassRunner.class)
@Import(value = {ServiceConfig.class, ServiceApiConfig.class, FrameworkCacheConfig.class})
@EnableJpaRepositories(value = {"com.oseasy"})
@EnableAutoConfiguration
@SpringApplicationConfiguration(OrderinfoFilterDataTest.class)
public class OrderinfoFilterDataTest {

    static {
        //将 spring boot 的默认配置文件设置为系统配置文件
        System.setProperty("spring.config.location", "classpath:" + Constants.DEFAULT_CONFIG_FILE);
    }


    @Bean
    public String getSystemId() {
        return "aa";
    }

    @Autowired
    private OrderProductService orderProductService;

    @Autowired
    private OrderInfoService orderInfoService;

    @Test
    public void test001() {
        //查询订单商品信息
        String hql = "from OrderProduct obj ";
        hql = HqlUtil.addCondition(hql, "orderInfo.orderNum", "OR201705250");
        Iterator<OrderProduct> iterator = orderProductService.list(hql).iterator();
        List<OrderProduct> list = new ArrayList<OrderProduct>();
        while (iterator.hasNext()) {
            list.add(iterator.next());
        }
        List<TenantProduct> tenantProductList = new ArrayList<TenantProduct>();
        for (OrderProduct orderProduct : list) {
            tenantProductList.add(orderProduct.getTenantProduct());
        }


        String aa = JSONUtil.listToJson(list, new String[]{
                "purchaseNum", "tenantProduct", "productName", "productType"
        });
        String bb = JSONUtil.listToJson(tenantProductList, new String[]{
                "distributionType", "productName", "productType"});

        System.out.println(JSONUtil.objectToJson(list));
        System.out.println(aa);
        System.out.println(bb);

    }


    /**
     * 获取订单信息（3种订单的相同信息:姓名、手机号、订单类型、所属商户、所属景区、下单时间、支付时间、支付方式、支付金额）
     */
    @Test
    public void getOrderinfo() {
        try {
            ////String orderNum = "OR201705251";//土特产商品
           /* String orderNum = "OR201705254";//门票商品

            String hql = "from OrderInfo obj ";
            hql = HqlUtil.addCondition(hql, "orderNum", orderNum);
            OrderInfo orderInfo = orderInfoService.findUnique(hql);*/

            /*JSONObject data = new JSONObject();
            data.put("name", orderInfo.getTenantUser().getName());
            data.put("telephone", orderInfo.getTenantUser().getTelephone());
            data.put("orderType", orderInfo.getOrderType());
            data.put("merchantFullName", orderInfo.getTenantMerchant().getMerchantFullName());
            data.put("senicName", orderInfo.getExtTicketPop1());*/
            TenantProduct tenantProduct = new TenantProduct();
            TenantScenicTicket tenantScenicTicket = new TenantScenicTicket();
            System.out.println(tenantScenicTicket instanceof TenantProduct);

        } catch (Exception ex) {
            ex.printStackTrace();
        }
    }


    @Autowired
    private OrderLogisticsService orderLogisticsService;

    /**
     * 获取发货号信息（3种订单的物流相同信息：发货物流号）
     */

    @Test
    public void getLogisticInfo() {
        try {
            String orderNum = "OR201705251";//土特产商品
            String hql = "from OrderLogistics obj ";
            hql = HqlUtil.addCondition(hql, "orderInfo.orderNum", orderNum);

            OrderLogistics logistics = orderLogisticsService.findUnique(hql);

            String a = JSONUtil.objectToJson(logistics, new String[]{"deliverNum"});


        } catch (Exception ex) {
            ex.printStackTrace();
        }
    }


    @Autowired
    private OrderRefundService orderRefundService;

    /**
     * 获取退款信息（3种订单中相同的信息：退款状态、退款申请时间、退款方式、退款订单号、退款物流号、退款时间、退款金额）
     */
    @Test
    public void getRefundInfo() {
        try {
            String orderNum = "OR201705253";//土特产商品
            String hql = "from OrderRefund obj ";
            hql = HqlUtil.addCondition(hql, "orderNo", orderNum);
            OrderRefund orderRefund = orderRefundService.findUnique(hql);

        } catch (Exception ex) {
            ex.printStackTrace();
        }
    }

}
