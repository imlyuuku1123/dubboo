package com.oseasy.service.test;

import com.oseasy.xlxq.service.ServiceConfig;
import com.oseasy.xlxq.service.api.ServiceApiConfig;
import com.oseasy.xlxq.service.api.order.model.ConfigExpress;
import com.oseasy.xlxq.service.api.order.model.HomeStayRoomReserve;
import com.oseasy.xlxq.service.api.order.service.ConfigExpressService;
import com.oseasy.xlxq.service.api.order.service.HomeStayRoomReserveService;
import com.oseasy.xlxq.service.cache.FrameworkCacheConfig;
import com.oseasy.xlxq.service.config.Constants;
import com.oseasy.xlxq.service.core.utils.HqlUtil;
import com.oseasy.xlxq.service.core.utils.JSONUtil2;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.autoconfigure.EnableAutoConfiguration;
import org.springframework.boot.test.SpringApplicationConfiguration;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Import;
import org.springframework.data.jpa.repository.config.EnableJpaRepositories;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

/**
 * Created by Tandy on 2016/7/14.
 */
@RunWith(SpringJUnit4ClassRunner.class)
@Import(value = {ServiceConfig.class, ServiceApiConfig.class, FrameworkCacheConfig.class})
@EnableJpaRepositories(value = {"com.oseasy"})
@EnableAutoConfiguration
@SpringApplicationConfiguration(HomestayRoomTest.class)
public class HomestayRoomTest {

    static {
        //将 spring boot 的默认配置文件设置为系统配置文件
        System.setProperty("spring.config.location", "classpath:" + Constants.DEFAULT_CONFIG_FILE);
    }


    @Bean
    public String getSystemId() {
        return "aa";
    }

    @Autowired
    private HomeStayRoomReserveService homeStayRoomReserveService;

    @Autowired
    private ConfigExpressService configExpressService;


    @Test
    public void test001() {

        String hql = "from HomeStayRoomReserve obj ";
        hql = HqlUtil.addCondition(hql, "orderInfo.orderNum", "OR2017052514");
        Iterator<HomeStayRoomReserve> iterator = homeStayRoomReserveService.list(hql).iterator();
        List<HomeStayRoomReserve> list = new ArrayList<HomeStayRoomReserve>();
        while (iterator.hasNext()) {
            list.add(iterator.next());
        }

        System.out.println(JSONUtil2.objectToJson(list));

    }


    @Test
    public void test002() {
        StringBuffer hql = new StringBuffer("from ConfigExpress obj ");
        HqlUtil.addCondition(hql, "name", "顺", HqlUtil.LOGIC_AND, HqlUtil.TYPE_STRING_LIKE, null);

        System.out.println(hql.toString());


        Iterator<ConfigExpress> iterator = configExpressService.list(hql.toString()).iterator();
        List<ConfigExpress> list = new ArrayList<ConfigExpress>();
        while (iterator.hasNext()) {
            list.add(iterator.next());
        }

        System.out.println(JSONUtil2.objectToJson(list));


    }


    @Test
    public void test003(){
        String hql = "from ConfigExpress obj ";
        hql = HqlUtil.addCondition(hql, "name", "顺丰");
        Iterator<ConfigExpress> iterator = configExpressService.list(hql).iterator();
        List<ConfigExpress> list = new ArrayList<ConfigExpress>();
        while (iterator.hasNext()) {
            list.add(iterator.next());
        }
        System.out.println(list.get(0).getCode());
    }

}
