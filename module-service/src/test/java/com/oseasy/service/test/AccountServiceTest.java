package com.oseasy.service.test;

import com.oseasy.xlxq.service.ServiceConfig;
import com.oseasy.xlxq.service.api.ServiceApiConfig;
import com.oseasy.xlxq.service.api.tenant.service.TenantUserService;
import com.oseasy.xlxq.service.cache.FrameworkCacheConfig;
import com.oseasy.xlxq.service.config.Constants;
import com.oseasy.xlxq.service.core.utils.PasswordUtil;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.autoconfigure.EnableAutoConfiguration;
import org.springframework.boot.test.SpringApplicationConfiguration;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Import;
import org.springframework.data.jpa.repository.config.EnableJpaRepositories;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

/**
 * Created by Tandy on 2016/7/14.
 */
@RunWith(SpringJUnit4ClassRunner.class)
@Import(value={ServiceConfig.class, ServiceApiConfig.class, FrameworkCacheConfig.class})
@EnableJpaRepositories(value = {"com.oseasy"})
@EnableAutoConfiguration
@SpringApplicationConfiguration(AccountServiceTest.class)
public class AccountServiceTest {

    static {
        //将 spring boot 的默认配置文件设置为系统配置文件
        System.setProperty("spring.config.location","classpath:"+ Constants.DEFAULT_CONFIG_FILE);
    }

    @Autowired
    private TenantUserService tenantUserService;



    @Bean
    public String getSystemId(){
        return "aa";
    }


    @Test
    public void test002(){

//        Cart tu = new Cart();
//        tu.setName("zhangsan");
//        tu.setBirthday(new Date());
//        Cart tux = tenantUserService.save(tu);
//
//        Assert.notNull(tux);
//        Assert.isTrue(tux.getId() != null);
//

//        for(int i=0;i<100;i++){
//            Cart tu = new Cart();
//            tu.setName("zhangsan_"+i);
//            tu.setBirthday(new Date());
//            Cart tux = tenantUserService.save(tu);
//        }
//        String hql = "from Cart obj";
//        hql= HqlUtil.addCondition(hql,"name","zhang",HqlUtil.LOGIC_AND,HqlUtil.TYPE_STRING_LIKE);
//        hql= HqlUtil.addCondition(hql,"birthday","2017-05-09",HqlUtil.LOGIC_AND,HqlUtil.TYPE_DATE,HqlUtil.COMPARECHAR_GREAT);
//        hql= HqlUtil.addCondition(hql,"birthday","2017-06-09",HqlUtil.LOGIC_AND,HqlUtil.TYPE_DATE,HqlUtil.COMPARECHAR_LESS_EQ);

//        System.out.println(hql);
//        String hql = "from Cart obj where obj.name like 'zhang%'";
//        Page page = tenantUserService.pageList(hql,1,10);
//        Assert.isTrue(page.getResult()!=null);
//        Assert.isTrue(page.getTotalCount() == 101);
//
//       String json =  JSONUtil.listToJson(page.getResult(),new String[]{"id","name","tenant"});
//        System.out.println(json);



    }

//
//    @Test
//    public void test002(){
//        Assert.notNull(jdbcTemplate);
//        String sql = "select * from db_oe_xlxq.tb_tenant_account";
//        List list = this.jdbcTemplate.queryForList(sql);
//        Assert.notNull(list);
//        Assert.isTrue(list.size()>0);
//    }



    @Test
    public void test12(){
        System.out.println(PasswordUtil.springSecurityPasswordEncode("admin","admin"));
    }


}
