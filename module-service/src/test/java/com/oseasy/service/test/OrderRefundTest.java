package com.oseasy.service.test;

import com.oseasy.xlxq.service.ServiceConfig;
import com.oseasy.xlxq.service.api.ServiceApiConfig;
import com.oseasy.xlxq.service.api.order.model.OrderRefund;
import com.oseasy.xlxq.service.api.order.service.OrderRefundService;
import com.oseasy.xlxq.service.cache.FrameworkCacheConfig;
import com.oseasy.xlxq.service.config.Constants;
import com.oseasy.xlxq.service.core.utils.HqlUtil;
import com.oseasy.xlxq.service.core.utils.JSONUtil;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.autoconfigure.EnableAutoConfiguration;
import org.springframework.boot.test.SpringApplicationConfiguration;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Import;
import org.springframework.data.jpa.repository.config.EnableJpaRepositories;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import java.util.List;

/**
 * Created by heckman on 2017/5/24.
 */

@RunWith(SpringJUnit4ClassRunner.class)
@Import(value = {ServiceConfig.class, ServiceApiConfig.class, FrameworkCacheConfig.class})
@EnableJpaRepositories(value = {"com.oseasy"})
@EnableAutoConfiguration
@SpringApplicationConfiguration(OrderRefundTest.class)
public class OrderRefundTest {

    static {
        //将 spring boot 的默认配置文件设置为系统配置文件
        System.setProperty("spring.config.location", "classpath:" + Constants.DEFAULT_CONFIG_FILE);
    }

    @Bean
    public String getSystemId() {
        return "aa";
    }

    @Autowired
    private OrderRefundService orderRefundService;


    @Test
    public void test() {
        try {
            String hql = "from OrderRefund obj ";

            hql = HqlUtil.addCondition(hql, "orderInfo.id", "4028b8815c1428e9015c142abad70001");
            hql = HqlUtil.addCondition(hql, "refundStatus", "15", HqlUtil.LOGIC_AND, HqlUtil.TYPE_STRING, HqlUtil.COMPARECHAR_NOT_EQ);
            //List<OrderRefund> list = orderRefundService.getList(hql);

            OrderRefund orderRefund = orderRefundService.findUnique(hql);


            System.out.println(JSONUtil.objectToJson(orderRefund));

        } catch (Exception ex) {
            ex.printStackTrace();
        }

    }


    @Test
    public void test0002() {
        String hql = "from OrderRefund obj ";
        hql = HqlUtil.addCondition(hql, "refundStatus", "15", HqlUtil.LOGIC_AND, HqlUtil.TYPE_STRING, HqlUtil.COMPARECHAR_NOT_EQ);
        //hql = HqlUtil.addCondition(hql, "orderInfo.id", "4028b8815c1428e9015c142abad70001");
        hql = HqlUtil.addCondition(hql, "refundId", "F2017052521");

        List<OrderRefund> list = orderRefundService.getList(hql);

        System.out.println(list.size());

        System.out.println(JSONUtil.objectToJson(list));


    }


    @Test
    public void test0003(){
        try {
            String hql = "from OrderRefund obj ";
            hql = HqlUtil.addCondition(hql, "refundId", "T2017052523");
            OrderRefund orderRefund = orderRefundService.findUnique(hql);

            System.out.println(JSONUtil.objectToJson(orderRefund));

        }catch (Exception ex){
            ex.printStackTrace();
        }
    }

}
