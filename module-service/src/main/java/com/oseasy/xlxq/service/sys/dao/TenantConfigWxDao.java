package com.oseasy.xlxq.service.sys.dao;


import com.oseasy.xlxq.service.api.base.BaseDaoInterface;
import com.oseasy.xlxq.service.api.sys.model.TenantConfigWx;

import java.io.Serializable;

/**
 *
 */
public interface TenantConfigWxDao extends BaseDaoInterface<TenantConfigWx, Serializable> {
}
