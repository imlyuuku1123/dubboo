package com.oseasy.xlxq.service.order.service;

import com.oseasy.xlxq.service.api.base.BaseDaoInterface;
import com.oseasy.xlxq.service.api.order.model.OrderInfo;
import com.oseasy.xlxq.service.api.order.service.OrderInfoService;
import com.oseasy.xlxq.service.base.AbstractService;
import com.oseasy.xlxq.service.order.dao.OrderInfoDao;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.io.Serializable;

/**
 *
 */
@Service
public class OrderInfoServiceImpl extends AbstractService<OrderInfo> implements OrderInfoService {

    @Autowired
    private OrderInfoDao orderInfoDao;

    @Override
    public BaseDaoInterface<OrderInfo, Serializable> getDao() {
        return orderInfoDao;
    }

}
