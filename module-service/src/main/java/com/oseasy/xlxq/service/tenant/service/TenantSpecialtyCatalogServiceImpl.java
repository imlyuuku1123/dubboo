package com.oseasy.xlxq.service.tenant.service;

import com.oseasy.xlxq.service.api.base.BaseDaoInterface;

import com.oseasy.xlxq.service.api.tenant.model.TenantSpecialtyCatalog;

import com.oseasy.xlxq.service.api.tenant.service.TenantSpecialtyCatalogService;
import com.oseasy.xlxq.service.base.AbstractService;

import com.oseasy.xlxq.service.tenant.dao.TenantSpecialtyCatalogDao;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.io.Serializable;

/**
 *
 */
@Service
public class TenantSpecialtyCatalogServiceImpl extends AbstractService<TenantSpecialtyCatalog> implements TenantSpecialtyCatalogService {

    @Autowired
    private TenantSpecialtyCatalogDao tenantSpecialtyCatalogDao;

    @Override
    public BaseDaoInterface<TenantSpecialtyCatalog, Serializable> getDao() {
        return tenantSpecialtyCatalogDao;
    }


}
