package com.oseasy.xlxq.service.tenant.dao;


import com.oseasy.xlxq.service.api.base.BaseDaoInterface;
import com.oseasy.xlxq.service.api.tenant.model.TenantScenic;

import java.io.Serializable;

/**
 *
 */
public interface TenantScenicDao extends BaseDaoInterface<TenantScenic, Serializable> {
}
