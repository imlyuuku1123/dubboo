package com.oseasy.xlxq.service.product.dao;


import com.oseasy.xlxq.service.api.base.BaseDaoInterface;
import com.oseasy.xlxq.service.api.product.model.ProductEvaluate;

import java.io.Serializable;

/**
 *
 */
public interface ProductEvaluateDao extends BaseDaoInterface<ProductEvaluate, Serializable> {
}
