package com.oseasy.xlxq.service.tenant.service;

import com.oseasy.xlxq.service.api.base.BaseDaoInterface;

import com.oseasy.xlxq.service.api.tenant.model.TenantHomeStay;
import com.oseasy.xlxq.service.api.tenant.service.TenantHomeStayService;
import com.oseasy.xlxq.service.base.AbstractService;
import com.oseasy.xlxq.service.tenant.dao.TenantHomeStayDao;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.io.Serializable;

/**
 *
 */
@Service
public class TenantHomeStayServiceImpl extends AbstractService<TenantHomeStay> implements TenantHomeStayService {

    @Autowired
    private TenantHomeStayDao tenantHomeStayDao;

    @Override
    public BaseDaoInterface<TenantHomeStay, Serializable> getDao() {
        return tenantHomeStayDao;
    }


}
