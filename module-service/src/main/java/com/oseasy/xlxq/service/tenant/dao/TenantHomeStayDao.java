package com.oseasy.xlxq.service.tenant.dao;


import com.oseasy.xlxq.service.api.base.BaseDaoInterface;
import com.oseasy.xlxq.service.api.tenant.model.TenantHomeStay;

import java.io.Serializable;

/**
 *
 */
public interface TenantHomeStayDao extends BaseDaoInterface<TenantHomeStay, Serializable> {
}
