package com.oseasy.xlxq.service.statistic.dao;


import com.oseasy.xlxq.service.api.base.BaseDaoInterface;
import com.oseasy.xlxq.service.api.statistic.model.StatisticHomeStayDay;

import java.io.Serializable;

/**
 *
 */
public interface StatisticHomeStayDayDao extends BaseDaoInterface<StatisticHomeStayDay, Serializable> {
}
