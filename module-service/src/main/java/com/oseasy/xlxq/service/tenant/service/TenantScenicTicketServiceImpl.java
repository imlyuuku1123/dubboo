package com.oseasy.xlxq.service.tenant.service;

import com.oseasy.xlxq.service.api.base.BaseDaoInterface;
import com.oseasy.xlxq.service.api.tenant.model.TenantScenicTicket;
import com.oseasy.xlxq.service.api.tenant.service.TenantScenicTicketService;
import com.oseasy.xlxq.service.base.AbstractService;
import com.oseasy.xlxq.service.tenant.dao.TenantScenicTicketDao;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.io.Serializable;

/**
 *
 */
@Service
public class TenantScenicTicketServiceImpl extends AbstractService<TenantScenicTicket> implements TenantScenicTicketService {

    @Autowired
    private TenantScenicTicketDao tenantScenicTicketDao;

    @Override
    public BaseDaoInterface<TenantScenicTicket, Serializable> getDao() {
        return tenantScenicTicketDao;
    }


}
