package com.oseasy.xlxq.service.tenant.service;

import com.oseasy.xlxq.service.api.base.BaseDaoInterface;
import com.oseasy.xlxq.service.api.sys.model.SiteAccount;
import com.oseasy.xlxq.service.api.sys.service.SiteAccountService;
import com.oseasy.xlxq.service.api.tenant.model.TenantProduct;
import com.oseasy.xlxq.service.api.tenant.service.TenantProductService;
import com.oseasy.xlxq.service.base.AbstractService;
import com.oseasy.xlxq.service.tenant.dao.TenantProductDao;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.io.Serializable;
import java.util.List;

/**
 *
 */
@Service
public class TenantProductServiceImpl extends AbstractService<TenantProduct> implements TenantProductService {

    @Autowired
    private TenantProductDao tenantProductDao;

    @Override
    public BaseDaoInterface<TenantProduct, Serializable> getDao() {
        return tenantProductDao;
    }

    @Override
    public List<TenantProduct> findRecommendedProductList() {
        String hql = "from TenantProduct obj where obj.recommended=?1 ";

        return this.findByCustomWithParams(hql,1);
    }
}
