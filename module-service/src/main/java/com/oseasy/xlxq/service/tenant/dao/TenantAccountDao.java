package com.oseasy.xlxq.service.tenant.dao;


import com.oseasy.xlxq.service.api.base.BaseDaoInterface;
import com.oseasy.xlxq.service.api.tenant.model.TenantAccount;

import java.io.Serializable;

/**
 * Created by Tandy on 2016/6/24.
 */
public interface TenantAccountDao extends BaseDaoInterface<TenantAccount, Serializable> {


    TenantAccount findAccountByUserName(String userName);
}
