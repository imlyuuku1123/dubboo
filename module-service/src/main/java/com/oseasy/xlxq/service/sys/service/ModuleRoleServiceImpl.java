package com.oseasy.xlxq.service.sys.service;

import com.oseasy.xlxq.service.api.base.BaseDaoInterface;
import com.oseasy.xlxq.service.api.sys.model.ModuleRole;
import com.oseasy.xlxq.service.api.sys.service.ModuleRoleService;
import com.oseasy.xlxq.service.base.AbstractService;
import com.oseasy.xlxq.service.sys.dao.ModuleRoleDao;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.io.Serializable;

/**
 *
 */
@Service
public class ModuleRoleServiceImpl extends AbstractService<ModuleRole> implements ModuleRoleService {

    @Autowired
    private ModuleRoleDao moduleRoleDao;

    @Override
    public BaseDaoInterface<ModuleRole, Serializable> getDao() {
        return moduleRoleDao;
    }


}
