package com.oseasy.xlxq.service.product.dao;


import com.oseasy.xlxq.service.api.base.BaseDaoInterface;

import com.oseasy.xlxq.service.api.product.model.ProductStockChange;

import java.io.Serializable;

/**
 *
 */
public interface ProductStockChangeDao extends BaseDaoInterface<ProductStockChange, Serializable> {
}
