package com.oseasy.xlxq.service.tenant.service;

import com.oseasy.xlxq.service.api.base.BaseDaoInterface;
import com.oseasy.xlxq.service.api.tenant.model.TenantAccount;
import com.oseasy.xlxq.service.api.tenant.model.Tenant;
import com.oseasy.xlxq.service.api.tenant.service.TenantService;
import com.oseasy.xlxq.service.base.AbstractService;
import com.oseasy.xlxq.service.tenant.dao.TenantDao;
import org.springframework.stereotype.Service;

import java.io.Serializable;

/**
 * Created by zhangxb on 2016/6/29.
 * 租户实现类
 */
@Service
public class TenantServiceImpl extends AbstractService<Tenant> implements TenantService {

    private TenantDao tenantDao;

    @Override
    public BaseDaoInterface<Tenant, Serializable> getDao() {
        return tenantDao;
    }

    @Override
    public Tenant createTenant(TenantAccount tenantAccount) {
        return null;
    }
}
