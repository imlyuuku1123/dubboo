package com.oseasy.xlxq.service.sys.service;

import com.oseasy.xlxq.service.api.base.BaseDaoInterface;
import com.oseasy.xlxq.service.api.order.model.Cart;
import com.oseasy.xlxq.service.api.order.service.CartService;
import com.oseasy.xlxq.service.api.sys.model.FeedBack;
import com.oseasy.xlxq.service.api.sys.service.FeedBackService;
import com.oseasy.xlxq.service.base.AbstractService;
import com.oseasy.xlxq.service.sys.dao.FeedBackDao;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.io.Serializable;

/**
 *
 */
@Service
public class FeedBackServiceImpl extends AbstractService<FeedBack> implements FeedBackService {

    @Autowired
    private FeedBackDao feedBackDao;

    @Override
    public BaseDaoInterface<FeedBack, Serializable> getDao() {
        return feedBackDao;
    }


}
