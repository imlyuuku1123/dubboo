package com.oseasy.xlxq.service.order.service;

import com.oseasy.xlxq.service.api.base.BaseDaoInterface;
import com.oseasy.xlxq.service.api.order.model.ConfigExpress;
import com.oseasy.xlxq.service.api.order.service.ConfigExpressService;
import com.oseasy.xlxq.service.base.AbstractService;
import com.oseasy.xlxq.service.order.dao.ConfigExpressDao;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.io.Serializable;

/**
 * Created by heckman on 2017/5/26.
 */
@Service
public class ConfigExpressServiceImpl extends AbstractService<ConfigExpress> implements ConfigExpressService {

    @Autowired
    private ConfigExpressDao configExpressDao;

    @Override
    public BaseDaoInterface<ConfigExpress, Serializable> getDao() {
        return configExpressDao;
    }
}
