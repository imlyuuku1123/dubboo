package com.oseasy.xlxq.service.statistic.service;

import com.oseasy.xlxq.service.api.base.BaseDaoInterface;
import com.oseasy.xlxq.service.api.statistic.model.StatisticSpecialtyDay;
import com.oseasy.xlxq.service.api.statistic.service.StatisticSpecialtyDayService;
import com.oseasy.xlxq.service.base.AbstractService;
import com.oseasy.xlxq.service.statistic.dao.StatisticSpecialtyDayDao;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.io.Serializable;

/**
 *
 */
@Service
public class StatisticSpecialtyDayServiceImpl extends AbstractService<StatisticSpecialtyDay> implements StatisticSpecialtyDayService {

    @Autowired
    private StatisticSpecialtyDayDao statisticSpecialtyDayDao;

    @Override
    public BaseDaoInterface<StatisticSpecialtyDay, Serializable> getDao() {
        return statisticSpecialtyDayDao;
    }


}
