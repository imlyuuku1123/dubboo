package com.oseasy.xlxq.service.statistic.dao;


import com.oseasy.xlxq.service.api.base.BaseDaoInterface;
import com.oseasy.xlxq.service.api.statistic.model.StatisticSpecialtyDay;

import java.io.Serializable;

/**
 *
 */
public interface StatisticSpecialtyDayDao extends BaseDaoInterface<StatisticSpecialtyDay, Serializable> {
}
