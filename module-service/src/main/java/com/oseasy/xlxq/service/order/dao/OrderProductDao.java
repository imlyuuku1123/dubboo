package com.oseasy.xlxq.service.order.dao;


import com.oseasy.xlxq.service.api.base.BaseDaoInterface;
import com.oseasy.xlxq.service.api.order.model.OrderProduct;

import java.io.Serializable;

/**
 *
 */
public interface OrderProductDao extends BaseDaoInterface<OrderProduct, Serializable> {
}
