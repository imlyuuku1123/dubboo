package com.oseasy.xlxq.service.order.dao;


import com.oseasy.xlxq.service.api.base.BaseDaoInterface;
import com.oseasy.xlxq.service.api.order.model.HomeStayRoomStock;

import java.io.Serializable;

/**
 *
 */
public interface HomeStayRoomStockDao extends BaseDaoInterface<HomeStayRoomStock, Serializable> {
}
