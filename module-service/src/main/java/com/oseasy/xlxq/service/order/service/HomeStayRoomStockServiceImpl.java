package com.oseasy.xlxq.service.order.service;

import com.oseasy.xlxq.service.api.base.BaseDaoInterface;
import com.oseasy.xlxq.service.api.order.model.HomeStayRoomStock;
import com.oseasy.xlxq.service.api.order.service.HomeStayRoomStockService;
import com.oseasy.xlxq.service.base.AbstractService;
import com.oseasy.xlxq.service.order.dao.HomeStayRoomStockDao;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.io.Serializable;

/**
 *
 */
@Service
public class HomeStayRoomStockServiceImpl extends AbstractService<HomeStayRoomStock> implements HomeStayRoomStockService {

    @Autowired
    private HomeStayRoomStockDao homeStayRoomStockDao;

    @Override
    public BaseDaoInterface<HomeStayRoomStock, Serializable> getDao() {
        return homeStayRoomStockDao;
    }


}
