package com.oseasy.xlxq.service.tenant.service;

import com.oseasy.xlxq.service.api.base.BaseDaoInterface;

import com.oseasy.xlxq.service.api.tenant.model.TenantHomeStayRoom;
import com.oseasy.xlxq.service.api.tenant.service.TenantHomeStayRoomService;
import com.oseasy.xlxq.service.base.AbstractService;

import com.oseasy.xlxq.service.tenant.dao.TenantHomeStayRoomDao;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.io.Serializable;

/**
 *
 */
@Service
public class TenantHomeStayRoomServiceImpl extends AbstractService<TenantHomeStayRoom> implements TenantHomeStayRoomService {

    @Autowired
    private TenantHomeStayRoomDao tenantHomeStayRoomDao;

    @Override
    public BaseDaoInterface<TenantHomeStayRoom, Serializable> getDao() {
        return tenantHomeStayRoomDao;
    }


}
