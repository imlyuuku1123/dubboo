package com.oseasy.xlxq.service.tenant.service;

import com.oseasy.xlxq.service.api.base.BaseDaoInterface;
import com.oseasy.xlxq.service.api.exceptions.AccountNotFoundException;
import com.oseasy.xlxq.service.api.tenant.model.TenantAccount;
import com.oseasy.xlxq.service.api.tenant.service.TenantAccountService;
import com.oseasy.xlxq.service.base.AbstractService;
import com.oseasy.xlxq.service.core.exceptions.MatchMutiEntitiesException;
import com.oseasy.xlxq.service.core.utils.PasswordUtil;
import com.oseasy.xlxq.service.tenant.dao.TenantAccountDao;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.io.Serializable;

/**
 * Created by Tandy on 2016/6/24.
 */
@Service
public class TenantAccountServiceImpl extends AbstractService<TenantAccount> implements TenantAccountService {

    @Autowired
    private TenantAccountDao tenantAccountDao;

    @Override
    public BaseDaoInterface<TenantAccount, Serializable> getDao() {
        return tenantAccountDao;
    }

    @Override
    public TenantAccount findAccountByUserName(String userName) {
        return tenantAccountDao.findAccountByUserName(userName);
    }

    @Override
    public TenantAccount findAccountByUserNameAndPassword(String username, String password) throws AccountNotFoundException, MatchMutiEntitiesException {
        String hql = "from TenantAccount obj where obj.userName=?1 and obj.status=?2";
        TenantAccount tenantAccount = this.findUnique(hql, username, TenantAccount.STATUS_NORMAL);
        if(tenantAccount != null) {
            if(!tenantAccount.getPassword().equals(PasswordUtil.springSecurityPasswordEncode(password, tenantAccount.getUserName()))){
                tenantAccount = null;
            }
        }else{
            throw new AccountNotFoundException("找不到账号");
        }
        return tenantAccount;
    }

}
