package com.oseasy.xlxq.service.product.service;

import com.oseasy.xlxq.service.api.base.BaseDaoInterface;

import com.oseasy.xlxq.service.api.product.model.ProductStockChange;
import com.oseasy.xlxq.service.api.product.service.ProductStockChangeService;
import com.oseasy.xlxq.service.base.AbstractService;
import com.oseasy.xlxq.service.product.dao.ProductStockChangeDao;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.io.Serializable;

/**
 *
 */
@Service
public class ProductStockChangeServiceImpl extends AbstractService<ProductStockChange> implements ProductStockChangeService {

    @Autowired
    private ProductStockChangeDao productStockChangeDao;

    @Override
    public BaseDaoInterface<ProductStockChange, Serializable> getDao() {
        return productStockChangeDao;
    }


}
