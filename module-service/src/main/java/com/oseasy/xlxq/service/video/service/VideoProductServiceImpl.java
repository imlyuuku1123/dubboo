package com.oseasy.xlxq.service.video.service;

import com.oseasy.xlxq.service.api.base.BaseDaoInterface;
import com.oseasy.xlxq.service.api.video.model.VideoProduct;
import com.oseasy.xlxq.service.api.video.service.VideoProductService;
import com.oseasy.xlxq.service.base.AbstractService;
import com.oseasy.xlxq.service.video.dao.VideoProductDao;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.io.Serializable;

/**
 *
 */
@Service
public class VideoProductServiceImpl extends AbstractService<VideoProduct> implements VideoProductService {

    @Autowired
    private VideoProductDao videoProductDao;

    @Override
    public BaseDaoInterface<VideoProduct, Serializable> getDao() {
        return videoProductDao;
    }


}
