package com.oseasy.xlxq.service.sms.dao;



import com.oseasy.xlxq.service.api.base.BaseDaoInterface;
import com.oseasy.xlxq.service.api.sms.model.SMSSendLog;

import java.io.Serializable;

/**
 * Created by Tandy on 2016/6/24.
 */
public interface SMSSendLogDao extends BaseDaoInterface<SMSSendLog, Serializable> {
}
