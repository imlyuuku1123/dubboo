package com.oseasy.xlxq.service.sys.dao;


import com.oseasy.xlxq.service.api.base.BaseDaoInterface;
import com.oseasy.xlxq.service.api.sys.model.Module;

import java.io.Serializable;

/**
 *
 */
public interface ModuleDao extends BaseDaoInterface<Module, Serializable> {
}
