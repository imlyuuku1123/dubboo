package com.oseasy.xlxq.service.video.service;

import com.oseasy.xlxq.service.api.base.BaseDaoInterface;
import com.oseasy.xlxq.service.api.product.model.ProductEvaluate;
import com.oseasy.xlxq.service.api.product.service.ProductEvaluateService;
import com.oseasy.xlxq.service.api.video.model.VideoChannel;
import com.oseasy.xlxq.service.api.video.service.VideoChannelService;
import com.oseasy.xlxq.service.base.AbstractService;
import com.oseasy.xlxq.service.video.dao.VideoChannelDao;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.io.Serializable;

/**
 *
 */
@Service
public class VideoChannelServiceImpl extends AbstractService<VideoChannel> implements VideoChannelService {

    @Autowired
    private VideoChannelDao videoChannelDao;

    @Override
    public BaseDaoInterface<VideoChannel, Serializable> getDao() {
        return videoChannelDao;
    }


}
