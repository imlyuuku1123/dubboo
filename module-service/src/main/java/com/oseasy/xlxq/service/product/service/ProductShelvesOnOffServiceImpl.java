package com.oseasy.xlxq.service.product.service;

import com.oseasy.xlxq.service.api.base.BaseDaoInterface;
import com.oseasy.xlxq.service.api.product.model.ProductShelvesOnOff;
import com.oseasy.xlxq.service.api.product.service.ProductShelvesOnOffService;
import com.oseasy.xlxq.service.base.AbstractService;
import com.oseasy.xlxq.service.product.dao.ProductShelvesOnOffDao;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.io.Serializable;

/**
 *
 */
@Service
public class ProductShelvesOnOffServiceImpl extends AbstractService<ProductShelvesOnOff> implements ProductShelvesOnOffService {

    @Autowired
    private ProductShelvesOnOffDao productShelvesOnOffDao;

    @Override
    public BaseDaoInterface<ProductShelvesOnOff, Serializable> getDao() {
        return productShelvesOnOffDao;
    }


}
