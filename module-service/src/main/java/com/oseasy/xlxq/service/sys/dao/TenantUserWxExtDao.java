package com.oseasy.xlxq.service.sys.dao;


import com.oseasy.xlxq.service.api.base.BaseDaoInterface;
import com.oseasy.xlxq.service.api.sys.model.TenantUserWxExt;

import java.io.Serializable;

/**
 *
 */
public interface TenantUserWxExtDao extends BaseDaoInterface<TenantUserWxExt, Serializable> {
}
