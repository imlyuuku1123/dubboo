package com.oseasy.xlxq.service.tenant.service;

import com.oseasy.xlxq.service.api.base.BaseDaoInterface;
import com.oseasy.xlxq.service.api.tenant.model.TenantSpecialty;
import com.oseasy.xlxq.service.api.tenant.service.TenantSpecialtyService;
import com.oseasy.xlxq.service.base.AbstractService;
import com.oseasy.xlxq.service.tenant.dao.TenantSpecialtyDao;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.io.Serializable;

/**
 *
 */
@Service
public class TenantSpecialtyServiceImpl extends AbstractService<TenantSpecialty> implements TenantSpecialtyService {

    @Autowired
    private TenantSpecialtyDao tenantSpecialtyDao;

    @Override
    public BaseDaoInterface<TenantSpecialty, Serializable> getDao() {
        return tenantSpecialtyDao;
    }



}
