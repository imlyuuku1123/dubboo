package com.oseasy.xlxq.service.product.service;

import com.oseasy.xlxq.service.api.base.BaseDaoInterface;
import com.oseasy.xlxq.service.api.order.model.OrderInfo;
import com.oseasy.xlxq.service.api.order.service.OrderInfoService;
import com.oseasy.xlxq.service.api.product.model.ProductWriteOff;
import com.oseasy.xlxq.service.api.product.service.ProductWriteOffService;
import com.oseasy.xlxq.service.base.AbstractService;
import com.oseasy.xlxq.service.product.dao.ProductWriteOffDao;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.io.Serializable;

/**
 *
 */
@Service
public class ProductWriteOffServiceImpl extends AbstractService<ProductWriteOff> implements ProductWriteOffService {

    @Autowired
    private ProductWriteOffDao productWriteOffDao;

    @Override
    public BaseDaoInterface<ProductWriteOff, Serializable> getDao() {
        return productWriteOffDao;
    }

}
