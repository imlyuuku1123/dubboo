package com.oseasy.xlxq.service.tenant.service;

import com.oseasy.xlxq.service.api.base.BaseDaoInterface;
import com.oseasy.xlxq.service.api.tenant.model.TenantMerchantSeller;
import com.oseasy.xlxq.service.api.tenant.service.TenantMerchantSellerService;
import com.oseasy.xlxq.service.base.AbstractService;
import com.oseasy.xlxq.service.tenant.dao.TenantMerchantSellerDao;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.io.Serializable;

/**
 *
 */
@Service
public class TenantMerchantSellerServiceImpl extends AbstractService<TenantMerchantSeller> implements TenantMerchantSellerService {

    @Autowired
    private TenantMerchantSellerDao tenantMerchantSellerDao;

    @Override
    public BaseDaoInterface<TenantMerchantSeller, Serializable> getDao() {
        return tenantMerchantSellerDao;
    }

}
