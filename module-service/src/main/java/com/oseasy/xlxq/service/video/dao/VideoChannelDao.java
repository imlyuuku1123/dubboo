package com.oseasy.xlxq.service.video.dao;


import com.oseasy.xlxq.service.api.base.BaseDaoInterface;
import com.oseasy.xlxq.service.api.video.model.VideoChannel;

import java.io.Serializable;

/**
 *
 */
public interface VideoChannelDao extends BaseDaoInterface<VideoChannel, Serializable> {
}
