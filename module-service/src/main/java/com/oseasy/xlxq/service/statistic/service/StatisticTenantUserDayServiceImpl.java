package com.oseasy.xlxq.service.statistic.service;

import com.oseasy.xlxq.service.api.base.BaseDaoInterface;
import com.oseasy.xlxq.service.api.statistic.model.StatisticTenantUserDay;
import com.oseasy.xlxq.service.api.statistic.service.StatisticTenantUserDayService;
import com.oseasy.xlxq.service.base.AbstractService;
import com.oseasy.xlxq.service.statistic.dao.StatisticTenantUserDayDao;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.io.Serializable;

/**
 *
 */
@Service
public class StatisticTenantUserDayServiceImpl extends AbstractService<StatisticTenantUserDay> implements StatisticTenantUserDayService {

    @Autowired
    private StatisticTenantUserDayDao statisticTenantUserDayDao;

    @Override
    public BaseDaoInterface<StatisticTenantUserDay, Serializable> getDao() {
        return statisticTenantUserDayDao;
    }


}
