package com.oseasy.xlxq.service.product.dao;


import com.oseasy.xlxq.service.api.base.BaseDaoInterface;

import com.oseasy.xlxq.service.api.product.model.ProductShelvesOnOff;

import java.io.Serializable;

/**
 *
 */
public interface ProductShelvesOnOffDao extends BaseDaoInterface<ProductShelvesOnOff, Serializable> {
}
