package com.oseasy.xlxq.service.tenant.dao;


import com.oseasy.xlxq.service.api.base.BaseDaoInterface;
import com.oseasy.xlxq.service.api.tenant.model.TenantHomeStayRoom;

import java.io.Serializable;

/**
 *
 */
public interface TenantHomeStayRoomDao extends BaseDaoInterface<TenantHomeStayRoom, Serializable> {
}
