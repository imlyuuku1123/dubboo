package com.oseasy.xlxq.service.statistic.dao;


import com.oseasy.xlxq.service.api.base.BaseDaoInterface;
import com.oseasy.xlxq.service.api.statistic.model.StatisticTicketDay;

import java.io.Serializable;

/**
 *
 */
public interface StatisticTicketDayDao extends BaseDaoInterface<StatisticTicketDay, Serializable> {
}
