package com.oseasy.xlxq.service.order.service;

import com.oseasy.xlxq.service.api.order.service.OrderNoGenService;
import com.oseasy.xlxq.service.cache.manager.RedisCacheService;
import com.oseasy.xlxq.service.core.utils.DateUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

/**
 * Created by heckman on 2017/5/25.
 */
@Component
public class OrderNoGenServiceImpl implements OrderNoGenService{

    @Autowired
    private RedisCacheService redisCacheService;
    @Override
    public String genOrderNo(int orderType) {
        String prefix = (orderType == ORDER_TYPE_FK ? "F" : "T");
        String dt = DateUtils.getDate("yyyyMMdd");
        String key = "order_no_" + prefix +"_" + dt;
        long no = redisCacheService.incr(key);
        String result = prefix + dt + no;
        return result;
    }
}
