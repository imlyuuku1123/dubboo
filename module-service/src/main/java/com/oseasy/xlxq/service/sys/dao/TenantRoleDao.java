package com.oseasy.xlxq.service.sys.dao;


import com.oseasy.xlxq.service.api.base.BaseDaoInterface;
import com.oseasy.xlxq.service.api.sys.model.TenantRole;

import java.io.Serializable;

/**
 *
 */
public interface TenantRoleDao extends BaseDaoInterface<TenantRole, Serializable> {
}
