package com.oseasy.xlxq.service.sys.dao;


import com.oseasy.xlxq.service.api.base.BaseDaoInterface;
import com.oseasy.xlxq.service.api.sys.model.ModuleRole;

import java.io.Serializable;

/**
 *
 */
public interface ModuleRoleDao extends BaseDaoInterface<ModuleRole, Serializable> {
}
