package com.oseasy.xlxq.service.tenant.dao;


import com.oseasy.xlxq.service.api.base.BaseDaoInterface;
import com.oseasy.xlxq.service.api.tenant.model.TenantMerchant;

import java.io.Serializable;

/**
 *
 */
public interface TenantMerchantDao extends BaseDaoInterface<TenantMerchant, Serializable> {
}
