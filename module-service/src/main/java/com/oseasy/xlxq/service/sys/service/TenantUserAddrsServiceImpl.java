package com.oseasy.xlxq.service.sys.service;

import com.oseasy.xlxq.service.api.base.BaseDaoInterface;
import com.oseasy.xlxq.service.api.statistic.model.StatisticTenantUserDay;
import com.oseasy.xlxq.service.api.statistic.service.StatisticTenantUserDayService;
import com.oseasy.xlxq.service.api.sys.model.TenantUserAddrs;
import com.oseasy.xlxq.service.api.sys.service.TenantUserAddrsService;
import com.oseasy.xlxq.service.base.AbstractService;
import com.oseasy.xlxq.service.sys.dao.TenantUserAddrsDao;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.io.Serializable;
import java.util.List;

/**
 *
 */
@Service
public class TenantUserAddrsServiceImpl extends AbstractService<TenantUserAddrs> implements TenantUserAddrsService {

    @Autowired
    private TenantUserAddrsDao tenantUserAddrsDao;

    @Override
    public BaseDaoInterface<TenantUserAddrs, Serializable> getDao() {
        return tenantUserAddrsDao;
    }



}
