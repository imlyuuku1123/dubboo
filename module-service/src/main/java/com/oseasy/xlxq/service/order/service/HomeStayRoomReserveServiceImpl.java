package com.oseasy.xlxq.service.order.service;

import com.oseasy.xlxq.service.api.base.BaseDaoInterface;
import com.oseasy.xlxq.service.api.order.model.HomeStayRoomReserve;
import com.oseasy.xlxq.service.api.order.service.HomeStayRoomReserveService;
import com.oseasy.xlxq.service.base.AbstractService;
import com.oseasy.xlxq.service.order.dao.HomeStayRoomReserveDao;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import java.io.Serializable;

/**
 *
 */
@Service
public class HomeStayRoomReserveServiceImpl extends AbstractService<HomeStayRoomReserve> implements HomeStayRoomReserveService {

    @Autowired
    private HomeStayRoomReserveDao homeStayRoomReserveDao;

    @Override
    public BaseDaoInterface<HomeStayRoomReserve, Serializable> getDao() {
        return homeStayRoomReserveDao;
    }


}
