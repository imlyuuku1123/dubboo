package com.oseasy.xlxq.service.sys.service;

import com.oseasy.xlxq.service.api.base.BaseDaoInterface;
import com.oseasy.xlxq.service.api.sys.model.TenantConfigWx;
import com.oseasy.xlxq.service.api.sys.service.TenantConfigWxService;
import com.oseasy.xlxq.service.base.AbstractService;
import com.oseasy.xlxq.service.sys.dao.TenantConfigWxDao;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.io.Serializable;

/**
 *
 */
@Service
public class TenantConfigWxServiceImpl extends AbstractService<TenantConfigWx> implements TenantConfigWxService {

    @Autowired
    private TenantConfigWxDao tenantConfigWxDao;

    @Override
    public BaseDaoInterface<TenantConfigWx, Serializable> getDao() {
        return tenantConfigWxDao;
    }


}
