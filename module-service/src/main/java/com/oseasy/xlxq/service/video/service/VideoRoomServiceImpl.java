package com.oseasy.xlxq.service.video.service;

import com.oseasy.xlxq.service.api.base.BaseDaoInterface;
import com.oseasy.xlxq.service.api.video.model.VideoRoom;
import com.oseasy.xlxq.service.api.video.service.VideoRoomService;
import com.oseasy.xlxq.service.base.AbstractService;
import com.oseasy.xlxq.service.video.dao.VideoRoomDao;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.io.Serializable;

/**
 *
 */
@Service
public class VideoRoomServiceImpl extends AbstractService<VideoRoom> implements VideoRoomService {

    @Autowired
    private VideoRoomDao videoRoomDao;

    @Override
    public BaseDaoInterface<VideoRoom, Serializable> getDao() {
        return videoRoomDao;
    }


}
