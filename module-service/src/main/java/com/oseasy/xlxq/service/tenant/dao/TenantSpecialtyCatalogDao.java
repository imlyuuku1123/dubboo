package com.oseasy.xlxq.service.tenant.dao;


import com.oseasy.xlxq.service.api.base.BaseDaoInterface;
import com.oseasy.xlxq.service.api.tenant.model.TenantSpecialtyCatalog;

import java.io.Serializable;

/**
 *
 */
public interface TenantSpecialtyCatalogDao extends BaseDaoInterface<TenantSpecialtyCatalog, Serializable> {
}
