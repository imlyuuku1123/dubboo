package com.oseasy.xlxq.service.sys.service;

import com.oseasy.xlxq.service.api.base.BaseDaoInterface;
import com.oseasy.xlxq.service.api.sys.model.SmsSendLog;
import com.oseasy.xlxq.service.api.sys.service.SmsSendLogService;
import com.oseasy.xlxq.service.base.AbstractService;
import com.oseasy.xlxq.service.sys.dao.SmsSendLogDao;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.io.Serializable;

/**
 *
 */
@Service
public class SmsSendLogServiceImpl extends AbstractService<SmsSendLog> implements SmsSendLogService {

    @Autowired
    private SmsSendLogDao smsSendLogDao;

    @Override
    public BaseDaoInterface<SmsSendLog, Serializable> getDao() {
        return smsSendLogDao;
    }


}
