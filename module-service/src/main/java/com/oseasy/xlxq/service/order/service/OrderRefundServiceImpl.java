package com.oseasy.xlxq.service.order.service;

import com.oseasy.xlxq.service.api.base.BaseDaoInterface;
import com.oseasy.xlxq.service.api.order.model.Cart;
import com.oseasy.xlxq.service.api.order.model.OrderRefund;
import com.oseasy.xlxq.service.api.order.service.CartService;
import com.oseasy.xlxq.service.api.order.service.OrderRefundService;
import com.oseasy.xlxq.service.base.AbstractService;
import com.oseasy.xlxq.service.order.dao.CartDao;
import com.oseasy.xlxq.service.order.dao.OrderRefundDao;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.io.Serializable;

/**
 *
 */
@Service
public class OrderRefundServiceImpl extends AbstractService<OrderRefund> implements OrderRefundService {

    @Autowired
    private OrderRefundDao orderRefundDao;

    @Override
    public BaseDaoInterface<OrderRefund, Serializable> getDao() {
        return orderRefundDao;
    }


}
