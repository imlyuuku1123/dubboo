package com.oseasy.xlxq.service.tenant.service;

import com.oseasy.xlxq.service.api.base.BaseDaoInterface;
import com.oseasy.xlxq.service.api.tenant.model.TenantUser;
import com.oseasy.xlxq.service.api.tenant.service.TenantUserService;
import com.oseasy.xlxq.service.base.AbstractService;
import com.oseasy.xlxq.service.tenant.dao.TenantUserDao;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.io.Serializable;

/**
 * Created by Tandy on 2016/6/24.
 */
@Service
public class TenantUserServiceImpl extends AbstractService<TenantUser> implements TenantUserService {
    @Autowired
    private TenantUserDao tenantUserDao;
    @Override
    public BaseDaoInterface<TenantUser, Serializable> getDao() {
        return tenantUserDao;
    }
}
