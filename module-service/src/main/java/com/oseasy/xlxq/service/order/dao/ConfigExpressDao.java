package com.oseasy.xlxq.service.order.dao;

import com.oseasy.xlxq.service.api.base.BaseDaoInterface;
import com.oseasy.xlxq.service.api.order.model.ConfigExpress;

import java.io.Serializable;

/**
 * Created by heckman on 2017/5/26.
 */
public interface ConfigExpressDao extends BaseDaoInterface<ConfigExpress, Serializable> {
}
