package com.oseasy.xlxq.service.sys.dao;


import com.oseasy.xlxq.service.api.base.BaseDaoInterface;
import com.oseasy.xlxq.service.api.sys.model.TenantAccountLog;

import java.io.Serializable;

/**
 *
 */
public interface TenantAccountLogDao extends BaseDaoInterface<TenantAccountLog, Serializable> {
}
