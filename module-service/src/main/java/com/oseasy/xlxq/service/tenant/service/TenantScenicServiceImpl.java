package com.oseasy.xlxq.service.tenant.service;

import com.oseasy.xlxq.service.api.base.BaseDaoInterface;
import com.oseasy.xlxq.service.api.sys.model.SmsSendLog;
import com.oseasy.xlxq.service.api.sys.service.SmsSendLogService;
import com.oseasy.xlxq.service.api.tenant.model.TenantScenic;
import com.oseasy.xlxq.service.api.tenant.service.TenantScenicService;
import com.oseasy.xlxq.service.base.AbstractService;
import com.oseasy.xlxq.service.tenant.dao.TenantScenicDao;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.io.Serializable;

/**
 *
 */
@Service
public class TenantScenicServiceImpl extends AbstractService<TenantScenic> implements TenantScenicService {

    @Autowired
    private TenantScenicDao tenantScenicDao;

    @Override
    public BaseDaoInterface<TenantScenic, Serializable> getDao() {
        return tenantScenicDao;
    }


}
