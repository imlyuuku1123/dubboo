package com.oseasy.xlxq.service.sys.service;

import com.oseasy.xlxq.service.api.base.BaseDaoInterface;
import com.oseasy.xlxq.service.api.statistic.model.StatisticTicketDay;
import com.oseasy.xlxq.service.api.statistic.service.StatisticTicketDayService;
import com.oseasy.xlxq.service.api.sys.model.TenantUserWxExt;
import com.oseasy.xlxq.service.api.sys.service.TenantUserWxExtService;
import com.oseasy.xlxq.service.base.AbstractService;
import com.oseasy.xlxq.service.sys.dao.TenantUserWxExtDao;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.io.Serializable;

/**
 *
 */
@Service
public class TenantUserWxExtServiceImpl extends AbstractService<TenantUserWxExt> implements TenantUserWxExtService {

    @Autowired
    private TenantUserWxExtDao tenantUserWxExtDao;

    @Override
    public BaseDaoInterface<TenantUserWxExt, Serializable> getDao() {
        return tenantUserWxExtDao;
    }

}
