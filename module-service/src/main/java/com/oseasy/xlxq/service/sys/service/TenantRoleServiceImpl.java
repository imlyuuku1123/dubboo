package com.oseasy.xlxq.service.sys.service;

import com.oseasy.xlxq.service.api.base.BaseDaoInterface;
import com.oseasy.xlxq.service.api.sys.model.TenantRole;
import com.oseasy.xlxq.service.api.sys.service.TenantRoleService;
import com.oseasy.xlxq.service.base.AbstractService;
import com.oseasy.xlxq.service.sys.dao.TenantRoleDao;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.io.Serializable;

/**
 *
 */
@Service
public class TenantRoleServiceImpl extends AbstractService<TenantRole> implements TenantRoleService {

    @Autowired
    private TenantRoleDao tenantRoleDao;

    @Override
    public BaseDaoInterface<TenantRole, Serializable> getDao() {
        return tenantRoleDao;
    }


}
