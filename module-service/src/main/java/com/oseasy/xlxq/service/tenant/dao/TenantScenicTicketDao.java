package com.oseasy.xlxq.service.tenant.dao;


import com.oseasy.xlxq.service.api.base.BaseDaoInterface;
import com.oseasy.xlxq.service.api.tenant.model.TenantScenicTicket;

import java.io.Serializable;

/**
 *
 */
public interface TenantScenicTicketDao extends BaseDaoInterface<TenantScenicTicket, Serializable> {
}
