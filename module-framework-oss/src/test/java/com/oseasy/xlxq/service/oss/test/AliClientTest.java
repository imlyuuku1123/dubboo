package com.oseasy.xlxq.service.oss.test;

import com.aliyun.oss.OSSClient;
import com.aliyun.oss.model.ListObjectsRequest;
import com.aliyun.oss.model.OSSObjectSummary;
import com.aliyun.oss.model.ObjectListing;
import com.oseasy.xlxq.service.config.Constants;
import com.oseasy.xlxq.service.core.utils.UUIDGenerator;
import com.oseasy.xlxq.service.oss.FrameworkOSSConfig;
import com.oseasy.xlxq.service.oss.OSSService;
import com.oseasy.xlxq.service.oss.ali.AliOSSClientFactoryBean;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.SpringApplicationConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.util.Assert;

import javax.activation.MimetypesFileTypeMap;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by Tandy on 2016/7/14.
 */
@RunWith(SpringJUnit4ClassRunner.class)
@SpringApplicationConfiguration(classes = FrameworkOSSConfig.class)
public class AliClientTest {
    static {
        //将 spring boot 的默认配置文件设置为系统配置文件
        System.setProperty("spring.config.location","classpath:"+ Constants.DEFAULT_CONFIG_FILE);
    }
    @Autowired
    private OSSService ossService;

    @Autowired
    private AliOSSClientFactoryBean afb;

    @Test
    public void test001() throws Exception {
        Assert.notNull(ossService);
        String repository="xlxq-dev-open";
        List<String> objs =  ossService.listObjects(repository,"/");
        Assert.notNull(objs);
        Assert.isTrue(objs.size()>0);
//        InputStream is = ossService.getFileStream(repository,fileKey);
//        File file = new File("./a.jpg");
//        if(!file.exists()){
//            file.createNewFile();
//        }
//        FileOutputStream fos = new FileOutputStream(file);
//        FileUtil.copyStream(is,fos);
//        fos.flush();
//        fos.close();
//        System.out.println(file.getAbsolutePath());
//        Assert.notNull(is);
////        FileUtil.delFile(file.getAbsolutePath());
    }

    @Test
    public void testDownload() throws Exception {
        String fileKey = "images/订单状态迁移图.png";
        String repository="xlxq-dev-open";
        String destFile = "d:/a.png";
        File file = this.ossService.downLoadFile(repository,fileKey,destFile);
        Assert.notNull(file);
        Assert.isTrue(file.length()>0);
    }

    @Test
    public void testUploadLocalFile() throws Exception {
        String fileKey = "images/20170525/cc.png";
        String repository="xlxq-dev-open";
        String destFile = "d:/a.png";

        boolean result = this.ossService.uploadFileLocal(new File(destFile),repository,fileKey);
        Assert.isTrue(result);
    }

    @Test
    public void testUploadStreamFile() throws FileNotFoundException {
        String fileKey = "tenants/20170614/cc2200433333.png";
        String repository="xlxq-dev-open";
        String destFile = "/Users/tandy/Desktop/pic01.png";
        File file = new File(destFile);
        InputStream is = new FileInputStream(file);
        boolean result = this.ossService.uploadFileStream(is,file.length(),destFile,repository,fileKey);
        Assert.isTrue(result);
    }

    @Test
    public void testDeleteFile(){
        String fileKey = "images/cc22004.png";
        String repository="yunhuni-development";
        this.ossService.deleteObject(repository,fileKey);
    }
    @Test
    public void testUploadFileLocal() throws Exception {
        String ossUri = "images/cc22004.png";
        String repository="yunhuni-development";
        String destFile = "d:/cc.png";
        String fileName = UUIDGenerator.uuid() + ".zip";
        ossService.uploadFileLocal(new File(destFile), repository, ossUri, fileName);
    }

    public static void main(String[] args) {
//        Path path = Paths.get("/Users/tandy/Desktop/QQ20170614-094516.png");
//        String contentType = null;
//        try {
//            contentType = Files.probeContentType(path);
//        } catch (IOException e) {
//            e.printStackTrace();
//        }
//        System.out.println("File content type is : " + contentType);

//        String contentType = new MimetypesFileTypeMap().getContentType(new File("/Users/tandy/Desktop/QQ20170614-094516.png"));
//        System.out.println(contentType);

//        FileNameMap fileNameMap = URLConnection.getFileNameMap();
//        String contentType = fileNameMap.getContentTypeFor("/Users/tandy/Desktop/OE_SP_PMC_谭畅_个人周报20170602.mp4");
//        System.out.println(contentType);

        File f = new File("gumby.mp4");
        System.out.println("Mime Type of " + f.getName() + " is " +
                new MimetypesFileTypeMap().getContentType("a.mp3"));
    }



    /**
     * 列出Object<br>
     *
     * @param client
     * @param bucketName
     * @param delimiter
     *            Delimiter 设置为 “/” 时，返回值就只罗列该文件夹下的文件，可以null
     * @param prefix
     *            Prefix 设为某个文件夹名，就可以罗列以此 Prefix 开头的文件，可以null
     * @return
     */
    private List<String> listObject(OSSClient client, String bucketName,
                                          String delimiter, String prefix) {

        // 是否循环的标识
        boolean hasNext = false;
        // 设定结果从Marker之后按字母排序的第一个开始返回
        String marker = "";
        //
        // ObjectListing listing = new ObjectListing();
        List<String> filePathList = new ArrayList<String>();
        // 构造ListObjectsRequest请求
        ListObjectsRequest listObjectsRequest = new ListObjectsRequest(
                bucketName);

        // 是一个用于对Object名字进行分组的字符。所有名字包含指定的前缀且第一次出现Delimiter字符之间的object作为一组元素:
        // CommonPrefixes
        listObjectsRequest.setDelimiter(delimiter);
        // 限定此次返回object的最大数，如果不设定，默认为100，MaxKeys取值不能大于1000
        listObjectsRequest.setMaxKeys(20);
        // 限定返回的object key必须以Prefix作为前缀。注意使用prefix查询时，返回的key中仍会包含Prefix
        listObjectsRequest.setPrefix(prefix);

        do {
            // 设定结果从Marker之后按字母排序的第一个开始返回
            listObjectsRequest.setMarker(marker);
            // 获取指定bucket下的所有Object信息
            ObjectListing sublisting = client.listObjects(listObjectsRequest);
            // 如果Bucket中的Object数量大于100，则只会返回100个Object， 且返回结果中 IsTruncated
            // 为false
            if (sublisting.isTruncated()) {
                hasNext = true;
                marker = sublisting.getNextMarker();
            } else {
                hasNext = false;
                marker = "";
            }
            // // 遍历所有Object
            for (OSSObjectSummary objectSummary : sublisting.getObjectSummaries()) {
                // System.out.println(objectSummary.getKey());
                filePathList.add(objectSummary.getKey());
            }
        } while (hasNext);

        return filePathList;
    }

    @Test
    public void testOSSClientListObject() throws Exception {
        OSSClient client = afb.getObject();
        System.out.println(client);

        List<String> listObject = listObject(client, "xlxq-dev-open", null, "ueditor/upload");

        System.out.println(listObject);

    }
}
