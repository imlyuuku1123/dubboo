package com.oseasy.xlxq.wx.web;

import com.oseasy.xlxq.service.cache.manager.RedisCacheService;
import com.oseasy.xlxq.service.core.utils.HttpClientUtil;
import com.oseasy.xlxq.service.core.utils.JSONUtil2;
import com.oseasy.xlxq.service.web.utils.WebUtils;
import com.oseasy.xlxq.wx.constant.Constant;
import com.oseasy.xlxq.wx.model.WxButtonMap;
import com.oseasy.xlxq.wx.model.WxClickButton;
import com.oseasy.xlxq.wx.model.WxSubButton;
import com.oseasy.xlxq.wx.utils.CommonUtils;
import com.oseasy.xlxq.wx.utils.TokenUtil;
import com.oseasy.xlxq.wx.utils.WebserviceUtil;
import com.oseasy.xlxq.wx.utils.WxJsonUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.util.*;

/**
 * Created by cannan on 2017/5/11.
 * 自定义菜单控制
 */
@RestController
public class CustomMenuConroller {

    private static Logger logger = LoggerFactory.getLogger(HaHaController.class);

    @Autowired
    private RedisCacheService redisCacheService;

    @RequestMapping("/custommenu/create")
    public void createmenu(HttpServletRequest request, HttpServletResponse response){
        String menujson = null;
//        RestResponse response = null;
        try {
            if(logger.isDebugEnabled()){
                logger.debug("debugger");
            }

            String urlforvbt1 = "https://open.weixin.qq.com/connect/oauth2/authorize?"+
                    "appid="+ Constant.App_ID +"&redirect_uri=http%3a%2f%2fwx.dev.os-easy.com%2fproduct%2findex"
                    +"&response_type=code&scope=snsapi_userinfo&state=STATE#wechat_redirect";

//            https://open.weixin.qq.com/connect/oauth2/authorize?appid=wx1c1d70fc747c14f7&redirect_uri=http%3a%2f%2fwx.dev.os-easy.com%2fwxproduct%2findex&response_type=code&scope=snsapi_userinfo&state=STATE#wechat_redirect
            WxSubButton sbt1 = new WxSubButton();
            WxSubButton sbt2 = new WxSubButton();
//            Map<String,String> vbt = new HashMap<String,String>();
            WxClickButton vbt1=new WxClickButton();
            List buttonlist1 = new ArrayList();
            vbt1.setUrl(urlforvbt1);
            vbt1.setName("土特产");
            vbt1.setType("view");
            buttonlist1.add(vbt1);

            WxClickButton vbt2=new WxClickButton();
            vbt2.setUrl("http://www.baidu.com");
            vbt2.setName("民宿");
            vbt2.setType("view");
            buttonlist1.add(vbt2);

            WxClickButton vbt3=new WxClickButton();
            vbt3.setUrl("http://www.baidu.com");
            vbt3.setName("门票");
            vbt3.setType("view");
            buttonlist1.add(vbt3);

            sbt1.setName("预定");
            sbt1.setSub_button(buttonlist1);


            WxClickButton vbt4=new WxClickButton();
            List buttonlist2 = new ArrayList();
            vbt4.setUrl("http://www.baidu.com");
            vbt4.setName("我是游客");
            vbt4.setType("view");
            buttonlist2.add(vbt4);

            WxClickButton vbt5=new WxClickButton();
            vbt5.setUrl("http://www.baidu.com");
            vbt5.setName("我是商户");
            vbt5.setType("view");
            buttonlist2.add(vbt5);

            sbt2.setName("个人中心");
            sbt2.setSub_button(buttonlist2);

            WxClickButton vbt6=new WxClickButton();
            List buttonlist3 = new ArrayList();
            vbt6.setUrl("http://www.baidu.com");
            vbt6.setName("乡村野趣");
            vbt6.setType("view");
            buttonlist3.add(vbt6);
            buttonlist3.add(sbt1);
            buttonlist3.add(sbt2);

//            menujson = JSONUtil2.objectToJsonExclude(buttonlist);


//
//            JSONArray button=new JSONArray();
//            button.add(vbt);
//            list.add(buttonOne);
//            list.add(cbt);
            WxButtonMap buttonmap = new WxButtonMap();
            buttonmap.setButton(buttonlist3);
//            menujson = WxJsonUtils.returnObject(buttonmap);
            menujson = JSONUtil2.objectToJsonExclude(buttonmap);

//            String wxcustomButton = WxJsonUtils.returnObject(WxCustomButtonServiceImpl.CustomButton());
//            response = RestResponse.success(menujson);


            //从redis中获取有效token
            String wxtoken = null;
            String token = redisCacheService.get(Constant.WX_TOKEN);
            wxtoken = TokenUtil.getValidTokenFromRedis(token);
            if(CommonUtils.notEmpty(wxtoken)){
                redisCacheService.set(Constant.WX_TOKEN,wxtoken,7100);
            }else {
                wxtoken = token;
            }



            String url = " https://api.weixin.qq.com/cgi-bin/menu/create?access_token="+ wxtoken;
            String result = HttpClientUtil.httpPostContent(url,menujson);
            System.out.print(result);
            WebUtils.outputJson(request,response,menujson);
        }catch(Exception ex) {
            logger.error("error is appreare",ex);
        }


    }
}
