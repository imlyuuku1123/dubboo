package com.oseasy.xlxq.wx.web;

import com.alibaba.fastjson.JSONObject;
import com.oseasy.xlxq.service.api.logistics.LogisticsService;
import com.oseasy.xlxq.service.api.order.model.ConfigExpress;
import com.oseasy.xlxq.service.api.order.model.OrderInfo;
import com.oseasy.xlxq.service.api.order.model.OrderLogistics;
import com.oseasy.xlxq.service.api.order.service.ConfigExpressService;
import com.oseasy.xlxq.service.api.order.service.OrderInfoService;
import com.oseasy.xlxq.service.api.order.service.OrderLogisticsService;
import com.oseasy.xlxq.service.core.utils.HqlUtil;
import com.oseasy.xlxq.service.web.rest.RestResponse;
import com.oseasy.xlxq.wx.constant.Constant;
import com.oseasy.xlxq.wx.utils.SessionUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.servlet.ModelAndView;

import javax.servlet.http.HttpServletRequest;
import java.util.List;

/**
 * Created by cannan on 2017/5/25.
 */
@RestController
public class TenantOrderController {

    private static Logger logger = LoggerFactory.getLogger(HaHaController.class);

    @Autowired
    private OrderInfoService orderInfoService;
    @Autowired
    private OrderLogisticsService orderLogisticsService;
    @Autowired
    private LogisticsService logisticsService;
    @Autowired
    private ConfigExpressService configExpressService;

    //===========================页面跳转============================

    //跳转到下单成功页面
    @RequestMapping("/order/toordersuc")
    public ModelAndView toordersuc (HttpServletRequest request){

        try {
            if(logger.isDebugEnabled()){
                logger.debug("debugger");
            }


        }catch(Exception ex) {
            logger.error("error is appreare",ex);

        }
        ModelAndView mav = new ModelAndView();

        mav.setViewName("/order/orderOk");
        return mav;
    }




    //===========================数据处理============================


    //根据传入参数改变订单状态
    @RequestMapping("/order/changeuserordertype")
    public String changeuserordertype(String orderid , String ordertype){
        String result = "";
        if(logger.isDebugEnabled()){
            logger.debug("debugger");
        }
        try {


            OrderInfo orderInfo = new OrderInfo();
            orderInfo.setOrderStatus(ordertype);
            orderInfoService.update(orderid,orderInfo);
            result = "success";

        }catch(Exception ex) {
            logger.error("error is appreare",ex);
            System.out.print(ex);
            result = "false";

        }
        return result;
    }


    //查询订单物流流转信息（根据物流公司code和订单号查询）

    @RequestMapping("/order/getlogisticinfo")
    public RestResponse<JSONObject> query( ) {
        RestResponse<JSONObject> restResponse = new RestResponse<JSONObject>();
        try {
            String orderId = SessionUtils.get(Constant.USER_MY_ORDER_ID) +"";
            //根据订单id查询物流公司和订单号
            String hql = "from OrderLogistics obj ";
            hql = HqlUtil.addCondition(hql, "orderInfo.id", orderId);
            OrderLogistics orderLogistics = orderLogisticsService.findUnique(hql);

            //..................获取物流公司code
            String companyCode = this.getExpressCode(orderLogistics.getDeliverCompany());

            //调用物流接口查询物流信息
            String str = logisticsService.logistics(companyCode, orderLogistics.getDeliverNum());
            JSONObject data = JSONObject.parseObject(str);

            restResponse.setData(data);
            restResponse.setSuccess(true);

        } catch (Exception ex) {
            logger.error("系统异常：", ex);
            restResponse.setSuccess(false);

        }
        return restResponse;
    }


    public String getExpressCode(String companyName) {
        String hql = "from ConfigExpress obj ";
        hql = HqlUtil.addCondition(hql, "name", companyName);
        List<ConfigExpress> list = configExpressService.getList(hql);
        return list.get(0).getCode();
    }


    //获取用户订单信息
    @RequestMapping("/order/getuserorderinfo")
    public RestResponse<OrderInfo> getuserorderinfo(){
        RestResponse<OrderInfo> response = new RestResponse<OrderInfo>();
        if(logger.isDebugEnabled()){
            logger.debug("debugger");
        }
        try {
            String orderid = SessionUtils.get(Constant.USER_MY_ORDER_ID)+"";

            OrderInfo orderInfo = orderInfoService.findById(orderid);
            response.setSuccess(true);
            response.setData(orderInfo);

        }catch(Exception ex) {
            logger.error("error is appreare",ex);
            System.out.print(ex);
           response.setSuccess(false);

        }
        return response;
    }

}
