package com.oseasy.xlxq.wx.web;

import com.oseasy.xlxq.service.api.sys.model.TenantUserAddrs;
import com.oseasy.xlxq.service.api.sys.service.TenantUserAddrsService;
import com.oseasy.xlxq.service.api.tenant.model.TenantUser;
import com.oseasy.xlxq.service.api.tenant.service.TenantUserService;
import com.oseasy.xlxq.service.core.utils.HqlUtil;
import com.oseasy.xlxq.service.core.utils.StringUtil;
import com.oseasy.xlxq.service.web.rest.RestResponse;
import com.oseasy.xlxq.wx.constant.Constant;
import com.oseasy.xlxq.wx.utils.SessionUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.servlet.ModelAndView;

import javax.servlet.http.HttpServletRequest;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

/**
 * Created by cannan on 2017/5/23.
 */
@RestController
public class TenantUserAdresController {

    private static Logger logger = LoggerFactory.getLogger(HaHaController.class);

    @Autowired
    private TenantUserAddrsService tenantUserAddrsService;
    @Autowired
    private TenantUserService tenantUserService;


    //===========================页面跳转============================

    //从土特产下单跳转到用户地址列表
    @RequestMapping("/useradrs/adrslist")
    public ModelAndView toaddresslist (String nowbuynum){

        try {
            if(logger.isDebugEnabled()){
                logger.debug("debugger");
            }
            SessionUtils.put(Constant.SPE_USER_BUYNUM , nowbuynum);

        }catch(Exception ex) {
            logger.error("error is appreare",ex);

        }
        ModelAndView mav = new ModelAndView();

        mav.setViewName("/merchant/tenantUserAddress");
        return mav;
    }

    //从我的页面跳转到用户地址列表
    @RequestMapping("/useradrs/toadreslist")
    public ModelAndView toadreslist (String nowbuynum){

        try {
            if(logger.isDebugEnabled()){
                logger.debug("debugger");
            }
            SessionUtils.put(Constant.SPE_USER_BUYNUM , nowbuynum);

        }catch(Exception ex) {
            logger.error("error is appreare",ex);

        }
        ModelAndView mav = new ModelAndView();

        mav.setViewName("/address/user_address_list");
        return mav;
    }

    //跳转到编辑地址列表
    @RequestMapping("/useradrs/editAddress")
    public ModelAndView addAddress (String editAdresId){

        try {
            if(logger.isDebugEnabled()){
                logger.debug("debugger");
            }

            SessionUtils.put(Constant.ADRES_EDIT_ID,editAdresId);
        }catch(Exception ex) {
            logger.error("error is appreare",ex);

        }
        ModelAndView mav = new ModelAndView();

        mav.setViewName("/address/user_address_edit");
        return mav;
    }

    //跳转回下单页面
    @RequestMapping("/useradrs/backorder")
    public ModelAndView backorder (String addressid){

        try {
            if(logger.isDebugEnabled()){
                logger.debug("debugger");
            }
            SessionUtils.put(Constant.USER_ADDRESSID, addressid);

        }catch(Exception ex) {
            logger.error("error is appreare",ex);

        }
        ModelAndView mav = new ModelAndView();

        mav.setViewName("/specialty/specialtyOderAddressOk");
        return mav;
    }

    //跳转到新增地址列表
    @RequestMapping("/useradrs/addAddress")
    public ModelAndView editAddress (){

        try {
            if(logger.isDebugEnabled()){
                logger.debug("debugger");
            }


        }catch(Exception ex) {
            logger.error("error is appreare",ex);

        }
        ModelAndView mav = new ModelAndView();

        mav.setViewName("/address/user_address_add");
        return mav;
    }

    //===========================数据处理============================
    //根据id获取地址详细(地址编辑用)
    @RequestMapping("/useradrs/getaddressbyid")
    public RestResponse<TenantUserAddrs> getaddressbyid(){
        RestResponse<TenantUserAddrs> response = new RestResponse<TenantUserAddrs>();
        if(logger.isDebugEnabled()){
            logger.debug("debugger");
        }
        try {
            String useadid =  SessionUtils.get(Constant.ADRES_EDIT_ID).toString();
            TenantUserAddrs tenantUserAddrs = tenantUserAddrsService.findById(useadid);
            response.setSuccess(true);
            response.setData(tenantUserAddrs);


        }catch(Exception ex) {
            logger.error("error is appreare",ex);
            System.out.print(ex);

        }



        return response;
    }


    //根据id获取地址详细（土特产选择用）
    @RequestMapping("/useradrs/getinfobyid")
    public RestResponse<TenantUserAddrs> getinfobyid(){
        RestResponse<TenantUserAddrs> response = new RestResponse<TenantUserAddrs>();
        if(logger.isDebugEnabled()){
            logger.debug("debugger");
        }
        try {
            String userid =  SessionUtils.get(Constant.USER_ID).toString();
            String hql = "from TenantUserAddrs obj";
            hql = HqlUtil.addCondition(hql , "isDefault" , "1");
            TenantUserAddrs tenantUserAddrs = tenantUserAddrsService.findUnique(hql);
            response.setSuccess(true);
            response.setData(tenantUserAddrs);


        }catch(Exception ex) {
            logger.error("error is appreare",ex);
            System.out.print(ex);

        }



        return response;
    }



    //获取地址列表
    @RequestMapping("/useradrs/getaddreslist")
    public RestResponse <List<TenantUserAddrs>> getaddreslist(){
        RestResponse<List<TenantUserAddrs>> response = new RestResponse<List<TenantUserAddrs>>();
        List<TenantUserAddrs> tenantUserAddrsslist = new ArrayList<TenantUserAddrs>();
        if(logger.isDebugEnabled()){
            logger.debug("debugger");
        }
        try {
            String userId =  SessionUtils.get(Constant.USER_ID).toString();
            String hql = "from TenantUserAddrs obj ";
            hql= HqlUtil.addCondition(hql,"tenantUser.id",userId);
            Iterator<TenantUserAddrs> tenantUserAddrsIterator = tenantUserAddrsService.list(hql).iterator();
            while (tenantUserAddrsIterator.hasNext()){
                tenantUserAddrsslist.add(tenantUserAddrsIterator.next());
            }
            response.setSuccess(true);
            response.setData(tenantUserAddrsslist);


        }catch(Exception ex) {
            logger.error("error is appreare",ex);
            System.out.print(ex);

        }



        return response;
    }


    //保存地址
    @RequestMapping("/useradrs/saveaddress")
    public RestResponse<TenantUserAddrs> saveaddress(TenantUserAddrs tenantUserAddrs ){
        RestResponse<TenantUserAddrs> response = new RestResponse<TenantUserAddrs>();
        if(logger.isDebugEnabled()){
            logger.debug("debugger");
        }
        try {
            String userid = SessionUtils.get(Constant.USER_ID).toString();
            TenantUser tenantUser = new TenantUser();
            tenantUser = tenantUserService.findById(userid);
//            String useadid =  SessionUtils.get("userAddressId").toString();
            tenantUserAddrs.setTenantUser(tenantUser);
            tenantUserAddrs = tenantUserAddrsService.save(tenantUserAddrs);
            SessionUtils.put(Constant.USER_ADDRESSID,tenantUserAddrs.getId());
            response.setSuccess(true);
            response.setData(tenantUserAddrs);


        }catch(Exception ex) {
            logger.error("error is appreare",ex);
            System.out.print(ex);

        }



        return response;
    }

    //保存编辑后的地址
    @RequestMapping("/useradrs/saveeditadre")
    public RestResponse<String> saveeditadre(TenantUserAddrs tenantUserAddrs ){
        RestResponse<String> response = new RestResponse<String>();
        if(logger.isDebugEnabled()){
            logger.debug("debugger");
        }
        try {
            String userid = SessionUtils.get(Constant.USER_ID).toString();
            TenantUser tenantUser = new TenantUser();
            tenantUser = tenantUserService.findById(userid);
            String addressid =  SessionUtils.get(Constant.ADRES_EDIT_ID).toString();
            tenantUserAddrs.setTenantUser(tenantUser);
            tenantUserAddrsService.update(addressid,tenantUserAddrs);
            response.setSuccess(true);
            response.setData("success!");


        }catch(Exception ex) {
            logger.error("error is appreare",ex);
            System.out.print(ex);

        }



        return response;
    }


    //删除地址
    @RequestMapping("/useradrs/deleteadre")
    public RestResponse<String> deleteadre(String addressid ){
        RestResponse<String> response = new RestResponse<String>();
        if(logger.isDebugEnabled()){
            logger.debug("debugger");
        }
        try {
            tenantUserAddrsService.delete(addressid);
            response.setSuccess(true);
            response.setData("success!");


        }catch(Exception ex) {
            logger.error("error is appreare",ex);
            System.out.print(ex);

        }



        return response;
    }


}
