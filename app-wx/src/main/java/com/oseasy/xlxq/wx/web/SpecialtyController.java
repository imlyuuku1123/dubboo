package com.oseasy.xlxq.wx.web;

import com.oseasy.xlxq.service.api.order.model.OrderInfo;
import com.oseasy.xlxq.service.api.order.model.OrderProduct;
import com.oseasy.xlxq.service.api.order.service.OrderInfoService;
import com.oseasy.xlxq.service.api.order.service.OrderNoGenService;
import com.oseasy.xlxq.service.api.order.service.OrderProductService;
import com.oseasy.xlxq.service.api.sys.model.TenantUserAddrs;
import com.oseasy.xlxq.service.api.sys.model.TenantUserWxExt;
import com.oseasy.xlxq.service.api.sys.service.TenantUserAddrsService;
import com.oseasy.xlxq.service.api.sys.service.TenantUserWxExtService;
import com.oseasy.xlxq.service.api.tenant.model.*;
import com.oseasy.xlxq.service.api.tenant.service.TenantProductService;
import com.oseasy.xlxq.service.api.tenant.service.TenantSpecialtyCatalogService;
import com.oseasy.xlxq.service.api.tenant.service.TenantSpecialtyService;
import com.oseasy.xlxq.service.core.utils.HqlUtil;
import com.oseasy.xlxq.service.core.utils.Page;
import com.oseasy.xlxq.service.core.utils.StringUtil;
import com.oseasy.xlxq.service.web.rest.RestResponse;
import com.oseasy.xlxq.wx.constant.Constant;
import com.oseasy.xlxq.wx.utils.SessionUtils;
import org.jasypt.commons.CommonUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.servlet.ModelAndView;

import javax.servlet.http.HttpServletRequest;
import javax.xml.transform.Result;
import java.math.BigDecimal;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * Created by cannan on 2017/5/22.
 */
@RestController
public class SpecialtyController {
    private static Logger logger = LoggerFactory.getLogger(HaHaController.class);

    @Autowired
    private TenantUserWxExtService tenantUserWxExtService;
    @Autowired
    private TenantUserAddrsService tenantUserAddrsService;
    @Autowired
    private TenantSpecialtyService tenantSpecialtyService;
    @Autowired
    private OrderInfoService orderInfoService;
    @Autowired
    private TenantProductService tenantProductService;
    @Autowired
    private OrderNoGenService orderNoGenService;
    @Autowired
    private OrderProductService orderProductService;
    @Autowired
    private TenantSpecialtyCatalogService tenantSpecialtyCatalogService;



    //=================页面跳转==================
    //跳转到购买下单页面
    @RequestMapping("/specialty/buynow")
    public ModelAndView toorderindex (String specialtyid,String buynum){
        ModelAndView mav = new ModelAndView();
        if(StringUtil.isNotBlank(specialtyid)){
            SessionUtils.put(Constant.SPE_ID,specialtyid);
        }
        SessionUtils.put(Constant.SPE_USER_BUYNUM,buynum);
        Map<String,String> sendmap = new HashMap<String,String>();
        try {
            if(logger.isDebugEnabled()){
                logger.debug("debugger");
            }
            TenantUserWxExt tenantUserWxExt = new TenantUserWxExt();
            TenantUserAddrs tenantUserAddrs = new TenantUserAddrs();
            String hql = "from TenantUserWxExt obj ";
            hql= HqlUtil.addCondition(hql,"wxOpenid",SessionUtils.get(Constant.OPEN_ID).toString());
//            hql= HqlUtil.addCondition(hql,"wxOpenid","oz6g2wFSqThweaFODkceY-eNYhjA");
//            hql= HqlUtil.addCondition(hql,"wxOpenid","oz6g2wNrsQvPMz_PO1J8eAkdhy_8");
            tenantUserWxExt = tenantUserWxExtService.findUnique(hql);

            if(tenantUserWxExt != null){
                if(StringUtil.isEmpty(tenantUserWxExt.getTenantUser().getTelephone()) ){
                    mav.setViewName("/merchant/bindphone");
                    return  mav;
                }
                SessionUtils.put(Constant.USER_ID,tenantUserWxExt.getTenantUser().getId());
                hql = "from TenantUserAddrs obj ";
                hql= HqlUtil.addCondition(hql,"tenantUser.id",tenantUserWxExt.getTenantUser().getId());
                hql= HqlUtil.addCondition(hql,"isDefault","1");
                tenantUserAddrs = tenantUserAddrsService.findUnique(hql);
                if(tenantUserAddrs != null ){
                    SessionUtils.put(Constant.USER_ADDRESSID, tenantUserAddrs.getId());
                    mav.setViewName("/specialty/specialtyOderAddressOk");
//                    result = "toaddok";

                }else{
                    mav.setViewName("/specialty/specialtyOrderSubmit");
//                    result = "toaddwrite";
                }


            }

        }catch(Exception ex) {
            logger.error("error is appreare",ex);

        }



        return mav;
    }

    //跳转到土特产列表
    @RequestMapping("/product/specialtyindex")
    public ModelAndView tospeindex (HttpServletRequest request){
        try {
            if(logger.isDebugEnabled()){
                logger.debug("debugger");
            }
//            String code = request.getAttribute("code").toString();

        }catch(Exception ex) {
            logger.error("error is appreare",ex);

        }
        ModelAndView mav = new ModelAndView();

        mav.setViewName("/specialty/specialtyList");
        return mav;
    }

    //跳转到土特产详情
    @RequestMapping("/product/specialtydetail")
    public ModelAndView tospedetail (String id){
        try {
            if(logger.isDebugEnabled()){
                logger.debug("debugger");
            }
            SessionUtils.put(Constant.SPE_ID,id);

        }catch(Exception ex) {
            logger.error("error is appreare",ex);

        }
        ModelAndView mav = new ModelAndView();

        mav.setViewName("/specialty/specialtyDetail");
        return mav;
    }






    //=================功能实现==================


    //获取土特产类别
    @RequestMapping("/product/getspetype")
    public RestResponse<List<TenantSpecialtyCatalog>> specialtycatalog(){
        RestResponse<List<TenantSpecialtyCatalog>> response = new RestResponse<List<TenantSpecialtyCatalog>>();
        if(logger.isDebugEnabled()){
            logger.debug("debugger");
        }
        try {
            List<TenantSpecialtyCatalog> specialtycatalog = (List<TenantSpecialtyCatalog>) tenantSpecialtyCatalogService.list();
            response.setSuccess(true);
            response.setData(specialtycatalog);


        }catch(Exception ex) {
            logger.error("error is appreare",ex);
            System.out.print(ex);

        }



        return response;
    }

    //获取土特产列表
    @RequestMapping( "/product/listbysearch")
    public RestResponse<Page<TenantSpecialty>> specialtylist(
            @RequestParam(defaultValue = "1") Integer pageno ,
            @RequestParam(defaultValue = "10") Integer pagesize,
            String searchtypep,String searchtypel,String sorttype
    ){
        RestResponse<Page<TenantSpecialty>> response = new RestResponse<Page<TenantSpecialty>>();
        try {
            if(logger.isDebugEnabled()){
                logger.debug("debugger");
            }
//            String code = request.getAttribute("code").toString();
            String hql = "";
            Page<TenantSpecialty> pagelist = new Page<TenantSpecialty>();
            if(CommonUtils.isNotEmpty(searchtypep) && CommonUtils.isNotEmpty(searchtypel) ){
                hql = "from TenantSpecialty obj ";
                hql= HqlUtil.addCondition(hql,"catalogRootId",searchtypep);
                hql= HqlUtil.addCondition(hql,"catalogId",searchtypel);
                if(sorttype.equals("1")){
                    hql=HqlUtil.addOrder(hql,"favorablePrice","desc");
                }else if(sorttype.equals("2")){
                    hql=HqlUtil.addOrder(hql,"favorablePrice","");
                }else if(sorttype.equals("3")){
                    hql=HqlUtil.addOrder(hql,"salesVolume","desc");
                }else if(sorttype.equals("4")){
                    hql=HqlUtil.addOrder(hql,"salesVolume","");
                }
                pagelist = tenantSpecialtyService.pageList(hql,pageno,pagesize);
            }else{
                if(CommonUtils.isEmpty(sorttype)){
                    pagelist = tenantSpecialtyService.pageList(pageno,pagesize);
                }else{
                    if(sorttype.equals("1")){
                        hql = "from TenantSpecialty obj order by obj.favorablePrice desc";
                    }else if(sorttype.equals("2")){
                        hql = "from TenantSpecialty obj order by obj.favorablePrice";
                    }else if(sorttype.equals("3")){
                        hql = "from TenantSpecialty obj order by obj.salesVolume desc";
                    }else if(sorttype.equals("4")){
                        hql = "from TenantSpecialty obj order by obj.salesVolume";
                    }
                    pagelist = tenantSpecialtyService.pageList(hql,pageno,pagesize);

                }

            }

            response.setSuccess(true);
            response.setData(pagelist);

        }catch(Exception ex) {
            logger.error("error is appreare",ex);
            System.out.print(ex);

        }

        return response;
    }

    //获取土特产详情
    @RequestMapping("/product/details")
    public RestResponse<TenantSpecialty> specialtydetails(){
        RestResponse<TenantSpecialty> response = new RestResponse<TenantSpecialty>();
        String id = SessionUtils.get(Constant.SPE_ID).toString();
        if(logger.isDebugEnabled()){
            logger.debug("debugger");
        }
        try {
            TenantSpecialty specialty =  tenantSpecialtyService.findById(id);
            response.setSuccess(true);
            response.setData(specialty);


        }catch(Exception ex) {
            logger.error("error is appreare",ex);
            System.out.print(ex);

        }



        return response;
    }

    //确认订单页面获取土特产详情
    @RequestMapping("/specialty/details")
    public RestResponse<TenantSpecialty> surespecialtydetails(){
        RestResponse<TenantSpecialty> response = new RestResponse<TenantSpecialty>();
        String id = SessionUtils.get(Constant.SPE_ID).toString();
        if(logger.isDebugEnabled()){
            logger.debug("debugger");
        }
        try {
            TenantSpecialty specialty =  tenantSpecialtyService.findById(id);
            if(StringUtil.isNotBlank(SessionUtils.get(Constant.SPE_USER_BUYNUM).toString())){
                specialty.setBuyNum(SessionUtils.get(Constant.SPE_USER_BUYNUM).toString());
            }
            response.setSuccess(true);
            response.setData(specialty);


        }catch(Exception ex) {
            logger.error("error is appreare",ex);
            System.out.print(ex);

        }



        return response;
    }


    //确认购买
    @RequestMapping("/specialty/submitform")
    public String submitform (String addressId,int buynum){
//        ModelAndView mav = new ModelAndView();
        String result = "";
        try {
            if(logger.isDebugEnabled()){
                logger.debug("debugger");
            }
            TenantMerchant tenantMerchant = new TenantMerchant();
            TenantUserAddrs tenantUserAddrs = new TenantUserAddrs();
            TenantProduct tenantProduct = new TenantProduct();
            TenantSpecialty tenantSpecialty = new TenantSpecialty();
            TenantUser tenantUser = new TenantUser();
            OrderProduct orderProduct = new OrderProduct();
            OrderInfo orderInfo = new OrderInfo();
            String ordernum = orderNoGenService.genOrderNo(OrderNoGenService.ORDER_TYPE_FK);

            String hql = "from TenantProduct obj ";
            hql= HqlUtil.addCondition(hql,"id",SessionUtils.get(Constant.SPE_ID).toString());
            tenantProduct = tenantProductService.findUnique(hql);
            tenantUserAddrs.setId(addressId);
            tenantUser.setId(SessionUtils.get(Constant.USER_ID).toString());
            tenantMerchant.setId(tenantProduct.getTenantMerchant().getId());
            orderInfo.setTenantUserAddrs(tenantUserAddrs);
            orderInfo.setTenantUser(tenantUser);
            orderInfo.setTenantMerchant(tenantMerchant);
            orderInfo.setOrderNum(ordernum);
            BigDecimal fprice = tenantProduct.getFavorablePrice();
            BigDecimal buynumber = new BigDecimal(buynum);
            BigDecimal freight = new BigDecimal(0);
            if(tenantSpecialty instanceof TenantProduct){
                tenantSpecialty = (TenantSpecialty) tenantProduct;
                freight = tenantSpecialty.getFreight();
            }
            if(freight == null){
                freight = new BigDecimal(0);
            }
            fprice = fprice.multiply(buynumber);
            freight = fprice.add(freight);
            orderInfo.setOrderSum(freight);
            orderInfo.setOrderType("1");
            orderInfo.setOrderStatus("1");
            //保存进订单信息
            orderInfo = orderInfoService.save(orderInfo);

            //订单产品信息

            orderProduct.setTenantProduct(tenantProduct);
            orderProduct.setTenantMerchant(tenantMerchant);
            orderProduct.setOrderInfo(orderInfo);
            orderProduct.setProductPrice(tenantProduct.getProductUnit());
            orderProduct.setPurchaseNum(buynum);
            orderProduct.setFavorablePrice(tenantProduct.getFavorablePrice());
            orderProduct = orderProductService.save(orderProduct);
            if(StringUtil.isNotBlank(orderInfo.getId().toString()) && StringUtil.isNotBlank(orderProduct.getId().toString())){
                result = "success";
            }else {
                result = "falls";
            }


        }catch(Exception ex) {
            logger.error("error is appreare",ex);

        }



        return result;
    }


    //含新地址下单
    @RequestMapping("/specialty/save_new_order")
    public String saveNewOrder (String consignee,String receivingTel,String receivingAddress,int buynum){
//        ModelAndView mav = new ModelAndView();
        String result = "";
        try {
            if(logger.isDebugEnabled()){
                logger.debug("debugger");
            }
            TenantMerchant tenantMerchant = new TenantMerchant();
            TenantUserAddrs tenantUserAddrs = new TenantUserAddrs();
            TenantProduct tenantProduct = new TenantProduct();
            TenantSpecialty tenantSpecialty = new TenantSpecialty();
            TenantUser tenantUser = new TenantUser();
            OrderProduct orderProduct = new OrderProduct();
            OrderInfo orderInfo = new OrderInfo();
            String ordernum = orderNoGenService.genOrderNo(OrderNoGenService.ORDER_TYPE_FK);

            String hql = "from TenantProduct obj ";
            hql= HqlUtil.addCondition(hql,"id",SessionUtils.get(Constant.SPE_ID).toString());
            tenantProduct = tenantProductService.findUnique(hql);

            tenantUser.setId(SessionUtils.get(Constant.USER_ID).toString());
            tenantUserAddrs.setConsignee(consignee);
            tenantUserAddrs.setTenantUser(tenantUser);
            tenantUserAddrs.setReceivingTel(receivingTel);
            tenantUserAddrs.setReceivingAddress(receivingAddress);
            tenantUserAddrs.setIsDefault(1);
            tenantUserAddrs = tenantUserAddrsService.save(tenantUserAddrs);

            tenantMerchant.setId(tenantProduct.getTenantMerchant().getId());
            orderInfo.setTenantUserAddrs(tenantUserAddrs);
            orderInfo.setTenantUser(tenantUser);
            orderInfo.setTenantMerchant(tenantMerchant);
            orderInfo.setOrderNum(ordernum);
            BigDecimal fprice = tenantProduct.getFavorablePrice();
            BigDecimal buynumber = new BigDecimal(buynum);
            BigDecimal freight = new BigDecimal(0);
            if(tenantSpecialty instanceof TenantProduct){
                tenantSpecialty = (TenantSpecialty) tenantProduct;
                freight = tenantSpecialty.getFreight();
            }
            if(freight == null){
                freight = new BigDecimal(0);
            }
            fprice = fprice.multiply(buynumber);
            freight = fprice.add(freight);
            orderInfo.setOrderSum(freight);
            orderInfo.setOrderType("1");
            orderInfo.setOrderStatus("1");
            //保存进订单信息
            orderInfo = orderInfoService.save(orderInfo);

            //订单产品信息

            orderProduct.setTenantProduct(tenantProduct);
            orderProduct.setTenantMerchant(tenantMerchant);
            orderProduct.setOrderInfo(orderInfo);
            orderProduct.setProductPrice(tenantProduct.getProductUnit());
            orderProduct.setPurchaseNum(buynum);
            orderProduct.setFavorablePrice(tenantProduct.getFavorablePrice());
            orderProduct = orderProductService.save(orderProduct);
            if(StringUtil.isNotBlank(orderInfo.getId().toString()) && StringUtil.isNotBlank(orderProduct.getId().toString())){
                result = "success";
            }else {
                result = "falls";
            }


        }catch(Exception ex) {
            logger.error("error is appreare",ex);

        }



        return result;
    }
}
