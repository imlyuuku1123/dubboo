package com.oseasy.xlxq.wx.web;

import com.oseasy.xlxq.wx.constant.Constant;
import com.oseasy.xlxq.wx.mp.aes.AesException;
import com.oseasy.xlxq.wx.mp.aes.WXBizMsgCrypt;
import java.io.IOException;
import java.io.StringReader;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;
import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.NodeList;
import org.xml.sax.InputSource;
import org.xml.sax.SAXException;

/**
 * 接收微信推送类
 * Created by cannan on 2017/5/11.
 */
@RestController
public class WXListener {

    private static final Logger logger = LoggerFactory.getLogger(WXListener.class);

    private static final String WX_HOST = "https://api.weixin.qq.com/cgi-bin/";
    private static final String TOKEN_GET = "token?grant_type=client_credential&";
    private static final String USER_GET = "user/get?";



//    public static void main(String[] dsf) throws AesException, ParserConfigurationException, SAXException, IOException {
//        String signature = "47d08c85d0f4929185f3ca6c12231b342f1df2d5";
//        String timestamp = "1435397029";
//        String nonce = "2057720322";
//        String xmlStr = "<xml><ToUserName><![CDATA[gh_a81b6e437e25]]></ToUserName><FromUserName><![CDATA[onZuus1UmJJbvHEC02ZpgFryHE1Y]]></FromUserName><CreateTime>1437702444</CreateTime><MsgType><![CDATA[event]]></MsgType><Event><![CDATA[TEMPLATESENDJOBFINISH]]></Event><MsgID>209088436</MsgID><Status><![CDATA[success]]></Status></xml>";
//        WXBizMsgCrypt wxcpt = new WXBizMsgCrypt(Constant.TOKEN,
//                Constant.ENCODINGAESKEY, Constant.App_ID);
//        String sMsg = wxcpt.decryptMsg(signature, timestamp, nonce, xmlStr);
//        System.out.println("after decrypt msg: " + sMsg);
//
//        // 解析出明文xml标签的内容进行处理
//        DocumentBuilderFactory dbf = DocumentBuilderFactory.newInstance();
//        DocumentBuilder db = dbf.newDocumentBuilder();
//        StringReader sr = new StringReader(sMsg);
//        InputSource is = new InputSource(sr);
//        Document document = db.parse(is);
//
//        Element root = document.getDocumentElement();
//        // 微信消息类型
//        NodeList nodelist1 = root.getElementsByTagName("MsgType");
//        String msgType = nodelist1.item(0).getTextContent();
//
//        // TODO
//        System.out.println("MsgType：" + msgType);
//    }

    /**
     * 验证URL
     */
    @RequestMapping(value = { "/open/wxcallback" }, method = RequestMethod.GET)
    public void listenGet(HttpServletRequest request, HttpServletResponse response) {

        logger.info("wxcallback is coming");
        String result = "";
        try {
            String signature = request.getParameter("signature");
            String timeStamp = request.getParameter("timestamp");
            String nonce = request.getParameter("nonce");
            String echoStr = request.getParameter("echostr");

            logger.info("signature = " + signature);
            logger.info("timeStamp = " + timeStamp);
            logger.info("nonce = " + nonce);
            logger.info("echoStr = " + echoStr);


            response.getWriter().write(echoStr);
            response.flushBuffer();

        } catch (Exception e) {
            logger.error("error is out",e);
        }
    }

}
