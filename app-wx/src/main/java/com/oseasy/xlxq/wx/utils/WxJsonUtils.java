package com.oseasy.xlxq.wx.utils;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

import net.sf.json.JSONArray;
import net.sf.json.JSONObject;
/**
 * Created by cannan on 2017/5/9.
 */
public class WxJsonUtils {
    /**
     * 把json对象串转换成map对象
     *
     * @param jsonObjStr
     *            e.g. {'name':'get','int':1,'double',1.1,'null':null}
     * @return Map
     */
    @SuppressWarnings({ "unchecked", "rawtypes" })
    public static Map getMapFromJsonObjStr(String jsonObjStr) {
        JSONObject jsonObject = JSONObject.fromObject(jsonObjStr);

        Map map = new HashMap();
        for (Iterator iter = jsonObject.keys(); iter.hasNext();) {
            String key = (String) iter.next();
            map.put(key, jsonObject.get(key));
        }
        return map;
    }

    /**
     * 把map对象串转换成json对象
     *
     * @param map
     * @return jsonObjStr e.g. {'name':'get','int':1,'double',1.1,'null':null}
     */
    public static String toJson(Map<String, Object> map) {
        JSONObject jsonObject = JSONObject.fromObject(map);
        String str = jsonObject.toString();
        // replace "{
        System.out.println("str = "+str);
        if(str.indexOf("\"{") > 0) {
            str = str.replaceAll("\"{", "{");
            str = str.replaceAll("}\"", "}");
        }
        if(str.indexOf("\\") > 0) {
            str = str.replaceAll("\\", "");
        }
        System.out.println("str = "+str);
        return str;
    }

    /**
     *  把list对象转换成json对象
     * 参数列表
     *            Page page 存放;
     *  {"data":{"result":[{"id":58,"title":"LOL"}]}, "page":
     *         {"pageNo":1,"pageSize":10,"allCount":19},"code":
     *         "SUCCESS","errorMsg":""}
     */
    public static String returnList(List list, int pageNo, int pageSize, int total, String code,
                                    String errorMsg) {
        Map<String, Object> returnMap = new HashMap<String, Object>();
        Map<String, Object> pageMap = new HashMap<String, Object>();
        Map<String, Object> resultMap = new HashMap<String, Object>();
        // "code": "SUCCESS"
        returnMap.put("code", code);
        // "errorMsg": "000000"
        returnMap.put("errmsg", errorMsg);
        // "page":{"pageNo":1,"pageSize":10,"allCount":19}
        pageMap.put("pageNo", pageNo);
        pageMap.put("pageSize", pageSize);
        pageMap.put("allCount", total);
        resultMap.put("pageCondition", pageMap);
        // resultMap
        resultMap.put("result", null == list ? new ArrayList() : list);
        returnMap.put("data", resultMap);
        JSONObject json = JSONObject.fromObject(returnMap);
        return json.toString();
    }

    /**
     * 把list对象转换成json对象
     *
     */
    public static String returnList(List list) {
        JSONArray jsonArray = JSONArray.fromObject(list);
        return jsonArray.toString();
    }

    /**
     * 把object对象转换成json对象
     *
     */
    public static String returnObject(Object obj) {
        JSONArray jsonArray = JSONArray.fromObject(obj);
        return jsonArray.toString();
    }

    /**
     *  把数组转换成json对象
     *
     *  arr
     */
    public static String returnStrings(String[] arr) {
        JSONArray jsonArray = JSONArray.fromObject(arr);
        return jsonArray.toString();
    }

    /**
     * 把obj对象转换成json对象
     *  参数列表
     *            Page page 存放;
     */
    public static String returnObj(Object obj, String objName, String code, String errorMsg) {
        Map<String, Object> returnMap = new HashMap<String, Object>();
        Map<String, Object> pageMap = new HashMap<String, Object>();
        Map<String, Object> resultMap = new HashMap<String, Object>();
        // "code": "SUCCESS"
        returnMap.put("code", code);
        // "errorMsg": "000000"
        returnMap.put("errmsg", errorMsg);
        // resultMap
        if (!"".equals(objName)) {
            resultMap.put(objName, obj);
        }
        returnMap.put("data", resultMap);
        JSONObject json = JSONObject.fromObject(returnMap);
        return json.toString();
    }
}
