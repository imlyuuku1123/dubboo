package com.oseasy.xlxq.wx.utils;

import com.oseasy.xlxq.service.cache.manager.RedisCacheService;
import com.oseasy.xlxq.wx.constant.Constant;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import scala.util.parsing.combinator.testing.Str;

import javax.annotation.PostConstruct;
import java.util.Map;

/**
 *  第三方Token业务接口
 * Created by cannan on 2017/5/9.
 */
public class TokenUtil {

//    @Autowired
//    private RedisCacheService redisCacheService;
//    private static TokenUtil tokenUtil;
//
//
//    @PostConstruct
//    public void init(){
//        tokenUtil = this;
//        tokenUtil.redisCacheService = this.redisCacheService;
//    }

    static String WXTOKEN = "";
    public  TokenUtil(String token)
    {
        WXTOKEN  = token;
    }

    //返回token
    public static String returntoken(){
        return WXTOKEN;
    }
    /**
     *  获取微信token
     * 参数列表
     * 		appid
     * 		secret
     * 返回值 token；
     */
    public static String getWXToken(String appid, String secret) throws Exception {
        if(CommonUtils.notEmpty(WXTOKEN))
            return WXTOKEN;
        refreshWXToken(appid, secret);
        return WXTOKEN;
    }

    /**
     *  刷新微信token
     * 参数列表
     * 		appid
     * 		secret
     * @return token；
     */
    public static String refreshWXToken(String appid, String secret) {
        WXTOKEN = "";
        int tryTimes = 0;
        String url = Constant.WXTOKEN_URL + Constant.WXTOKEN_URL_APPID + appid + Constant.WXTOKEN_URL_SECRET + secret;
        do {
            tryTimes++;
            try {
                String tokenResult = WebserviceUtil.get(url);
                // TODO test
//                System.out.println("tokenJSON = " + tokenResult);

                String token = (String) WxJsonUtils.getMapFromJsonObjStr(tokenResult).get("access_token");
                WXTOKEN = token;
                tryTimes = 6;
            } catch (Exception e) {
                e.printStackTrace();
                tryTimes++;
            }
        } while (tryTimes <= 5);
        return WXTOKEN;
    }

    /**
     *  验证微信token
     *  参数列表
     * 		appid
     * 		secret
     */
    public static void getValidToken() {
        //获取当前token
        String token = TokenUtil.returntoken();
        System.out.println("token1---"+token);
        Map<String, Object> result = WebserviceUtil.postresultmap("https://api.weixin.qq.com/cgi-bin/user/info", "access_token="+token+"&openid=oxQoqt3lVUx30_KGJuTGWXG3ECbE&lang=zh_CN");
        if(result.get("errcode")==null){
            return;
        }
        //token无效
        if(result.get("errcode").toString().equals("40001")||result.get("errcode").toString().equals("41001")){
            //获取最新token
            try {
                token = TokenUtil.getWXToken(Constant.App_ID, Constant.App_Secret);
                TokenUtil tokenUtil=new TokenUtil(token);
                System.out.println("token2---"+token);
                System.out.println(tokenUtil);
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
        // 14:56
//		String token = "DOJMwiSzREGOsL5YFEg1mNqDuj-davK7j729wNJ2rAjH-H7E8M8T0MIhpWpGDs7TdRREzXqqd31dH_P0sjGXjB1RTDps6AOdIIsRZzy52LI";
//		TokenUtil tokenUtil=new TokenUtil(token);
    }

    /**
     *  验证微信token
     *  参数列表
     * 		appid
     * 		secret
     *
     */
    public static String  getValidTokenFromRedis(String token) {
        String wxtoken = null;
//        String token = tokenUtil.redisCacheService.get("wxtoken");

            Map<String, Object> result = WebserviceUtil.postresultmap("https://api.weixin.qq.com/cgi-bin/user/info", "access_token="+token+"&openid=oxQoqt3lVUx30_KGJuTGWXG3ECbE&lang=zh_CN");
            if(result.get("errcode")==null){
                wxtoken = null;

            }
            //token无效
            if(result.get("errcode").toString().equals("40001")||result.get("errcode").toString().equals("41001")){
                //获取最新token
                try {
                    wxtoken = TokenUtil.getWXToken(Constant.App_ID, Constant.App_Secret);

                } catch (Exception e) {
                    e.printStackTrace();
                }
            }


//        tokenUtil.redisCacheService.set("wxtoken",wxtoken,7100);
        return wxtoken;
    }
}
