package com.oseasy.xlxq.wx.model;

import java.util.LinkedList;
import java.util.List;

/**
 * 点击型菜单事件
 * @author cannan
 */

public class WxSubButton {

	private String name;
	private List sub_button;

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public List getSub_button() {
		return sub_button;
	}

	public void setSub_button(List sub_button) {
		this.sub_button = sub_button;
	}
}
