package com.oseasy.xlxq.wx.web;

import com.oseasy.xlxq.service.api.order.model.HomeStayRoomStock;
import com.oseasy.xlxq.service.api.order.model.OrderInfo;
import com.oseasy.xlxq.service.api.order.model.OrderProduct;
import com.oseasy.xlxq.service.api.order.service.OrderInfoService;
import com.oseasy.xlxq.service.api.order.service.OrderNoGenService;
import com.oseasy.xlxq.service.api.order.service.OrderProductService;
import com.oseasy.xlxq.service.api.sys.service.TenantUserWxExtService;
import com.oseasy.xlxq.service.api.tenant.model.*;
import com.oseasy.xlxq.service.api.tenant.service.TenantProductService;
import com.oseasy.xlxq.service.api.tenant.service.TenantScenicTicketService;
import com.oseasy.xlxq.service.api.tenant.service.TenantUserService;
import com.oseasy.xlxq.service.core.utils.HqlUtil;
import com.oseasy.xlxq.service.core.utils.Page;
import com.oseasy.xlxq.service.core.utils.StringUtil;
import com.oseasy.xlxq.service.web.rest.RestResponse;
import com.oseasy.xlxq.wx.constant.Constant;
import com.oseasy.xlxq.wx.utils.SessionUtils;
import org.jasypt.commons.CommonUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.servlet.ModelAndView;

import javax.servlet.http.HttpServletRequest;
import java.math.BigDecimal;
import java.text.SimpleDateFormat;
import java.util.*;

/**
 * Created by cannan on 2017/5/31.
 */
@RestController
public class TenantTickController {

    private static Logger logger = LoggerFactory.getLogger(HaHaController.class);

    @Autowired
    private TenantScenicTicketService tenantScenicTicketService;
    @Autowired
    private TenantUserWxExtService tenantUserWxExtService;
    @Autowired
    private TenantUserService tenantUserService;
    @Autowired
    private OrderNoGenService orderNoGenService;
    @Autowired
    private TenantProductService tenantProductService;
    @Autowired
    private OrderInfoService orderInfoService;
    @Autowired
    private OrderProductService orderProductService;

    //=================页面跳转==================
    //跳转到门票列表
    @RequestMapping("/product/ticketindex")
    public ModelAndView ticketindex (HttpServletRequest request){
        try {
            if(logger.isDebugEnabled()){
                logger.debug("debugger");
            }
//            String code = request.getAttribute("code").toString();

        }catch(Exception ex) {
            logger.error("error is appreare",ex);

        }
        ModelAndView mav = new ModelAndView();

        mav.setViewName("/ticket/ticket_index");
        return mav;
    }

    //跳转到门票详情
    @RequestMapping("/product/ticketdetail")
    public ModelAndView toticket (String id){
        try {
            if(logger.isDebugEnabled()){
                logger.debug("debugger");
            }
            SessionUtils.put(Constant.ST_ID,id);

        }catch(Exception ex) {
            logger.error("error is appreare",ex);

        }
        ModelAndView mav = new ModelAndView();

        mav.setViewName("/ticket/ticket_details");
        return mav;
    }

    //跳转到购买下单页面
    @RequestMapping("/ticket/buynow")
    public ModelAndView toticketorderindex (String ticketid,String buynum){
        ModelAndView mav = new ModelAndView();
        if(StringUtil.isNotBlank(ticketid)){
            SessionUtils.put(Constant.TIC_USER_BUY_ID,ticketid);
        }
        SessionUtils.put(Constant.TIC_USER_BUYNUM,buynum);
        Map<String,String> sendmap = new HashMap<String,String>();
        try {
            if(logger.isDebugEnabled()){
                logger.debug("debugger");
            }
            ;


        }catch(Exception ex) {
            logger.error("error is appreare",ex);

        }


        mav.setViewName("/ticket/ticket_order");
        return mav;
    }

    //===========================数据处理============================

    //获取门票列表
    @RequestMapping( "product/ticketlistbysearch")
    public RestResponse<Page<TenantScenicTicket>> homestaylist(
            @RequestParam(defaultValue = "1") Integer pageno ,
            @RequestParam(defaultValue = "10") Integer pagesize,
            String searchtypep,String sorttype
    ){
        RestResponse<Page<TenantScenicTicket>> response = new RestResponse<Page<TenantScenicTicket>>();
        try {
            if(logger.isDebugEnabled()){
                logger.debug("debugger");
            }
//            String code = request.getAttribute("code").toString();
            String hql = "";
            Page<TenantScenicTicket> pagelist = new Page<TenantScenicTicket>();
            if(CommonUtils.isNotEmpty(searchtypep)  ){
                hql = "from TenantScenicTicket obj ";
                hql= HqlUtil.addCondition(hql,"tenantScenic.id",searchtypep);
                if(sorttype.equals("1")){
                    hql=HqlUtil.addOrder(hql,"favorablePrice","desc");
                }else if(sorttype.equals("2")){
                    hql=HqlUtil.addOrder(hql,"favorablePrice","");
                }else if(sorttype.equals("3")){
                    hql=HqlUtil.addOrder(hql,"salesVolume","desc");
                }else if(sorttype.equals("4")){
                    hql=HqlUtil.addOrder(hql,"salesVolume","");
                }
                pagelist = tenantScenicTicketService.pageList(hql,pageno,pagesize);
            }else{
                if(CommonUtils.isEmpty(sorttype)){
                    pagelist = tenantScenicTicketService.pageList(pageno,pagesize);
                }else{
                    if(sorttype.equals("1")){
                        hql = "from TenantScenicTicket obj order by obj.favorablePrice desc";
                    }else if(sorttype.equals("2")){
                        hql = "from TenantScenicTicket obj order by obj.favorablePrice asc";
                    }else if(sorttype.equals("3")){
                        hql = "from TenantScenicTicket obj order by obj.salesVolume desc";
                    }else if(sorttype.equals("4")){
                        hql = "from TenantScenicTicket obj order by obj.salesVolume asc";
                    }
                    pagelist = tenantScenicTicketService.pageList(hql,pageno,pagesize);

                }

            }

            response.setSuccess(true);
            response.setData(pagelist);

        }catch(Exception ex) {
            logger.error("error is appreare",ex);
            System.out.print(ex);

        }

        return response;
    }


    //获取门票详情
    @RequestMapping("/product/ticketdetails")
    public RestResponse<TenantScenicTicket> ticketdetails(){
        RestResponse<TenantScenicTicket> response = new RestResponse<TenantScenicTicket>();
        String id = SessionUtils.get(Constant.ST_ID).toString();
        if(logger.isDebugEnabled()){
            logger.debug("debugger");
        }
        try {
            TenantScenicTicket scenicTicket =  tenantScenicTicketService.findById(id);
            response.setSuccess(true);
            response.setData(scenicTicket);


        }catch(Exception ex) {
            logger.error("error is appreare",ex);
            System.out.print(ex);

        }



        return response;
    }


    //确认订单页面获取门票详情
    @RequestMapping("/ticket/ticketdetails")
    public RestResponse<TenantScenicTicket> sureticketdetails(){
        RestResponse<TenantScenicTicket> response = new RestResponse<TenantScenicTicket>();
        String id = SessionUtils.get(Constant.TIC_USER_BUY_ID).toString();
        if(logger.isDebugEnabled()){
            logger.debug("debugger");
        }
        try {
            TenantScenicTicket tenantScenicTicket =  tenantScenicTicketService.findById(id);
            if(StringUtil.isNotBlank(SessionUtils.get(Constant.TIC_USER_BUYNUM).toString())){
                tenantScenicTicket.setBuyNum(SessionUtils.get(Constant.TIC_USER_BUYNUM).toString());
            }
            response.setSuccess(true);
            response.setData(tenantScenicTicket);


        }catch(Exception ex) {
            logger.error("error is appreare",ex);
            System.out.print(ex);

        }



        return response;
    }

    //下单页获取用户信息
//    @RequestMapping("/ticket/getuserinfo")
//    public RestResponse<TenantUser> getuserinfo(){
//        RestResponse<TenantUser> response = new RestResponse<TenantUser>();
//        if(logger.isDebugEnabled()){
//            logger.debug("debugger");
//        }
//        try {
////            String userid = SessionUtils.get("userId").toString();
//            String userid = "2";
//            TenantUser tenantUser =tenantUserService.findById(userid);
//
//        }catch(Exception ex) {
//            logger.error("error is appreare",ex);
//            System.out.print(ex);
//
//        }
//        return response;
//    }

    //保存门票订单
    @RequestMapping("/scenic/ticketsubmit")
    public String ticketsubmit(String username, String userphone, String buynum){
        String result = "";
        String id = SessionUtils.get(Constant.TIC_USER_BUY_ID).toString();
        String userid = SessionUtils.get(Constant.USER_ID)+"";
        SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd ");
//        String userid = "4028d70a5c477e3c015c4782c38d0004";
        if(logger.isDebugEnabled()){
            logger.debug("debugger");
        }
        try {

            TenantUser tenantUser = tenantUserService.findById(userid);


            TenantMerchant tenantMerchant = new TenantMerchant();
            TenantProduct tenantProduct = new TenantProduct();
            OrderProduct orderProduct = new OrderProduct();
            OrderInfo orderInfo = new OrderInfo();
            String ordernum = orderNoGenService.genOrderNo(OrderNoGenService.ORDER_TYPE_FK);

            String hql = "from TenantProduct obj ";
            hql= HqlUtil.addCondition(hql,"id",id);
            tenantProduct = tenantProductService.findUnique(hql);
            tenantUser.setId(userid);
            tenantMerchant.setId(tenantProduct.getTenantMerchant().getId());
            orderInfo.setTenantUser(tenantUser);
            orderInfo.setTenantMerchant(tenantMerchant);
            orderInfo.setOrderNum(ordernum);
            BigDecimal fprice = tenantProduct.getFavorablePrice();
            BigDecimal buynumber = new BigDecimal(buynum);
            fprice = fprice.multiply(buynumber);
            orderInfo.setOrderSum(fprice);
            orderInfo.setOrderType("3");
            orderInfo.setOrderStatus("1");
            orderInfo.setOrderUserName(username);
            orderInfo.setOrderUserPhone(userphone);
            //保存进订单信息
            orderInfo = orderInfoService.save(orderInfo);

            //订单产品信息

            orderProduct.setTenantProduct(tenantProduct);
            orderProduct.setTenantMerchant(tenantMerchant);
            orderProduct.setOrderInfo(orderInfo);
            orderProduct.setProductPrice(tenantProduct.getProductUnit());
            orderProduct.setPurchaseNum(Integer.parseInt(buynum));
            orderProduct.setFavorablePrice(tenantProduct.getFavorablePrice());
            orderProduct = orderProductService.save(orderProduct);
            if(StringUtil.isNotBlank(orderInfo.getId().toString()) && StringUtil.isNotBlank(orderProduct.getId().toString())){
                result = "success";

            }else {
                result = "falls";
            }



        }catch(Exception ex) {
            logger.error("error is appreare",ex);
            System.out.print(ex);

        }



        return result;
    }
}
