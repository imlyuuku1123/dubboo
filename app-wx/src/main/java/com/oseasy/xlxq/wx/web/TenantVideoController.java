package com.oseasy.xlxq.wx.web;

import com.alibaba.fastjson.JSONObject;
import com.oseasy.xlxq.service.api.sys.model.TenantUserWxExt;
import com.oseasy.xlxq.service.api.sys.service.TenantUserWxExtService;
import com.oseasy.xlxq.service.api.tenant.model.TenantScenicTicket;
import com.oseasy.xlxq.service.api.tenant.model.TenantUser;
import com.oseasy.xlxq.service.api.tenant.service.TenantUserService;
import com.oseasy.xlxq.service.api.video.model.VideoChannel;
import com.oseasy.xlxq.service.api.video.model.VideoProduct;
import com.oseasy.xlxq.service.api.video.model.VideoRoom;
import com.oseasy.xlxq.service.api.video.service.VideoChannelService;
import com.oseasy.xlxq.service.api.video.service.VideoProductService;
import com.oseasy.xlxq.service.api.video.service.VideoRoomService;
import com.oseasy.xlxq.service.core.utils.HqlUtil;
import com.oseasy.xlxq.service.core.utils.HttpClientUtil;
import com.oseasy.xlxq.service.core.utils.Page;
import com.oseasy.xlxq.service.core.utils.StringUtil;
import com.oseasy.xlxq.service.web.rest.RestResponse;
import com.oseasy.xlxq.wx.constant.Constant;
import com.oseasy.xlxq.wx.utils.SessionUtils;
import com.oseasy.xlxq.wx.utils.WebserviceUtil;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.servlet.ModelAndView;

import javax.servlet.http.HttpServletRequest;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

/**
 * Created by cannan on 2017/6/3.
 */
@RestController
public class TenantVideoController {
    private static Logger logger = LoggerFactory.getLogger(HaHaController.class);

    @Autowired
    private VideoChannelService videoChannelService;
    @Autowired
    private VideoRoomService videoRoomService;
    @Autowired
    private VideoProductService videoProductService;
    @Autowired
    private TenantUserService tenantUserService;
    @Autowired
    private TenantUserWxExtService tenantUserWxExtService;

    //=================页面跳转==================
    //跳转到视频首页
    @RequestMapping("/video/videoindex")
    public ModelAndView videoindex (String code){
        try {
            if(logger.isDebugEnabled()){
                logger.debug("debugger");
            }

            Map<String, Object> userinfo= WebserviceUtil.giveuserinfo(code);
            System.out.println("微信用户信息:"+userinfo);


            TenantUserWxExt tenantUserWxExt = new TenantUserWxExt();
            TenantUser tenantUser = new TenantUser();

            String hql = "from TenantUserWxExt obj ";
            hql= HqlUtil.addCondition(hql,"wxOpenid",userinfo.get("openid").toString());
            tenantUserWxExt = tenantUserWxExtService.findUnique(hql);
            if(null  == tenantUserWxExt){
                tenantUser.setName(userinfo.get("nickname").toString());
                tenantUser = tenantUserService.save(tenantUser);
                SessionUtils.put(Constant.USER_ID,tenantUser.getId());
                tenantUserWxExt = new TenantUserWxExt();
                tenantUserWxExt.setWxOpenid(userinfo.get("openid").toString());
                tenantUserWxExt.setWxNickName(userinfo.get("nickname").toString());
                tenantUserWxExt.setSexFlag(Integer.parseInt(userinfo.get("sex").toString()));
                tenantUserWxExt.setWxheadUrl(userinfo.get("headimgurl").toString());
//                    tenantUserWxExt.setWxUnionid(userinfo.get("unionid").toString());
                tenantUserWxExt.setTenantUser(tenantUser);
                tenantUserWxExtService.save(tenantUserWxExt);
            }
            SessionUtils.put(Constant.USER_ID,tenantUserWxExt.getTenantUser().getId());
            SessionUtils.put(Constant.OPEN_ID, userinfo.get("openid"));

        }catch(Exception ex) {
            logger.error("error is appreare",ex);

        }
        ModelAndView mav = new ModelAndView();

        mav.setViewName("/video/video_index");
        return mav;
    }

    //跳转到更多频道页面
    @RequestMapping("/video/tomorechannel")
    public ModelAndView tomorechannel (){
        try {
            if(logger.isDebugEnabled()){
                logger.debug("debugger");
            }

        }catch(Exception ex) {
            logger.error("error is appreare",ex);

        }
        ModelAndView mav = new ModelAndView();

        mav.setViewName("/video/video_more");
        return mav;
    }

    //跳转到频道视频列表
    @RequestMapping("/video/tovideoProject")
    public ModelAndView tovideoProject (String channelid ){
        try {
            if(logger.isDebugEnabled()){
                logger.debug("debugger");
            }
            if(StringUtil.isNotBlank(channelid)){
                SessionUtils.put(Constant.VIDEO_CHANNEL_ID,channelid);
            }
        }catch(Exception ex) {
            logger.error("error is appreare",ex);

        }
        ModelAndView mav = new ModelAndView();

        mav.setViewName("/video/video_project");
        return mav;
    }

    //跳转到房间详情页面
    @RequestMapping("/video/videoroomDetails")
    public ModelAndView videoroomDetails (String roomid ){
        try {
            if(logger.isDebugEnabled()){
                logger.debug("debugger");
            }
            if(StringUtil.isNotBlank(roomid)){
                SessionUtils.put(Constant.VIDEO_ROOM_ID,roomid);
            }
        }catch(Exception ex) {
            logger.error("error is appreare",ex);

        }
        ModelAndView mav = new ModelAndView();

        mav.setViewName("/video/video_detail");
        return mav;
    }



    //===========================数据处理============================
    //获取直播频道列表
    @RequestMapping("/video/channellist")
    public RestResponse<Page<VideoChannel>> channellist(){
        RestResponse<Page<VideoChannel>> response = new RestResponse<Page<VideoChannel>>();
        if(logger.isDebugEnabled()){
            logger.debug("debugger");
        }
        try {
            Page<VideoChannel> videoChannel =  videoChannelService.pageList(1,7);
            response.setSuccess(true);
            response.setData(videoChannel);


        }catch(Exception ex) {
            logger.error("error is appreare",ex);
            System.out.print(ex);

        }



        return response;
    }

    //根据频道id获取直播房间列表
    @RequestMapping("/video/roomlist")
    public RestResponse<Page<VideoRoom>> roomlist(){
        RestResponse<Page<VideoRoom>> response = new RestResponse<Page<VideoRoom>>();
        if(logger.isDebugEnabled()){
            logger.debug("debugger");
        }
        try {
            String channelid = SessionUtils.get(Constant.VIDEO_CHANNEL_ID)+"";
            String hql = "from VideoRoom obj";
            hql = HqlUtil.addCondition(hql,"videoChannel.id",channelid);
            Page<VideoRoom> videoRoom =  videoRoomService.pageList(hql,1,10);
            response.setSuccess(true);
            response.setData(videoRoom);


        }catch(Exception ex) {
            logger.error("error is appreare",ex);
            System.out.print(ex);

        }



        return response;
    }

    //获取全部直播频道列表
    @RequestMapping("/video/allchannellist")
    public RestResponse<List<VideoChannel>> allchannellist(){
        RestResponse<List<VideoChannel>> response = new RestResponse<List<VideoChannel>>();
        List<VideoChannel> videolist = new ArrayList<VideoChannel>();
        if(logger.isDebugEnabled()){
            logger.debug("debugger");
        }
        try {
            Iterator<VideoChannel> videoChannel =  videoChannelService.list().iterator();
            while (videoChannel.hasNext()){
                videolist.add(videoChannel.next());
            }
            response.setSuccess(true);
            response.setData(videolist);


        }catch(Exception ex) {
            logger.error("error is appreare",ex);
            System.out.print(ex);

        }



        return response;
    }

    //获取全部推荐视频列表
    @RequestMapping("/video/recommendroomlist")
    public RestResponse<List<VideoRoom>> recommendroomlist(){
        RestResponse<List<VideoRoom>> response = new RestResponse<List<VideoRoom>>();
        List<VideoRoom> videoroomlist = new ArrayList<VideoRoom>();
        if(logger.isDebugEnabled()){
            logger.debug("debugger");
        }
        try {
            String hql = "from VideoRoom obj ";
            hql = HqlUtil.addCondition(hql, "ifrecommend", 1);
            videoroomlist = videoRoomService.getList(hql);
            response.setSuccess(true);
            response.setData(videoroomlist);


        }catch(Exception ex) {
            logger.error("error is appreare",ex);
            System.out.print(ex);

        }

        return response;
    }

    //获取直播间详情
    @RequestMapping("/video/getroomdetail")
    public RestResponse<JSONObject> getroomdetail(){
        RestResponse<JSONObject> restResponse = new RestResponse<JSONObject>();
        //JSONObject data = new JSONObject();
        try {
            String id = SessionUtils.get(Constant.VIDEO_ROOM_ID)+"";
            if (StringUtil.isNotBlank(id)) {
                VideoRoom videoRoom = videoRoomService.findById(id);
                //restResponse.setData(videoRoom);

                String hql = "from VideoProduct obj ";
                hql = HqlUtil.addCondition(hql, "videoRoom.id", id);
                List<VideoProduct> list = videoProductService.getList(hql);

                JSONObject data = JSONObject.parseObject(JSONObject.toJSONString(videoRoom));

                data.put("videoProductList", list);
                restResponse.setData(data);

                restResponse.setSuccess(true);
            } else {
                restResponse.setSuccess(false);

            }
        }catch(Exception ex) {
            logger.error("error is appreare",ex);
            System.out.print(ex);

        }



        return restResponse;
    }



}
