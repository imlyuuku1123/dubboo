package com.oseasy.xlxq.wx.constant;

/**
 * Created by cannan on 2017/5/9.
 */
public class Constant {

    /** 微信 接口常量 */
    // 获取Token
    public static final String WXTOKEN_URL = "https://api.weixin.qq.com/cgi-bin/token?grant_type=client_credential";
    public static final String WXTOKEN_URL_APPID = "&appid=";
    public static final String WXTOKEN_URL_SECRET = "&secret=";


    //微信服务号开发版
    public static final String App_ID = "wx1c1d70fc747c14f7";
    public static final String App_Secret = "e90dae3a3d00164a2c3f451d8bd26d93";

    /** 微信 Token */
    public final static String TOKEN = "oseasy_xlxq";
    /** 微信 EncodingAESKey */
    public final static String ENCODINGAESKEY = "In1u4xmWuwTD5SQ7UAB1a18fCxVJQ30Qj0f3xI5HPQH";

    public static final String VC_KEY = "validateCode";

    /** 微信 临时调试Token */
    public final static String WXTOKEN = "I2zcF7msgCTNjP9vp-cY2XLzJdTuUU2SH9hBcYXXipmn42ZxBCmHHGwaJBlhulbxvPA5wNgjIC5b89AcZNvrg0LAVBrmSZ_3q6GVxV0jWuwOWPhACAYGZ";


    /** 后台 redis调用 wxtoken*/
    public static final String WX_TOKEN = "wxtoken";

    /** 后台 session 名*/
    public static final String HMS_ID= "hmsid";//跳转到民宿详情时的民宿id
    public static final String HMSR_BUY_ID= "hmsrbuyid";//跳转到民宿房间下单页面时所预定的房间id
    public static final String HMSR_BUY_NUM= "hmsbuynum";//跳转到民宿房间下单页面时所预定的房间数量
    public static final String HMSR_ID= "hmsrid";//跳转到民宿详情时获取房型详情的房型id
    public static final String HM_STARTTIME= "starttime";//跳转到民宿详情时获取房型详情列表的起始时间
    public static final String HM_ENDTIME= "endtime";//跳转到民宿详情时获取房型详情的房型id
    public static final String USER_ID= "userId";//全局使用的用户userid
    public static final String OPEN_ID= "openId";//全局使用的用户openidspeid
    public static final String SPE_ID= "speid";//跳转到土特产购买下单页面的土特产speid
    public static final String SPE_USER_BUYNUM= "userBuyNum";//跳转到土特产购买下单页面的土特产userBuyNum
    public static final String USER_ADDRESSID= "userAddressId";//跳转到土特产购买下单页面的用户地址userAddressId
    public static final String ST_ID= "stid";//跳转到门票详情的门票stid
    public static final String TIC_USER_BUY_ID= "userBuyticId";//跳转到门票下单页的门票id  userBuyticId
    public static final String TIC_USER_BUYNUM= "userBuyNum";//跳转到门票购买下单页面的门票购买数量 userBuyNum
    public static final String ADRES_EDIT_ID= "editAdresId";//跳转编辑地址页面id editAdresId   channelid
    public static final String VIDEO_CHANNEL_ID= "channelid";//跳转到频道列表的频道channelid
    public static final String VIDEO_ROOM_ID= "roomid";//跳转到频道列表的频道roomid
    public static final String USER_MY_ORDER_TYPE= "ordertype";//跳转到频道列表的频道ordertype
    public static final String USER_MY_ORDER_ID= "orderId";//跳转到订单操作的订单id orderId
    public static final String USER_MY_INFO_EDIT_CODE= "usercode";//用户绑定时申请的验证码







}
