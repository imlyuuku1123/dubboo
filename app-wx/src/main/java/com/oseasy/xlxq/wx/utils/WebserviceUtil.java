package com.oseasy.xlxq.wx.utils;

import java.net.URL;
import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;
import java.io.PrintWriter;
import java.net.HttpURLConnection;
import java.net.URLEncoder;
import java.util.Map;

import com.oseasy.xlxq.wx.constant.Constant;
import net.sf.json.JSONObject;
import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.entity.StringEntity;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.message.BasicHeader;
import org.apache.http.protocol.HTTP;
import org.apache.http.util.EntityUtils;

/**
 * Created by cannan on 2017/5/9.
 */
public class WebserviceUtil {
    /**
     *  发送get请求
     * 参数列表
     *            String urlStr 请求url地址;
     *  String 返回值
     */
    public static String get(String urlStr) throws Exception {
        // url地址
        URL url = null;
        // http连接
        HttpURLConnection httpConn = null;
        // 输入流
        BufferedReader in = null;
        StringBuffer sb = new StringBuffer();
        try {
            url = new URL(urlStr);
            httpConn = (HttpURLConnection) url.openConnection();
            // 提交模式
            httpConn.setRequestMethod("GET");
            in = new BufferedReader(new InputStreamReader(httpConn.getInputStream(), "UTF-8"));
            String str = null;
            while ((str = in.readLine()) != null) {
                sb.append(str);
            }
        }
        catch (Exception exception) {
            exception.printStackTrace();
        }
        finally {
            in.close();
        }
        String result = sb.toString();
        return result;
    }

    /**
     * 发送post请求
     * 参数列表
     *            String urlStr 请求url地址;
     *  String 返回值
     */
    public static String post(String urlStr) throws Exception {
        // url地址
        URL url = null;
        // http连接
        HttpURLConnection httpConn = null;
        // 输入流
        BufferedReader in = null;
        StringBuffer sb = new StringBuffer();
        try {
            url = new URL(urlStr);
            httpConn = (HttpURLConnection) url.openConnection();
            // 提交模式
            httpConn.setRequestMethod("POST");
            in = new BufferedReader(new InputStreamReader(httpConn.getInputStream(), "UTF-8"));
            String str = null;
            while ((str = in.readLine()) != null) {
                sb.append(str);
            }
        }
        catch (Exception exception) {
            exception.printStackTrace();
        }
        finally {
            in.close();
        }
        String result = sb.toString();
        return result;
    }

    /**
     *  发送post请求
     *  参数列表
     *            String urlStr 请求url地址; String paramStr 参数列表
     *            paramData=123&name=abc
     *
     * @return String 返回值
     */
    public static String post(String urlStr, String paramStr) {
        // url地址
        URL url = null;
        // http连接
        HttpURLConnection httpConn = null;
        // 输入流
        BufferedReader in = null;
        PrintWriter out = null;
        StringBuffer sb = new StringBuffer();
        try {
            url = new URL(urlStr);
            httpConn = (HttpURLConnection) url.openConnection();
            // 提交模式
            httpConn.setRequestMethod("POST");
            httpConn.setDoOutput(true);
            httpConn.setDoInput(true);
            out = new PrintWriter(httpConn.getOutputStream());
            out.print(paramStr);
            out.flush();
//            OutputStreamWriter out2 = new OutputStreamWriter(httpConn.getOutputStream(), "utf-8");
//            out2.write(paramStr);
//            out2.flush();
            in = new BufferedReader(new InputStreamReader(httpConn.getInputStream(), "UTF-8"));
//             in = new BufferedReader(new InputStreamReader(httpConn.getInputStream(), "GBK"));
            String str = null;
            while ((str = in.readLine()) != null) {
                sb.append(str);
            }
        }
        catch (Exception exception) {
            exception.printStackTrace();
        }
        finally {
            try {
                if (in != null) {
                    in.close();
                }
            }
            catch (IOException ex) {
            }
        }
        String result = sb.toString();
        return result;
    }


    /**
     *  发送post请求(httppost)方法
     *  参数列表
     *            String urlStr 请求url地址; String paramStr 参数列表
     *            paramData=123&name=abc
     *
     * @return String 返回值
     */
    public static String postbyString(String urlStr, String paramStr) {
        // url地址
        URL url = null;

        String result = null;


        try {
            // 将JSON进行UTF-8编码,以便传输中文
            String encoderJson = URLEncoder.encode(paramStr, HTTP.UTF_8);

            DefaultHttpClient httpClient = new DefaultHttpClient();
            HttpPost httpPost = new HttpPost(urlStr);
            httpPost.addHeader(HTTP.CONTENT_TYPE, "application/json");

            StringEntity se = new StringEntity(encoderJson);
            se.setContentType("text/json");
            se.setContentEncoding(new BasicHeader(HTTP.CONTENT_TYPE, "application/json"));
            httpPost.setEntity(se);
            HttpResponse response = httpClient.execute(httpPost);
            HttpEntity entity = response.getEntity();

            result = EntityUtils.toString(entity, "UTF-8");
        }
        catch (Exception exception) {
            exception.printStackTrace();
        }


        return result;
    }


    /**
     *  发送post请求 --发送模板消息
     *  参数列表
     *            String urlStr 请求url地址; String paramStr 参数列表
     *            paramData=123&name=abc
     *
     *  String 返回值
     */
    public static String postTemMsg(String urlStr, String paramStr) {
        // url地址
        URL url = null;
        // http连接
        HttpURLConnection httpConn = null;
        // 输入流
        BufferedReader in = null;
        StringBuffer sb = new StringBuffer();
        try {
            url = new URL(urlStr);
            httpConn = (HttpURLConnection) url.openConnection();
            // 提交模式
            httpConn.setRequestMethod("POST");
            httpConn.setDoOutput(true);
            httpConn.setDoInput(true);
            OutputStreamWriter out2 = new OutputStreamWriter(httpConn.getOutputStream(), "utf-8");
            out2.write(paramStr);
            out2.flush();
            in = new BufferedReader(new InputStreamReader(httpConn.getInputStream(), "UTF-8"));
            String str = null;
            while ((str = in.readLine()) != null) {
                sb.append(str);
            }
        }
        catch (Exception exception) {
            exception.printStackTrace();
        }
        finally {
            try {
                if (in != null) {
                    in.close();
                }
            }
            catch (IOException ex) {
            }
        }
        String result = sb.toString();
        return result;
    }
    //获取微信用户openid
    public static String giveopenid(String code) {
        String result = WebserviceUtil.post(
                "https://api.weixin.qq.com/sns/oauth2/access_token",
                "appid="+Constant.App_ID+"&"
                        + "secret="+ Constant.App_Secret+"&"
                        + "code="+code+"&" + "grant_type=authorization_code");
        JSONObject jasonObject = JSONObject.fromObject(result);
        Map<String, Object> map2 = jasonObject;
        return map2.get("openid").toString();
    }

    //获取微信用户信息
    public static Map<String, Object> giveuserinfo(String code) {
        System.out.println("开始获取openid:");
        //用code换取openid和access_token
        String result = WebserviceUtil.post(
                "https://api.weixin.qq.com/sns/oauth2/access_token",
                "appid="+Constant.App_ID+"&"
                        + "secret="+Constant.App_Secret+"&"
                        + "code="+code+"&" + "grant_type=authorization_code");
        JSONObject jasonObject = JSONObject.fromObject(result);
        Map<String, Object> map = jasonObject;
        //微信用户openid
        String openid = map.get("openid").toString();
        //access_token
        String accessToken=map.get("access_token").toString();
        System.out.println("开始获取用户信息:");
        //用openid和access_token得到用户的信息
        String userinfo = WebserviceUtil.post(
                "https://api.weixin.qq.com/sns/userinfo",
                "access_token="+accessToken+"&"
                        + "openid="+openid+"&"
                        + "lang=zh_CN");
        JSONObject jasonObject2 = JSONObject.fromObject(userinfo);
        Map<String, Object> map2 = jasonObject2;
        return map2;
    }

    //传入url和数据，返回map
    @SuppressWarnings("unchecked")
    public static Map<String, Object> postresultmap(String urlStr, String paramStr) {
        String userinfo = WebserviceUtil.post(urlStr,paramStr);
        JSONObject jasonObject2 = JSONObject.fromObject(userinfo);
        Map<String, Object> map2 = jasonObject2;
        return map2;
    }
}
