package com.oseasy.xlxq.wx.web;


import com.oseasy.xlxq.service.api.sys.model.TenantUserWxExt;
import com.oseasy.xlxq.service.api.sys.service.TenantUserWxExtService;
import com.oseasy.xlxq.service.api.tenant.model.*;
import com.oseasy.xlxq.service.api.tenant.service.*;
import com.oseasy.xlxq.service.core.utils.HqlUtil;
import com.oseasy.xlxq.service.core.utils.Page;
import com.oseasy.xlxq.service.core.utils.StringUtil;
import com.oseasy.xlxq.service.web.rest.RestResponse;
import com.oseasy.xlxq.wx.constant.Constant;
import com.oseasy.xlxq.wx.utils.SessionUtils;

import com.oseasy.xlxq.wx.utils.WebserviceUtil;
import org.apache.commons.collections.map.HashedMap;
import org.jasypt.commons.CommonUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.servlet.ModelAndView;

import javax.servlet.http.HttpServletRequest;
import java.util.List;
import java.util.Map;

/**
 * Created by cannan on 2017/5/11.
 *
 */
@RestController
public class ProductIndexConroller {

    private static Logger logger = LoggerFactory.getLogger(HaHaController.class);

    @Autowired
    private TenantProductService tenantProductService;
    @Autowired
    private TenantUserWxExtService tenantUserWxExtService;

    @Autowired
    private TenantSpecialtyService tenantSpecialtyService;
    @Autowired
    private TenantScenicService tenantScenicService;

    @Autowired
    private TenantUserService tenantUserService;



    //=================页面跳转==================

    //跳转到产品首页
    @RequestMapping("/product/index")
    public ModelAndView toproindex (String code){
        ModelAndView mav = new ModelAndView();
        try {
            if(logger.isDebugEnabled()){
                logger.debug("debugger");
            }
//            if(SessionUtils.get("openId") == null){

                Map<String, Object> userinfo= WebserviceUtil.giveuserinfo(code);
                System.out.println("微信用户信息:"+userinfo);
//                获取测试
//                Map<String, Object> userinfo = new HashedMap();
//                userinfo.put("openid","oz6g2wCNf7XO8UuuROvhIPAEzqrY");
//                userinfo.put("nickname","流火");
//                userinfo.put("sex","1");
//                userinfo.put("headimgurl","http://wx.qlogo.cn/mmopen/PiajxSqBRaEJzsIonE2pveSGwiaEU1V8Vr6ibARfmo46hyH6IIY91SsloBdooECUxjOiclBtv1gQpWLibiabV0iarCVSg/0");


                TenantUserWxExt tenantUserWxExt = new TenantUserWxExt();
                TenantUser tenantUser = new TenantUser();

                String hql = "from TenantUserWxExt obj ";
                hql= HqlUtil.addCondition(hql,"wxOpenid",userinfo.get("openid").toString());
                tenantUserWxExt = tenantUserWxExtService.findUnique(hql);
                if(null  == tenantUserWxExt){
                    tenantUser.setName(userinfo.get("nickname").toString());
                    tenantUser = tenantUserService.save(tenantUser);
                    SessionUtils.put(Constant.USER_ID,tenantUser.getId());
                    tenantUserWxExt = new TenantUserWxExt();
                    tenantUserWxExt.setWxOpenid(userinfo.get("openid").toString());
                    tenantUserWxExt.setWxNickName(userinfo.get("nickname").toString());
                    tenantUserWxExt.setSexFlag(Integer.parseInt(userinfo.get("sex").toString()));
                    tenantUserWxExt.setWxheadUrl(userinfo.get("headimgurl").toString());
//                    tenantUserWxExt.setWxUnionid(userinfo.get("unionid").toString());
                    tenantUserWxExt.setTenantUser(tenantUser);
                    tenantUserWxExtService.save(tenantUserWxExt);
                }

                SessionUtils.put(Constant.USER_ID,tenantUserWxExt.getTenantUser().getId());
                SessionUtils.put(Constant.OPEN_ID, userinfo.get("openid"));

                mav.addAllObjects(userinfo);

//            }


        }catch(Exception ex) {
            logger.error("error is appreare",ex);

        }


        mav.setViewName("/product/preIndex");

        return mav;
    }







    //===========================数据处理============================

    //获取产品首页列表
    @RequestMapping("/product/list")
    public RestResponse<List<TenantProduct>> productinfo(){
        List<TenantProduct> productinfoList = tenantProductService.findRecommendedProductList();
        RestResponse response = RestResponse.success(productinfoList);
        return response;
    }



    //获取景区信息
    @RequestMapping("/product/getscenic")
    public RestResponse<List<TenantScenic>> getscenic(){
        RestResponse<List<TenantScenic>> response = new RestResponse<List<TenantScenic>>();
        if(logger.isDebugEnabled()){
            logger.debug("debugger");
        }
        try {
            List<TenantScenic> TenantScenic = (List<TenantScenic>) tenantScenicService.list();
            response.setSuccess(true);
            response.setData(TenantScenic);


        }catch(Exception ex) {
            logger.error("error is appreare",ex);
            System.out.print(ex);

        }



        return response;
    }


}
