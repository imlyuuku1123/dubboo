package com.oseasy.xlxq.wx.utils;

import com.oseasy.xlxq.service.cache.manager.RedisCacheService;
import com.oseasy.xlxq.wx.constant.Constant;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import javax.annotation.PostConstruct;
import java.util.concurrent.Executors;
import java.util.concurrent.ScheduledExecutorService;
import java.util.concurrent.TimeUnit;

@Component
public class WxJsTicketSchemaTask {
    private static final Logger logger = LoggerFactory.getLogger(WxJsTicketSchemaTask.class);
    private static ScheduledExecutorService scheduledExecutorService = Executors.newSingleThreadScheduledExecutor();

    @Autowired
    private RedisCacheService redisCacheService;

    // 刷新wxjsticket到redis，redie的key为wxjsticket
    private class RefreshJsTicket implements Runnable{

        @Override
        public void run() {
            String wxtoken = TokenUtil.getValidTokenFromRedis(TokenUtil.returntoken());
            int tryTimes = 0;
            String url = "https://api.weixin.qq.com/cgi-bin/ticket/getticket?access_token=" + wxtoken + "&type=jsapi";
            do {
                tryTimes++;
                try {
                    String tokenResult = WebserviceUtil.get(url);
                    String jsTicket = (String) WxJsonUtils.getMapFromJsonObjStr(tokenResult).get("ticket");
                    logger.info("刷新JSTICKET值为：" + jsTicket);
                    redisCacheService.set("wxjsticket", jsTicket, 7100);
                    tryTimes = 6;
                } catch (Exception e) {
                    e.printStackTrace();
                    tryTimes++;
                    if (tryTimes == 5) {
                        logger.error("刷新JSTICKET失败");
                    }
                }
            } while (tryTimes <= 5);
        }
    };

    @PostConstruct
    public void init(){
        scheduledExecutorService.scheduleAtFixedRate(new RefreshJsTicket(), 1, 7000, TimeUnit.SECONDS);
    }
}
