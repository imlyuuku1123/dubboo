<%--
  Created by IntelliJ IDEA.
  User: cannan
  Date: 2017/6/12
  Time: 17:57
  To change this template use File | Settings | File Templates.
--%>
<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8" isELIgnored="false"%>
<%@include file="/inc/import.jsp" %>
<!DOCTYPE html>
<html>

<head>
    <meta charset="UTF-8">
    <meta http-equiv="Cache-Control" content="no-cache" />
    <meta name="viewport" content="width=device-width,initial-scale=1,user-scalable=0">
    <meta name="format-detection" content="telphone=yes, email=yes" />
    <title>评价订单</title>
    <link rel="stylesheet" type="text/css" href="/resources/css/weui.min.css" />
    <link rel="stylesheet" type="text/css" href="/resources/css/jquery-weui.css" />
    <link rel="stylesheet" type="text/css" href="/resources/css/app.css" />
    <style type="text/css">
        .weui-cells:after {
            border-bottom: 0;
        }
    </style>
</head>

<body ontouchstart>
<h1 class="goodtit">红富士</h1>
<form id="formafter" action="" class="am-form">

    <div class="weui-cells weui-cells_form outform">
        <div class="weui-cell">
            <div class="weui-cell__bd">
                <textarea class="weui-textarea" placeholder="亲！服务满意吗？" rows="4"></textarea>
                <!--<div class="weui-textarea-counter"><span>0</span>/200</div>-->
            </div>
        </div>
    </div>

    <div class="weui-cells weui-cells_form m-t-0">
        <div class="weui-cell">
            <div class="weui-cell__bd">
                <div class="weui-uploader">
                    <div class="weui-uploader__hd">
                        <p class="weui-uploader__title">图片上传</p>
                        <div class="weui-uploader__info">0/2</div>
                    </div>
                    <div class="weui-uploader__bd">
                        <ul class="weui-uploader__files" id="uploaderFiles">
                            <li class="weui-uploader__file"><img src="/resources/img/58be5d19e2dc6_100_100.jpg" /></li>
                            <li class="weui-uploader__file"><img src="/resources/img/58be5d19e2dc6_100_100.jpg" /></li>
                            <li class="weui-uploader__file"><img src="/resources/img/58be5d19e2dc6_100_100.jpg" /></li>
                        </ul>
                        <div class="weui-uploader__input-box">
                            <input id="uploaderInput" class="weui-uploader__input" type="file"  multiple="" >
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</form>
<div id="cart1" class="cart-concern-btm-fixed five-column four-column " style="display: table; z-index: 0;">
    <div class="weui-flex">
        <div class="weui-flex__item">
            <button class="weui-btn weui-btn_primary savebtn">发表评论</button>
        </div>

    </div>
</div>
</body>
<script type="text/javascript" src="/resources/js/jquery-2.1.4.js"></script>
<script type="text/javascript" src="/resources/js/jquery-weui.js"></script>
<script type="text/javascript" src="/resources/js/fastclick.js"></script>
<script type="text/javascript" src="/resources/js/app.js"></script>
<script type="text/javascript" src=" http://res.wx.qq.com/open/js/jweixin-1.2.0.js"></script>
<script type="text/javascript">
    wx.config({
        debug: true, // 开启调试模式,调用的所有api的返回值会在客户端alert出来，若要查看传入的参数，可以在pc端打开，参数信息会通过log打出，仅在pc端时才会打印。
        appId: 'wx1c1d70fc747c14f7', // 必填，公众号的唯一标识
        timestamp: , // 必填，生成签名的时间戳
        nonceStr: '', // 必填，生成签名的随机串
        signature: '',// 必填，签名，见附录1
        jsApiList: [] // 必填，需要使用的JS接口列表，所有JS接口列表见附录2
    });
</script>

</html>
