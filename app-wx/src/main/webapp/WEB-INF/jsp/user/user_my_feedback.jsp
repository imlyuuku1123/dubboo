<%--
  Created by IntelliJ IDEA.
  User: cannan
  Date: 2017/6/14
  Time: 15:31
  To change this template use File | Settings | File Templates.
--%>
<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8" isELIgnored="false"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@include file="/inc/import.jsp" %>
<!DOCTYPE html>
<html>

<head>
    <%@ page isELIgnored="false" %>
    <meta charset="UTF-8">
    <meta http-equiv="Cache-Control" content="no-cache" />
    <meta name="viewport" content="width=device-width,initial-scale=1,user-scalable=0">
    <meta name="format-detection" content="telphone=yes, email=yes" />
    <title>意见反馈</title>
    <link rel="stylesheet" type="text/css" href="/resources/css/weui.min.css" />
    <link rel="stylesheet" type="text/css" href="/resources/css/jquery-weui.css" />
    <link rel="stylesheet" type="text/css" href="/resources/css/app.css" />
    <style type="text/css">
        .weui-navbar:after {
            border: 0;
        }

        .weui-navbar__item.weui-bar__item--on {
            background: #fafafa;
            border-bottom: 1px solid #F1F1F1;
        }

        .weui-navbar__item:after {
            border-right: 1px solid #F1F1F1;
        }

        .weui-navbar {
            background: #fff;
        }

    </style>
</head>

<body ontouchstart>

<div class="weui-tab">
    <div class="weui-navbar">
        <a class="weui-navbar__item weui-bar__item--on" href="#tab1">
            意见反馈
        </a>
        <a class="weui-navbar__item" href="#tab2">
            反馈记录
        </a>

    </div>
    <div class="weui-tab__bd">
        <div id="tab1" class="weui-tab__bd-item weui-tab__bd-item--active feedbacktab
">
            <div class="main replaycon">
                <ul class="commlist">
                    <li>
                        <div class="cons">
                            <h1>我：</h1>

                            <div class="introduce
	">吧啦吧吧啦吧啦啦
                            </div>
                            <span class="time">2017年4月13日  <em>13:26</em>

										<em class="fr replaystate">已回复</em>

									</span>
                        </div>
                        <ul class="reply">
                            <li>
                                <div class="cons">

                                    <div class="introduce
	">客服回复：吧啦吧吧啦吧啦啦
                                    </div>
                                    <span class="time">2017年4月13日  <em>13:26</em></span>
                                </div>
                            </li>
                        </ul>
                    </li>
                    <li>
                        <div class="cons">
                            <h1>我：</h1>

                            <div class="introduce
	">吧啦吧吧啦吧啦啦
                            </div>
                            <span class="time">2017年4月13日  <em>13:26</em>

										<em class="fr replaystate">已回复</em>

									</span>
                        </div>
                        <ul class="reply">
                            <li>
                                <div class="cons">

                                    <div class="introduce
	">客服回复：吧啦吧吧啦吧啦啦
                                    </div>
                                    <span class="time">2017年4月13日  <em>13:26</em></span>
                                </div>
                            </li>
                        </ul>
                    </li>
                    <li>
                        <div class="cons">
                            <h1>我：</h1>

                            <div class="introduce
	">吧啦吧吧啦吧啦啦
                            </div>
                            <span class="time">2017年4月13日  <em>13:26</em>

										<em class="fr replaystate">未回复</em>

									</span>
                        </div>

                    </li>

                </ul>
            </div>
        </div>
        <div id="tab2" class="weui-tab__bd-item" style="padding: 0;">
            <div class="weui-cell">
                <div class="weui-cell__hd"><label class="weui-label" style="width:85px;">电话号码</label></div>
                <div class="weui-cell__bd">
                    <input class="weui-input" value="18267148001" type="text" placeholder="">
                </div>
            </div>
            <div class="weui-cells weui-cells_form outform">
                <div class="weui-cell">
                    <div class="weui-cell__bd">
                        <textarea class="weui-textarea" placeholder="亲！服务满意吗？" rows="4"></textarea>
                        <!--<div class="weui-textarea-counter"><span>0</span>/200</div>-->
                    </div>
                </div>
            </div>
            <div class="weui-cells weui-cells_form m-t-0 upload
">
                <div class="weui-cell">
                    <div class="weui-cell__bd">
                        <div class="weui-uploader">
                            <div class="weui-uploader__hd">
                                <p class="weui-uploader__title">图片上传</p>
                                <div class="weui-uploader__info">0/2</div>
                            </div>
                            <div class="weui-uploader__bd">
                                <ul class="weui-uploader__files" id="uploaderFiles">
                                    <li class="weui-uploader__file"><img src="../img/58be5d19e2dc6_100_100.jpg" /></li>
                                    <li class="weui-uploader__file"><img src="../img/58be5d19e2dc6_100_100.jpg" /></li>
                                    <li class="weui-uploader__file"><img src="../img/58be5d19e2dc6_100_100.jpg" /></li>
                                </ul>
                                <div class="weui-uploader__input-box">
                                    <input id="uploaderInput" class="weui-uploader__input" type="file" multiple="">
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>

    </div>
</div>

<div id="cart1" class="cart-concern-btm-fixed five-column four-column " style="display: table; z-index: 0;">
    <div class="weui-flex">
        <div class="weui-flex__item">
            <button class="weui-btn weui-btn_primary savebtn">发表评论</button>
        </div>

    </div>
</div>
</body>
<script type="text/javascript" src="/resources/js/jquery-2.1.4.js"></script>
<script type="text/javascript" src="/resources/js/jquery-weui.js"></script>
<script type="text/javascript" src="/resources/js/fastclick.js"></script>
<script type="text/javascript" src="/resources/js/app.js"></script>
<script type="text/javascript" src="/resources/js/sha1.js"></script>
<script type="text/javascript" src="http://res.wx.qq.com/open/js/jweixin-1.2.0.js"></script>
<script>
    $(document).ready(function() {
        var wxjsticket = '';
        wxjsticket = '' + '${wxjsticket}';
        console.log(wxjsticket);
        var timestamp = new Date().getTime();
        var nonceStr = "test";
        var url = "http://wx.dev.os-easy.com/user/tomyfeedback";
        var toSha1 = "jsapi_ticket=" + wxjsticket + "&noncestr=" + nonceStr + "&timestamp=" + timestamp + "&url=" + url;
        var signature = hex_sha1(toSha1);
        wx.config({
            debug: true, // 开启调试模式,调用的所有api的返回值会在客户端alert出来，若要查看传入的参数，可以在pc端打开，参数信息会通过log打出，仅在pc端时才会打印。
            appId: 'wx1c1d70fc747c14f7', // 必填，公众号的唯一标识
            timestamp: timestamp, // 必填，生成签名的时间戳
            nonceStr: nonceStr, // 必填，生成签名的随机串
            signature: signature,// 必填，签名，见附录1
            jsApiList: ['scanQRCode'] // 必填，需要使用的JS接口列表，所有JS接口列表见附录2
        });
        wx.ready(function(){
            // 在ready里面才能调用wxjssdk，切记
            $('.savebtn').on('click', function() {
                wx.scanQRCode({
                    needResult: 1, // 默认为0，扫描结果由微信处理，1则直接返回扫描结果，
                    scanType: ["qrCode","barCode"], // 可以指定扫二维码还是一维码，默认二者都有
                    success: function (res) {
                        var result = res.resultStr; // 当needResult 为 1 时，扫码返回的结果
                        alert(result);
                    }
                });
            });
        });
        wx.error(function(){
        });
    });
</script>
</html>
