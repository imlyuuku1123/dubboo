<%--
  Created by IntelliJ IDEA.
  User: cannan
  Date: 2017/6/12
  Time: 16:20
  To change this template use File | Settings | File Templates.
--%>
<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8" isELIgnored="false"%>
<%@include file="/inc/import.jsp" %>
<!DOCTYPE html>
<html>

<head>
    <meta charset="UTF-8">
    <meta http-equiv="Cache-Control" content="no-cache" />
    <meta name="viewport" content="width=device-width,initial-scale=1,user-scalable=0">
    <meta name="format-detection" content="telphone=yes, email=yes" />
    <title>申请售后</title>
    <link rel="stylesheet" type="text/css" href="/resources/css/weui.min.css" />
    <link rel="stylesheet" type="text/css" href="/resources/css/jquery-weui.css" />
    <link rel="stylesheet" type="text/css" href="/resources/css/app.css" />
    <style type="text/css">
        .weui-cells:after {
            border-bottom: 0;
        }
    </style>
</head>

<body ontouchstart>
<!--订单号-->
<div class="weui-cells m-t-0">
    <div class="weui-cell log">
        <div class="weui-cell__bd weui-cell_primary">
            <p class="recipient">
                <span id="ordernum"></span>
                <span class="fr" id="ordertime">

						</span>

            </p>
        </div>
        <div class="weui-cell__ft"> </div>
    </div>
</div>

<form id="formafter" action="" class="am-form">
    <div class="weui-flex">
        <div class="weui-flex__item">
            <div class="checkeactive box-flex-c">
                <span class="cart-checkbox" state="0"></span>
                <input type="checkbox" name="" value="1" style="display:none;"> 仅退款
            </div>

        </div>
        <div class="weui-flex__item">
            <div class="checkeactive box-flex-c">
                <span class="cart-checkbox" state="0"></span>
                <input type="checkbox" name="" value="2" style="display:none;"> 退货退款
            </div>
        </div>
    </div>

    <div class="weui-cells weui-cells_form outform">
        <div class="weui-cell">
            <div class="weui-cell__bd">
                <textarea class="weui-textarea" placeholder="退货原因" rows="4" id="refundreason"></textarea>
                <!--<div class="weui-textarea-counter"><span>0</span>/200</div>-->
            </div>
        </div>
    </div>

</form>
<div id="cart1" class="cart-concern-btm-fixed five-column four-column " style="display: table; z-index: 0;">
    <div class="weui-flex">
        <div class="weui-flex__item">
            <button class="weui-btn weui-btn_primary savebtn">申请售后</button>
        </div>

    </div>
</div>
</body>
<script type="text/javascript" src="/resources/js/jquery-2.1.4.js"></script>
<script type="text/javascript" src="/resources/js/jquery-weui.js"></script>
<script type="text/javascript" src="/resources/js/fastclick.js"></script>
<script type="text/javascript" src="/resources/js/app.js"></script>
<script type="text/javascript">
    var checkvideo = "";
    $(function(){
        getorderinfo();
        $(".cart-checkbox").click(function(){
            $(".cart-checkbox").removeClass("checked");
            $(this).addClass("checked");
            checkvideo = $(this).next().val();
        });
        $(".savebtn").click(function(){
            if ($(".cart-checkbox").hasClass("checked")) {
                $.post("${cxt}/user/saverefund",
                    {
                        refundType : checkvideo,
                        refundreason : $("#refundreason").val()
                    },
                    function (data) {
                        if(data.success){
                            $.toast("申请售后成功");
                            window.location.href = "${cxt}/user/index";
                        }
                    }
                    )
                return true;
            } else{
                $.toast('选择您的退款方式', "cancel");
                return false;
            }
        });

    })



    //获取订单信息
    function  getorderinfo() {
        $.get("${ctx}/order/getuserorderinfo",
            function(data) {
                var html = "";
                if(data.success){
                    html = '订单号：<em>'+data.data.orderNum+'</em>';
                    $("#ordernum").html(html);
                    var time1 = new Date(data.data.createTime).Format("yyyy-MM-dd hh:mm:ss");
                    html = '时间：<em class="orderdate">'+time1+'</em>';
                    $("#ordertime").html(html);
                }

            }
        );
    }


    // 对Date的扩展，将 Date 转化为指定格式的String
    // 月(M)、日(d)、小时(h)、分(m)、秒(s)、季度(q) 可以用 1-2 个占位符，
    // 年(y)可以用 1-4 个占位符，毫秒(S)只能用 1 个占位符(是 1-3 位的数字)
    // 例子：
    // (new Date()).Format("yyyy-MM-dd hh:mm:ss.S") ==> 2006-07-02 08:09:04.423
    // (new Date()).Format("yyyy-M-d h:m:s.S")      ==> 2006-7-2 8:9:4.18
    Date.prototype.Format = function (fmt) { //author: meizz
        var o = {        "M+": this.getMonth() + 1, //月份
            "d+": this.getDate(), //日
            "h+": this.getHours(), //小时
            "m+": this.getMinutes(), //分
            "s+": this.getSeconds(), //秒
            "q+": Math.floor((this.getMonth() + 3) / 3), //季度
            "S": this.getMilliseconds() //毫秒
        };    if (/(y+)/.test(fmt)) fmt = fmt.replace(RegExp.$1, (this.getFullYear() + "").substr(4 - RegExp.$1.length));    for (var k in o)    if (new RegExp("(" + k + ")").test(fmt)) fmt = fmt.replace(RegExp.$1, (RegExp.$1.length == 1) ? (o[k]) : (("00" + o[k]).substr(("" + o[k]).length)));    return fmt;}
</script>
</html>