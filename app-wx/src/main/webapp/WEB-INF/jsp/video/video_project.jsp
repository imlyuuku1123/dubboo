<%--
  Created by IntelliJ IDEA.
  User: cannan
  Date: 2017/6/3
  Time: 17:48
  To change this template use File | Settings | File Templates.
--%>
<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8" isELIgnored="false"%>
<%@include file="/inc/import.jsp" %>
<!DOCTYPE html>
<html>

<head>
    <meta charset="UTF-8">
    <meta http-equiv="Cache-Control" content="no-cache" />
    <meta name="applicable-device" content="mobile">
    <meta name="viewport" content="width=device-width,initial-scale=1,user-scalable=0">

    <!--苹果专用-->
    <meta name="apple-mobile-web-app-capable" content="yes">
    <meta name="apple-mobile-web-app-title" content="乡丽乡亲">
    <title id="channeltitle">民宿风情</title>
    <link rel="stylesheet" type="text/css" href="/resources/css/weui.min.css" />
    <link rel="stylesheet" type="text/css" href="/resources/css/jquery-weui.css" />
    <link rel="stylesheet" type="text/css" href="/resources/css/app.css" />
</head>

<body ontouchstart>
<div class="content main">
    <div class="whitebg weui-grids m-t-3">

        <ul class="sp_list" id="roomlist">
            <li>
                <a href="ticket_order.html" title="">
                    <div class="pic_div"><img src="/resources/img/58f1e56f03ab4.jpg"></div>
                    <div class="tit_div_line">详细抓鱼
                        <span class="lookcount">
									<i class="lookicon"></i>
									<i class="fr">99</i>
								</span>
                    </div>

                </a>
            </li>
            <li>
                <a href="ticket_order.html" title="">
                    <div class="pic_div"><img src="/resources/img/58f1e56f03ab4.jpg"></div>
                    <div class="tit_div_line">详细抓鱼
                        <span class="lookcount">
									<i class="lookicon"></i>
									<i class="fr">99</i>
								</span>
                    </div>

                </a>
            </li>
            <li>
                <a href="ticket_order.html" title="">
                    <div class="pic_div"><img src="/resources/img/58f1e56f03ab4.jpg"></div>
                    <div class="tit_div_line">详细抓鱼
                        <span class="lookcount">
									<i class="lookicon"></i>
									<i class="fr">99</i>
								</span>
                    </div>

                </a>
            </li>
            <li>
                <a href="ticket_order.html" title="">
                    <div class="pic_div"><img src="/resources/img/58f1e56f03ab4.jpg"></div>
                    <div class="tit_div_line">详细抓鱼
                        <span class="lookcount">
									<i class="lookicon"></i>
									<i class="fr">99</i>
								</span>
                    </div>

                </a>
            </li>
        </ul>

    </div>
</div>

<!--底部部分-->
<%@include file="/inc/footdown.jsp" %>
<script type="text/javascript" src="/resources/js/jquery-2.1.4.js"></script>
<script type="text/javascript" src="/resources/js/jquery-weui.js"></script>
<script type="text/javascript" src="/resources/js/fastclick.js"></script>
<script type="text/javascript" src="/resources/js/app.js"></script>
</body>
<script >
    $(function () {
        getvideoRoom();
    });

    //获取房间列表
    function getvideoRoom() {
        $.get("${ctx}/video/roomlist",
            function(data) {
                var html = "";
                for(var i=0;i<data.data.result.length;i++){
                    html += '<li>';
                    html += '<a href="${cxt}/video/videoroomDetails?roomid='+data.data.result[i].id+'" title="">';
                    html += '<div class="pic_div"><img src="'+data.data.result[i].livecover+'"></div>>';
                    html += '<div class="tit_div_line">'+data.data.result[i].roomname;
                    html += '<span class="lookcount">';
                    html += '<i class="lookicon"></i>';
//                    html += '<i class="fr">99</i>'
                    html += '</span>';
                    html += ' </div>';
                    html += '</a>';
                    html += '</li>';

                }

                $("#roomlist").html(html);


            }
        );
    }

</script>

</html>
