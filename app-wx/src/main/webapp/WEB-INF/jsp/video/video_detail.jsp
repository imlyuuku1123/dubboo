<%--
  Created by IntelliJ IDEA.
  User: cannan
  Date: 2017/6/5
  Time: 15:42
  To change this template use File | Settings | File Templates.
--%>
<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8" isELIgnored="false"%>
<%@include file="/inc/import.jsp" %>
<!DOCTYPE html>
<html>

<head>
    <meta charset="UTF-8">
    <meta http-equiv="Cache-Control" content="no-cache" />
    <meta name="viewport" content="width=device-width,initial-scale=1,user-scalable=0">
    <meta name="format-detection" content="telphone=yes, email=yes" />
    <title id="roomtitle">乡溪抓鱼</title>
    <link rel="stylesheet" type="text/css" href="/resources/css/weui.min.css" />
    <link rel="stylesheet" type="text/css" href="/resources/css/jquery-weui.css" />
    <link rel="stylesheet" type="text/css" href="/resources/css/app.css" />
    <link rel="stylesheet" type="text/css" href="/resources/css/plyr.css" />
</head>

<body ontouchstart>
<!--主体部分 开始-->
<section>
    <div class="content">
        <!--播放器-->

        <div class="playbox">
            <video controls class="vs" poster="../img/swiper-1.png">
                <source src="../demo.MP4" type="video/mp4">
            </video>
        </div>

        <div class="sortbox border-bottom">
            <div class="weui-row row50">
                <div class="weui-col-50">
                    <a href="javascript:" id="bg1" class="firstmenu on" onclick="chgtt(1)">
                        <i>商品</i>
                    </a>
                </div>
                <div class="weui-col-50">
                    <a href="javascript:" id="bg2" class="firstmenu" onclick="chgtt(2)">
                        <i>主播</i>
                    </a>
                </div>
            </div>
        </div>
        <!--购买-->
        <div id="tab1">
            <div class="weui-panel__bd hidden homeproduct" id="productlist">
                <div class="weui-media-box weui-media-box_appmsg">
                    <div class="weui-media-box__hd">
                        <img class="weui-media-box__thumb" src="../img/58be5d19e2dc6_100_100.jpg" id="pb1">
                    </div>
                    <div class="weui-media-box__bd">
                        <div class="tit_div">我是土特产</div>
                        <div class="m-b-10">
                            <em class="shipping">已售44</em>
                        </div>
                        <div class="price_div">
										<span class="product-price1">¥<span class="big-price">98</span>
										<span class="small-price">.00</span>
										<em class="originalprice">￥180</em></span>
                        </div>

                        <div class="weui-cell wellbox">
                            <div class="weui-cell__bd weui-cell_primary">

                            </div>
                        </div>

                    </div>
                    <div class="resbox">

                        <button class="weui-btn weui-btn_primary reserBtn red-blue">购买</button>
                    </div>

                </div>

                <div class="weui-media-box weui-media-box_appmsg">
                    <div class="weui-media-box__hd">
                        <img class="weui-media-box__thumb" src="../img/58be5d19e2dc6_100_100.jpg" id="pb2">
                    </div>
                    <div class="weui-media-box__bd">
                        <div class="tit_div">我是民宿</div>
                        <div class="m-b-10">
                            <em class="shipping">已售44</em>
                        </div>
                        <div class="price_div">
										<span class="product-price1">¥<span class="big-price">98</span>
										<span class="small-price">.00</span>
										<em class="originalprice">￥180</em></span>
                        </div>

                        <div class="weui-cell wellbox">
                            <div class="weui-cell__bd weui-cell_primary">

                            </div>
                        </div>

                    </div>
                    <div class="resbox">

                        <button class="weui-btn reserBtn weui-btn_disabled addCard">售罄</button>
                    </div>
                </div>

            </div>
        </div>

        <div id="tab2" style="display: none;" class="m-t-15">
            <ul>
                <li>
                    <div class="comicon"><img id="anchorhead" src="../img/pic.png" width="58" height="58"></div>
                    <div class="cons">
                        <h1 id="anchorname">陈一发儿</h1>

                        <div class="introduce" id="anchorprofile">我是视频介绍我是视频介绍我是视频介绍我是视频介绍我是视频介绍 我是视频介绍我是视频介绍我是视频介绍 我是视频介绍我是视频介绍我是视频介绍 我是视频介绍我是视频介绍我是视频介绍我是视频介绍我是视频介绍 我是视频介绍我是视频介绍我是视频介绍我是视频介绍我是视频介绍 我是视频介绍我是视频介绍 我是视频介绍 我是视频介绍我是视频介绍我是视频介绍我是视频介绍我是视频介绍
                        </div>
                    </div>

                </li>

            </ul>
        </div>


    </div>

</section>

<!--底部固定导航 结束-->
<script type="text/javascript" src="/resources/js/jquery-2.1.4.js"></script>
<script type="text/javascript" src="/resources/js/jquery-weui.js"></script>
<script type="text/javascript" src="/resources/js/fastclick.js"></script>
<script type="text/javascript" src="/resources/js/app.js"></script>
<script src="/resources/js/plyr.js"></script>

<script>
    var largePrice = "";
    var smallPrice = "";
    $(function () {
        getroomdetailsinfo();
    });

    plyr.setup();

    function chgtt(d1) {
        var NowFrame; // 用于存储 用于显示在一组数据中的第几个

        if(Number(d1)) {
            NowFrame = d1; /*动态的获取我移入是哪一个元素*/
        } else {
            NowFrame = 1; /*如果没有默认为第一个*/
        }

        for(var i = 1; i <= 2; i++) { //循环3个盒子
            /*如果i和当前的NowFrame相等*/
            if(i == NowFrame) {
                document.getElementById("tab" + NowFrame).style.display = "block"; //通过NowFrame全局变量选出第几个显示盒子的内容显示
                document.getElementById("bg" + NowFrame).className = "on"; //为每个移入的样式添加样式
            } else {
                /*通过i和否来影藏其他的层*/
                document.getElementById("tab" + i).style.display = "none"; //隐藏其他层
                document.getElementById("bg" + i).className = "nobg";
            }
        }

    }

    window.onLoad = chgtt();

    
    //获取直播间详情信息
    function  getroomdetailsinfo() {
        $.get("${ctx}/video/getroomdetail",
            function(data) {
                $("#roomtitle").html(data.data.roomname);
                $("#anchorhead").attr("src", data.data.livecover);
                $("#anchorname").html(data.data.anchorname);
                $("#anchorprofile").html(data.data.anchorprofile);
                var html = "";
                for(var i=0;i<data.data.videoProductList.length;i++){

                    html += '<div class="weui-media-box weui-media-box_appmsg">';
                    html += '<div class="weui-media-box__hd">';
                    html += ' <img class="weui-media-box__thumb" src="'+data.data.videoProductList[i].tenantProduct.titleImgUrl+'" >';
                    html += '</div>';
                    html += ' <div class="weui-media-box__bd">';
                    html += ' <div class="tit_div">'+data.data.videoProductList[i].tenantProduct.productName+'</div>';
                    html += '<div class="m-b-10">';
                    html += ' <em class="shipping">已售'+data.data.videoProductList[i].tenantProduct.salesVolume+'</em>';
                    html += '</div>';
                    html += '<div class="price_div">';
                    returnFloat(data.data.videoProductList[i].tenantProduct.favorablePrice);
                    html += ' <span class="product-price1">¥<span class="big-price">'+largePrice+'</span>';
                    html += '<span class="small-price">'+smallPrice+'</span>';
                    html += '<em class="originalprice">￥'+data.data.videoProductList[i].tenantProduct.productUnit+'</em></span>';
                    html += '</div>';
                    html += '<div class="weui-cell wellbox">';
                    html += '<div class="weui-cell__bd weui-cell_primary">';
                    html += '</div>';
                    html += '</div>';
                    html += '</div>';
                    html += '<div class="resbox">';
                    html += '<button class="weui-btn weui-btn_primary reserBtn red-blue" onclick="tobuyweb(\''+data.data.videoProductList[i].tenantProduct.id+'\',\''+data.data.videoProductList[i].tenantProduct.productType+'\')">购买</button>';
                    html += '</div>';
                    html += '</div>';



                }


                $("#productlist").html(html);



            }
        );
    }


    function returnFloat(value){
        var value=Math.round(parseFloat(value)*100)/100;
        var xsd=value.toString().split(".");
        if(xsd.length==1){
            value=value.toString()+".00";
            result = value .split(".");
            largePrice = result[0];
            smallPrice = '.'+ result[1];

        }
        if(xsd.length>1){
            if(xsd[1].length<2){
                value=value.toString()+"0";
                result = value .split(".");
                largePrice = result[0];
                smallPrice = '.'+ result[1];

            }

        }

    }

    //根据producttype跳转到相应的产品下单页面
    function tobuyweb(productid , producttype ) {
        if (producttype == "土特产"){
            window.location.href = "${ctx}/specialty/buynow?specialtyid="+productid+"&buynum=1";
        }else if(producttype == "民宿"){
            window.location.href = "${ctx}/homestay/homestayok?roomid="+productid+"&buynum=1";
        }else if(producttype == "门票"){
            window.location.href = "${ctx}/ticket/buynow?ticketid="+productid+"&buynum=1"
        }
    }
</script>
</body>

</html>
