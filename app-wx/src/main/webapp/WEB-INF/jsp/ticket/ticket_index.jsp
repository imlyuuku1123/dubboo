<%--
  Created by IntelliJ IDEA.
  User: cannan
  Date: 2017/5/31
  Time: 15:33
  To change this template use File | Settings | File Templates.
--%>
<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8" isELIgnored="false"%>
<%@include file="/inc/import.jsp" %>
<!DOCTYPE html>
<html>

<head>
    <meta charset="UTF-8">
    <meta http-equiv="Cache-Control" content="no-cache" />
    <meta name="viewport" content="width=device-width,initial-scale=1,user-scalable=0">
    <meta name="format-detection" content="telphone=yes, email=yes" />
    <title>门票</title>
    <!--苹果专用-->
    <meta name="apple-mobile-web-app-capable" content="yes">
    <meta name="apple-mobile-web-app-title" content="乡丽乡亲">

    <link rel="stylesheet" type="text/css" href="/resources/css/weui.min.css" />
    <link rel="stylesheet" type="text/css" href="/resources/css/jquery-weui.css" />
    <link rel="stylesheet" type="text/css" href="/resources/css/app.css" />

    <link rel="stylesheet" type="text/css" href="/resources/css/JTCalendar.css" />
</head>

<body ontouchstart class="bg_fafafa">
<!--主体部分 开始-->
<div class="content">
    <div class="sortbox">
        <div class="weui-row">
            <div class="weui-col-50">
                <a href="javascript:" class="on firstmenu">
                    <i class="type scenic"></i>
                    <i class="classType">景点</i>
                    <i class="up"></i>
                </a>
                <input type="text" name="" hidden="hidden" value="" class="menuval" id="menuval"/>
                <ul class="menu" id="scenic">

                </ul>
            </div>
            <div class="weui-col-50">
                <a href="javascript:" class="firstmenu">
                    <i class="type priceicon"></i>
                    <i id="sort">排序</i>
                    <i class="up"></i>
                </a>
                <ul class="menu sortmenu">
                    <li id="pricedown">
                        价格从高到低
                    </li>
                    <li  id="priceup">
                        价格从低到高

                    </li>
                    <li id="inventorynumdown">
                        销量从高到低
                    </li>
                    <li id="inventorynumup">
                        销量从低到高
                    </li>
                </ul>
            </div>
        </div>
    </div>
    <div class="main">
        <div class="whitebg weui-grids m-t-3">

            <ul class="sp_list" id="ticketlist">
                <li>
                    <a href="ticket_order.html" title="">
                        <div class="pic_div"><img src="/resources/img/58f1e56f03ab4.jpg"></div>
                        <div class="tit_div_line">无敌火龙果无敌火龙果无敌火龙果无敌火龙果无敌火龙果无敌火龙果无敌火龙果无敌火龙果无敌火龙果无敌火龙果
                        </div>
                        <div class="price_div">
									<span class="product-price1">¥<span class="big-price">98</span>
									<span class="small-price">.00</span>
									<em class="originalprice">￥180</em></span>
                            <span class="product-xiaoliang">已售88</span>
                        </div>
                    </a>
                </li>
                <li>
                    <a href="product_detail.html" title="">
                        <div class="pic_div"><img src="/resources/img/58f1e56f03ab4.jpg"></div>
                        <div class="tit_div_line">无敌火龙果无敌火龙果无敌火龙果无敌火龙果无敌火龙果无敌火龙果无敌火龙果无敌火龙果无敌火龙果无敌火龙果
                        </div>
                        <div class="price_div">
									<span class="product-price1">¥<span class="big-price">98</span>
									<span class="small-price">.00</span>
									<em class="originalprice">￥180</em></span>
                            <span class="product-xiaoliang">已售88</span>
                        </div>
                    </a>
                </li>
            </ul>

        </div>
    </div>

    <!--点击下一页-->
    <div id="next" style="display:none;">
        <span style="cursor: pointer; ">共有记录855 ,共<span id="count">86</span>页&nbsp;</span>
        <span style="cursor: pointer; ">当前为第<em class="pageIndex">1</em>页&nbsp;</span>
    </div>
    <!--加载按钮-->
    <div class="weui-loadmore" id="infinite-1">
        <i class="weui-loading"></i>
        <span class="weui-loadmore__tips">正在加载</span>
    </div>

</div>
<!--主体部分 结束-->

<!--底部固定导航 开始-->
<div class="weui-tabbar">
    <a href="user_vidio.html" class="weui-tabbar__item ">
        <div class="weui-tabbar__icon"> <i class="sst-shouyeblue"></i> </div>
        <p class="weui-tabbar__label color_27332b">直播</p>
    </a>
    <a href="#" class="weui-tabbar__item ">
        <div class="weui-tabbar__icon"> <i class="sst-xuetangblack"></i> </div>
        <p class="weui-tabbar__label">预定</p>
    </a>
    <a href="user_my.html" class="weui-tabbar__item ">
        <div class="weui-tabbar__icon message"> <i class="sst-dashizaixian"></i> </div>

        <p class="weui-tabbar__label">我的</p>
    </a>

</div>
<script type="text/javascript" src="/resources/js/jquery-2.1.4.js"></script>
<script type="text/javascript" src="/resources/js/jquery-weui.js"></script>
<script type="text/javascript" src="/resources/js/fastclick.js"></script>
<script type="text/javascript" src="/resources/js/app.js"></script>
<!--	<script type="text/javascript" src="../js/zepto.min.js"></script>-->
<!--	<script type="text/javascript" src="../js/JTCalendar.js"></script>-->
<script type="text/javascript">

    var largePrice = "";
    var smallPrice = "";
    var pageNo = "1";
    var sortType = "";

    /*点击下一页*/
    var html = '<li>';
    html += '<a href="" title="">';
    html += '<div class="pic_div"><img src="../img/caomei_03.jpg"></div>';
    html += '<div class="tit_div_line">无敌火龙果无敌火龙果无敌火龙 果无敌火龙果无敌火龙果无	</div>';

    html += '<span class="product-price1">¥<span class="big-price">98</span>';
    html += '<span class="small-price">.00</span>';
    html += '<em class="originalprice">￥180</em></span>';
    html += '<span class="product-xiaoliang">已售88</span>';
    html += '</div>';
    html += "</a>";
    html += '</li>';

    /*点击下一页*/
    var pageIndex = parseInt($(".pageIndex").text());

    function GoToNextPage() {
        var loading = false; //状态标记
        $(document.body).infinite().on("infinite", function() {
            if(pageIndex + 1 <= parseInt($("#count").text())) {

                pageIndex += 1;
                $(".pageIndex").text(pageIndex);
                if(loading) return;
                loading = true;
                setTimeout(function() {
                    $("#goodslist").append(html);
                    loading = false;
                }, 100); //模拟延迟

            } else {
                $("#infinite-1").html("(^_^)没有更多咯！");
            }

        });

    }

    $(function() {
        GoToNextPage();

        getscenic();
        getticketlist("","","");

        //排序方式
        $("#pricedown").click(function () {
            var typep = $("#menuval").val();
            sortType = "1";
            reflashdiv();
            getticketlist(typep,1);
        });
        $("#priceup").click(function () {
            var typep = $("#menuval").val();
            sortType = "2";
            reflashdiv();
            getticketlist(typep,2);
        });
        $("#inventorynumdown").click(function () {
            var typep = $("#menuval").val();
            sortType = "3";
            reflashdiv();
            getticketlist(typep,3);
        });
        $("#inventorynumup").click(function () {
            var typep = $("#menuval").val();
            sortType = "4";
            reflashdiv();
            getticketlist(typep,4);
        });

        /*二级菜单加下划线*/
        $(".sortbox .firstmenu").click(function() {
            $(".sortbox .firstmenu").removeClass("on");
            $(this).addClass("on");
        });

        /*点一级菜单取值*/
        $(".menu li .a_1").click(function() {
            $(".menuval").val($(this).attr("id"));
            $(".classType").text($(this).html());
        });
        /*点二级导航栏的交互效果*/
        $(".menu ul li").on('click', function() {
            $(this).find("a").attr("id");
            $(".secondary").val($(this).find("a").attr("id"));
            $(".classType").text($(this).find("a").text());
        });
        $(".sortmenu li").click(function() {
            $("#sort").text($(this).text());
        });

    })


    //获取景区信息
    function getscenic() {

        $.get("${ctx}/product/getscenic",
            function(data) {
                var html = "";
                //拼装列表
                for(var i=0;i<data.data.length;i++){
                    html += "<li id='"+data.data[i].id+"' onclick='choosebyscenic("+data.data[i].id+",\""+data.data[i].scenicName+"\")'>"+data.data[i].scenicName;
                    html += "</li>"
                }
                $("#scenic").append(html);

            }
        );
    }

    //获取门票列表
    function getticketlist(pid,sorttype) {

        $.get("${ctx}/product/ticketlistbysearch",
            {
                pageno: pageNo,
                pagesize: "10",
                searchtypep: pid,
                sorttype :   sorttype
            },
            function(data) {

                for(var i=0;i<data.data.result.length;i++){
                    returnFloat(data.data.result[i].favorablePrice);

                    var html = "<li>";
                    html += " <a href='${cxt}/product/ticketdetail?id="+data.data.result[i].id+"' title=''>";
                    html += "<div class='pic_div'><img src='"+data.data.result[i].titleImgUrl+"'></div>";
                    html += "<div class='tit_div_line'><input value='"+data.data.result[i].productName+"' maxlength='6'></input>";
                    html += "</div>";
                    html += "<div class='price_div'>";
                    html += "<span class='product-price1'>¥<span class='big-price'>"+largePrice+"</span>";
                    html += "<span class='small-price'>"+smallPrice+"起</span>";
                    html += "</span>";
                    html += "<span class='product-xiaoliang'>已售"+data.data.result[i].salesVolume+"</span>";
                    html += " </div>";
                    html += " </a>";
                    html += " </li>";

                    if(data.data.result[i].productType == "门票"){
                        $("#ticketlist").append(html);
                    }

                }
            }
        );
    }

    //清空当前页面
    function  reflashdiv() {
        $("#ticketlist").empty();
        pageNo = "1";
    }

    //根据所选景区搜索
    function choosebyscenic(scenicid , scenicidname) {

        $(".classType").text(scenicidname);
        $(".menuval").val(scenicid);
        var typep = $("#menuval").val();
        reflashdiv();
        getticketlist(typep,sortType);
    }

    function returnFloat(value){
        var value=Math.round(parseFloat(value)*100)/100;
        var xsd=value.toString().split(".");
        if(xsd.length==1){
            value=value.toString()+".00";
            result = value .split(".");
            largePrice = result[0];
            smallPrice = '.'+ result[1];

        }
        if(xsd.length>1){
            if(xsd[1].length<2){
                value=value.toString()+"0";
                result = value .split(".");
                largePrice = result[0];
                smallPrice = '.'+ result[1];

            }

        }

    }

</script>
</body>

</html>
