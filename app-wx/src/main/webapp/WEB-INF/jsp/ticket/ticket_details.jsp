<%--
  Created by IntelliJ IDEA.
  User: cannan
  Date: 2017/5/31
  Time: 15:51
  To change this template use File | Settings | File Templates.
--%>
<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8" isELIgnored="false"%>
<%@include file="/inc/import.jsp" %>
<!DOCTYPE html>
<html>

<head>
    <meta charset="UTF-8">
    <meta http-equiv="Cache-Control" content="no-cache" />
    <meta name="applicable-device" content="mobile">
    <meta name="viewport" content="width=device-width,initial-scale=1,user-scalable=0">

    <!--苹果专用-->
    <meta name="apple-mobile-web-app-capable" content="yes">
    <meta name="apple-mobile-web-app-title" content="乡丽乡亲">
    <title>门票详情</title>
    <link rel="stylesheet" type="text/css" href="/resources/css/weui.min.css" />
    <link rel="stylesheet" type="text/css" href="/resources/css/jquery-weui.css" />
    <link rel="stylesheet" type="text/css" href="/resources/css/app.css" />
</head>

<body ontouchstart>
<div class="content">
    <div class="main">
        <div class="swiper-container" data-space-between='10' data-pagination='.swiper-pagination' data-autoplay="1000">
            <div class="swiper-wrapper">
                <div class="swiper-slide"><img src="/resources/img/swiper-1.jpg" alt=""></div>
                <div class="swiper-slide"><img src="/resources/img/swiper-2.jpg" alt=""></div>
                <div class="swiper-slide"><img src="/resources/img/swiper-3.jpg" alt=""></div>
                <div class="swiper-slide"><img src="/resources/img/swiper-3.jpg" alt=""></div>
            </div>
            <!-- If we need pagination -->
            <div class="swiper-pagination" id="on"></div>
        </div>
        <!--banner 结束-->
        <div class="product_info" id="ticketinfo">

        </div>
        <div class="hr"></div>
        <div class="sortbox">
            <div class="weui-row row50">
                <div class="weui-col-50">
                    <a href="javascript:" class="on firstmenu scroll_a">
                        <i class="type detailicon"></i>
                        <i>详情</i>
                    </a>

                </div>
                <div class="weui-col-50">
                    <a href="javascript:" class="firstmenu scroll_b">
                        <i class="type pinjia"></i>
                        <i>评价</i>
                    </a>

                </div>
            </div>
        </div>

        <div class="titone"></div>
        <div id="tab1" class="weui-tab__bd_item">
            <!--<div class="content-box">
                <p><br></p>
                <p><img src="../img/58f1e56e9502a.jpg" style=""></p>
                <p><img src="../img/58f1e56f03ab4.jpg" style=""></p>
                <p><img src="../img/58f1e5703f079.jpg" style=""></p>
                <p><img src="../img/58f1e56f2d0ef.jpg" style=""></p>
                <p><img src="../img/58f1cbd829aaf.jpg" style=""></p>
                <p><img src="../img/58f1cbd83c803.jpg" style=""></p>
                <p><img src="../img/58f1e73990350.jpg" style=""></p>
                <p><br></p>
            </div>-->
            <div class="content-box">
                <div class="articalcon">
                    <h1>购买说明</h1>
                    <p id="purchaseDes">由各种物质组成的巨型球状天体，叫做星球。星球有一定的形状，有自己的运行轨道由各种物质组成的巨型球状天体，叫做星球。星球有一定的形状，有自己的运行轨道</p>
                </div>
                <div class="articalcon">
                    <h1>重要条款</h1>
                    <p id="importantCla">由各种物质组成的巨型球状天体，叫做星球。星球有一定的形状，有自己的运行轨道由各种物质组成的巨型球状天体，叫做星球。星球有一定的形状，有自己的运行轨道</p>
                </div>
                <div class="articalcon">
                    <h1>改退规则</h1>
                    <p id="changeRule">由各种物质组成的巨型球状天体，叫做星球。星球有一定的形状，有自己的运行轨道由各种物质组成的巨型球状天体，叫做星球。星球有一定的形状，有自己的运行轨道</p>
                </div>
            </div>


        </div>
        <div class="tittwo"></div>
        <!--评论页面开始-->
        <div class="tab2 whitebg">
            <ul class="comments commlist">
                <li>
                    <div class="comicon"><img src="/resources/img/caomei_03.jpg" /></div>
                    <div class="cons">
                        <h1>Deadeasy</h1>
                        <span class="time">10分钟前</span>
                        <div class="introduce
">水果收到了，特别新鲜，下次还来买！
                        </div>
                    </div>

                </li>
                <li>
                    <div class="comicon"><img src="/resources/img/caomei_03.jpg" /></div>
                    <div class="cons">
                        <h1>Deadeasy</h1>
                        <span class="time">10分钟前</span>
                        <div class="introduce">
                            <p>水果收到了，特别新鲜，下次还来买！</p>

                        </div>
                    </div>

                </li>
                <li>
                    <div class="comicon"><img src="/resources/img/caomei_03.jpg" /></div>
                    <div class="cons">
                        <h1>Deadeasy</h1>
                        <span class="time">10分钟前</span>
                        <div class="introduce">
                            <ul class="graphic">
                                <li>
                                    <div class="pic">
                                        <img src="/resources/img/shouji.png" alt="土特产">
                                    </div>
                                </li>
                                <li>
                                    <div class="pic">
                                        <img src="/resources/img/shouji.png" alt="土特产">
                                    </div>
                                </li>
                                <li>
                                    <div class="pic">
                                        <img src="/resources/img/s2.png" alt="土特产">
                                    </div>
                                </li>
                                <li>
                                    <div class="pic">
                                        <img src="/resources/img/shouji.png" alt="土特产">
                                    </div>
                                </li>
                            </ul>

                            <p>水果收到了，特别新鲜，下次还来买！</p>
                        </div>
                    </div>

                </li>
                <div class="btnbox">
                    <a href="product_comments.html" class="weui-btn weui-btn_plain-primary">查看全部488条评论</a>
                </div>
            </ul>

        </div>

    </div>
</div>

<!--主体部分 结束-->
<!--底部固定导航 开始-->
<div id="cart1" class="cart-concern-btm-fixed five-column four-column" style="display: table;">
    <div class="concern-cart">
        <a href="tel:13545350035" class="dong-dong-icn J_ping" id="imBottom" href="#"> <em class="btm-act-icn"></em> <span class="color_27332b">联系客服 </span> </a>
    </div>

    <div class="action-list ticketbtn">
        <a  class="red-blue buytime" >立即购买</a>
    </div>
</div>

<script type="text/javascript" src="/resources/js/jquery-2.1.4.js"></script>
<script type="text/javascript" src="/resources/js/jquery-weui.js"></script>
<script type='text/javascript' src='/resources/js/swiper.min.js' charset='utf-8'></script>
<script type="text/javascript" src="/resources/js/fastclick.js"></script>
<script type="text/javascript" src="/resources/js/app.js"></script>
<script type="text/javascript">
    /*图片插件轮播初始化*/

    var largePrice = "";
    var smallPrice = "";
    var pageNo = "1";
    var productId = "";
    var inventoryNum ="";
    $(function() {
        swiper();
        getticketdetials();

        /*页面平滑滚动到指定的位置*/
        $(".scroll_a").click(function() {
            $('html,body').animate({
                scrollTop: $("#tab1").offset().top
            }, 800);
        });
        $(".scroll_b").click(function() {
            $('html,body').animate({
                scrollTop: $(".tab2").offset().top
            }, 800);
        });

        $(".sortbox .firstmenu").click(function() {
            $(".sortbox .firstmenu").removeClass("on");
            $(this).addClass("on");
        });
        /*$('.number-selector-plus').click(function() {
         upDownOperation($(this));
         });*/



        $('.number-selector-sub').click(function() {
            upDownOperation($(this));
        });

        $(".product_info").on('click', '.number-selector-plus', function() {
            upDownOperation($(this));
        });


        /*点击立即购买判断购买数量*/

        $(".buytime").click(function(){
            var inventory = parseInt($(".inventory").text());
            var count_shop = parseInt($("#qty_num").val()) ;
            if (inventory < count_shop) {
                $.toast('库存不足', "cancel");
                return false;
            }
            var buynum = $("#qty_num").val();
            window.location.href = "${ctx}/ticket/buynow?ticketid="+productId+"&buynum="+buynum;
        });

    });



    //获取门票详情
    function getticketdetials() {
        $.get("${ctx}/product/ticketdetails",
            function(data) {
                returnFloat(data.data.favorablePrice);
                var youhui = data.data.productUnit - data.data.favorablePrice;
                html = " <div class='tit_div'>"+data.data.productName+"</div>";
                html += "<div class='price_div'>";
                html += " <span class='product-price1'>¥<span class='big-price'>"+largePrice+"</span>";
                html += "<span class='small-price'>"+smallPrice+"</span>";
                html += " <em class='originalprice'>￥"+data.data.productUnit+"</em></span>";
                html += "<span class='product-xiaoliang'>已售"+data.data.salesVolume+"</span>";
                html += "</div>";
                html += "  <div class='weui-cell wellbox'>";
                html += "<div class='weui-cell__bd weui-cell_primary'>";
                html += "<div class='shopping_count' id='detailone'>";
                html += " <span>购买数量<em class='shipping'>(库存"+data.data.inventoryNum+"件)</em></span>";
                html += "<span class='fr'><em class='youhui'>(已优惠"+youhui+"元)</em></span>"
                html += "</div>";
                html += "</div>";
                html += " <div style='font-size: 0px;' class='weui-cell__ft'>";
                html += "<span class='number-selector number-selector-sub needsclick'>-</span>";
                html += " <input id='qty_num' pattern='[0-9]*' class='number-input' style='width: 50px;' value='1' data-min='1' data-step='1'>";
                html += "<span class='number-selector number-selector-plus needsclick'>+</span>";
                html += " </div>" ;
                html += "</div>" ;
                html += "<p class='title_flower'><i></i>"+data.data.tenantMerchant.merchantName+"</p>";

                $("#ticketinfo").append(html);

//                html1='';
//                html1 = '<div class="content-box">';
//                html1 += data.data.productDetails;
//                html1 += ' </div>';
//                $("#tab1").append(html1);

                productId = data.data.id;
                inventoryNum = data.data.inventoryNum;
                $("#purchaseDes").text(data.data.purchaseDes);
                $("#importantCla").text(data.data.importantCla);
                $("#changeRule").text(data.data.changeRule);


            }
        );
    }

    function returnFloat(value){
        var value=Math.round(parseFloat(value)*100)/100;
        var xsd=value.toString().split(".");
        if(xsd.length==1){
            value=value.toString()+".00";
            result = value .split(".");
            largePrice = result[0];
            smallPrice = '.'+ result[1];

        }
        if(xsd.length>1){
            if(xsd[1].length<2){
                value=value.toString()+"0";
                result = value .split(".");
                largePrice = result[0];
                smallPrice = '.'+ result[1];

            }

        }

    }
</script>
</body>

</html>
