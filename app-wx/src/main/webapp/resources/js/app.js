/*获取今天明天*/
FastClick.attach(document.body);

function GetNowDate() {
	var myDate, myDateStr;
	myDate = new Date(); //创建日期对象	
	var MM = myDate.getMonth() + 1; //取月份
	if(MM < 10) //如果月份小于10就在前面加0
		MM = "0" + MM;
	var dd = myDate.getDate(); //取日  
	if(dd < 10)
		dd = "0" + dd;

	myDateStr = MM + "月" + dd + "日";
	$("#startTime").text(myDateStr);
	return myDateStr;
}

function GetTomorrow() {
	var myDate, myDateStr;
	myDate = new Date();
	var yyyy = myDate.getFullYear(); //取四位年份  
	var MM = myDate.getMonth() + 1;
	if(MM < 10)
		MM = "0" + MM;
	var dd = myDate.getDate();
	if(dd < 10)
		dd = "0" + dd;
	myDateStr = yyyy + "-" + MM + "-" + dd;
	var arr = myDateStr.split("-");
	var newdt = new Date(Number(arr[0]), Number(arr[1]) - 1, Number(arr[2]) + 1);
	repnewdt = (newdt.getMonth() + 1) + "月" + newdt.getDate() + "日";
	$('#endTime').text(repnewdt);
}

/*日期初始化*/

/*表单非空验证*/
function trim(str) {
	return str.replace(/(^\s*)|(\s*$)/g, "");
}

/*绑定手机号码以及个人资粮非空验证*/
function validation() {
	//var name = $("input[name='name']").val();
	var phone = $("input[name='phone']").val();
    var code = $("input[name='code']").val();
	
	if(trim(phone) == '') {
		$.toast('请填写手机号', "cancel");
		return false;
	}
	if(!(/^1[3|4|5|7|8]\d{9}$/.test(phone))) {
		$.toast('请填写合法的手机号', "cancel");
		return false;
	}
	if(trim(code) == '') {
		$.toast('请输入验证码', "cancel");
		return false;
	}
  
}

/*时间插件*/
function JTCalendarData() {
	$('#calendar').JTCalendar({
		title: '选择入住离店日期',
		inTime: new Date(),
		minDay: 1,
		monthNumber: 3,
		onClick: function($elm, cb) {
			console.log($elm, cb);

			$elm.find('[data-intime] .choose_date').text(cb.inTime.month + '月' + cb.inTime.date + '日')
				.next('i.f').text(cb.inTime.dayText);

			$elm.find('[data-outtime] .choose_date').text(cb.outTime.month + '月' + cb.outTime.date + '日')
				.next('i.f').text(cb.outTime.dayText);

			$elm.find('i.tottime').text(cb.totTime);

		}
	})

}

/*轮播图初始化*/
function swiper() {
	$(".swiper-container").swiper({
		loop: true,
		autoplay: 3000
	});

}

/*评论方法初始化*/

function commd() {
	var graphic = $.photoBrowser({
		items: [
			"../img/swiper-1.jpg" /*tpa=http://jqweui.com/dist/demos/images/swiper-1.jpg*/ ,
			"../img/swiper-2.jpg" /*tpa=http://jqweui.com/dist/demos/images/swiper-2.jpg*/ ,
			"../img/swiper-3.jpg" /*tpa=http://jqweui.com/dist/demos/images/swiper-3.jpg*/ ,
			"../img/swiper-3.jpg" /*tpa=http://jqweui.com/dist/demos/images/swiper-3.jpg*/
		],

		onSlideChange: function(index) {
			console.log(this, index);
		},

		onOpen: function() {
			console.log("onOpen", this);

		},
		onClose: function() {
			console.log("onClose", this);
		}
	});

	graphic.open();
}

/*订单页面加入购物车选择商品数量*/
function upDownOperation(element) {
	var _input = element.parent().find('input'),
		_value = _input.val(),
		_step = _input.attr('data-step') || 1;
	//检测当前操作的元素是否有disabled，有则去除
	element.hasClass('disabled') && element.removeClass('disabled');
	//检测当前操作的元素是否是操作的添加按钮（.input-num-up）'是' 则为加操作，'否' 则为减操作
	if(element.hasClass('number-selector-plus')) {
		var _new_value = parseInt(parseFloat(_value) + parseFloat(_step)),
			_max = _input.attr('data-max') || false,
			_down = element.parent().find('.number-selector-sub');

		//若执行'加'操作且'减'按钮存在class='disabled'的话，则移除'减'操作按钮的class 'disabled'
		_down.hasClass('disabled') && _down.removeClass('disabled');
		if(_max && _new_value >= _max) {
			_new_value = _max;
			element.addClass('disabled');
		}
	} else {
		var _new_value = parseInt(parseFloat(_value) - parseFloat(_step)),
			_min = _input.attr('data-min') || false,
			_up = element.parent().find('.number-selector-plus');
		//若执行'减'操作且'加'按钮存在class='disabled'的话，则移除'加'操作按钮的class 'disabled'
		_up.hasClass('disabled') && _up.removeClass('disabled');
		if(_min && _new_value <= _min) {
			_new_value = _min;
			element.addClass('disabled');
		}
	}
	_input.val(_new_value);
};

/*订单页面以及详情页面 中 点击数量 算出合计,运费,*/
function upDownOperation2(element, money, yujia, yufei) {
	var _input = element.parent().find('input'),
		_value = _input.val(),
		_step = _input.attr('data-step') || 1;

	//检测当前操作的元素是否有disabled，有则去除
	element.hasClass('disabled') && element.removeClass('disabled');
	//检测当前操作的元素是否是操作的添加按钮（.input-num-up）'是' 则为加操作，'否' 则为减操作
	if(element.hasClass('number-selector-plus')) {
		var _new_value = parseInt(parseFloat(_value) + parseFloat(_step)),
			_max = _input.attr('data-max') || false,
			_down = element.parent().find('.number-selector-sub');

		//若执行'加'操作且'减'按钮存在class='disabled'的话，则移除'减'操作按钮的class 'disabled'
		_down.hasClass('disabled') && _down.removeClass('disabled');
		if(_max && _new_value >= _max) {
			_new_value = _max;
			element.addClass('disabled');
		}
	} else {
		var _new_value = parseInt(parseFloat(_value) - parseFloat(_step)),
			_min = _input.attr('data-min') || false,
			_up = element.parent().find('.number-selector-plus');
		//若执行'减'操作且'加'按钮存在class='disabled'的话，则移除'加'操作按钮的class 'disabled'
		_up.hasClass('disabled') && _up.removeClass('disabled');
		if(_min && _new_value <= _min) {
			_new_value = _min;
			element.addClass('disabled');
		}
	}
	_input.val(_new_value);
	_youhui = parseFloat(parseFloat(yujia) - parseFloat(money)) * _new_value;
	_xiaoji = parseFloat(money) * _new_value + parseFloat(yufei);
	$(".youh").text(_youhui);

	$(".pieces").text(_new_value);
	$(".totalpro").text(_xiaoji);
	/*$(".yufei").text(yufei);
	$('.freight').text(yufei);*/
	$('.combined_total em ').text(_xiaoji);
};

/* 锚点平滑滚动*/
// 说明 ：用 Javascript 实现锚点(Anchor)间平滑跳转
// 转换为数字
function intval(v) {
	v = parseInt(v);
	return isNaN(v) ? 0 : v;
}

// 获取元素信息
function getPos(e) {
	var l = 0;
	var t = 0;
	var w = intval(e.style.width);
	var h = intval(e.style.height);
	var wb = e.offsetWidth;
	var hb = e.offsetHeight;
	while(e.offsetParent) {
		l += e.offsetLeft + (e.currentStyle ? intval(e.currentStyle.borderLeftWidth) : 0);
		t += e.offsetTop + (e.currentStyle ? intval(e.currentStyle.borderTopWidth) : 0);
		e = e.offsetParent;
	}
	l += e.offsetLeft + (e.currentStyle ? intval(e.currentStyle.borderLeftWidth) : 0);
	t += e.offsetTop + (e.currentStyle ? intval(e.currentStyle.borderTopWidth) : 0);
	return {
		x: l,
		y: t,
		w: w,
		h: h,
		wb: wb,
		hb: hb
	};
}

// 获取滚动条信息
function getScroll() {
	var t, l, w, h;

	if(document.documentElement && document.documentElement.scrollTop) {
		t = document.documentElement.scrollTop;
		l = document.documentElement.scrollLeft;
		w = document.documentElement.scrollWidth;
		h = document.documentElement.scrollHeight;
	} else if(document.body) {
		t = document.body.scrollTop;
		l = document.body.scrollLeft;
		w = document.body.scrollWidth;
		h = document.body.scrollHeight;
	}
	return {
		t: t,
		l: l,
		w: w,
		h: h
	};
}

// 锚点(Anchor)间平滑跳转
function scroller(el, duration) {
	if(typeof el != 'object') {
		el = document.getElementById(el);
	}

	if(!el) return;

	var z = this;
	z.el = el;
	z.p = getPos(el);
	z.s = getScroll();
	z.clear = function() {
		window.clearInterval(z.timer);
		z.timer = null
	};
	z.t = (new Date).getTime();

	z.step = function() {
		var t = (new Date).getTime();
		var p = (t - z.t) / duration;
		if(t >= duration + z.t) {
			z.clear();
			window.setTimeout(function() {
				z.scroll(z.p.y, z.p.x)
			}, 13);
		} else {
			st = ((-Math.cos(p * Math.PI) / 2) + 0.5) * (z.p.y - z.s.t) + z.s.t;
			sl = ((-Math.cos(p * Math.PI) / 2) + 0.5) * (z.p.x - z.s.l) + z.s.l;
			z.scroll(st, sl);
		}
	};
	z.scroll = function(t, l) {
		window.scrollTo(l, t)
	};
	z.timer = window.setInterval(function() {
		z.step();
	}, 13);
}