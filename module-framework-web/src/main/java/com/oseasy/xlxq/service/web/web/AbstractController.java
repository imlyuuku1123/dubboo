package com.oseasy.xlxq.service.web.web;

import javax.servlet.http.HttpServletRequest;

import com.oseasy.xlxq.service.core.security.SecurityUser;

public abstract class AbstractController {

	public SecurityUser getCurrentUser(HttpServletRequest request){
		SecurityUser user = (SecurityUser)request.getSession().getAttribute("currentUser");
		return user;
	}
}
