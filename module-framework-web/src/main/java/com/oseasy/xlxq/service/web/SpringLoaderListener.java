package com.oseasy.xlxq.service.web;

import javax.servlet.ServletContext;
import javax.servlet.ServletContextEvent;

import com.oseasy.xlxq.service.core.web.SpringContextUtil;
import org.springframework.context.ApplicationContext;
import org.springframework.web.context.ContextLoaderListener;
import org.springframework.web.context.support.WebApplicationContextUtils;

public class SpringLoaderListener extends ContextLoaderListener
{
  public void contextInitialized(ServletContextEvent event)
  {
    super.contextInitialized(event);
    ServletContext context = event.getServletContext();
    ApplicationContext ctx = WebApplicationContextUtils.getWebApplicationContext(context);
    SpringContextUtil.setApplicationContext(ctx);
  }
}  