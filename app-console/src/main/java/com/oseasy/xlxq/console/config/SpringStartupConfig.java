package com.oseasy.xlxq.console.config;

import com.oseasy.xlxq.service.ServiceConfig;
import com.oseasy.xlxq.service.api.ServiceApiConfig;
import com.oseasy.xlxq.service.cache.FrameworkCacheConfig;
import com.oseasy.xlxq.service.monitor.FrameworkMonitorConfig;
import com.oseasy.xlxq.service.mq.FrameworkMQConfig;
import com.oseasy.xlxq.service.oss.FrameworkOSSConfig;
import com.oseasy.xlxq.console.security.SpringSecurityConfig;
import org.springframework.boot.autoconfigure.EnableAutoConfiguration;
import org.springframework.boot.autoconfigure.data.redis.RedisAutoConfiguration;
import org.springframework.context.annotation.*;
import org.springframework.data.jpa.repository.config.EnableJpaRepositories;
import org.springframework.web.servlet.config.annotation.EnableWebMvc;

/**
 * Created by Tandy on 2016/6/7.
 */
@ComponentScan({"com.oseasy.xlxq.console"})
@EnableWebMvc
@Configuration
@PropertySource("classpath:/config.properties")
@Import({SpringSecurityConfig.class, RedisAutoConfiguration.class,
        FrameworkCacheConfig.class, FrameworkOSSConfig.class, FrameworkMonitorConfig.class,
        ServiceConfig.class, ServiceApiConfig.class,
        FrameworkMQConfig.class,
        FrameworkOSSConfig.class
})
@EnableJpaRepositories(value = {"com.oseasy"})
@EnableAutoConfiguration
public class SpringStartupConfig {
    @Bean
    public String getSystemId(){
        return "app.console";
    }
}
