package com.oseasy.xlxq.console.base;

import com.oseasy.xlxq.service.api.tenant.service.TenantAccountService;
import com.oseasy.xlxq.service.core.security.SecurityUser;
import com.oseasy.xlxq.service.core.utils.ExportExcel;
import com.oseasy.xlxq.service.core.utils.JSONUtil;
import com.oseasy.xlxq.service.web.rest.RestResponse;
import org.apache.commons.lang.StringUtils;
import org.apache.poi.hssf.usermodel.HSSFWorkbook;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.servlet.ModelAndView;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.OutputStream;
import java.util.Collection;
import java.util.Date;

/**
 * Created by liups on 2016/6/28.
 */
public abstract class AbstractPortalController {

    @Autowired
    private TenantAccountService tenantAccountService;

    public static final Logger logger = LoggerFactory.getLogger(AbstractPortalController.class);
    /**
     * 对Controller层统一的异常处理
     * @param request
     * @param ex
     * @return
     */
    @ExceptionHandler(Exception.class)
    public ModelAndView exp(HttpServletRequest request,HttpServletResponse response,Exception ex) {
        logger.error("异常",ex);
        ModelAndView mav;
        //Ajax请求带有X-Requested-With:XMLHttpRequest
        String xRequestedWith = request.getHeader("X-Requested-With");
        if (StringUtils.isNotBlank(xRequestedWith) && "XMLHttpRequest".equals(xRequestedWith)) {
            String result = JSONUtil.objectToJson(RestResponse.failed("0000",ex.getMessage()));
            try {
                response.getWriter().write(result);
            }catch (Exception e){}
            return null;
        }else{
            mav = new ModelAndView("error_page");
        }
        return mav;
    }

    /**
     * 获取当前用户（SecurityUser）
     * @param request
     * @return
     */
    public SecurityUser getCurrentUser(HttpServletRequest request){
        return (SecurityUser) request.getSession().getAttribute("currentUser");
    }


    /**
     * 导出文件
     * @param response
     */
    public <T>  void downloadExcel(String title, String one, String[] headers, String[] values, Collection<T> dataset, String pattern, String money, HttpServletResponse response) {
        try {
            Date d = new Date();
            HSSFWorkbook wb = ExportExcel.exportExcel(title,one,headers,values,dataset,pattern,money);
            //response.setHeader("Content-disposition", "attachment;filename=导出数据.xls");
            //response.setContentType("application/vnd.ms-excel");

            response.setContentType("application/vnd.ms-excel");
            String fileName = "导出数据";
            response.setCharacterEncoding("utf-8");
            response.setHeader("content-disposition", "attachment;filename=" + new String(fileName.getBytes(), "ISO8859-1") + ".xls");

            OutputStream ouputStream = response.getOutputStream();
            wb.write(ouputStream);
            ouputStream.flush();
            ouputStream.close();
        } catch (Exception ex) {
            ex.printStackTrace();
        }
    }


}
