package com.oseasy.xlxq.console.tenant;

import com.alibaba.fastjson.JSONObject;
import com.oseasy.xlxq.console.base.AbstractPortalController;
import com.oseasy.xlxq.service.api.exceptions.ErrorConstant;
import com.oseasy.xlxq.service.api.tenant.model.Tenant;
import com.oseasy.xlxq.service.api.tenant.model.TenantAccount;
import com.oseasy.xlxq.service.api.tenant.service.TenantAccountService;
import com.oseasy.xlxq.service.core.security.SecurityUser;
import com.oseasy.xlxq.service.core.utils.HqlUtil;
import com.oseasy.xlxq.service.core.utils.Page;
import com.oseasy.xlxq.service.core.utils.PasswordUtil;
import com.oseasy.xlxq.service.core.utils.StringUtil;
import com.oseasy.xlxq.service.web.rest.RestResponse;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpServletRequest;
import java.util.List;

/**
 * Created by heckman on 2017/6/1.
 */
@RestController
@RequestMapping("/console/tenantaccount")
public class TenantAccountController extends AbstractPortalController {

    public static final Logger logger = LoggerFactory.getLogger(TenantAccountController.class);

    @Autowired
    private TenantAccountService tenantAccountService;

    /**
     * 运营账号列表查询接口
     *
     * @param pageNo
     * @param pageSize
     * @param userName
     * @param displayName
     * @param status
     * @return
     */
    @RequestMapping("/list")
    public RestResponse<Page<TenantAccount>> list(@RequestParam(defaultValue = "1") Integer pageNo,
                                                  @RequestParam(defaultValue = "20") Integer pageSize,
                                                  String userName, String displayName,
                                                  String status) {
        RestResponse<Page<TenantAccount>> restResponse = new RestResponse<Page<TenantAccount>>();
        try {
            String hql = "from TenantAccount obj ";
            if (StringUtil.isNotBlank(userName)) {
                hql = HqlUtil.addCondition(hql, "userName", userName, HqlUtil.LOGIC_AND, HqlUtil.TYPE_STRING_LIKE, null);
            }
            if (StringUtil.isNotBlank(displayName)) {
                hql = HqlUtil.addCondition(hql, "displayName", displayName, HqlUtil.LOGIC_AND, HqlUtil.TYPE_STRING_LIKE, null);
            }
            if (StringUtil.isNotBlank(status)) {
                hql = HqlUtil.addCondition(hql, "status", status);
            }
            hql=HqlUtil.addOrder(hql,"createTime","desc");

            hql = HqlUtil.addOrder(hql, "createTime", "desc");
            Page<TenantAccount> page = tenantAccountService.pageList(hql, pageNo, pageSize);
            restResponse.setSuccess(true);
            restResponse.setData(page);

        } catch (Exception ex) {
            logger.error("系统异常：", ex);
            restResponse.setSuccess(false);
            restResponse.setErrorCode(ErrorConstant.INNER_ERROR_CODE);
            restResponse.setErrorMsg(ErrorConstant.getErrMsg(ErrorConstant.INNER_ERROR_CODE));
        }
        return restResponse;
    }


    /**
     * 账户启用接口（支持批量操作）
     *
     * @param ids
     * @return
     */
    @RequestMapping("/enable")
    public RestResponse enable(@RequestParam String ids) {
        RestResponse restResponse = new RestResponse();
        try {
            if (ids.contains(",")) {
                String[] idsArray = ids.split(",");
                tenantAccountService.updateByArray("status", TenantAccount.STATUS_NORMAL, "id", idsArray);
            } else {
                String[] idsArray = new String[]{ids};
                tenantAccountService.updateByArray("status", TenantAccount.STATUS_NORMAL, "id", idsArray);
            }
            restResponse.setSuccess(true);
        } catch (Exception ex) {
            logger.error("系统异常：", ex);
            restResponse.setSuccess(false);
            restResponse.setErrorCode(ErrorConstant.INNER_ERROR_CODE);
            restResponse.setErrorMsg(ErrorConstant.getErrMsg(ErrorConstant.INNER_ERROR_CODE));
        }
        return restResponse;
    }

    /**
     * 账户禁用接口（支持批量操作）
     *
     * @param ids
     * @return
     */
    @RequestMapping("/disable")
    public RestResponse disable(@RequestParam String ids) {
        RestResponse restResponse = new RestResponse();
        try {
            if (ids.contains(",")) {
                String[] idsArray = ids.split(",");
                tenantAccountService.updateByArray("status", TenantAccount.STATUS_LOCK, "id", idsArray);
            } else {
                String[] idsArray = new String[]{ids};
                tenantAccountService.updateByArray("status", TenantAccount.STATUS_LOCK, "id", idsArray);
            }
            restResponse.setSuccess(true);
        } catch (Exception ex) {
            logger.error("系统异常：", ex);
            restResponse.setSuccess(false);
            restResponse.setErrorCode(ErrorConstant.INNER_ERROR_CODE);
            restResponse.setErrorMsg(ErrorConstant.getErrMsg(ErrorConstant.INNER_ERROR_CODE));
        }
        return restResponse;
    }

    /**
     * 运营账号新增、修改接口
     *
     * @param tenantAccount
     * @return
     */
    @RequestMapping("/save")
    public RestResponse save(HttpServletRequest request, @RequestBody TenantAccount tenantAccount) {
        RestResponse restResponse = new RestResponse();
        try {
            if (StringUtil.isNotBlank(tenantAccount.getId())) {
                //修改操作
                TenantAccount tenantAccount1 = tenantAccountService.findById(tenantAccount.getId());
                tenantAccount.setTenant(tenantAccount1.getTenant());
                tenantAccount.setStatus(tenantAccount1.getStatus());
                tenantAccount.setPassword(tenantAccount1.getPassword());
                tenantAccount.setCreateTime(tenantAccount1.getCreateTime());

                tenantAccountService.save(tenantAccount);
            } else {
                //新增操作
                //从登录信息中拿到租户id
                SecurityUser securityUser = getCurrentUser(request);
                Tenant tenant = new Tenant();
                tenant.setId(securityUser.getTenantId());
                tenantAccount.setTenant(tenant);
                tenantAccount.setStatus(TenantAccount.STATUS_NORMAL);
                tenantAccount.setPassword(PasswordUtil.springSecurityPasswordEncode(tenantAccount.getPassword(), tenantAccount.getUserName()));
                tenantAccountService.save(tenantAccount);
            }
            restResponse.setSuccess(true);
        } catch (Exception ex) {
            logger.error("系统异常：", ex);
            restResponse.setSuccess(false);
            restResponse.setErrorCode(ErrorConstant.INNER_ERROR_CODE);
            restResponse.setErrorMsg(ErrorConstant.getErrMsg(ErrorConstant.INNER_ERROR_CODE));
        }
        return restResponse;
    }

    /**
     * 运营账号删除接口
     *
     * @param id
     * @return
     */
    @RequestMapping("/delete")
    public RestResponse delete(@RequestParam String id) {
        RestResponse restResponse = new RestResponse();
        try {
            TenantAccount tenantAccount = tenantAccountService.findById(id);
            tenantAccountService.delete(tenantAccount);
            restResponse.setSuccess(true);
        } catch (Exception ex) {
            logger.error("系统异常：", ex);
            restResponse.setSuccess(false);
            restResponse.setErrorCode(ErrorConstant.INNER_ERROR_CODE);
            restResponse.setErrorMsg(ErrorConstant.getErrMsg(ErrorConstant.INNER_ERROR_CODE));
        }
        return restResponse;
    }

    /**
     * 运营账号重置密码接口
     *
     * @param id
     * @return
     */
    @RequestMapping("/reset_password")
    public RestResponse<JSONObject> resetPassword(@RequestParam String id) {
        RestResponse<JSONObject> restResponse = new RestResponse<JSONObject>();
        JSONObject data = new JSONObject();
        try {
            TenantAccount tenantAccount = tenantAccountService.findById(id);
            tenantAccount.setPassword(PasswordUtil.springSecurityPasswordEncode(TenantAccount.DEFAULT_PASSWORD, tenantAccount.getUserName()));
            tenantAccountService.update(id, tenantAccount);
            data.put("result", TenantAccount.DEFAULT_PASSWORD);
            restResponse.setSuccess(true);
            restResponse.setData(data);
        } catch (Exception ex) {
            logger.error("系统异常：", ex);
            restResponse.setSuccess(false);
            restResponse.setErrorCode(ErrorConstant.INNER_ERROR_CODE);
            restResponse.setErrorMsg(ErrorConstant.getErrMsg(ErrorConstant.INNER_ERROR_CODE));
        }
        return restResponse;
    }

    @RequestMapping("/has_accountame")
    public RestResponse<JSONObject> hasAccountName(@RequestParam String accountName) {
        RestResponse<JSONObject> restResponse = new RestResponse<JSONObject>();
        JSONObject data = new JSONObject();
        try {
            String hql = "from TenantAccount obj ";
            hql = HqlUtil.addCondition(hql, "userName", accountName);
            List<TenantAccount> list = tenantAccountService.getList(hql);
            if (list.size() > 0) {
                data.put("result", true);
            } else {
                data.put("result", false);
            }
            restResponse.setData(data);
        } catch (Exception ex) {
            logger.error("系统异常：", ex);
            restResponse.setSuccess(false);
            restResponse.setErrorCode(ErrorConstant.INNER_ERROR_CODE);
            restResponse.setErrorMsg(ErrorConstant.getErrMsg(ErrorConstant.INNER_ERROR_CODE));
        }
        return restResponse;
    }

    @RequestMapping("/find/{id}")
    public RestResponse<TenantAccount> find(@PathVariable String id) {
        RestResponse<TenantAccount> restResponse = new RestResponse<TenantAccount>();
        try {
            TenantAccount tenantAccount = tenantAccountService.findById(id);
            restResponse.setData(tenantAccount);
            restResponse.setSuccess(true);
        } catch (Exception ex) {
            logger.error("系统异常：", ex);
            restResponse.setSuccess(false);
            restResponse.setErrorCode(ErrorConstant.INNER_ERROR_CODE);
            restResponse.setErrorMsg(ErrorConstant.getErrMsg(ErrorConstant.INNER_ERROR_CODE));
        }
        return restResponse;
    }

}
