package com.oseasy.xlxq.console.statistics;

import com.alibaba.fastjson.JSONObject;
import com.oseasy.xlxq.console.base.AbstractPortalController;
import com.oseasy.xlxq.service.api.exceptions.ErrorConstant;
import com.oseasy.xlxq.service.api.statistic.model.StatisticSellReport;
import com.oseasy.xlxq.service.api.statistic.model.StatisticTicketDay;
import com.oseasy.xlxq.service.api.statistic.model.StatisticTicketDayExcel;
import com.oseasy.xlxq.service.api.statistic.model.StatisticTicketSellExcel;
import com.oseasy.xlxq.service.api.statistic.service.StatisticTicketDayService;
import com.oseasy.xlxq.service.core.utils.HqlUtil;
import com.oseasy.xlxq.service.core.utils.Page;
import com.oseasy.xlxq.service.core.utils.StringUtil;
import com.oseasy.xlxq.service.web.rest.RestResponse;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import javax.servlet.http.HttpServletResponse;
import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by heckman on 2017/6/12.
 */

@RestController
@RequestMapping("/console/ticket_statistic")
public class TicketDailyStisController extends AbstractPortalController {

    public static final Logger logger = LoggerFactory.getLogger(TicketDailyStisController.class);

    @Autowired
    private StatisticTicketDayService service;

    /**
     * 门票日销量列表接口
     *
     * @param merchantId
     * @param scenicId
     * @param ticketId
     * @param startTime
     * @param endTime
     * @param pageNo
     * @param pageSize
     * @return
     */
    @RequestMapping("/daily_statistic")
    public RestResponse dailyStatistic(String merchantId, String scenicId, String ticketId,
                                       String startTime, String endTime,
                                       @RequestParam(defaultValue = "1") Integer pageNo,
                                       @RequestParam(defaultValue = "20") Integer pageSize) {
        RestResponse restResponse = new RestResponse();
        try {
            String hql = "from StatisticTicketDay obj ";
            if (StringUtil.isNotBlank(merchantId)) {
                hql = HqlUtil.addCondition(hql, "tenantMerchant.id", merchantId);
            }
            if (StringUtil.isNotBlank(startTime)) {
                hql = HqlUtil.addCondition(hql, "stasticsDay", startTime, HqlUtil.LOGIC_AND, HqlUtil.TYPE_STRING, HqlUtil.COMPARECHAR_GREAT_EQ);
            }
            if (StringUtil.isNotBlank(endTime)) {
                hql = HqlUtil.addCondition(hql, "stasticsDay", endTime, HqlUtil.LOGIC_AND, HqlUtil.TYPE_STRING, HqlUtil.COMPARECHAR_LESS_EQ);
            }
            if (StringUtil.isNotBlank(scenicId)) {
                hql = HqlUtil.addCondition(hql, "tenantScenic.id", scenicId);
            }
            if (StringUtil.isNotBlank(ticketId)) {
                hql = HqlUtil.addCondition(hql, "tenantScenicTicket.id", ticketId);
            }
            hql = HqlUtil.addOrder(hql, "stasticsDay", "");

            Page<StatisticTicketDay> page = service.pageList(hql, pageNo, pageSize);
            restResponse.setData(page);
            restResponse.setSuccess(true);
        } catch (Exception ex) {
            logger.error("系统异常：", ex);
            restResponse.setSuccess(false);
            restResponse.setErrorCode(ErrorConstant.INNER_ERROR_CODE);
            restResponse.setErrorMsg(ErrorConstant.getErrMsg(ErrorConstant.INNER_ERROR_CODE));
        }
        return restResponse;
    }


    /**
     * 门票日销量导出
     *
     * @param merchantId
     * @param scenicId
     * @param ticketId
     * @param startTime
     * @param endTime
     */
    @RequestMapping("/excel")
    public void export(HttpServletResponse response, String merchantId, String scenicId, String ticketId,
                       String startTime, String endTime) {
        try {
            String hql = "from StatisticTicketDay obj ";
            if (StringUtil.isNotBlank(merchantId)) {
                hql = HqlUtil.addCondition(hql, "tenantMerchant.id", merchantId);
            }
            if (StringUtil.isNotBlank(startTime)) {
                hql = HqlUtil.addCondition(hql, "stasticsDay", startTime, HqlUtil.LOGIC_AND, HqlUtil.TYPE_STRING, HqlUtil.COMPARECHAR_GREAT_EQ);
            }
            if (StringUtil.isNotBlank(endTime)) {
                hql = HqlUtil.addCondition(hql, "stasticsDay", endTime, HqlUtil.LOGIC_AND, HqlUtil.TYPE_STRING, HqlUtil.COMPARECHAR_LESS_EQ);
            }
            if (StringUtil.isNotBlank(scenicId)) {
                hql = HqlUtil.addCondition(hql, "tenantScenic.id", scenicId);
            }
            if (StringUtil.isNotBlank(ticketId)) {
                hql = HqlUtil.addCondition(hql, "tenantScenicTicket.id", ticketId);
            }
            hql = HqlUtil.addOrder(hql, "stasticsDay", "");

            List<StatisticTicketDay> list = service.getList(hql);
            System.out.println(JSONObject.toJSONString(list));
            List<StatisticTicketDayExcel> list2 = new ArrayList<StatisticTicketDayExcel>();
            StatisticTicketDayExcel st=null;
            for(StatisticTicketDay std:list){
                st=new StatisticTicketDayExcel();
                st.setMerchantName(std.getTenantMerchant()==null?"":std.getTenantMerchant().getMerchantName());
                st.setScenicName(std.getTenantScenic()==null?"":std.getTenantScenic().getScenicName());
                st.setSellNum(std.getSaleNum() + "");
                st.setSellSum(std.getSaleMoney().setScale(2, BigDecimal.ROUND_HALF_UP) + "");
                st.setTicketName(std.getTenantScenicTicket()==null?"":std.getTenantScenicTicket().getProductName());
                list2.add(st);
            }

            String title = "门票日销量统计";
            String[] headers = new String[]{"景区", "商户", "门票", "销售数量", "销售金额"};
            String[] values = new String[]{"scenicName", "merchantName", "ticketName", "sellNum", "sellSum"};
            downloadExcel(title, null, headers, values, list2, null, null, response);
        } catch (Exception ex) {
            logger.error("系统异常：", ex);
        }
    }

    @RequestMapping("/report")
    public RestResponse report(String merchantId, String scenicId, String ticketId,
                               String startTime, String endTime,
                               @RequestParam(defaultValue = "1") Integer pageNo,
                               @RequestParam(defaultValue = "20") Integer pageSize){

        RestResponse restResponse=new RestResponse();
        try{
            StringBuffer sqlWhere = new StringBuffer("");

            if(StringUtil.isNotBlank(merchantId)){
                sqlWhere.append(" and ttt.merchant_id ='"+merchantId+"'");
            }
            if(StringUtil.isNotBlank(scenicId)){
                sqlWhere.append(" and ttt.scenicspots_id='"+scenicId+"'");
            }
            if(StringUtil.isNotBlank(ticketId)){
                sqlWhere.append(" and ttp.id='"+ticketId+"'");
            }
            if(StringUtil.isNotBlank(startTime) && StringUtil.isBlank(endTime)){
                sqlWhere.append(" and STR_TO_DATE(ttt.dateStr,'%Y-%m-%d') >= '"+startTime+"' ");
            }else if(StringUtil.isNotBlank(endTime) && StringUtil.isBlank(startTime)){
                sqlWhere.append(" and STR_TO_DATE(ttt.dateStr,'%Y-%m-%d') <= '"+endTime+"' ");
            }else if(StringUtil.isNotBlank(startTime) && StringUtil.isNotBlank(endTime)){
                sqlWhere.append(" and STR_TO_DATE(ttt.dateStr,'%Y-%m-%d') >= '"+startTime+"' and STR_TO_DATE(ttt.dateStr,'%Y-%m-%d') <= '"+endTime+"' ");
            }

            String sql = "SELECT  " +
                    "  bs.dateStr,  " +
                    "  bs.merchantName,  " +
                    "  bs.productName,  " +
                    "  bs.scenicName,  " +
                    "  bs.sellNum,  " +
                    "  bs.sellSum  " +
                    "FROM  " +
                    "  (  " +
                    "    SELECT  " +
                    "      ttt.dateStr,  " +
                    "      ttt.merchant_id,  " +
                    "      ttt.scenicspots_id,  " +
                    "      ttt.ticket_id,  " +
                    "      ttp.product_name AS productName,  " +
                    "      ttm.merchant_name AS merchantName,  " +
                    "      tts.scenic_name AS scenicName,  " +
                    "      sum(ttt.sale_num) AS sellNum,  " +
                    "      sum(ttt.sale_money) AS sellSum  " +
                    "    FROM  " +
                    "      (  " +
                    "        SELECT  " +
                    "          CONCAT(  " +
                    "            obj.`year`,  " +
                    "            '-',  " +
                    "            obj.`month`,  " +
                    "            '-',  " +
                    "            obj.`day`  " +
                    "          ) AS dateStr,  " +
                    "          CASE  " +
                    "        WHEN obj.sale_num IS NULL THEN  " +
                    "          0  " +
                    "        ELSE  " +
                    "          obj.sale_num  " +
                    "        END AS sale_num,  " +
                    "        CASE  " +
                    "      WHEN obj.sale_money IS NULL THEN  " +
                    "        0  " +
                    "      ELSE  " +
                    "        obj.sale_money  " +
                    "      END AS sale_money,  " +
                    "      obj.ticket_id,  " +
                    "      obj.merchant_id,  " +
                    "      obj.scenicspots_id  " +
                    "    FROM  " +
                    "      tb_statistic_ticket_day obj  " +
                    "      ) ttt  " +
                    "    LEFT OUTER JOIN tb_tenant_product ttp ON ttt.ticket_id = ttp.id  " +
                    "    LEFT OUTER JOIN tb_tenant_merchant ttm ON ttt.merchant_id = ttm.id  " +
                    "    LEFT OUTER JOIN tb_tenant_scenic tts ON ttt.scenicspots_id = tts.id where 1=1 " + sqlWhere.toString() +
                    "    GROUP BY  " +
                    "      ttt.dateStr,  " +
                    "      ttt.merchant_id  " +
                    "  ) bs  " +
                    "WHERE  " +
                    "  1 = 1";

            String total_sql = "select count(1) from ( "+ sql +" ) as tt";
            String list_sql = "select * from ( "+ sql +" ) as tt limit "+(pageNo-1)*pageSize+","+pageSize;
            Long total = (service.executNativeTotalQuery(total_sql)).longValue();
            List list = service.executNativeQuery(list_sql, StatisticSellReport.class);

            Page page=new Page((pageNo-1)*pageSize,total,pageSize,null);
            page.setResult(list);
            System.out.println(JSONObject.toJSONString(page));
            restResponse.setData(page);
            restResponse.setSuccess(true);

        }catch (Exception ex){
            logger.error("系统异常：", ex);
            restResponse.setSuccess(false);
            restResponse.setErrorCode(ErrorConstant.INNER_ERROR_CODE);
            restResponse.setErrorMsg(ErrorConstant.getErrMsg(ErrorConstant.INNER_ERROR_CODE));
        }
        return  restResponse;
    }

    @RequestMapping("/report_excel")
    public void  reportExcel(HttpServletResponse response,String merchantId, String scenicId, String ticketId,
                             String startTime, String endTime){
        try {
            StringBuffer sqlWhere = new StringBuffer("");

            if(StringUtil.isNotBlank(merchantId)){
                sqlWhere.append(" and ttt.merchant_id ='"+merchantId+"'");
            }
            if(StringUtil.isNotBlank(scenicId)){
                sqlWhere.append(" and ttt.scenicspots_id='"+scenicId+"'");
            }
            if(StringUtil.isNotBlank(ticketId)){
                sqlWhere.append(" and ttp.id='"+ticketId+"'");
            }
            if(StringUtil.isNotBlank(startTime) && StringUtil.isBlank(endTime)){
                sqlWhere.append(" and STR_TO_DATE(ttt.dateStr,'%Y-%m-%d') >= '"+startTime+"' ");
            }else if(StringUtil.isNotBlank(endTime) && StringUtil.isBlank(startTime)){
                sqlWhere.append(" and STR_TO_DATE(ttt.dateStr,'%Y-%m-%d') <= '"+endTime+"' ");
            }else if(StringUtil.isNotBlank(startTime) && StringUtil.isNotBlank(endTime)){
                sqlWhere.append(" and STR_TO_DATE(ttt.dateStr,'%Y-%m-%d') >= '"+startTime+"' and STR_TO_DATE(ttt.dateStr,'%Y-%m-%d') <= '"+endTime+"' ");
            }

            String sql = "SELECT  " +
                    "  bs.dateStr,  " +
                    "  bs.merchantName,  " +
                    "  bs.productName,  " +
                    "  bs.scenicName,  " +
                    "  bs.sellNum,  " +
                    "  bs.sellSum  " +
                    "FROM  " +
                    "  (  " +
                    "    SELECT  " +
                    "      ttt.dateStr,  " +
                    "      ttt.merchant_id,  " +
                    "      ttt.scenicspots_id,  " +
                    "      ttt.ticket_id,  " +
                    "      ttp.product_name AS productName,  " +
                    "      ttm.merchant_name AS merchantName,  " +
                    "      tts.scenic_name AS scenicName,  " +
                    "      sum(ttt.sale_num) AS sellNum,  " +
                    "      sum(ttt.sale_money) AS sellSum  " +
                    "    FROM  " +
                    "      (  " +
                    "        SELECT  " +
                    "          CONCAT(  " +
                    "            obj.`year`,  " +
                    "            '-',  " +
                    "            obj.`month`,  " +
                    "            '-',  " +
                    "            obj.`day`  " +
                    "          ) AS dateStr,  " +
                    "          CASE  " +
                    "        WHEN obj.sale_num IS NULL THEN  " +
                    "          0  " +
                    "        ELSE  " +
                    "          obj.sale_num  " +
                    "        END AS sale_num,  " +
                    "        CASE  " +
                    "      WHEN obj.sale_money IS NULL THEN  " +
                    "        0  " +
                    "      ELSE  " +
                    "        obj.sale_money  " +
                    "      END AS sale_money,  " +
                    "      obj.ticket_id,  " +
                    "      obj.merchant_id,  " +
                    "      obj.scenicspots_id  " +
                    "    FROM  " +
                    "      tb_statistic_ticket_day obj  " +
                    "      ) ttt  " +
                    "    LEFT OUTER JOIN tb_tenant_product ttp ON ttt.ticket_id = ttp.id  " +
                    "    LEFT OUTER JOIN tb_tenant_merchant ttm ON ttt.merchant_id = ttm.id  " +
                    "    LEFT OUTER JOIN tb_tenant_scenic tts ON ttt.scenicspots_id = tts.id where 1=1 " + sqlWhere.toString() +
                    "    GROUP BY  " +
                    "      ttt.dateStr,  " +
                    "      ttt.merchant_id  " +
                    "  ) bs  " +
                    "WHERE  " +
                    "  1 = 1";


            String list_sql = "select * from ( "+ sql +" ) as tt ";
            List list = service.executNativeQuery(list_sql, StatisticSellReport.class);

            List<StatisticTicketSellExcel> list2=new ArrayList<StatisticTicketSellExcel>();

            StatisticSellReport ssr=null;
            StatisticTicketSellExcel sse=null;
            for(int i=0;i<list.size();i++){
                ssr=(StatisticSellReport)list.get(i);
                sse=new StatisticTicketSellExcel();
                sse.setDateStr(ssr.getDateStr());
                sse.setMerchantName(ssr.getMerchantName());
                sse.setScenicName(ssr.getScenicName());
                sse.setSellNum(ssr.getSellNum()+"");
                sse.setSellSum(ssr.getSellSum().setScale(2,BigDecimal.ROUND_HALF_UP)+"");
                sse.setTicketName(ssr.getProductName());
                list2.add(sse);
            }
            String title = "门票销售明细表";
            String[] headers = new String[]{"时间", "景区","商户", "门票", "销售数量", "销售金额"};
            String[] values = new String[]{"dateStr", "scenicName", "merchantName","ticketName", "sellNum", "sellSum"};
            downloadExcel(title, null, headers, values, list2, null, null, response);

        }catch (Exception ex){
            logger.error("系统异常：", ex);
        }
    }

}
