package com.oseasy.xlxq.console.tenant;

import com.alibaba.fastjson.JSONObject;
import com.oseasy.xlxq.console.base.AbstractPortalController;
import com.oseasy.xlxq.service.api.exceptions.ErrorConstant;
import com.oseasy.xlxq.service.api.tenant.model.MerchantSellerExcel;
import com.oseasy.xlxq.service.api.tenant.model.TenantAccount;
import com.oseasy.xlxq.service.api.tenant.model.TenantMerchantSeller;
import com.oseasy.xlxq.service.api.tenant.service.TenantMerchantSellerService;
import com.oseasy.xlxq.service.core.utils.HqlUtil;
import com.oseasy.xlxq.service.core.utils.Page;
import com.oseasy.xlxq.service.core.utils.PasswordUtil;
import com.oseasy.xlxq.service.core.utils.StringUtil;
import com.oseasy.xlxq.service.web.rest.RestResponse;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpServletResponse;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by heckman on 2017/6/6.
 */

@RestController
@RequestMapping("/console/merchantseller")
public class TenantMerchantSellerController extends AbstractPortalController {

    public static final Logger logger = LoggerFactory.getLogger(TenantMerchantSellerController.class);

    @Autowired
    private TenantMerchantSellerService tenantMerchantSellerService;

    /**
     * 查询列表数据接口
     *
     * @param pageNo
     * @param pageSize
     * @param username
     * @param displayName
     * @param isUse
     * @return
     */
    @RequestMapping("/list")
    public RestResponse<Page<TenantMerchantSeller>> list(@RequestParam(defaultValue = "1") Integer pageNo,
                                                         @RequestParam(defaultValue = "20") Integer pageSize,
                                                         String username, String displayName, String isUse) {
        RestResponse<Page<TenantMerchantSeller>> restResponse = new RestResponse<Page<TenantMerchantSeller>>();
        try {
            String hql = "from TenantMerchantSeller obj ";
            if (StringUtil.isNotBlank(username)) {
                hql = HqlUtil.addCondition(hql, "username", username, HqlUtil.LOGIC_AND, HqlUtil.TYPE_STRING_LIKE, null);
            }
            if (StringUtil.isNotBlank(displayName)) {
                hql = HqlUtil.addCondition(hql, "displayName", displayName, HqlUtil.LOGIC_AND, HqlUtil.TYPE_STRING_LIKE, null);
            }
            if (StringUtil.isNotBlank(isUse)) {
                hql = HqlUtil.addCondition(hql, "isUse", isUse);
            }
            hql = HqlUtil.addOrder(hql, "createTime", "");
            Page<TenantMerchantSeller> page = tenantMerchantSellerService.pageList(hql, pageNo, pageSize);
            restResponse.setData(page);
            restResponse.setSuccess(true);
        } catch (Exception ex) {
            logger.error("系统异常：", ex);
            restResponse.setSuccess(false);
            restResponse.setErrorCode(ErrorConstant.INNER_ERROR_CODE);
            restResponse.setErrorMsg(ErrorConstant.getErrMsg(ErrorConstant.INNER_ERROR_CODE));
        }
        return restResponse;
    }

    /**
     * 保存账户
     *
     * @param tenantMerchantSeller
     * @return
     */
    @RequestMapping("/save")
    public RestResponse save(@RequestBody TenantMerchantSeller tenantMerchantSeller) {
        RestResponse restResponse = new RestResponse();
        try {
            if (StringUtil.isNotBlank(tenantMerchantSeller.getId())) {
                //修改
                TenantMerchantSeller tenantMerchantSeller1 = tenantMerchantSellerService.findById(tenantMerchantSeller.getId());

            } else {
                //新增

                tenantMerchantSeller.setPassword(PasswordUtil.springSecurityPasswordEncode(tenantMerchantSeller.getPassword(), tenantMerchantSeller.getUsername()));
                tenantMerchantSeller.setIsUse(TenantMerchantSeller.ENABLE);
                tenantMerchantSellerService.save(tenantMerchantSeller);
            }
            restResponse.setSuccess(true);
        } catch (Exception ex) {
            logger.error("系统异常：", ex);
            restResponse.setSuccess(false);
            restResponse.setErrorCode(ErrorConstant.INNER_ERROR_CODE);
            restResponse.setErrorMsg(ErrorConstant.getErrMsg(ErrorConstant.INNER_ERROR_CODE));
        }
        return restResponse;
    }

    /**
     * 根据id查找单个账户
     *
     * @param id
     * @return
     */
    @RequestMapping("/find/{id}")
    public RestResponse<TenantMerchantSeller> find(@PathVariable String id) {
        RestResponse<TenantMerchantSeller> restResponse = new RestResponse<TenantMerchantSeller>();
        try {
            TenantMerchantSeller tenantMerchantSeller = tenantMerchantSellerService.findById(id);
            restResponse.setSuccess(true);
            restResponse.setData(tenantMerchantSeller);
        } catch (Exception ex) {
            logger.error("系统异常：", ex);
            restResponse.setSuccess(false);
            restResponse.setErrorCode(ErrorConstant.INNER_ERROR_CODE);
            restResponse.setErrorMsg(ErrorConstant.getErrMsg(ErrorConstant.INNER_ERROR_CODE));
        }
        return restResponse;
    }

    /**
     * 删除账户
     *
     * @param id
     * @return
     */
    @RequestMapping("/delete/{id}")
    public RestResponse delete(@PathVariable String id) {
        RestResponse restResponse = new RestResponse();
        try {
            tenantMerchantSellerService.delete(id);
            restResponse.setSuccess(true);
        } catch (Exception ex) {
            logger.error("系统异常：", ex);
            restResponse.setSuccess(false);
            restResponse.setErrorCode(ErrorConstant.INNER_ERROR_CODE);
            restResponse.setErrorMsg(ErrorConstant.getErrMsg(ErrorConstant.INNER_ERROR_CODE));
        }
        return restResponse;
    }

    /**
     * 启用用户
     *
     * @param id
     * @return
     */
    @RequestMapping("/enable")
    public RestResponse enable(@RequestParam String id) {
        RestResponse restResponse = new RestResponse();
        try {
            TenantMerchantSeller tenantMerchantSeller = tenantMerchantSellerService.findById(id);
            tenantMerchantSeller.setIsUse(TenantMerchantSeller.ENABLE);
            tenantMerchantSellerService.save(tenantMerchantSeller);
            restResponse.setSuccess(true);
        } catch (Exception ex) {
            logger.error("系统异常：", ex);
            restResponse.setSuccess(false);
            restResponse.setErrorCode(ErrorConstant.INNER_ERROR_CODE);
            restResponse.setErrorMsg(ErrorConstant.getErrMsg(ErrorConstant.INNER_ERROR_CODE));
        }
        return restResponse;
    }

    /**
     * 禁用用户
     *
     * @param id
     * @return
     */
    @RequestMapping("/disable")
    public RestResponse disable(@RequestParam String id) {
        RestResponse restResponse = new RestResponse();
        try {
            TenantMerchantSeller tenantMerchantSeller = tenantMerchantSellerService.findById(id);
            tenantMerchantSeller.setIsUse(TenantMerchantSeller.DISABLE);
            tenantMerchantSellerService.save(tenantMerchantSeller);
            restResponse.setSuccess(true);
        } catch (Exception ex) {
            logger.error("系统异常：", ex);
            restResponse.setSuccess(false);
            restResponse.setErrorCode(ErrorConstant.INNER_ERROR_CODE);
            restResponse.setErrorMsg(ErrorConstant.getErrMsg(ErrorConstant.INNER_ERROR_CODE));
        }
        return restResponse;
    }

    /**
     * 重置密码
     *
     * @param id
     * @return
     */
    @RequestMapping("/reset_password")
    public RestResponse<JSONObject> resetPassword(@RequestParam String id) {
        RestResponse<JSONObject> restResponse = new RestResponse<JSONObject>();
        JSONObject data = new JSONObject();
        try {
            TenantMerchantSeller tenantMerchantSeller = tenantMerchantSellerService.findById(id);
            tenantMerchantSeller.setPassword(PasswordUtil.springSecurityPasswordEncode(TenantAccount.DEFAULT_PASSWORD, tenantMerchantSeller.getUsername()));
            tenantMerchantSellerService.save(tenantMerchantSeller);
            data.put("result", TenantAccount.DEFAULT_PASSWORD);
            restResponse.setSuccess(true);
            restResponse.setData(data);
        } catch (Exception ex) {
            logger.error("系统异常：", ex);
            restResponse.setSuccess(false);
            restResponse.setErrorCode(ErrorConstant.INNER_ERROR_CODE);
            restResponse.setErrorMsg(ErrorConstant.getErrMsg(ErrorConstant.INNER_ERROR_CODE));
        }
        return restResponse;
    }

    /**
     * 查询商户账号表中是否存在该用户名
     *
     * @param username
     * @return true=>存在；false=>不存在
     */
    @RequestMapping("/has_username")
    public RestResponse<JSONObject> hasUsername(@RequestParam String username) {
        RestResponse<JSONObject> restResponse = new RestResponse<JSONObject>();
        JSONObject data = new JSONObject();
        try {
            String hql = "from TenantMerchantSeller obj ";
            hql = HqlUtil.addCondition(hql, "username", username);
            List<TenantMerchantSeller> list = tenantMerchantSellerService.getList(hql);
            if (list.size() > 0) {
                data.put("result", true);
            } else {
                data.put("result", false);
            }
            restResponse.setData(data);
            restResponse.setSuccess(true);
        } catch (Exception ex) {
            logger.error("系统异常：", ex);
            restResponse.setSuccess(false);
            restResponse.setErrorCode(ErrorConstant.INNER_ERROR_CODE);
            restResponse.setErrorMsg(ErrorConstant.getErrMsg(ErrorConstant.INNER_ERROR_CODE));
        }
        return restResponse;
    }

    /**
     * 导出商户账号excel接口
     *
     * @param response
     * @param username
     * @param displayName
     * @param isUse
     */
    @RequestMapping("/excel")
    public void excel(HttpServletResponse response,
                      String username, String displayName, String isUse) {
        try {
            String hql = "from TenantMerchantSeller obj ";
            if (StringUtil.isNotBlank(username)) {
                hql = HqlUtil.addCondition(hql, "username", username, HqlUtil.LOGIC_AND, HqlUtil.TYPE_STRING_LIKE, null);
            }
            if (StringUtil.isNotBlank(displayName)) {
                hql = HqlUtil.addCondition(hql, "displayName", displayName, HqlUtil.LOGIC_AND, HqlUtil.TYPE_STRING_LIKE, null);
            }
            if (StringUtil.isNotBlank(isUse)) {
                hql = HqlUtil.addCondition(hql, "isUse", isUse);
            }
            hql = HqlUtil.addOrder(hql, "createTime", "");
            List<TenantMerchantSeller> list = tenantMerchantSellerService.getList(hql);
            List<MerchantSellerExcel> list2 = new ArrayList<MerchantSellerExcel>();
            MerchantSellerExcel data;
            if (list.size() > 0) {
                for (TenantMerchantSeller ts : list) {
                    data = new MerchantSellerExcel();
                    data.setUsername(ts.getUsername());
                    data.setDisplayName(ts.getDisplayName());
                    data.setMerchantName(ts.getTenantMerchant() == null ? "" : ts.getTenantMerchant().getMerchantName());
                    data.setCreateTime(ts.getCreateTime());
                    data.setIsUse(ts.getIsUse() == 0 ? "禁用" : "启用");
                    list2.add(data);
                }
            }
            String title = "商户账号";
            //lastTime
            String[] headers = new String[]{"用户名", "姓名", "商户", "创建时间", "状态"};
            String[] values = new String[]{"username", "displayName", "merchantName", "createTime", "isUse"};
            downloadExcel(title, null, headers, values, list2, null, null, response);

        } catch (Exception ex) {
            logger.error("系统异常：", ex);
        }
    }

}
