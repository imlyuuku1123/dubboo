package com.oseasy.xlxq.console.order;


import com.oseasy.xlxq.console.base.AbstractPortalController;
import com.oseasy.xlxq.service.api.exceptions.ErrorConstant;
import com.oseasy.xlxq.service.api.order.model.*;
import com.oseasy.xlxq.service.api.order.service.OrderInfoService;
import com.oseasy.xlxq.service.api.order.service.OrderLogisticsService;
import com.oseasy.xlxq.service.api.order.service.OrderNoGenService;
import com.oseasy.xlxq.service.api.order.service.OrderRefundService;
import com.oseasy.xlxq.service.api.sys.model.TenantUserAddrs;
import com.oseasy.xlxq.service.api.sys.service.TenantUserAddrsService;
import com.oseasy.xlxq.service.core.utils.HqlUtil;
import com.oseasy.xlxq.service.core.utils.Page;
import com.oseasy.xlxq.service.core.utils.StringUtil;
import com.oseasy.xlxq.service.web.rest.RestResponse;
import org.apache.commons.lang.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpServletResponse;
import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

/**
 * Created by heckman on 2017/5/23.
 */

@RestController
@RequestMapping("/console/order_refund")
public class OrderRefundCotroller extends AbstractPortalController{

    private static final Logger logger = LoggerFactory.getLogger(OrderRefundCotroller.class);

    @Autowired
    private OrderLogisticsService orderLogisticsService;

    @Autowired
    private OrderInfoService orderInfoService;

    @Autowired
    private TenantUserAddrsService tenantUserAddrsService;

    @Autowired
    private OrderRefundService orderRefundService;

    @Autowired
    private OrderNoGenService orderNoGenService;

    /**
     * 订单退款接口
     *
     * @param orderRefund
     * @return
     */
    /*@RequestMapping(value = "/refund", method = RequestMethod.POST)
    public RestResponse refund(@RequestBody OrderRefund orderRefund) {
        RestResponse restResponse = new RestResponse();
        try {
            //根据订单号查询订单信息
            String order_hql = "from OrderInfo obj ";
            order_hql = HqlUtil.addCondition(order_hql, "id", orderRefund.getOrderInfo().getId());
            OrderInfo orderInfo = orderInfoService.findUnique(order_hql);

            //只能在待发货状态时才能退款
            if (OrderStatus.PENDING_SHIP_CODE.equals(orderInfo.getOrderStatus())
                    || OrderStatus.TRADE_SUCCESS_CODE.equals(orderInfo.getOrderStatus())) {

                //根据订单号查询该订单的物流信息
                String hql = "from OrderLogistics obj ";
                hql = HqlUtil.addCondition(hql, "orderInfo.id", orderInfo.getId());
                OrderLogistics orderLogistics = orderLogisticsService.findUnique(hql);

                //筛选出购买人信息
                TenantUser user = orderInfo.getTenantUser();
                //TenantUser user = orderInfo.getTenantUser();

                //根据用户id查询用户默认的详细收货地址
                String tenant_user_addrs_hql = "from TenantUserAddrs obj ";
                tenant_user_addrs_hql = HqlUtil.addCondition(tenant_user_addrs_hql, "isDefault", "1");
                tenant_user_addrs_hql = HqlUtil.addCondition(tenant_user_addrs_hql, "tenantUser.id", user.getId());
                TenantUserAddrs tenantUserAddrs = tenantUserAddrsService.findUnique(tenant_user_addrs_hql);

                //拼出需要保存的信息
                orderRefund.setRefundStatus(OrderRefund.REFUND_APPROVAL_PENGDING);
                orderRefund.setRefundId(orderNoGenService.genOrderNo(OrderNoGenService.ORDER_TYPE_TK));
                orderRefund.setAddressId(tenantUserAddrs.getId());
                orderRefund.setRefundTime(new Date());

                //保存
                orderRefundService.save(orderRefund);

                orderInfo.setOrderStatus(OrderStatus.REFUNDED_APPROVAL_CODE);
                orderInfoService.save(orderInfo);

                restResponse.setSuccess(true);
            } else {
                restResponse.setErrorCode(ErrorConstant.CANNOT_REFUND_ORDER_CODE);
                restResponse.setErrorMsg(ErrorConstant.getErrMsg(ErrorConstant.CANNOT_REFUND_ORDER_CODE));
            }
        } catch (Exception ex) {
            logger.error("系统异常：", ex);
            restResponse.setSuccess(false);
            restResponse.setErrorCode(ErrorConstant.INNER_ERROR_CODE);
            restResponse.setErrorMsg(ErrorConstant.getErrMsg(ErrorConstant.INNER_ERROR_CODE));
        }
        return restResponse;
    }*/

    /**
     * 商家退款，不用审核
     *
     * @return
     */
    @RequestMapping("/refund")
    public RestResponse merchantRefund(@RequestBody OrderRefund orderRefund) {
        RestResponse restResponse = new RestResponse();
        try {
            OrderInfo orderInfo = orderInfoService.findById(orderRefund.getOrderInfo().getId());
            //待发货，待确认才能退款
            /*if (OrderStatus.PENDING_SHIP_CODE.equals(orderInfo.getOrderStatus())
                    || OrderStatus.PENDING_SURE_CODE.equals(orderInfo.getOrderStatus())) {*/

                //土特产退款
                //根据订单号查询该订单的物流信息
                String hql = "from OrderLogistics obj ";
                hql = HqlUtil.addCondition(hql, "orderInfo.id", orderInfo.getId());
                OrderLogistics orderLogistics = orderLogisticsService.findUnique(hql);

                //筛选出购买人信息
                //TenantUser user = orderInfo.getTenantUser();
                //根据用户id查询用户默认的详细收货地址
                    /*String tenant_user_addrs_hql = "from TenantUserAddrs obj ";
                    tenant_user_addrs_hql = HqlUtil.addCondition(tenant_user_addrs_hql, "isDefault", "1");
                    tenant_user_addrs_hql = HqlUtil.addCondition(tenant_user_addrs_hql, "tenantUser.id", user.getId());
                    TenantUserAddrs tenantUserAddrs = tenantUserAddrsService.findUnique(tenant_user_addrs_hql);*/

                TenantUserAddrs tenantUserAddrs = orderInfo.getTenantUserAddrs();

                //拼出需要保存的信息
                orderRefund.setRefundStatus(OrderRefund.REFUND_PENDING);
                orderRefund.setRefundId(orderNoGenService.genOrderNo(OrderNoGenService.ORDER_TYPE_TK));
                orderRefund.setRefundTime(new Date());

                //土特产退款时，退款信息中需写入地址id
                if (OrderInfo.TYPE_SPECIALTY.equals(orderInfo.getOrderType())) {
                    orderRefund.setAddressId(tenantUserAddrs.getId());
                }

                //保存
                orderRefundService.save(orderRefund);
                //状态=》退款中
                orderInfo.setOrderStatus(OrderStatus.REFUNED_STAND_BY_CODE);
                orderInfoService.save(orderInfo);

                restResponse.setSuccess(true);
            /*} else {
                restResponse.setErrorCode(ErrorConstant.CANNOT_REFUND_ORDER_CODE);
                restResponse.setErrorMsg(ErrorConstant.getErrMsg(ErrorConstant.CANNOT_REFUND_ORDER_CODE));
            }*/
        } catch (Exception ex) {
            logger.error("系统异常：", ex);
            restResponse.setSuccess(false);
            restResponse.setErrorCode(ErrorConstant.INNER_ERROR_CODE);
            restResponse.setErrorMsg(ErrorConstant.getErrMsg(ErrorConstant.INNER_ERROR_CODE));
        }
        return restResponse;
    }


    /**
     * 订单退款详情
     *
     * @param id
     * @return
     */
    @RequestMapping("/detail/{id}")
    public RestResponse<OrderRefund> refundDetail(@PathVariable String id) {
        RestResponse<OrderRefund> restResponse = new RestResponse<OrderRefund>();
        try {
            OrderRefund orderRefund =orderRefundService.findById(id);
            restResponse.setSuccess(true);
            restResponse.setData(orderRefund);
        } catch (Exception ex) {
            logger.error("系统异常：", ex);
            restResponse.setSuccess(false);
            restResponse.setErrorCode(ErrorConstant.INNER_ERROR_CODE);
            restResponse.setErrorMsg(ErrorConstant.getErrMsg(ErrorConstant.INNER_ERROR_CODE));
        }
        return restResponse;
    }


    @RequestMapping("/list")
    public RestResponse<Page<OrderRefund>> list(@RequestParam(defaultValue = "1") Integer pageNo,
                                                @RequestParam(defaultValue = "20") Integer pageSize,
                                                String refundId,
                                                String merchantId,
                                                String regStartTime,
                                                String regEndTime,
                                                String startRefundTime,
                                                String endRefundTime,
                                                String refundStatus,
                                                String telephone) {
        RestResponse<Page<OrderRefund>> restResponse = new RestResponse<Page<OrderRefund>>();
        try {
            String hql = "from OrderRefund obj ";
            if (StringUtil.isNotBlank(refundId)) {
                hql = HqlUtil.addCondition(hql, "refundId", refundId);
            }
            if (StringUtil.isNotBlank(regStartTime) && StringUtils.isNotBlank(regEndTime)) {
                hql = HqlUtil.addBetweenCondition(hql, "createTime", regStartTime, regEndTime);
            }
            if (StringUtil.isNotBlank(startRefundTime) && StringUtils.isNotBlank(endRefundTime)) {
                hql = HqlUtil.addBetweenCondition(hql, "refundTime", startRefundTime, endRefundTime);
            }
            if (StringUtil.isNotBlank(refundStatus)) {
                hql = HqlUtil.addCondition(hql, "refundStatus", refundStatus, HqlUtil.LOGIC_AND, HqlUtil.TYPE_IN, "");
            }
            if (StringUtil.isNotBlank(merchantId)) {
                hql = HqlUtil.addCondition(hql, "orderInfo.tenantMerchant.id", merchantId);
            }
            if (StringUtil.isNotBlank(telephone)) {
                hql = HqlUtil.addCondition(hql, "orderInfo.tenantUser.telephone", telephone);
            }
            hql = HqlUtil.addOrder(hql,"createTime","desc");
            Page<OrderRefund> page = orderRefundService.pageList(hql, pageNo, pageSize);

            restResponse.setSuccess(true);
            restResponse.setData(page);

        } catch (Exception ex) {
            logger.error("系统异常：", ex);
            restResponse.setSuccess(false);
            restResponse.setErrorCode(ErrorConstant.INNER_ERROR_CODE);
            restResponse.setErrorMsg(ErrorConstant.getErrMsg(ErrorConstant.INNER_ERROR_CODE));
        }
        return restResponse;
    }

    /**
     * 立即退款
     *
     * @param id 主键id
     * @return
     */
    @RequestMapping("/refund_now")
    public RestResponse refundNow(@RequestParam String id) {
        RestResponse restResponse = new RestResponse();
        try {
            OrderRefund orderRefund = orderRefundService.findById(id);
            orderRefund.setRefundStatus(OrderRefund.REFUND_PENDING);

            OrderInfo orderInfo = orderRefund.getOrderInfo();
            orderInfo.setOrderStatus(OrderStatus.REFUNED_STAND_BY_CODE);

            orderRefundService.save(orderRefund);
            orderInfoService.save(orderInfo);

            restResponse.setSuccess(true);
        } catch (Exception ex) {
            logger.error("系统异常：", ex);
            restResponse.setSuccess(false);
            restResponse.setErrorCode(ErrorConstant.INNER_ERROR_CODE);
            restResponse.setErrorMsg(ErrorConstant.getErrMsg(ErrorConstant.INNER_ERROR_CODE));
        }
        return restResponse;
    }

    /**
     * 取消退款
     *
     * @param id 主键id
     * @return
     */
    @RequestMapping("/refund_cancel")
    public RestResponse cancelRefund(@RequestParam String id) {
        RestResponse restResponse = new RestResponse();
        try {
            OrderRefund orderRefund = orderRefundService.findById(id);
            //orderRefund.setRefundStatus(OrderRefund.REFUND_PENDING);

            OrderInfo orderInfo = orderRefund.getOrderInfo();
            orderInfo.setOrderStatus(OrderStatus.TRADE_SUCCESS_CODE);

            //orderRefundService.save(orderRefund);
            orderRefundService.delete(id);
            orderInfoService.save(orderInfo);
            restResponse.setSuccess(true);
        } catch (Exception ex) {
            logger.error("系统异常：", ex);
            restResponse.setSuccess(false);
            restResponse.setErrorCode(ErrorConstant.INNER_ERROR_CODE);
            restResponse.setErrorMsg(ErrorConstant.getErrMsg(ErrorConstant.INNER_ERROR_CODE));
        }
        return restResponse;
    }

    /**
     * 退款审批借口
     *
     * @param id
     * @return
     */
    @RequestMapping("/approval_refund")
    public RestResponse approvalRefund(@RequestParam String id) {
        RestResponse restResponse = new RestResponse();
        try {
            OrderRefund orderRefund = orderRefundService.findById(id);
            OrderInfo orderInfo = orderRefund.getOrderInfo();

            orderRefund.setRefundStatus(OrderRefund.REFUND_PENDING);
            orderInfo.setOrderStatus(OrderStatus.REFUNED_STAND_BY_CODE);

            orderRefundService.save(orderRefund);
            orderInfoService.save(orderInfo);
            restResponse.setSuccess(true);
        } catch (Exception ex) {
            logger.error("系统异常：", ex);
            restResponse.setSuccess(false);
            restResponse.setErrorCode(ErrorConstant.INNER_ERROR_CODE);
            restResponse.setErrorMsg(ErrorConstant.getErrMsg(ErrorConstant.INNER_ERROR_CODE));
        }
        return restResponse;
    }

    @RequestMapping("/excel")
    public void export(String refundId,
                       String merchantId,
                       String regStartTime,
                       String regEndTime,
                       String startRefundTime,
                       String endRefundTime,
                       String refundStatus,
                       String telephone, HttpServletResponse response) {
        try {
            String hql = "from OrderRefund obj ";
            if (StringUtil.isNotBlank(refundId)) {
                hql = HqlUtil.addCondition(hql, "refundId", refundId);
            }
            if (StringUtil.isNotBlank(regStartTime) && StringUtils.isNotBlank(regEndTime)) {
                hql = HqlUtil.addBetweenCondition(hql, "createTime", regStartTime, regEndTime);
            }
            if (StringUtil.isNotBlank(startRefundTime) && StringUtils.isNotBlank(endRefundTime)) {
                hql = HqlUtil.addBetweenCondition(hql, "refundTime", startRefundTime, endRefundTime);
            }
            if (StringUtil.isNotBlank(refundStatus)) {
                hql = HqlUtil.addCondition(hql, "refundStatus", refundStatus, HqlUtil.LOGIC_AND, HqlUtil.TYPE_IN, "");
            }
            if (StringUtil.isNotBlank(merchantId)) {
                hql = HqlUtil.addCondition(hql, "orderInfo.tenantMerchant.id", merchantId);
            }
            if (StringUtil.isNotBlank(telephone)) {
                hql = HqlUtil.addCondition(hql, "orderInfo.tenantUser.telephone", telephone);
            }

            List<OrderRefund> page = orderRefundService.getList(hql);
            List<OrderRefundExcel> list = new ArrayList<OrderRefundExcel>();
            OrderRefundExcel ore;
            for (OrderRefund or : page) {
                ore = new OrderRefundExcel();
                ore.setName(or.getOrderInfo().getTenantUser().getName());
                ore.setTelephone(or.getOrderInfo().getTenantUser().getTelephone());
                ore.setMerchantName(or.getOrderInfo().getTenantMerchant().getMerchantName());
                ore.setOrderNum(or.getOrderInfo().getOrderNum());
                ore.setRefundId(or.getRefundId());
                ore.setOrderSum(or.getOrderInfo().getOrderSum().setScale(2, BigDecimal.ROUND_HALF_UP)+"");
                ore.setCreateTime(or.getCreateTime());
                ore.setRefundMoney(or.getRefundMoney().setScale(2,BigDecimal.ROUND_HALF_UP)+"");
                ore.setRefundTime(or.getRefundTime());
                ore.setRefundStatus(or.getRefundStatus()==null?"":OrderStatus.getOrderDescription(or.getRefundStatus()));
                list.add(ore);
            }
            String title = "退款订单";
            String[] headers = new String[]{"姓名","联系电话", "商户", "付款订单号", "退款订单号", "订单金额", "申请退款时间", "退款金额", "退款时间", "退款状态"};
            String[] values = new String[]{"name","telephone","merchantName", "orderNum", "refundId", "orderSum", "createTime", "refundMoney", "refundTime", "refundStatus"};
            downloadExcel(title, null, headers, values, list, null, null, response);
        } catch (Exception ex) {
            logger.error("系统异常：", ex);
        }
    }


}
