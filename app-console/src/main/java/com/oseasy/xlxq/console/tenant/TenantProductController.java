package com.oseasy.xlxq.console.tenant;

import com.oseasy.xlxq.console.base.AbstractPortalController;
import com.oseasy.xlxq.console.comm.ConsoleConstants;
import com.oseasy.xlxq.service.api.exceptions.ErrorConstant;
import com.oseasy.xlxq.service.api.tenant.model.TenantProduct;
import com.oseasy.xlxq.service.api.tenant.service.TenantProductService;
import com.oseasy.xlxq.service.core.utils.HqlUtil;
import com.oseasy.xlxq.service.core.utils.Page;
import com.oseasy.xlxq.service.core.utils.StringUtil;
import com.oseasy.xlxq.service.web.rest.RestResponse;
import net.sf.json.JSONObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

/**
 * 产品管理
 */
@RestController
@RequestMapping("/console/product")
public class TenantProductController extends AbstractPortalController {

    @Autowired
    private TenantProductService tenantProductService;

    /**
     * 获取实体信息
     * @param id
     * @return
     */
    @RequestMapping("/{id}")
    public RestResponse getEntityById(@PathVariable("id") String id) {
        RestResponse restResponse = new RestResponse();
        try {
            TenantProduct tenantProduct = tenantProductService.findById(id);
            restResponse.setData(tenantProduct);
            restResponse.setSuccess(true);
            restResponse.setErrorCode("1001");
            restResponse.setErrorMsg("操作成功");
        } catch (Exception e) {
            logger.error(e.getMessage(),e);
            restResponse.setSuccess(false);
            restResponse.setErrorCode("0000");
            restResponse.setErrorMsg("操作异常");
        }
        return restResponse;
    }


    /**
     * 保存实体
     * @param
     * @return
     */
    @RequestMapping("/save")
    public RestResponse save(@RequestBody TenantProduct tenantProduct) {
        RestResponse restResponse = new RestResponse();
        try {
            if(StringUtil.isNotBlank(tenantProduct.getId())){
                tenantProductService.update(tenantProduct.getId(),tenantProduct);
            }else {
                tenantProductService.save(tenantProduct);
            }
            restResponse.setSuccess(true);
            restResponse.setErrorCode("1001");
            restResponse.setErrorMsg("操作成功");
        } catch (Exception e) {
            logger.error(e.getMessage(),e);
            restResponse.setSuccess(false);
            restResponse.setErrorCode("0000");
            restResponse.setErrorMsg("操作异常");
        }
        return restResponse;
    }


    /**
     * 删除
     * @param id
     * @return
     */
    @RequestMapping("/delete/{id}")
    public RestResponse delete(@PathVariable("id") String id) {
        RestResponse restResponse = new RestResponse();
        try {
            tenantProductService.delete(id);
            restResponse.setSuccess(true);
            restResponse.setErrorCode("1001");
            restResponse.setErrorMsg("操作成功");
        } catch (Exception e) {
            logger.error(e.getMessage(),e);
            restResponse.setSuccess(false);
            restResponse.setErrorCode("0000");
            restResponse.setErrorMsg("操作异常");
        }
        return restResponse;
    }


    /**
     * 查询列表
     * @param
     * @param pageNo
     * @param pageSize
     * @return
     */
    @RequestMapping("/list")
    public RestResponse<Page<TenantProduct>> getPageList(@RequestParam(defaultValue = "1") Integer pageNo,
                                    @RequestParam(defaultValue = "20") Integer pageSize,
                                    String productType,String productName,String merchantName) {
        RestResponse<Page<TenantProduct>> restResponse = new RestResponse<Page<TenantProduct>>();
        try {
            String hql = "from TenantProduct obj";
            if(StringUtil.isNotBlank(productType)){
                hql = HqlUtil.addCondition(hql,"productType",productType);
                //hql = HqlUtil.addCondition(hql, "productType", productType, HqlUtil.LOGIC_AND, HqlUtil.TYPE_STRING_LIKE, null);
            }
            if(StringUtil.isNotBlank(productName)){
                hql = HqlUtil.addCondition(hql, "productName", productName, HqlUtil.LOGIC_AND, HqlUtil.TYPE_STRING_LIKE, null);
            }
            if(StringUtil.isNotBlank(merchantName)){
                hql = HqlUtil.addCondition(hql, "tenantMerchant.merchantName", merchantName, HqlUtil.LOGIC_AND, HqlUtil.TYPE_STRING_LIKE, null);
            }
            Page<TenantProduct> page = tenantProductService.pageList(hql,pageNo,pageSize);
            restResponse.setData(page);
            restResponse.setSuccess(true);
            restResponse.setErrorCode("1001");
            restResponse.setErrorMsg("操作成功");
        } catch (Exception e) {
            logger.error(e.getMessage(),e);
            restResponse.setSuccess(false);
            restResponse.setErrorCode("0000");
            restResponse.setErrorMsg("操作异常");
        }
        return restResponse;
    }

    /**
     * 批量操作
     * @param
     * @return
     */
    @RequestMapping(value = "/batchoperate",method = RequestMethod.POST)
    public RestResponse batchoperate(@RequestBody JSONObject jsonObj) {
        RestResponse restResponse = new RestResponse();
        String ids =  jsonObj.getString("ids");
        String type =  jsonObj.getString("type");
        try {
            if (StringUtil.isNotBlank(ids)&&StringUtil.isNotBlank(type)) {
                String [] idArray = ids.split(",");
                if (ConsoleConstants.OPERATE_TYPE_DELETE.equals(type)) {
                    tenantProductService.updateByArray("deleted", ConsoleConstants.DELETE_FLAG, "id", idArray);
                }
            }else{
                return RestResponse.failed("0000","请求参数错误");
            }
            restResponse.setSuccess(true);
            restResponse.setErrorCode("1001");
            restResponse.setErrorMsg("操作成功");
        } catch (Exception e) {
            logger.error(e.getMessage(),e);
            restResponse.setSuccess(false);
            restResponse.setErrorCode("0000");
            restResponse.setErrorMsg("操作异常");
        }
        return restResponse;
    }

    /**
     * 根据id批量查询产品列表
     * @param ids
     * @return
     */
    @RequestMapping("/list_by_ids")
    public RestResponse<Page<TenantProduct>> queryByIdList(@RequestParam(defaultValue = "1") Integer pageNo,
                                                           @RequestParam(defaultValue = "5") Integer pageSize,
                                                           @RequestParam String ids){
        RestResponse<Page<TenantProduct>> restResponse=new RestResponse<Page<TenantProduct>>();
        try{
            String hql = "from TenantProduct obj ";
            String[] str;
            if (ids.contains(",")){
                str = ids.split(",");
            }else {
                str = new String[]{ids};
            }
            StringBuffer sb=new StringBuffer();
            for(int i=0;i<str.length;i++){
                if(i==str.length-1){
                    sb.append("'"+str[i]+"'");
                }else{
                    sb.append("'"+str[i]+"',");
                }
            }
            hql = HqlUtil.addCondition(hql,"id",sb.toString(),HqlUtil.LOGIC_AND,HqlUtil.TYPE_IN,null);
            Page<TenantProduct> page=tenantProductService.pageList(hql,pageNo,pageSize);
            restResponse.setData(page);
            restResponse.setSuccess(true);
        }catch (Exception ex){
            logger.error("系统异常：", ex);
            restResponse.setSuccess(false);
            restResponse.setErrorCode(ErrorConstant.INNER_ERROR_CODE);
            restResponse.setErrorMsg(ErrorConstant.getErrMsg(ErrorConstant.INNER_ERROR_CODE));
        }
        return restResponse;
    }


}
