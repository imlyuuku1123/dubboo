package com.oseasy.xlxq.console.base;

import com.oseasy.xlxq.service.core.utils.JSONUtil;
import com.oseasy.xlxq.service.web.utils.WebUtils;
import org.springframework.stereotype.Component;
import org.springframework.web.servlet.view.AbstractView;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.util.Map;

/**
 * Created by tandy on 17/5/24.
 */
@Component("apiModelAndView")
public class APIModelAndView extends AbstractView {
    public static final String APIVIEW = "apiModelAndView";
    @Override
    protected void renderMergedOutputModel(Map<String, Object> model, HttpServletRequest request, HttpServletResponse response) throws Exception {
        Object obj = model.get("data");
        WebUtils.outputJson(request,response,JSONUtil.objectToJson(obj));
    }
}
