package com.oseasy.xlxq.console.tenant;

import com.oseasy.xlxq.console.base.AbstractPortalController;
import com.oseasy.xlxq.console.comm.ConsoleConstants;
import com.oseasy.xlxq.service.api.exceptions.ErrorConstant;
import com.oseasy.xlxq.service.api.tenant.model.TenantSpecialty;
import com.oseasy.xlxq.service.api.tenant.service.TenantSpecialtyService;
import com.oseasy.xlxq.service.core.utils.HqlUtil;
import com.oseasy.xlxq.service.core.utils.Page;
import com.oseasy.xlxq.service.core.utils.StringUtil;
import com.oseasy.xlxq.service.web.rest.RestResponse;
import net.sf.json.JSONObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpServletRequest;
import java.util.List;

/**
 * 土特产管理
 */
@RestController
@RequestMapping("/console/specialty")
public class TenantSpecialtyController extends AbstractPortalController {

    @Autowired
    private TenantSpecialtyService tenantSpecialtyService;

    /**
     * 获取实体信息
     * @param id
     * @return
     */
    @RequestMapping("/{id}")
    public RestResponse getEntityById(@PathVariable("id") String id) {
        RestResponse restResponse = new RestResponse();
        try {
            TenantSpecialty tenantSpecialty = tenantSpecialtyService.findById(id);
            restResponse.setData(tenantSpecialty);
            restResponse.setSuccess(true);
            restResponse.setErrorCode("1001");
            restResponse.setErrorMsg("操作成功");
        } catch (Exception e) {
            logger.error(e.getMessage(),e);
            restResponse.setSuccess(false);
            restResponse.setErrorCode("0000");
            restResponse.setErrorMsg("操作异常");
        }
        return restResponse;
    }


    /**
     * 保存实体
     * @param
     * @return
     */
    @RequestMapping("/save")
    public RestResponse save(@RequestBody TenantSpecialty tenantSpecialty) {
        RestResponse restResponse = new RestResponse();
        try {
            if(StringUtil.isNotBlank(tenantSpecialty.getId())){
                tenantSpecialtyService.update(tenantSpecialty.getId(),tenantSpecialty);
            }else {
                tenantSpecialtyService.save(tenantSpecialty);
            }
            restResponse.setSuccess(true);
            restResponse.setErrorCode("1001");
            restResponse.setErrorMsg("操作成功");
        } catch (Exception e) {
            logger.error(e.getMessage(),e);
            restResponse.setSuccess(false);
            restResponse.setErrorCode("0000");
            restResponse.setErrorMsg("操作异常");
        }
        return restResponse;
    }


    /**
     * 删除
     * @param id
     * @return
     */
    @RequestMapping("/delete/{id}")
    public RestResponse delete(@PathVariable("id") String id) {
        RestResponse restResponse = new RestResponse();
        try {
            tenantSpecialtyService.delete(id);
            restResponse.setSuccess(true);
            restResponse.setErrorCode("1001");
            restResponse.setErrorMsg("操作成功");
        } catch (Exception e) {
            logger.error(e.getMessage(),e);
            restResponse.setSuccess(false);
            restResponse.setErrorCode("0000");
            restResponse.setErrorMsg("操作异常");
        }
        return restResponse;
    }


    /**
     * 查询列表
     * @param
     * @param pageNo
     * @param pageSize
     * @return
     */
    @RequestMapping("/list")
    public RestResponse getPageList(@RequestParam(defaultValue = "1") Integer pageNo,
                                    @RequestParam(defaultValue = "20") Integer pageSize,
                                    HttpServletRequest request) {
        RestResponse restResponse = new RestResponse();
        try {
            String hql = "from TenantSpecialty obj";
            /*String moduleName = module.getModuleName();
            if (StringUtil.isNotBlank(moduleName)) {
                hql = HqlUtil.addCondition(hql, "moduleName", moduleName,null,"Like",null);
            }*/
            String productName = request.getParameter("productName");
            String state = request.getParameter("state");
            String timeStart = request.getParameter("timeStart");
            String timeEnd = request.getParameter("timeEnd");
            if (StringUtil.isNotBlank(productName)) {
                hql = HqlUtil.addCondition(hql, "productName", productName,HqlUtil.LOGIC_AND,HqlUtil.TYPE_STRING_LIKE,HqlUtil.TYPE_STRING_LIKE);
            }
            if (StringUtil.isNotBlank(state)) {
                hql = HqlUtil.addCondition(hql, "state", state,HqlUtil.LOGIC_AND,HqlUtil.TYPE_NUMBER,HqlUtil.COMPARECHAR_EQ);
            }
            if (StringUtil.isNotBlank(timeStart)) {
                hql = HqlUtil.addCondition(hql, "createTime", timeStart,HqlUtil.LOGIC_AND,HqlUtil.TYPE_DATE,HqlUtil.COMPARECHAR_GREAT_EQ);
            }
            if (StringUtil.isNotBlank(timeEnd)) {
                hql = HqlUtil.addCondition(hql, "createTime", timeEnd,HqlUtil.LOGIC_AND,HqlUtil.TYPE_DATE,HqlUtil.COMPARECHAR_LESS_EQ);
            }
            Page page = tenantSpecialtyService.pageList(hql,pageNo,pageSize);
            restResponse.setData(page);
            restResponse.setSuccess(true);
            restResponse.setErrorCode("1001");
            restResponse.setErrorMsg("操作成功");
        } catch (Exception e) {
            logger.error(e.getMessage(),e);
            restResponse.setSuccess(false);
            restResponse.setErrorCode("0000");
            restResponse.setErrorMsg("操作异常");
        }
        return restResponse;
    }

    /**
     * 批量操作
     * @param
     * @return
     */
    @RequestMapping(value = "/batchoperate",method = RequestMethod.POST)
    public RestResponse batchoperate(@RequestBody JSONObject jsonObj) {
        RestResponse restResponse = new RestResponse();
        String ids =  jsonObj.getString("ids");
        String type =  jsonObj.getString("type");
        try {
            if (StringUtil.isNotBlank(ids)&&StringUtil.isNotBlank(type)) {
                String [] idArray = ids.split(",");
                if (ConsoleConstants.OPERATE_TYPE_DELETE.equals(type)) {
                    tenantSpecialtyService.updateByArray("deleted", ConsoleConstants.DELETE_FLAG, "id", idArray);
                }
            }else{
                return RestResponse.failed("0000","请求参数错误");
            }
            restResponse.setSuccess(true);
            restResponse.setErrorCode("1001");
            restResponse.setErrorMsg("操作成功");
        } catch (Exception e) {
            logger.error(e.getMessage(),e);
            restResponse.setSuccess(false);
            restResponse.setErrorCode("0000");
            restResponse.setErrorMsg("操作异常");
        }
        return restResponse;
    }

    /**
     *
     * 土特产列表，非分页
     * @return
     */
    @RequestMapping("/list2")
    public RestResponse list(String catalogRootId,String catalogId){
        RestResponse restResponse=new RestResponse();
        try{
            String hql="from TenantSpecialty obj ";
            if(StringUtil.isNotBlank(catalogRootId)){
                hql =HqlUtil.addCondition(hql,"catalogRoot.id",catalogRootId);
            }
            if(StringUtil.isNotBlank(catalogId)){
                hql =HqlUtil.addCondition(hql,"catalog.id",catalogId);
            }
            List<TenantSpecialty> list = tenantSpecialtyService.getList(hql);
            restResponse.setSuccess(true);
            restResponse.setData(list);
        }catch (Exception ex){
            logger.error("系统异常：", ex);
            restResponse.setSuccess(false);
            restResponse.setErrorCode(ErrorConstant.INNER_ERROR_CODE);
            restResponse.setErrorMsg(ErrorConstant.getErrMsg(ErrorConstant.INNER_ERROR_CODE));
        }
        return  restResponse;
    }

}
