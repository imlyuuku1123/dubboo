package com.oseasy.xlxq.console.index;

import com.oseasy.xlxq.console.base.AbstractPortalController;
import com.oseasy.xlxq.console.comm.ConsoleConstants;

import com.oseasy.xlxq.service.api.statistic.model.StatisticTenantUserDay;

import com.oseasy.xlxq.service.api.statistic.service.StatisticHomeStayDayService;
import com.oseasy.xlxq.service.api.statistic.service.StatisticSpecialtyDayService;
import com.oseasy.xlxq.service.api.statistic.service.StatisticTenantUserDayService;
import com.oseasy.xlxq.service.api.statistic.service.StatisticTicketDayService;

import com.oseasy.xlxq.service.api.tenant.service.TenantHomeStayRoomService;
import com.oseasy.xlxq.service.api.tenant.service.TenantScenicTicketService;
import com.oseasy.xlxq.service.api.tenant.service.TenantSpecialtyService;
import com.oseasy.xlxq.service.core.utils.DateUtils;
import com.oseasy.xlxq.service.core.utils.HqlUtil;

import com.oseasy.xlxq.service.web.rest.RestRequest;
import com.oseasy.xlxq.service.web.rest.RestResponse;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;

import org.springframework.web.bind.annotation.*;

import scala.App;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.util.*;

/**
 * 登录后的首页
 * Created by liups on 2016/6/27.
 */
@RestController
@RequestMapping("/console/index")
public class IndexController extends AbstractPortalController {

    private static final Logger logger = LoggerFactory.getLogger(IndexController.class);

    @Autowired
    private StatisticHomeStayDayService statisticHomeStayDayService;

    @Autowired
    private StatisticSpecialtyDayService statisticSpecialtyDayService;

    @Autowired
    private StatisticTenantUserDayService statisticTenantUserDayService;

    @Autowired
    private StatisticTicketDayService statisticTicketDayService;

    @Autowired
    private TenantSpecialtyService tenantSpecialtyService;
    @Autowired
    private TenantHomeStayRoomService tenantHomeStayRoomService;
    @Autowired
    private TenantScenicTicketService tenantScenicTicketService;

    /**
     * 返回首页页面
     */
   /*@RequestMapping(value = "",method = RequestMethod.GET)
    public ModelAndView index(HttpServletRequest request){
        Map<String,Object> model = new HashMap<>();
        IndexVO indexVO = buildINdexVO();
        model.put("indexVO",indexVO);
        return new ModelAndView("index",model);
    }*/

   @RequestMapping(value = "",method = RequestMethod.GET)
    public void index(HttpServletRequest request, HttpServletResponse response){
        try {
            String backIndexUrl = ConsoleConstants.BACK_INDEX_URL;
           // return "redirect:"+backIndexUrl;
            response.sendRedirect(backIndexUrl);
        }catch (Exception e){
            logger.error("重定向到后台首页异常！");
            logger.error(e.getMessage(),e);
           // String toLoginUrl = "login";
           // return null;
        }

    }

//
//    /**
//     * 获取今日数据和本月数据
//     * @param request
//     * @param type
//     * @return
//     */
//    @RequestMapping(value = "/get_avg_der/{type}",method = RequestMethod.GET)
//    public RestResponse getAvgDer(HttpServletRequest request,@PathVariable String type){
//        String token = getSecurityToken(request);
//        String searchType;
//        if("today".equals(type)) {
//            searchType = "today";
//        }else {
//            searchType = "month";
//        }
//        String url = ConsoleConstants.REST_PREFIX_URL +   "/rest/day_statics/get?type={1}";
//        return RestRequest.buildSecurityRequest(token).get(url, Map.class,searchType);
//    }

    /**
     * 根据token构建首页vo
     * @return
     */
    private IndexVO buildINdexVO(){
        IndexVO vo = new IndexVO();
        return vo;
    }


    /**
     * 查询昨日销售额
     * @return
     */
    @RequestMapping("/getAllIndexData")
    public RestResponse getAllIndexData(HttpServletRequest request){
        RestResponse response = null;
        Map<String,Object> rMap = new HashMap<String,Object>();
        try {
            if(logger.isDebugEnabled()){
                logger.debug("getAllIndexData");
            }

           String yesterDay = DateUtils.getYestday();
           String [] dateArray = yesterDay.split("-");
            String year = dateArray[0];
            String month = dateArray[1];
            String day = dateArray[2];

            Map<String,Object> paraMap = new HashMap<String,Object>();
            paraMap.put("year",year);
            paraMap.put("month",month);
            paraMap.put("day",day);

            Double  saleMoneySpecialtys=0.00D ;//土特产总销额
            Long saleNumSpecialtys=0L ;//土特产总数量

            Double  saleMoneyHomeStays = 0.00D;//民宿总销额
            Long saleNumHomeStays = 0L ;//民宿总数量

            Double  saleMoneyTickets =0.00D ;//门票总销额
            Long saleNumTickets =0L;//门票总数量

            Integer personDayNum = 0;//人数（每日新增）
            Long personSumNum = 0L;//人数（平台总数）

            Double  saleMoneyTotal = 0.00D;//销额总计
            Long saleNumTotal = 0L;//数量总计

            String hql1_1 = "select sum(obj.saleMoney) from StatisticSpecialtyDay obj";
            hql1_1 = HqlUtil.addCondition(hql1_1,paraMap);
            saleMoneySpecialtys = (Double) statisticSpecialtyDayService.getByHql(hql1_1);

            String hql1_2 = "select sum(obj.saleNum) from StatisticSpecialtyDay obj";
            hql1_2 = HqlUtil.addCondition(hql1_2,paraMap);
            saleNumSpecialtys = (Long) statisticSpecialtyDayService.getByHql(hql1_2);

            String hql2_1 = "select sum(obj.saleMoney) from StatisticHomeStayDay obj";
            hql2_1 = HqlUtil.addCondition(hql2_1,paraMap);
            saleMoneyHomeStays = (Double) statisticHomeStayDayService.getByHql(hql2_1);

            String hql2_2 = "select sum(obj.saleNum) from StatisticHomeStayDay obj";
            hql2_2 = HqlUtil.addCondition(hql2_2,paraMap);
            saleNumHomeStays = (Long)statisticHomeStayDayService.getByHql(hql2_2);

            String hql3_1 = "select sum(obj.saleMoney) from StatisticTicketDay obj";
            hql3_1 = HqlUtil.addCondition(hql3_1,paraMap);
            saleMoneyTickets =(Double) statisticTicketDayService.getByHql(hql3_1);

            String hql3_2 = "select sum(obj.saleNum) from StatisticTicketDay obj";
            hql3_2 = HqlUtil.addCondition(hql3_2,paraMap);
            saleNumTickets =(Long) statisticTicketDayService.getByHql(hql3_2);

            String hql4_1 = "from StatisticTenantUserDay obj";
            hql4_1 = HqlUtil.addCondition(hql4_1,paraMap);
            StatisticTenantUserDay su = statisticTenantUserDayService.findUnique(hql4_1);

             saleMoneyTotal = (saleMoneySpecialtys==null?0:saleMoneySpecialtys) + (saleMoneyHomeStays==null?0:saleMoneyHomeStays) + (saleMoneyTickets==null?0:saleMoneyTickets);//销额总计
             saleNumTotal = (saleNumSpecialtys==null?0:saleNumSpecialtys) + (saleNumHomeStays==null?0:saleNumHomeStays) +  (saleNumTickets==null?0:saleNumTickets);//数量总计

             personDayNum = su==null?0:su.getPersonDayNum();//人数（每日新增）
             personSumNum = su==null?0:su.getPersonSumNum();//人数（平台总数）


            Calendar calendar = Calendar.getInstance();
            int currYear = calendar.get(Calendar.YEAR);
            int currMonth = calendar.get(Calendar.MONTH)+1;

            String sql1_1 = "select sum(obj.sale_money) as sum,obj.specialty_id,p.product_name  from  tb_statistic_specialty_day obj left join tb_tenant_specialty c on c.id = obj.specialty_id left join tb_tenant_product p on p.id = c.id " +
                    "where obj.deleted = 0 and p.deleted=0 and c.deleted=0 and obj.year = " + currYear + " and obj.month = " +currMonth + "  group by obj.specialty_id order by sum desc limit 5" ;

            List list1_1 =  statisticSpecialtyDayService.getListBySql(sql1_1);

            String sql1_2 = "select sum(obj.sale_num) as sum,obj.specialty_id,p.product_name from  tb_statistic_specialty_day obj left join tb_tenant_specialty c on c.id = obj.specialty_id left join tb_tenant_product p on p.id = c.id  " +
                    "where obj.deleted = 0 and p.deleted=0 and c.deleted=0 and  obj.year = " + currYear + " and obj.month = " +currMonth + " group by obj.specialty_id  order by sum desc limit 5";

            List list1_2 =  statisticSpecialtyDayService.getListBySql(sql1_2);

            String sql2_1 = "select sum(obj.sale_money) as sum,obj.room_id,p.product_name  from  tb_statistic_homestay_day obj left join tb_tenant_homestay_room c on c.id = obj.room_id  left join tb_tenant_product p on p.id = c.id  " +
                    "where obj.deleted = 0 and p.deleted=0 and c.deleted=0 and obj.year = " + currYear + " and obj.month = " +currMonth + " group by obj.room_id  order by sum desc limit 5";

            List list2_1 =  statisticHomeStayDayService.getListBySql(sql2_1);

            String sql2_2 = "select sum(obj.sale_num) as sum,obj.room_id,p.product_name  from  tb_statistic_homestay_day obj left join tb_tenant_homestay_room c on c.id = obj.room_id   left join tb_tenant_product p on p.id = c.id  " +
                    "where obj.deleted = 0 and p.deleted=0 and c.deleted=0 and obj.year = " + currYear + " and obj.month = " +currMonth + " group by obj.room_id  order by sum desc limit 5";

            List list2_2 =  statisticHomeStayDayService.getListBySql(sql2_2);

            String sql3_1 = "select sum(obj.sale_money) as sum,obj.ticket_id,p.product_name  from  tb_statistic_ticket_day obj left join tb_tenant_scenic_ticket c on c.id = obj.ticket_id   left join tb_tenant_product p on p.id = c.id  " +
                    "where obj.deleted = 0 and p.deleted=0 and c.deleted=0 and  obj.year = " + currYear + " and obj.month = " +currMonth + " group by obj.ticket_id  order by sum desc limit 5";

            List list3_1 =  statisticTicketDayService.getListBySql(sql3_1);

            String sql3_2= "select sum(obj.sale_num) as sum,obj.ticket_id,p.product_name  from  tb_statistic_ticket_day obj left join tb_tenant_scenic_ticket c on c.id = obj.ticket_id left join tb_tenant_product p on p.id = c.id  " +
                    "where obj.deleted = 0 and p.deleted=0 and c.deleted=0 and  obj.year = " + currYear + " and obj.month = " +currMonth + " group by obj.ticket_id  order by sum desc limit 5";

            List list3_2 =  statisticTicketDayService.getListBySql(sql3_2);

            /*logger.info("=============1-1:"+list1_1.size());
            logger.info("=============1-2:"+list1_2.size());
            logger.info("=============2-1:"+list2_1.size());
            logger.info("=============2-2:"+list2_2.size());
            logger.info("=============3-1:"+list3_1.size());
            logger.info("=============3-2:"+list3_2.size());*/


            List<Map<String,Object>> rList1 = new ArrayList<Map<String,Object>>();
            List<Map<String,Object>> rList2 = new ArrayList<Map<String,Object>>();
            List<Map<String,Object>> rList3 = new ArrayList<Map<String,Object>>();
            List<Map<String,Object>> rList4 = new ArrayList<Map<String,Object>>();
            List<Map<String,Object>> rList5 = new ArrayList<Map<String,Object>>();
            List<Map<String,Object>> rList6 = new ArrayList<Map<String,Object>>();

            java.text.DecimalFormat   df   =new   java.text.DecimalFormat("#.00");

            if(list1_1!=null&&list1_1.size()>0){
                Iterator iterator = list1_1.iterator();
               // int sort =1;
                while(iterator.hasNext()){
                  Object object =  iterator.next();
                  if(object instanceof Object []){
                     Object [] sa =  (Object[]) object;
                     String sumSaleMoney = String.valueOf(sa[0]);
                      sumSaleMoney=df.format(sumSaleMoney==null?0.00D:Double.valueOf(sumSaleMoney));
                     String specialtyId = String.valueOf(sa[1]);
                      String specialtyName = String.valueOf(sa[2]);
                   /*   TenantSpecialty tenantSpecialty = tenantSpecialtyService.findById(specialtyId);
                     if(tenantSpecialty!=null){
                         specialtyName = tenantSpecialty.getProductName();
                     }*/
                      Map<String,Object> tMap = new HashMap<String,Object>();
                      tMap.put("sale",sumSaleMoney);
                      tMap.put("content",specialtyName);
                   //   tMap.put("sort",sort);
                   //   sort++;
                      rList1.add(tMap);
                    }
                }
            }


            if(list1_2!=null&&list1_2.size()>0){
                Iterator iterator = list1_2.iterator();
              //  int sort =1;
                while(iterator.hasNext()){
                    Object object =  iterator.next();
                    if(object instanceof Object []){
                        Object [] sa =  (Object[]) object;
                        String sumSaleNum = String.valueOf(sa[0]);
                        String specialtyId = String.valueOf(sa[1]);
                        String specialtyName = String.valueOf(sa[2]);
                     /*   TenantSpecialty tenantSpecialty = tenantSpecialtyService.findById(specialtyId);
                        if(tenantSpecialty!=null){
                            specialtyName = tenantSpecialty.getProductName();
                        }*/
                        Map<String,Object> tMap = new HashMap<String,Object>();
                        tMap.put("sale",sumSaleNum);
                        tMap.put("content",specialtyName);
                     //   tMap.put("sort",sort);
                   //     sort++;
                        rList2.add(tMap);
                    }
                }
            }

            if(list2_1!=null&&list2_1.size()>0){
                Iterator iterator = list2_1.iterator();
             //   int sort=1;
                while(iterator.hasNext()){
                    Object object =  iterator.next();
                    if(object instanceof Object []){
                        Object [] sa =  (Object[]) object;
                        String sumSaleMoney = String.valueOf(sa[0]);
                        sumSaleMoney=df.format(sumSaleMoney==null?0.00D:Double.valueOf(sumSaleMoney));
                        String homeStayId = String.valueOf(sa[1]);
                        String homeStayName = String.valueOf(sa[2]);
                      /*  TenantHomeStayRoom tenantHomeStayRoom = t(HomeStayId);
                        if(tenantSpecialty!=null){
                            specialtyName = tenantSpecialty.getProductName();
                        }*/
                        Map<String,Object> tMap = new HashMap<String,Object>();
                        tMap.put("sale",sumSaleMoney);
                        tMap.put("content",homeStayName);
                    //    tMap.put("sort",sort);
                    //    sort++;
                        rList3.add(tMap);
                    }
                }
            }

            if(list2_2!=null&&list2_2.size()>0){
                Iterator iterator = list2_2.iterator();
            //    int sort=1;
                while(iterator.hasNext()){
                    Object object =  iterator.next();
                    if(object instanceof Object []){
                        Object [] sa =  (Object[]) object;
                        String sumSaleNum = String.valueOf(sa[0]);
                        String homeStayId = String.valueOf(sa[1]);
                        String homeStayName = String.valueOf(sa[2]);
                      /*  TenantSpecialty tenantSpecialty = tenantSpecialtyService.findById(specialtyId);
                        if(tenantSpecialty!=null){
                            specialtyName = tenantSpecialty.getProductName();
                        }*/
                        Map<String,Object> tMap = new HashMap<String,Object>();
                        tMap.put("sale",sumSaleNum);
                        tMap.put("content",homeStayName);
                  //      tMap.put("sort",sort);
                  //      sort++;
                        rList4.add(tMap);
                    }
                }
            }

            if(list3_1!=null&&list3_1.size()>0){
                Iterator iterator = list3_1.iterator();
             //   int sort=1;
                while(iterator.hasNext()){
                    Object object =  iterator.next();
                    if(object instanceof Object []){
                        Object [] sa =  (Object[]) object;
                        String sumSaleMoney = String.valueOf(sa[0]);
                        sumSaleMoney=df.format(sumSaleMoney==null?0.00D:Double.valueOf(sumSaleMoney));
                        String ticketId = String.valueOf(sa[1]);
                        String ticketName = String.valueOf(sa[2]);
                      /*  TenantHomeStayRoom tenantHomeStayRoom = t(HomeStayId);
                        if(tenantSpecialty!=null){
                            specialtyName = tenantSpecialty.getProductName();
                        }*/
                        Map<String,Object> tMap = new HashMap<String,Object>();
                        tMap.put("sale",sumSaleMoney);
                        tMap.put("content",ticketName);
                 //       tMap.put("sort",sort);
                  //      sort++;
                        rList5.add(tMap);
                    }
                }
            }


            if(list3_2!=null&&list3_2.size()>0){
                Iterator iterator = list3_2.iterator();
            //    int sort=1;
                while(iterator.hasNext()){
                    Object object =  iterator.next();
                    if(object instanceof Object []){
                        Object [] sa =  (Object[]) object;
                        String sumSaleNum = String.valueOf(sa[0]);
                        String ticketId = String.valueOf(sa[1]);
                        String ticketName = String.valueOf(sa[2]);
                      /*  TenantSpecialty tenantSpecialty = tenantSpecialtyService.findById(specialtyId);
                        if(tenantSpecialty!=null){
                            specialtyName = tenantSpecialty.getProductName();
                        }*/
                        Map<String,Object> tMap = new HashMap<String,Object>();
                        tMap.put("sale",sumSaleNum);
                        tMap.put("content",ticketName);
                  //      tMap.put("sort",sort);
                  //      sort++;
                        rList6.add(tMap);
                    }
                }
            }

           // Map<String,Object> tMap1_1 = new HashMap<String,Object>();//昨日销售额
            //Map<String,Object> tMap1_2 = new HashMap<String,Object>();//昨日订单数量
            List<Map<String,Object>> tList1_1 = new ArrayList<Map<String,Object>>();//昨日销售额
            List<Map<String,Object>> tList1_2 = new ArrayList<Map<String,Object>>();//昨日订单数量
            List<Map<String,Object>> tList1_3 = new ArrayList<Map<String,Object>>();//用户数

           // Map<String,Object> tMap1_3 = new HashMap<String,Object>();//昨日用户数

            Map<String,Object> tMap1_1_1 = new HashMap<String,Object>();
            tMap1_1_1.put("name","土特产");
            tMap1_1_1.put("value",saleMoneySpecialtys);
           // tMap1_1_1.put("sort","1");
            //tMap1_1.put("saleMoneySpecialtys",tMap1_1_1);
            tList1_1.add(tMap1_1_1);

            Map<String,Object> tMap1_1_2 = new HashMap<String,Object>();
            tMap1_1_2.put("name","民宿");
            tMap1_1_2.put("value",saleMoneyHomeStays);
            //tMap1_1_2.put("sort","2");
            //tMap1_1.put("saleMoneyHomeStays",tMap1_1_2);
            tList1_1.add(tMap1_1_2);

            Map<String,Object> tMap1_1_3 = new HashMap<String,Object>();
            tMap1_1_3.put("name","门票");
            tMap1_1_3.put("value",saleMoneyTickets);
           // tMap1_1_3.put("sort","3");
           //tMap1_1.put("saleMoneyTickets",tMap1_1_3);
            tList1_1.add(tMap1_1_3);

            Map<String,Object> tMap1_1_4 = new HashMap<String,Object>();
            tMap1_1_4.put("name","合计");
            tMap1_1_4.put("value",saleMoneyTotal);
            //tMap1_1_4.put("sort","4");
            //tMap1_1.put("saleMoneyTotal",tMap1_1_4);
            tList1_1.add(tMap1_1_4);

            //rMap.put("yesterDaySaleMoney",tMap1_1);
            rMap.put("yesterDaySaleMoney",tList1_1);

            Map<String,Object> tMap1_2_1 = new HashMap<String,Object>();
            tMap1_2_1.put("name","土特产");
            tMap1_2_1.put("value",saleNumSpecialtys);
            //tMap1_2_1.put("sort","1");
            //tMap1_2.put("saleNumSpecialtys",tMap1_2_1);
            tList1_2.add(tMap1_2_1);

            Map<String,Object> tMap1_2_2 = new HashMap<String,Object>();
            tMap1_2_2.put("name","民宿");
            tMap1_2_2.put("value",saleNumHomeStays);
            //tMap1_2_2.put("sort","2");
           // tMap1_2.put("saleNumHomeStays",tMap1_2_2);
            tList1_2.add(tMap1_2_2);


            Map<String,Object> tMap1_2_3 = new HashMap<String,Object>();
            tMap1_2_3.put("name","门票");
            tMap1_2_3.put("value",saleNumTickets);
            //tMap1_2_3.put("sort","3");
            //tMap1_2.put("saleNumTickets",tMap1_2_3);
            tList1_2.add(tMap1_2_3);

            Map<String,Object> tMap1_2_4 = new HashMap<String,Object>();
            tMap1_2_4.put("name","合计");
            tMap1_2_4.put("value",saleNumTotal);
            //tMap1_2_4.put("sort","4");
            //tMap1_2.put("saleNumTotal",tMap1_2_4);
            tList1_2.add(tMap1_2_4);
           // rMap.put("yesterDaySaleNum",tMap1_2);
            rMap.put("yesterDaySaleNum",tList1_2);


            Map<String,Object> tMap1_3_1 = new HashMap<String,Object>();
            tMap1_3_1.put("name","昨日新增");
            tMap1_3_1.put("value",personDayNum);
            //tMap1_2_4.put("sort","4");
            //tMap1_2.put("saleNumTotal",tMap1_2_4);
            tList1_3.add(tMap1_3_1);
           // tMap1_3.put("personDayNum",personDayNum);

            Map<String,Object> tMap1_3_2 = new HashMap<String,Object>();
            tMap1_3_2.put("name","平台累计");
            tMap1_3_2.put("value",personSumNum);
            //tMap1_2_4.put("sort","4");
            //tMap1_2.put("saleNumTotal",tMap1_2_4);
            tList1_3.add(tMap1_3_2);
           // tMap1_3.put("personSumNum",personSumNum);
           // rMap.put("yesterDayPersonNum",tMap1_3);
            rMap.put("yesterDayPersonNum",tList1_3);

           // Map<String,Object> tMap2_1 = new HashMap<String,Object>();//top5销售数量
           // Map<String,Object> tMap2_2 = new HashMap<String,Object>();//top5销售金额
            List<Map<String,Object>> tList2_1 =  new ArrayList<Map<String,Object>>();//top5销售数量
            List<Map<String,Object>> tList2_2 =  new ArrayList<Map<String,Object>>();//top5销售金额

            Map<String,Object> tMap2_1_1 = new HashMap<String,Object>();
            tMap2_1_1.put("name","土特产");
            tMap2_1_1.put("top5",rList2);
            //tMap2_1.put("top5SaleNumSpecialty",rList2);
            tList2_1.add(tMap2_1_1);


            Map<String,Object> tMap2_1_2 = new HashMap<String,Object>();
            tMap2_1_2.put("name","民宿");
            tMap2_1_2.put("top5",rList4);
            //tMap2_1.put("top5SaleNumSpecialty",rList2);
            tList2_1.add(tMap2_1_2);
            //tMap2_1.put("top5SaleNumHomeStay",rList4);


            Map<String,Object> tMap2_1_3 = new HashMap<String,Object>();
            tMap2_1_3.put("name","门票");
            tMap2_1_3.put("top5",rList6);
            //tMap2_1.put("top5SaleNumSpecialty",rList2);
            tList2_1.add(tMap2_1_3);
            //tMap2_1.put("top5SaleNumTicket",rList6);



            Map<String,Object> tMap2_2_1 = new HashMap<String,Object>();
            tMap2_2_1.put("name","土特产");
            tMap2_2_1.put("top5",rList1);
            //tMap2_1.put("top5SaleNumSpecialty",rList2);
            tList2_2.add(tMap2_2_1);
           // tMap2_2.put("top5SaleMoneySpecialty",rList1);


            Map<String,Object> tMap2_2_2 = new HashMap<String,Object>();
            tMap2_2_2.put("name","民宿");
            tMap2_2_2.put("top5",rList3);
            //tMap2_1.put("top5SaleNumSpecialty",rList2);
            tList2_2.add(tMap2_2_2);
           // tMap2_2.put("top5SaleMoneyHomeStay",rList3);


            Map<String,Object> tMap2_2_3 = new HashMap<String,Object>();
            tMap2_2_3.put("name","门票");
            tMap2_2_3.put("top5",rList5);
            //tMap2_1.put("top5SaleNumSpecialty",rList2);
            tList2_2.add(tMap2_2_3);
            //tMap2_2.put("top5SaleMoneyTicket",rList5);

            rMap.put("top5SaleNum",tList2_1);
            rMap.put("top5SaleMoney",tList2_2);

            response = response.success(rMap);
            response.setErrorCode("1001");
            response.setSuccess(true);
            response.setErrorMsg("查询成功！");

        }catch(Exception e) {
            logger.error(e.getMessage(),e);
            response = RestResponse.failed("0000","系统异常");
        }
        return response;
    }




}
