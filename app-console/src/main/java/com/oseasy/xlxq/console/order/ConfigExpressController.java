package com.oseasy.xlxq.console.order;

import com.oseasy.xlxq.service.api.exceptions.ErrorConstant;
import com.oseasy.xlxq.service.api.order.model.ConfigExpress;
import com.oseasy.xlxq.service.api.order.service.ConfigExpressService;
import com.oseasy.xlxq.service.core.utils.HqlUtil;
import com.oseasy.xlxq.service.core.utils.StringUtil;
import com.oseasy.xlxq.service.web.rest.RestResponse;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

/**
 * Created by heckman on 2017/5/26.
 */

@RestController
@RequestMapping("/console/express")
public class ConfigExpressController {

    private static final Logger logger = LoggerFactory.getLogger(ConfigExpressController.class);

    @Autowired
    private ConfigExpressService configExpressService;

    /**
     * 模糊查找物流公司（用于页面自动补全物流公司）
     *
     * @return
     */
    @RequestMapping(value = "/auto_complete")
    public RestResponse<List<ConfigExpress>> list(String name, String code) {
        RestResponse<List<ConfigExpress>> restResponse = new RestResponse<List<ConfigExpress>>();
        try {
            String hql = "from ConfigExpress obj ";
            //模糊搜索name
            if (StringUtil.isNotBlank(name)) {
                hql = HqlUtil.addCondition(hql, "name", name, HqlUtil.LOGIC_AND, HqlUtil.TYPE_STRING_LIKE, null);
            }
            //模糊搜索code
            if (StringUtil.isNotBlank(code)) {
                hql = HqlUtil.addCondition(hql, "code", code, HqlUtil.LOGIC_AND, HqlUtil.TYPE_STRING_LIKE, null);
            }

            Iterator<ConfigExpress> iterator = configExpressService.list(hql).iterator();
            List<ConfigExpress> list = new ArrayList<ConfigExpress>();
            while (iterator.hasNext()) {
                list.add(iterator.next());
            }

            restResponse.setSuccess(true);
            restResponse.setData(list);

        } catch (Exception ex) {
            logger.error("系统异常：", ex);
            restResponse.setSuccess(false);
            restResponse.setErrorCode(ErrorConstant.INNER_ERROR_CODE);
            restResponse.setErrorMsg(ErrorConstant.getErrMsg(ErrorConstant.INNER_ERROR_CODE));
        }
        return restResponse;
    }


}
