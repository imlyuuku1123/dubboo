package com.oseasy.xlxq.console.security;

import com.oseasy.xlxq.service.api.tenant.model.TenantAccount;
import com.oseasy.xlxq.service.api.tenant.service.TenantAccountService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.InitializingBean;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.MessageSource;
import org.springframework.context.MessageSourceAware;
import org.springframework.context.support.MessageSourceAccessor;
import org.springframework.security.authentication.AuthenticationProvider;
import org.springframework.security.authentication.BadCredentialsException;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.AuthenticationException;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.SpringSecurityMessageSource;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.stereotype.Component;
import org.springframework.util.Assert;

import java.util.*;

/**
 * Created by liups on 2016/6/21.
 * 自定义的校验过滤器
 */
@Component
public class UserNamePasswordAuthenticationProvider implements AuthenticationProvider, InitializingBean, MessageSourceAware {

    private static final Logger logger = LoggerFactory.getLogger(UserNamePasswordAuthenticationProvider.class);


    protected MessageSourceAccessor messages = SpringSecurityMessageSource.getAccessor();

    @Autowired
    private TenantAccountService tenantAccountService;

    @Override
    public void afterPropertiesSet() throws Exception {
        //属性设置后的一些处理及检查
        Assert.notNull(this.messages, "A message source must be set");
    }

    @Override
    public Authentication authenticate(Authentication authentication) throws AuthenticationException {
        Assert.isInstanceOf(UsernamePasswordAuthenticationToken.class, authentication,
                messages.getMessage(
                        "AbstractUserDetailsAuthenticationProvider.onlySupports",
                        "Only UsernamePasswordAuthenticationToken is supported"));


        // Determine username
        String username = (authentication.getPrincipal() == null) ? "NONE_PROVIDED"
                : authentication.getName();
        Object password = authentication.getCredentials();

        TenantAccount tenantAccount = null;
        try {
            tenantAccount = tenantAccountService.findAccountByUserNameAndPassword(username,password.toString());
        } catch (Exception e) {
            logger.error("账号密码错误异常：" + username +":" + password,e);
        }

        if (null == tenantAccount) {
            //这个类不要return null,以异常的形式处理结果
            throw new BadCredentialsException("账号密码错误,或账号被锁定");
        } else {
            return createSuccessAuthentication(username, authentication,roles("ROLE_TENANT_USER"));
        }
    }

    protected Authentication createSuccessAuthentication(Object principal,
                                                         Authentication authentication, Collection<? extends GrantedAuthority> authorities) {
        UsernamePasswordAuthenticationToken result = new UsernamePasswordAuthenticationToken(
                principal, authentication.getCredentials(),authorities);
        return result;
    }

    private List<GrantedAuthority> roles(String... roles) {
        List<GrantedAuthority> authorities = new ArrayList<>(
                roles.length);
        for (String role : roles) {
            authorities.add(new SimpleGrantedAuthority( role));
        }
        return authorities;
    }


    @Override
    public boolean supports(Class<?> authentication) {
        //认证前的检查
        return (UsernamePasswordAuthenticationToken.class
                .isAssignableFrom(authentication));
    }

    @Override
    public void setMessageSource(MessageSource messageSource) {
        this.messages = new MessageSourceAccessor(messageSource);
    }
}
