package com.oseasy.xlxq.console.order;

import com.alibaba.fastjson.JSONObject;
import com.oseasy.xlxq.console.base.AbstractPortalController;
import com.oseasy.xlxq.service.api.exceptions.ErrorConstant;
import com.oseasy.xlxq.service.api.order.model.*;
import com.oseasy.xlxq.service.api.order.service.*;
import com.oseasy.xlxq.service.api.sys.service.TenantUserAddrsService;
import com.oseasy.xlxq.service.core.utils.HqlUtil;
import com.oseasy.xlxq.service.core.utils.JSONUtil;
import com.oseasy.xlxq.service.core.utils.Page;
import com.oseasy.xlxq.service.core.utils.StringUtil;
import com.oseasy.xlxq.service.web.rest.RestResponse;
import org.apache.commons.lang.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import javax.servlet.http.HttpServletResponse;
import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Date;
import java.util.Iterator;
import java.util.List;

/**
 * Created by heckman on 2017/5/19.
 */

@RequestMapping("/console/orderinfo")
@RestController
public class OrderInfoController extends AbstractPortalController {

    private static final Logger logger = LoggerFactory.getLogger(OrderInfoController.class);

    @Autowired
    private OrderInfoService orderInfoService;

    @Autowired
    private OrderProductService orderProductService;

    @Autowired
    private TenantUserAddrsService tenantUserAddrsService;

    @Autowired
    private OrderLogisticsService orderLogisticsService;

    @Autowired
    private OrderRefundService orderRefundService;

    @Autowired
    private HomeStayRoomReserveService homeStayRoomReserveService;


    @RequestMapping("/list")
    public RestResponse<Page<OrderInfo>> list(@RequestParam(defaultValue = "1") Integer pageNo,
                                              @RequestParam(defaultValue = "20") Integer pageSize,
                                              String orderNum,
                                              String telephone,
                                              String merchantId,
                                              String orderStartTime,
                                              String orderEndTime,
                                              String orderStatus,
                                              String payStartTime,
                                              String payEndTime,
                                              @RequestParam String orderType) {

        RestResponse<Page<OrderInfo>> restResponse = new RestResponse<Page<OrderInfo>>();
        try {
            String hql = " from OrderInfo obj ";
            //根据订单号查询
            if (StringUtil.isNotBlank(orderNum)) {
                //hql = HqlUtil.addCondition(hql, "orderNum", orderNum);
                hql = HqlUtil.addCondition(hql, "orderNum", orderNum, HqlUtil.LOGIC_AND, HqlUtil.TYPE_STRING_LIKE, null);
            }
            //根据订单人telephone查询
            if (StringUtil.isNotBlank(telephone)) {
                //hql = HqlUtil.addCondition(hql, "tenantUser.telephone", telephone);
                hql = HqlUtil.addCondition(hql, "tenantUser.telephone", telephone, HqlUtil.LOGIC_AND, HqlUtil.TYPE_STRING_LIKE, null);
            }
            //根据商户id查询
            if (StringUtil.isNotBlank(merchantId)) {
                hql = HqlUtil.addCondition(hql, "tenantMerchant.id", merchantId);
            }
            //根据订单状态查询
            if (StringUtil.isNotBlank(orderStatus)) {
                //hql = HqlUtil.addCondition(hql, "orderStatus", orderStatus);
                hql = HqlUtil.addCondition(hql, "orderStatus", orderStatus, HqlUtil.LOGIC_AND, HqlUtil.TYPE_IN, "");
            }
            //根据下单起止时间查询
            if (StringUtil.isNotBlank(orderStartTime) && StringUtils.isNotBlank(orderEndTime)) {
                hql = HqlUtil.addBetweenCondition(hql, "createTime", orderStartTime, orderEndTime);
            }
            //根据支付起止时间查询
            if (StringUtil.isNotBlank(payStartTime) && StringUtils.isNotBlank(payEndTime)) {
                hql = HqlUtil.addBetweenCondition(hql, "payTime", payStartTime, payEndTime);
            }
            //查询订单类型1：土特产；2：民宿；3：门票
            if (StringUtil.isNotBlank(orderType)) {
                hql = HqlUtil.addCondition(hql, "orderType", orderType);
            }
            hql = HqlUtil.addOrder(hql,"createTime","desc");
            Page<OrderInfo> page = orderInfoService.pageList(hql, pageNo, pageSize);
            restResponse.setSuccess(true);
            restResponse.setData(page);
        } catch (Exception ex) {
            logger.error("系统异常：", ex);
            restResponse.setSuccess(false);
            restResponse.setErrorCode(ErrorConstant.INNER_ERROR_CODE);
            restResponse.setErrorMsg(ErrorConstant.getErrMsg(ErrorConstant.INNER_ERROR_CODE));
        }
        return restResponse;
    }


    /**
     * 查询订单详细信息（土特产，门票，民宿）
     *
     * @param orderId
     * @return
     */
    @RequestMapping("/detail/{orderId}")
    public RestResponse<JSONObject> detail(@PathVariable String orderId) {
        RestResponse<JSONObject> restResponse = new RestResponse<JSONObject>();
        try {
            JSONObject data = new JSONObject();
            //查询订单信息
            OrderInfo orderInfo = orderInfoService.findById(orderId);

            //查询土特产订单商品信息
            data.put("productList", JSONObject.parse(this.getOrderProList(orderInfo.getOrderNum())));
            //土特产订单，查询订单下所有产品信息
            if (OrderInfo.TYPE_SPECIALTY.equals(orderInfo.getOrderType())) {
                //查询物流单号信息
                String hql_log = "from OrderLogistics obj ";
                hql_log = HqlUtil.addCondition(hql_log, "orderInfo.orderNum", orderInfo.getOrderNum());
                OrderLogistics orderLogistics = orderLogisticsService.findUnique(hql_log);
                String orderLogisticStr = JSONUtil.objectToJson(orderLogistics, new String[]{
                        "deliverNum", "deliverCompany"
                });
                data.put("orderLogistics", JSONObject.parse(orderLogisticStr));
            } else if (OrderInfo.TYPE_HOMESTATY.equals(orderInfo.getOrderType())) {
                //民宿订单，查询预定记录
                String homestay_hql = "from HomeStayRoomReserve obj ";
                homestay_hql = HqlUtil.addCondition(homestay_hql, "orderInfo.orderNum", orderInfo.getOrderNum());
                Iterator<HomeStayRoomReserve> iterator = homeStayRoomReserveService.list(homestay_hql).iterator();
                List<HomeStayRoomReserve> list = new ArrayList<HomeStayRoomReserve>();
                while (iterator.hasNext()) {
                    list.add(iterator.next());
                }
                data.put("homestayList", list);
            }

            //查询退款信息
            String hql_orderrefund = "from OrderRefund obj ";
            hql_orderrefund = HqlUtil.addCondition(hql_orderrefund, "orderInfo.id", orderInfo.getId());
            OrderRefund orderRefund = orderRefundService.findUnique(hql_orderrefund);
            data.put("orderRefund", orderRefund);

            //查询订单商品信息
            data.put("orderInfo", orderInfo);

            restResponse.setSuccess(true);
            restResponse.setData(data);

        } catch (Exception ex) {
            logger.error("系统异常：", ex);
            restResponse.setSuccess(false);
            restResponse.setErrorCode(ErrorConstant.INNER_ERROR_CODE);
            restResponse.setErrorMsg(ErrorConstant.getErrMsg(ErrorConstant.INNER_ERROR_CODE));
        }
        return restResponse;
    }

    /**
     * 根据订单号查询订单商品信息
     *
     * @param orderNum
     * @return
     */
    public String getOrderProList(String orderNum) {
        String order_hql = "from OrderProduct obj ";
        order_hql = HqlUtil.addCondition(order_hql, "orderInfo.orderNum", orderNum);
        List<OrderProduct> list = orderProductService.getList(order_hql);
        return JSONUtil.listToJson(list, new String[]{
                "purchaseNum", "tenantProduct", "productName", "productType","distributionType"});
    }

    /**
     * 取消订单/关闭订单接口
     *
     * @param id
     * @return
     */
    @RequestMapping("/cancel_order/{id}")
    public RestResponse cancelOrder(@PathVariable String id) {
        RestResponse restResponse = new RestResponse();
        try {
            OrderInfo orderInfo = orderInfoService.findById(id);
            //只能在待支付状态下才能取消订单
            if (OrderStatus.PENDING_PAY_CODE.equals(orderInfo.getOrderStatus())) {
                orderInfo.setOrderStatus(OrderStatus.TRADE_CLOSE_CODE);
                orderInfoService.save(orderInfo);
                restResponse.setSuccess(true);
            } else {
                restResponse.setSuccess(false);
                restResponse.setErrorCode(ErrorConstant.CANNOT_CANCEL_ORDER_CODE);
                restResponse.setErrorMsg(ErrorConstant.getErrMsg(ErrorConstant.CANNOT_CANCEL_ORDER_CODE));
            }
        } catch (Exception ex) {
            logger.error("系统异常：", ex);
            restResponse.setSuccess(false);
            restResponse.setErrorCode(ErrorConstant.INNER_ERROR_CODE);
            restResponse.setErrorMsg(ErrorConstant.getErrMsg(ErrorConstant.INNER_ERROR_CODE));
        }
        return restResponse;
    }

    /**
     * 确认房间订单
     *
     * @param orderId
     * @return
     */
    @RequestMapping("/confirm_homestay")
    public RestResponse confirmHomestay(@RequestParam String orderId) {
        RestResponse restResponse = new RestResponse();
        try {
            OrderInfo orderInfo = orderInfoService.findById(orderId);
            orderInfo.setOrderStatus(OrderStatus.TRADE_SUCCESS_CODE);
            orderInfoService.save(orderInfo);
            restResponse.setSuccess(true);
        } catch (Exception ex) {
            logger.error("系统异常：", ex);
            restResponse.setSuccess(false);
            restResponse.setErrorCode(ErrorConstant.INNER_ERROR_CODE);
            restResponse.setErrorMsg(ErrorConstant.getErrMsg(ErrorConstant.INNER_ERROR_CODE));
        }
        return restResponse;
    }

    @RequestMapping("/excel")
    public void exportExcel(String orderNum,
                            String telephone,
                            String merchantId,
                            String orderStartTime,
                            String orderEndTime,
                            String orderStatus,
                            String payStartTime,
                            String payEndTime,
                            @RequestParam String orderType, HttpServletResponse response) {
        try {
            String hql = " from OrderInfo obj ";
            //根据订单号查询
            if (StringUtil.isNotBlank(orderNum)) {
                //hql = HqlUtil.addCondition(hql, "orderNum", orderNum);
                hql = HqlUtil.addCondition(hql, "orderNum", orderNum, HqlUtil.LOGIC_AND, HqlUtil.TYPE_STRING_LIKE, null);
            }
            //根据订单人telephone查询
            if (StringUtil.isNotBlank(telephone)) {
                //hql = HqlUtil.addCondition(hql, "tenantUser.telephone", telephone);
                hql = HqlUtil.addCondition(hql, "tenantUser.telephone", telephone, HqlUtil.LOGIC_AND, HqlUtil.TYPE_STRING_LIKE, null);
            }
            //根据商户id查询
            if (StringUtil.isNotBlank(merchantId)) {
                hql = HqlUtil.addCondition(hql, "tenantMerchant.id", merchantId);
            }
            //根据订单状态查询
            if (StringUtil.isNotBlank(orderStatus)) {
                //hql = HqlUtil.addCondition(hql, "orderStatus", orderStatus);
                hql = HqlUtil.addCondition(hql, "orderStatus", orderStatus, HqlUtil.LOGIC_AND, HqlUtil.TYPE_IN, "");

            }
            //根据下单起止时间查询
            if (StringUtil.isNotBlank(orderStartTime) && StringUtils.isNotBlank(orderEndTime)) {
                hql = HqlUtil.addBetweenCondition(hql, "createTime", orderStartTime, orderEndTime);
            }
            //根据支付起止时间查询
            if (StringUtil.isNotBlank(payStartTime) && StringUtils.isNotBlank(payEndTime)) {
                hql = HqlUtil.addBetweenCondition(hql, "payTime", payStartTime, payEndTime);
            }
            //查询订单类型1：土特产；2：民宿；3：门票
            if (StringUtil.isNotBlank(orderType)) {
                hql = HqlUtil.addCondition(hql, "orderType", orderType);
            }
            hql = HqlUtil.addOrder(hql,"createTime","");
            List<OrderInfo> page = orderInfoService.getList(hql);
            List<OrderInfoExcel> list = new ArrayList<OrderInfoExcel>();
            if (page.size() > 0) {
                OrderInfoExcel oix;
                for (OrderInfo oi : page) {
                    oix = new OrderInfoExcel();
                    oix.setOrderNum(oi.getOrderNum());
                    oix.setMerchantName(oi.getTenantMerchant() == null ? "" : oi.getTenantMerchant().getMerchantName());
                    oix.setName(oi.getTenantUser() == null ? "" : oi.getTenantUser().getName());
                    oix.setTelephone(oi.getTenantUser() == null ? "" : oi.getTenantUser().getTelephone());
                    oix.setCreateTime(oi.getCreateTime());
                    oix.setLastTime(oi.getLastTime());
                    oix.setExtHomestayPop1(oi.getExtHomestayPop1());
                    oix.setExtTicketPop1(oi.getExtTicketPop1());
                    if ("1".equals(oi.getOrderType())) {
                        oix.setPayType("微信支付");
                    } else {
                        oix.setPayType("其他支付");
                    }
                    oix.setOrderStatus(oi.getOrderStatus() == null ? "" : OrderStatus.getOrderDescription(oi.getOrderStatus()));
                    oix.setOrderSum(oi.getOrderSum().setScale(2, BigDecimal.ROUND_HALF_UP)+"");
                    list.add(oix);
                }
            }

            String title = null;
            String[] headers = null;
            String[] values = null;
            if (OrderInfo.TYPE_SPECIALTY.equals(orderType)) {
                title = "土特产订单";
                headers = new String[]{"订单号", "商户", "姓名", "电话", "下单时间", "支付方式", "金额", "订单状态", "时间"};
                values = new String[]{"orderNum", "merchantName", "name", "telephone", "createTime", "payType", "orderSum", "orderStatus", "lastTime"};
            } else if (OrderInfo.TYPE_HOMESTATY.equals(orderType)) {
                title = "民宿订单";
                headers = new String[]{"订单号", "商户", "景区", "姓名", "电话", "下单时间", "支付方式", "金额", "订单状态", "时间"};
                values = new String[]{"orderNum", "merchantName", "extHomestayPop1", "name", "telephone", "createTime", "payType", "orderSum", "orderStatus", "lastTime"};
            } else if (OrderInfo.TYPE_TICKET.equals(orderType)) {
                title = "门票订单";
                headers = new String[]{"订单号", "商户", "景区", "姓名", "电话", "下单时间", "支付方式", "金额", "订单状态", "时间"};
                values = new String[]{"orderNum", "merchantName", "extTicketPop1", "name", "telephone", "createTime", "payType", "orderSum", "orderStatus", "lastTime"};
            }
            downloadExcel(title, null, headers, values, list, null, null, response);
        } catch (Exception ex) {
            logger.error("系统异常：", ex);
        }

    }

    /**
     * 支付接口，仅用于测试
     * @param id
     * @return
     */
    @RequestMapping("/pay")
    public RestResponse pay(@RequestParam String id){
        RestResponse restResponse=new RestResponse();
        try{
            OrderInfo orderInfo = orderInfoService.findById(id);
            orderInfo.setOrderStatus(OrderStatus.PENDING_SHIP_CODE);
            orderInfo.setPayTime(new Date());
            orderInfo.setLastTime(new Date());
            orderInfoService.save(orderInfo);
            restResponse.setSuccess(true);
        }catch (Exception ex){
            logger.error("系统异常：", ex);
            restResponse.setSuccess(false);
            restResponse.setErrorCode(ErrorConstant.INNER_ERROR_CODE);
            restResponse.setErrorMsg(ErrorConstant.getErrMsg(ErrorConstant.INNER_ERROR_CODE));
        }
        return restResponse;
    }


}
