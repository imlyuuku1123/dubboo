package com.oseasy.xlxq.console.exceptions;

import com.oseasy.xlxq.console.base.APIModelAndView;
import com.oseasy.xlxq.service.api.exceptions.ErrorConstant;
import com.oseasy.xlxq.service.api.exceptions.XlxqRuntimeException;
import com.oseasy.xlxq.service.web.rest.RestResponse;
import org.apache.commons.lang.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Component;
import org.springframework.web.multipart.MaxUploadSizeExceededException;
import org.springframework.web.servlet.HandlerExceptionResolver;
import org.springframework.web.servlet.ModelAndView;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 * Created by tandy on 17/5/24.
 * 异常统一处理
 */
@Component
public class SystemExceptionHandler implements HandlerExceptionResolver {
    public static final String ERROR_500 = "/500.jsp";


    private static final Logger logger = LoggerFactory.getLogger(SystemExceptionHandler.class);
    public ModelAndView resolveException(HttpServletRequest request,
                                         HttpServletResponse response, Object handler, Exception ex) {
        logger.error("出现异常，将进行统一处理：",ex);
        ModelAndView mav = null;
        //由于文件上传超出大小异常如果在此拦截会导致无法输出返回数据到前台，原因暂时不明，交由500.jsp直接处理
        if(ex instanceof MaxUploadSizeExceededException){
            return null;
        }
        //Ajax请求带有X-Requested-With:XMLHttpRequest
        String xRequestedWith = request.getHeader("X-Requested-With");
        if (StringUtils.isNotBlank(xRequestedWith) && "XMLHttpRequest".equals(xRequestedWith)) {
            String result;
            RestResponse apiResponse = null;
            if(ex instanceof XlxqRuntimeException){
                XlxqRuntimeException xlxqex = (XlxqRuntimeException) ex;
                apiResponse = RestResponse.failed((xlxqex.getCode()),xlxqex.getMsg());
            }else{
                apiResponse = RestResponse.failed(ErrorConstant.INNER_ERROR.getErrCode(),ex.getMessage());
            }
            mav = new ModelAndView(APIModelAndView.APIVIEW);
            mav.addObject("data",apiResponse);

        }else{
            mav = new ModelAndView("error_page");
        }
        return mav;
    }
}
