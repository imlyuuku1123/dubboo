package com.oseasy.xlxq.console.security;

import com.oseasy.xlxq.service.core.web.SpringContextUtil;
import com.oseasy.xlxq.service.web.security.SecurityUserRepository;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import javax.servlet.annotation.WebFilter;

/**
 * Created by Tandy on 2016/6/8.
 */
@WebFilter(filterName = "securityFilter", urlPatterns = "/console/*",
        asyncSupported = true //支持异步Servlet
)
public class SecurityFilter extends com.oseasy.xlxq.service.web.security.SecurityFilter {
    private static final Log logger = LogFactory.getLog(SecurityFilter.class);

    @Override
    public SecurityUserRepository getSecurityUserRepository() {
        return SpringContextUtil.getApplicationContext().getBean(SecurityUserRepository.class);
    }
}
