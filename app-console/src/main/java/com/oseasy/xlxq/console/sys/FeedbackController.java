package com.oseasy.xlxq.console.sys;

import com.oseasy.xlxq.console.base.AbstractPortalController;
import com.oseasy.xlxq.console.comm.ConsoleConstants;
import com.oseasy.xlxq.service.api.sys.model.FeedBack;
import com.oseasy.xlxq.service.api.sys.service.FeedBackService;
import com.oseasy.xlxq.service.core.utils.*;
import com.oseasy.xlxq.service.web.rest.RestResponse;
import net.sf.json.JSONObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpServletRequest;
import java.util.Date;

/**
 *意见反馈
 */
@RestController
@RequestMapping("/console/feedback")
public class FeedbackController extends AbstractPortalController {

    @Autowired
    private FeedBackService feedBackService;

    /**
     * 获取实体信息
     * @param id
     * @return
     */
    @RequestMapping("/{id}")
    public RestResponse getEntityById(@PathVariable("id") String id) {
        RestResponse restResponse = new RestResponse();
        try {
            FeedBack feedBack = feedBackService.findById(id);
            restResponse.setData(feedBack);
            restResponse.setSuccess(true);
            restResponse.setErrorCode("1001");
            restResponse.setErrorMsg("操作成功");
        } catch (Exception e) {
            logger.error(e.getMessage(),e);
            restResponse.setSuccess(false);
            restResponse.setErrorCode("0000");
            restResponse.setErrorMsg("操作异常");
        }
        return restResponse;
    }


    /**
     * 保存实体
     * @param
     * @return
     */
    @RequestMapping("/save")
    public RestResponse save(@RequestBody FeedBack feedBack) {
        RestResponse restResponse = new RestResponse();
        try {
            if("1".equals(feedBack.getIsReply())){
                feedBack.setFeedbackStatus("1");
                feedBack.setReplyTime(new Date());
            }
            if(StringUtil.isNotBlank(feedBack.getId())){
                feedBackService.update(feedBack.getId(),feedBack);
            }else {
                feedBackService.save(feedBack);
            }
            restResponse.setSuccess(true);
            restResponse.setErrorCode("1001");
            restResponse.setErrorMsg("操作成功");
        } catch (Exception e) {
            logger.error(e.getMessage(),e);
            restResponse.setSuccess(false);
            restResponse.setErrorCode("0000");
            restResponse.setErrorMsg("操作异常");
        }
        return restResponse;
    }


    /**
     * 删除
     * @param id
     * @return
     */
    @RequestMapping("/delete/{id}")
    public RestResponse delete(@PathVariable("id") String id) {
        RestResponse restResponse = new RestResponse();
        try {
            feedBackService.delete(id);
            restResponse.setSuccess(true);
            restResponse.setErrorCode("1001");
            restResponse.setErrorMsg("操作成功");
        } catch (Exception e) {
            logger.error(e.getMessage(),e);
            restResponse.setSuccess(false);
            restResponse.setErrorCode("0000");
            restResponse.setErrorMsg("操作异常");
        }
        return restResponse;
    }


    /**
     * 查询列表
     * @param
     * @param pageNo
     * @param pageSize
     * @return
     */
    @RequestMapping("/list")
    public RestResponse getPageList(@RequestParam(defaultValue = "1") Integer pageNo,
                                    @RequestParam(defaultValue = "20") Integer pageSize, HttpServletRequest request) {
        RestResponse restResponse = new RestResponse();
        try {
            String hql = "from FeedBack obj";
            String phone = request.getParameter("phone");
            String timeStart = request.getParameter("timeStart");
            String timeEnd = request.getParameter("timeEnd");
            if (StringUtil.isNotBlank(phone)) {
                hql = HqlUtil.addCondition(hql, "tenantUser.telephone", phone,HqlUtil.LOGIC_AND,HqlUtil.TYPE_STRING_LIKE,null);
            }
            if (StringUtil.isNotBlank(timeStart)) {
                hql = HqlUtil.addCondition(hql, "createTime", timeStart,HqlUtil.LOGIC_AND,HqlUtil.TYPE_DATE,HqlUtil.COMPARECHAR_GREAT_EQ);
            }
            if (StringUtil.isNotBlank(timeEnd)) {
                hql = HqlUtil.addCondition(hql, "createTime", timeEnd,HqlUtil.LOGIC_AND,HqlUtil.TYPE_DATE,HqlUtil.COMPARECHAR_LESS_EQ);
            }
            Page page = feedBackService.pageList(hql,pageNo,pageSize);
            restResponse.setData(page);
            restResponse.setSuccess(true);
            restResponse.setErrorCode("1001");
            restResponse.setErrorMsg("操作成功");
        } catch (Exception e) {
            logger.error(e.getMessage(),e);
            restResponse.setSuccess(false);
            restResponse.setErrorCode("0000");
            restResponse.setErrorMsg("操作异常");
        }
        return restResponse;
    }

    /**
     * 批量操作
     * @param
     * @return
     */
    @RequestMapping(value = "/batchoperate",method = RequestMethod.POST)
    public RestResponse batchoperate(@RequestBody JSONObject jsonObj) {
        RestResponse restResponse = new RestResponse();
        String ids =  jsonObj.getString("ids");
        String type =  jsonObj.getString("type");
        try {
            if (StringUtil.isNotBlank(ids)&&StringUtil.isNotBlank(type)) {
                String [] idArray = ids.split(",");
                if (ConsoleConstants.OPERATE_TYPE_DELETE.equals(type)) {
                    feedBackService.updateByArray("deleted", ConsoleConstants.DELETE_FLAG, "id", idArray);
                }
            }else{
                return RestResponse.failed("0000","请求参数错误");
            }
            restResponse.setSuccess(true);
            restResponse.setErrorCode("1001");
            restResponse.setErrorMsg("操作成功");
        } catch (Exception e) {
            logger.error(e.getMessage(),e);
            restResponse.setSuccess(false);
            restResponse.setErrorCode("0000");
            restResponse.setErrorMsg("操作异常");
        }
        return restResponse;
    }

}
