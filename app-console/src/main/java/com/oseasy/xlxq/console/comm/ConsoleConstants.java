package com.oseasy.xlxq.console.comm;

import com.oseasy.xlxq.service.config.SystemConfig;

/**
 * Created by liups on 2016/6/16.
 */
public class ConsoleConstants {

    //验证码保存在session里的key
    public static final String VC_KEY = "validateCode";

    //手机验证码保存在session里的key
    public static final String MC_KEY = "mobileCodeChecker";

    //请求前缀
    public static final String REST_PREFIX_URL = SystemConfig.getProperty("console.rest.api.url","http://localhost:8080");

   //保存在session里的token的key
   public static final String SSO_TOKEN = SystemConfig.getProperty("global.rest.api.security.header","X-OSEASY-API-TOKEN");

   public static final String BACK_INDEX_URL = SystemConfig.getProperty("global.back.index.url","");

   public static final String OPERATE_TYPE_DELETE = "1";

    public static final String  DELETE_FLAG = "1";


}
