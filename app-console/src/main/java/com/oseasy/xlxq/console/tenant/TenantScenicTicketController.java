package com.oseasy.xlxq.console.tenant;

import com.oseasy.xlxq.console.base.AbstractPortalController;
import com.oseasy.xlxq.console.comm.ConsoleConstants;
import com.oseasy.xlxq.service.api.exceptions.ErrorConstant;
import com.oseasy.xlxq.service.api.tenant.model.TenantScenicTicket;
import com.oseasy.xlxq.service.api.tenant.service.TenantScenicTicketService;
import com.oseasy.xlxq.service.core.utils.Page;
import com.oseasy.xlxq.service.core.utils.StringUtil;
import com.oseasy.xlxq.service.web.rest.RestResponse;
import net.sf.json.JSONObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;

/**
 * 门票管理
 */
@RestController
@RequestMapping("/console/scenicticket")
public class TenantScenicTicketController extends AbstractPortalController {

    @Autowired
    private TenantScenicTicketService tenantScenicTicketService;

    /**
     * 获取实体信息
     * @param id
     * @return
     */
    @RequestMapping("/{id}")
    public RestResponse getEntityById(@PathVariable("id") String id) {
        RestResponse restResponse = new RestResponse();
        try {
            TenantScenicTicket tenantScenicTicket = tenantScenicTicketService.findById(id);
            restResponse.setData(tenantScenicTicket);
            restResponse.setSuccess(true);
            restResponse.setErrorCode("1001");
            restResponse.setErrorMsg("操作成功");
        } catch (Exception e) {
            logger.error(e.getMessage(),e);
            restResponse.setSuccess(false);
            restResponse.setErrorCode("0000");
            restResponse.setErrorMsg("操作异常");
        }
        return restResponse;
    }


    /**
     * 保存实体
     * @param
     * @return
     */
    @RequestMapping("/save")
    public RestResponse save(@RequestBody TenantScenicTicket tenantScenicTicket) {
        RestResponse restResponse = new RestResponse();
        try {
            if(StringUtil.isNotBlank(tenantScenicTicket.getId())){
                tenantScenicTicketService.update(tenantScenicTicket.getId(),tenantScenicTicket);
            }else {
                tenantScenicTicketService.save(tenantScenicTicket);
            }
            restResponse.setSuccess(true);
            restResponse.setErrorCode("1001");
            restResponse.setErrorMsg("操作成功");
        } catch (Exception e) {
            logger.error(e.getMessage(),e);
            restResponse.setSuccess(false);
            restResponse.setErrorCode("0000");
            restResponse.setErrorMsg("操作异常");
        }
        return restResponse;
    }


    /**
     * 删除
     * @param id
     * @return
     */
    @RequestMapping("/delete/{id}")
    public RestResponse delete(@PathVariable("id") String id) {
        RestResponse restResponse = new RestResponse();
        try {
            tenantScenicTicketService.delete(id);
            restResponse.setSuccess(true);
            restResponse.setErrorCode("1001");
            restResponse.setErrorMsg("操作成功");
        } catch (Exception e) {
            logger.error(e.getMessage(),e);
            restResponse.setSuccess(false);
            restResponse.setErrorCode("0000");
            restResponse.setErrorMsg("操作异常");
        }
        return restResponse;
    }


    /**
     * 查询列表
     * @param
     * @param pageNo
     * @param pageSize
     * @return
     */
    @RequestMapping("/list")
    public RestResponse getPageList(@RequestParam(defaultValue = "1") Integer pageNo,
                                    @RequestParam(defaultValue = "20") Integer pageSize) {
        RestResponse restResponse = new RestResponse();
        try {
            String hql = "from TenantScenicTicket obj";
           /* String moduleName = module.getModuleName();
            if (StringUtil.isNotBlank(moduleName)) {
                hql = HqlUtil.addCondition(hql, "moduleName", moduleName,null,"Like",null);
            }*/
            Page page = tenantScenicTicketService.pageList(hql,pageNo,pageSize);
            restResponse.setData(page);
            restResponse.setSuccess(true);
            restResponse.setErrorCode("1001");
            restResponse.setErrorMsg("操作成功");
        } catch (Exception e) {
            logger.error(e.getMessage(),e);
            restResponse.setSuccess(false);
            restResponse.setErrorCode("0000");
            restResponse.setErrorMsg("操作异常");
        }
        return restResponse;
    }

    /**
     * 批量操作
     * @param
     * @return
     */
    @RequestMapping(value = "/batchoperate",method = RequestMethod.POST)
    public RestResponse batchoperate(@RequestBody JSONObject jsonObj) {
        RestResponse restResponse = new RestResponse();
        String ids =  jsonObj.getString("ids");
        String type =  jsonObj.getString("type");
        try {
            if (StringUtil.isNotBlank(ids)&&StringUtil.isNotBlank(type)) {
                String [] idArray = ids.split(",");
                if (ConsoleConstants.OPERATE_TYPE_DELETE.equals(type)) {
                    tenantScenicTicketService.updateByArray("deleted", ConsoleConstants.DELETE_FLAG, "id", idArray);
                }
            }else{
                return RestResponse.failed("0000","请求参数错误");
            }
            restResponse.setSuccess(true);
            restResponse.setErrorCode("1001");
            restResponse.setErrorMsg("操作成功");
        } catch (Exception e) {
            logger.error(e.getMessage(),e);
            restResponse.setSuccess(false);
            restResponse.setErrorCode("0000");
            restResponse.setErrorMsg("操作异常");
        }
        return restResponse;
    }

    /**
     * 门票列表展示，非分页
     * @return
     */
    @RequestMapping("/list2")
    public RestResponse list2(){
        RestResponse restResponse=new RestResponse();
        try{
            String hql="from TenantScenicTicket obj ";
            List<TenantScenicTicket> list = tenantScenicTicketService.getList(hql);
            restResponse.setSuccess(true);
            restResponse.setData(list);
        }catch (Exception ex){
            logger.error("系统异常：", ex);
            restResponse.setSuccess(false);
            restResponse.setErrorCode(ErrorConstant.INNER_ERROR_CODE);
            restResponse.setErrorMsg(ErrorConstant.getErrMsg(ErrorConstant.INNER_ERROR_CODE));
        }
        return  restResponse;
    }

}
