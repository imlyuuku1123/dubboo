package com.oseasy.xlxq.console.video;

import com.oseasy.xlxq.service.api.exceptions.ErrorConstant;
import com.oseasy.xlxq.console.order.OrderLogisticsController;
import com.oseasy.xlxq.service.api.tenant.model.TenantProduct;
import com.oseasy.xlxq.service.api.video.model.VideoProduct;
import com.oseasy.xlxq.service.api.video.model.VideoRoom;
import com.oseasy.xlxq.service.api.video.service.VideoProductService;
import com.oseasy.xlxq.service.web.rest.RestResponse;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

/**
 * Created by heckman on 2017/5/24.
 */

@RestController
@RequestMapping("/video_product")
public class VideoProductController {

    private static final Logger logger = LoggerFactory.getLogger(VideoProductController.class);

    @Autowired
    private VideoProductService videoProductService;

    @RequestMapping("/save")
    public RestResponse addProduct(@RequestParam String charoomId, @RequestParam String productIds) {
        RestResponse restResponse = new RestResponse();
        try {
            VideoRoom videoRoom = new VideoRoom();
            videoRoom.setId(charoomId);
            VideoProduct videoProduct = null;
            TenantProduct tenantProduct = null;
            String[] params = new String[]{};
            //添加多个产品
            if (productIds.contains(",")) {
                params = productIds.split(",");
            } else {
                //添加单个产品
                params = new String[]{productIds};
            }
            for (String str : params) {
                tenantProduct = new TenantProduct();
                tenantProduct.setId(str);
                videoProduct = new VideoProduct();
                videoProduct.setVideoRoom(videoRoom);
                videoProduct.setTenantProduct(tenantProduct);
                videoProductService.save(videoProduct);
            }
            restResponse.setSuccess(true);
        } catch (Exception ex) {
            logger.error("系统异常：", ex);
            restResponse.setSuccess(false);
            restResponse.setErrorCode(ErrorConstant.INNER_ERROR_CODE);
            restResponse.setErrorMsg(ErrorConstant.getErrMsg(ErrorConstant.INNER_ERROR_CODE));
        }
        return restResponse;
    }


}
