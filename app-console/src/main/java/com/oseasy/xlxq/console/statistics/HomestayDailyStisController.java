package com.oseasy.xlxq.console.statistics;

import com.alibaba.fastjson.JSONObject;
import com.oseasy.xlxq.console.base.AbstractPortalController;
import com.oseasy.xlxq.service.api.exceptions.ErrorConstant;
import com.oseasy.xlxq.service.api.statistic.model.StatisticHomeStayDay;
import com.oseasy.xlxq.service.api.statistic.model.StatisticHomeStayDayExcel;
import com.oseasy.xlxq.service.api.statistic.model.StatisticHomeStaySellExcel;
import com.oseasy.xlxq.service.api.statistic.model.StatisticSellReport;
import com.oseasy.xlxq.service.api.statistic.service.StatisticHomeStayDayService;
import com.oseasy.xlxq.service.core.utils.HqlUtil;
import com.oseasy.xlxq.service.core.utils.Page;
import com.oseasy.xlxq.service.core.utils.StringUtil;
import com.oseasy.xlxq.service.web.rest.RestResponse;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import javax.servlet.http.HttpServletResponse;
import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by heckman on 2017/6/12.
 */

@RequestMapping("/console/homestay_statistic")
@RestController
public class HomestayDailyStisController extends AbstractPortalController {

    public static final Logger logger = LoggerFactory.getLogger(HomestayDailyStisController.class);

    @Autowired
    private StatisticHomeStayDayService service;

    /**
     * 民宿日统计接口
     *
     * @param merchantId
     * @param scenicId
     * @param roomId
     * @param startTime
     * @param endTime
     * @param pageNo
     * @param pageSize
     * @return
     */
    @RequestMapping("/daily_statistic")
    public RestResponse dailyStatistic(String merchantId, String scenicId, String roomId,
                                       String startTime, String endTime,
                                       @RequestParam(defaultValue = "1") Integer pageNo,
                                       @RequestParam(defaultValue = "20") Integer pageSize) {
        RestResponse restResponse = new RestResponse();
        try {
            String hql = "from StatisticHomeStayDay obj ";
            if (StringUtil.isNotBlank(merchantId)) {
                hql = HqlUtil.addCondition(hql, "tenantMerchant.id", merchantId);
            }
            if (StringUtil.isNotBlank(startTime)) {
                hql = HqlUtil.addCondition(hql, "stasticsDay", startTime, HqlUtil.LOGIC_AND, HqlUtil.TYPE_STRING, HqlUtil.COMPARECHAR_GREAT_EQ);
            }
            if (StringUtil.isNotBlank(endTime)) {
                hql = HqlUtil.addCondition(hql, "stasticsDay", endTime, HqlUtil.LOGIC_AND, HqlUtil.TYPE_STRING, HqlUtil.COMPARECHAR_LESS_EQ);
            }
            if (StringUtil.isNotBlank(scenicId)) {
                hql = HqlUtil.addCondition(hql, "tenantScenic.id", scenicId);
            }
            if (StringUtil.isNotBlank(roomId)) {
                hql = HqlUtil.addCondition(hql, "tenantHomeStayRoom.id", roomId);
            }
            hql = HqlUtil.addOrder(hql, "stasticsDay", "");

            Page<StatisticHomeStayDay> page = service.pageList(hql, pageNo, pageSize);
            restResponse.setData(page);
            restResponse.setSuccess(true);
        } catch (Exception ex) {
            logger.error("系统异常：", ex);
            restResponse.setSuccess(false);
            restResponse.setErrorCode(ErrorConstant.INNER_ERROR_CODE);
            restResponse.setErrorMsg(ErrorConstant.getErrMsg(ErrorConstant.INNER_ERROR_CODE));
        }
        return restResponse;
    }

    @RequestMapping("/excel")
    public void export(HttpServletResponse response,
                       String merchantId, String scenicId, String roomId,
                       String startTime, String endTime) {
        try {
            String hql = "from StatisticHomeStayDay obj ";
            if (StringUtil.isNotBlank(merchantId)) {
                hql = HqlUtil.addCondition(hql, "tenantMerchant.id", merchantId);
            }
            if (StringUtil.isNotBlank(startTime)) {
                hql = HqlUtil.addCondition(hql, "stasticsDay", startTime, HqlUtil.LOGIC_AND, HqlUtil.TYPE_STRING, HqlUtil.COMPARECHAR_GREAT_EQ);
            }
            if (StringUtil.isNotBlank(endTime)) {
                hql = HqlUtil.addCondition(hql, "stasticsDay", endTime, HqlUtil.LOGIC_AND, HqlUtil.TYPE_STRING, HqlUtil.COMPARECHAR_LESS_EQ);
            }
            if (StringUtil.isNotBlank(scenicId)) {
                hql = HqlUtil.addCondition(hql, "tenantScenic.id", scenicId);
            }
            if (StringUtil.isNotBlank(roomId)) {
                hql = HqlUtil.addCondition(hql, "tenantHomeStayRoom.id", roomId);
            }
            hql = HqlUtil.addOrder(hql, "stasticsDay", "");

            List<StatisticHomeStayDay> list1 = service.getList(hql);

            System.out.println(JSONObject.toJSONString(list1));

            List<StatisticHomeStayDayExcel> list = new ArrayList<StatisticHomeStayDayExcel>();
            StatisticHomeStayDayExcel se = null;
            for (StatisticHomeStayDay sd : list1) {
                se = new StatisticHomeStayDayExcel();
                se.setMerchantName(sd.getTenantMerchant()==null?"":sd.getTenantMerchant().getMerchantName());
                se.setRoomType(sd.getTenantHomeStayRoom()==null?"":sd.getTenantHomeStayRoom().getProductName());
                se.setScenicName(sd.getTenantScenic()==null?"":sd.getTenantScenic().getScenicName());
                se.setSellNum(sd.getSaleNum() + "");
                se.setSellSum(sd.getSaleMoney().setScale(2, BigDecimal.ROUND_HALF_UP) + "");
                list.add(se);
            }

            String title = "民宿销量统计";
            String[] headers = new String[]{"景区", "商户", "房型", "销售数量", "销售金额"};
            String[] values = new String[]{"scenicName", "merchantName", "roomType", "sellNum", "sellSum"};
            downloadExcel(title, null, headers, values, list, null, null, response);
        } catch (Exception ex) {
            logger.error("系统异常：", ex);
        }
    }


    @RequestMapping("/report")
    public RestResponse report(String merchantId, String scenicId, String roomId,
                               String startTime, String endTime,
                               @RequestParam(defaultValue = "1") Integer pageNo,
                               @RequestParam(defaultValue = "20") Integer pageSize){
        RestResponse restResponse=new RestResponse();
        try{

            StringBuffer sqlWhere = new StringBuffer("");

            if(StringUtil.isNotBlank(merchantId)){
                sqlWhere.append(" and ttt.merchant_id='"+merchantId+"'");
            }
            if(StringUtil.isNotBlank(scenicId)){
                sqlWhere.append(" and ttt.scenicspots_id='"+scenicId+"'");
            }
            if(StringUtil.isNotBlank(roomId)){
                sqlWhere.append(" and ttt.room_id='"+roomId+"'");
            }
            if(StringUtil.isNotBlank(startTime)){
                sqlWhere.append(" and STR_TO_DATE(ttt.dateStr,'%Y-%m-%d') >= '"+startTime+"' ");
            }
            if(StringUtil.isNotBlank(endTime)){
                sqlWhere.append(" and STR_TO_DATE(ttt.dateStr,'%Y-%m-%d') <= '"+endTime+"' ");
            }

            String sql = "SELECT  " +
                    "  *  " +
                    "FROM  " +
                    "  (  " +
                    "    SELECT  " +
                    "      ttt.dateStr AS dateStr,  " +
                    "      sum(ttt.sale_num) AS sellNum,  " +
                    "      sum(ttt.sale_money) AS sellSum,  " +
                    "      ttp.product_name AS productName,  " +
                    "      tts.scenic_name AS scenicName,  " +
                    "      ttm.merchant_name AS merchantName  " +
                    "    FROM  " +
                    "      (  " +
                    "        SELECT  " +
                    "          CONCAT(  " +
                    "            obj.`year`,  " +
                    "            '-',  " +
                    "            obj.`month`,  " +
                    "            '-',  " +
                    "            obj.`day`  " +
                    "          ) AS dateStr,  " +
                    "          CASE  " +
                    "        WHEN obj.sale_num IS NULL THEN  " +
                    "          0  " +
                    "        ELSE  " +
                    "          obj.sale_num  " +
                    "        END AS sale_num,  " +
                    "        CASE  " +
                    "      WHEN obj.sale_money IS NULL THEN  " +
                    "        0  " +
                    "      ELSE  " +
                    "        obj.sale_money  " +
                    "      END AS sale_money,  " +
                    "      obj.merchant_id,  " +
                    "      obj.scenicspots_id,  " +
                    "      obj.room_id  " +
                    "    FROM  " +
                    "      tb_statistic_homestay_day obj  " +
                    "      ) ttt  " +
                    "    LEFT OUTER JOIN tb_tenant_product ttp ON ttt.room_id = ttp.id  " +
                    "    LEFT OUTER JOIN tb_tenant_scenic tts ON ttt.scenicspots_id = tts.id  " +
                    "    LEFT OUTER JOIN tb_tenant_merchant ttm ON ttt.merchant_id = ttm.id  where 1=1 " + sqlWhere.toString() +
                    "    GROUP BY  " +
                    "      ttt.dateStr,  " +
                    "      ttt.merchant_id  " +
                    "    ORDER BY  " +
                    "      ttt.dateStr  " +
                    "  ) bs  " +
                    "WHERE  " +
                    "  1 = 1";

            String total_sql = "select count(1) from ( "+ sql +" ) as tt";
            String list_sql = "select * from ( "+ sql +" ) as tt limit "+(pageNo-1)*pageSize+","+pageSize;
            Long total = (service.executNativeTotalQuery(total_sql)).longValue();
            List list = service.executNativeQuery(list_sql, StatisticSellReport.class);

            Page page=new Page((pageNo-1)*pageSize,total,pageSize,null);
            page.setResult(list);
            System.out.println(JSONObject.toJSONString(page));
            restResponse.setData(page);
            restResponse.setSuccess(true);

        }catch (Exception ex){
            logger.error("系统异常：", ex);
            restResponse.setSuccess(false);
            restResponse.setErrorCode(ErrorConstant.INNER_ERROR_CODE);
            restResponse.setErrorMsg(ErrorConstant.getErrMsg(ErrorConstant.INNER_ERROR_CODE));
        }
        return  restResponse;
    }

    @RequestMapping("/report_excel")
    public void reportExcel(HttpServletResponse response,String merchantId, String scenicId, String roomId,
                            String startTime, String endTime){

        try{
            StringBuffer sqlWhere = new StringBuffer("");

            if(StringUtil.isNotBlank(merchantId)){
                sqlWhere.append(" and ttt.merchant_id='"+merchantId+"'");
            }
            if(StringUtil.isNotBlank(scenicId)){
                sqlWhere.append(" and ttt.scenicspots_id='"+scenicId+"'");
            }
            if(StringUtil.isNotBlank(roomId)){
                sqlWhere.append(" and ttt.room_id='"+roomId+"'");
            }
            if(StringUtil.isNotBlank(startTime)){
                sqlWhere.append(" and STR_TO_DATE(ttt.dateStr,'%Y-%m-%d') >= '"+startTime+"' ");
            }
            if(StringUtil.isNotBlank(endTime)){
                sqlWhere.append(" and STR_TO_DATE(ttt.dateStr,'%Y-%m-%d') <= '"+endTime+"' ");
            }

            String sql = "SELECT  " +
                    "  *  " +
                    "FROM  " +
                    "  (  " +
                    "    SELECT  " +
                    "      ttt.dateStr AS dateStr,  " +
                    "      sum(ttt.sale_num) AS sellNum,  " +
                    "      sum(ttt.sale_money) AS sellSum,  " +
                    "      ttp.product_name AS productName,  " +
                    "      tts.scenic_name AS scenicName,  " +
                    "      ttm.merchant_name AS merchantName  " +
                    "    FROM  " +
                    "      (  " +
                    "        SELECT  " +
                    "          CONCAT(  " +
                    "            obj.`year`,  " +
                    "            '-',  " +
                    "            obj.`month`,  " +
                    "            '-',  " +
                    "            obj.`day`  " +
                    "          ) AS dateStr,  " +
                    "          CASE  " +
                    "        WHEN obj.sale_num IS NULL THEN  " +
                    "          0  " +
                    "        ELSE  " +
                    "          obj.sale_num  " +
                    "        END AS sale_num,  " +
                    "        CASE  " +
                    "      WHEN obj.sale_money IS NULL THEN  " +
                    "        0  " +
                    "      ELSE  " +
                    "        obj.sale_money  " +
                    "      END AS sale_money,  " +
                    "      obj.merchant_id,  " +
                    "      obj.scenicspots_id,  " +
                    "      obj.room_id  " +
                    "    FROM  " +
                    "      tb_statistic_homestay_day obj  " +
                    "      ) ttt  " +
                    "    LEFT OUTER JOIN tb_tenant_product ttp ON ttt.room_id = ttp.id  " +
                    "    LEFT OUTER JOIN tb_tenant_scenic tts ON ttt.scenicspots_id = tts.id  " +
                    "    LEFT OUTER JOIN tb_tenant_merchant ttm ON ttt.merchant_id = ttm.id  where 1=1 " + sqlWhere.toString() +
                    "    GROUP BY  " +
                    "      ttt.dateStr,  " +
                    "      ttt.merchant_id  " +
                    "    ORDER BY  " +
                    "      ttt.dateStr  " +
                    "  ) bs  " +
                    "WHERE  " +
                    "  1 = 1";


            String list_sql = "select * from ( "+ sql +" ) as tt ";
            List list = service.executNativeQuery(list_sql, StatisticSellReport.class);

            List<StatisticHomeStaySellExcel> list2=new ArrayList<StatisticHomeStaySellExcel>();
            StatisticHomeStaySellExcel she=null;
            StatisticSellReport ssr=null;
            for(int i=0;i<list.size();i++){
                she=new StatisticHomeStaySellExcel();
                ssr=(StatisticSellReport)list.get(i);
                she.setDateStr(ssr.getDateStr());
                she.setMerchantName(ssr.getMerchantName());
                she.setProductName(ssr.getProductName());
                she.setScenicName(ssr.getScenicName());
                she.setSellNum(ssr.getSellNum()+"");
                she.setSellSum(ssr.getSellSum().setScale(2,BigDecimal.ROUND_HALF_UP)+"");
                list2.add(she);
            }

            String title = "民宿销售明细表";
            String[] headers = new String[]{"时间", "景区","商户", "房型", "销售数量", "销售金额"};
            String[] values = new String[]{"dateStr", "scenicName", "merchantName","productName", "sellNum", "sellSum"};
            downloadExcel(title, null, headers, values, list2, null, null, response);

        }catch (Exception ex){
            logger.error("系统异常：", ex);
        }
    }

}
