package com.oseasy.xlxq.console.files;

import com.oseasy.xlxq.console.base.AbstractPortalController;
import com.oseasy.xlxq.service.api.exceptions.UploadFileException;
import com.oseasy.xlxq.service.config.SystemConfig;
import com.oseasy.xlxq.service.core.utils.DateUtils;
import com.oseasy.xlxq.service.core.utils.Image2Base64Util;
import com.oseasy.xlxq.service.core.utils.UUIDGenerator;
import com.oseasy.xlxq.service.oss.OSSService;
import com.oseasy.xlxq.service.web.rest.RestResponse;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.multipart.MultipartFile;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.ByteArrayInputStream;
import java.io.InputStream;
import java.util.HashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;

/**
 * Created by tandy on 2017/5/23.
 */
@RestController()
@RequestMapping("/console/files/")
public class FileUploadController extends AbstractPortalController{

    @Autowired
    private OSSService ossService;

    /**
     * 上传资源文件到OSS 支持多文件上传 多图片上传
     * @param request
     * @param response
     * @param files
     * @return
     *
     *{
     * "success":true,"errorCode":null,"errorMsg":null,
     *  "data":[
     *      {"filename":"[TTG] The.Great.Wall.2016.720p.BluRay.x264-WiKi.torrent","filepath":"oss://tenants/1/20170525/f2bd7817b2a4ee47a5706b84dd7f5bac","url":"http://xlxq-dev-open.oss-cn-shanghai.aliyuncs.com/tenants/1/20170525/f2bd7817b2a4ee47a5706b84dd7f5bac","fileid":"f2bd7817b2a4ee47a5706b84dd7f5bac"},
     *      {"filename":"1600新创建直播-添加商品2_PxCook.png","filepath":"oss://tenants/1/20170525/f28965262bd9c508dbb96fe6d7e43e01","url":"http://xlxq-dev-open.oss-cn-shanghai.aliyuncs.com/tenants/1/20170525/f28965262bd9c508dbb96fe6d7e43e01","fileid":"f28965262bd9c508dbb96fe6d7e43e01"}
     *  ]
     *}
     */
    @RequestMapping(value = "/upload_resources",method = RequestMethod.POST)
    public RestResponse uploadResouces(HttpServletRequest request, HttpServletResponse response, @RequestParam("file") MultipartFile[] files) throws UploadFileException {
        List<Map> uploadFilesResult = new LinkedList<>();

        for(MultipartFile file : files) {
            Map fileResult = uploadFile(request,file);
            uploadFilesResult.add(fileResult);
        }
        RestResponse responsex = RestResponse.success(uploadFilesResult);
        return responsex;
    }

    /**
     * 上传视频媒体素材库 单文件上传
     * 视频素材库上传后作为租户资源存储在OSS 对应租户目录下的资源中并返回OSS存储路径
     * /tenants/{tenant.id}/{yyyyMMdd}/file.id
     * @param request
     * @param response
     * @param file
     * @return
     * {"success":true,"errorCode":null,"errorMsg":null,"data":
     *  {"filename":"[TTG] The.Great.Wall.2016.720p.BluRay.x264-WiKi.torrent","filepath":"oss://tenants/1/20170525/77b8a987ab255760872ca5a5d81da6f8","url":"http://xlxq-dev-open.oss-cn-shanghai.aliyuncs.com/tenants/1/20170525/77b8a987ab255760872ca5a5d81da6f8","fileid":"77b8a987ab255760872ca5a5d81da6f8"}
     * }
     */
    @RequestMapping(value = "/upload_video",method = RequestMethod.POST)
    public RestResponse uploadVideoFile(HttpServletRequest request, HttpServletResponse response, @RequestParam("file") MultipartFile file) throws UploadFileException {
        Map resultMap = uploadFile(request,file);
        RestResponse responsex = RestResponse.success(resultMap);
        return responsex;
    }

    private Map uploadFile(HttpServletRequest request,MultipartFile file) throws UploadFileException {
        String fileid = UUIDGenerator.uuid();
        String tenantId = getCurrentUser(request).getTenantId();
        String dt = DateUtils.getDate("yyyyMMdd");
        String fileKey = String.format("tenants/%s/%s/%s",tenantId,dt,fileid);
        String ossBucket = SystemConfig.getProperty("global.oss.aliyun.bucket");
        boolean uploadResult = false;
        try {
            if (file != null) {
                uploadResult = ossService.uploadFileStream(file.getInputStream(), file.getSize(), file.getOriginalFilename(), ossBucket, fileKey);
                if(!uploadResult){
                    throw new UploadFileException("上传文件时出现异常");
                }
            }
        }catch (Exception ex){
            throw new UploadFileException(ex);
        }
        if(logger.isDebugEnabled()){
            logger.debug("文件上传成功 {},{}",file.getOriginalFilename(),fileKey);
        }

        Map resultMap = new HashMap<>();
        resultMap.put("fileid",fileid);
        resultMap.put("filepath","oss://" + fileKey);
        resultMap.put("url",SystemConfig.getProperty("global.oss.aliyun.endpoint.internet") + "/" + fileKey);
        resultMap.put("filename",file.getOriginalFilename());
        return resultMap;
    }

    /**
     * 上传视频媒体素材库 base64 上传
     * 视频素材库上传后作为租户资源存储在OSS 对应租户目录下的资源中并返回OSS存储路径
     * /tenants/{tenant.id}/{yyyyMMdd}/file.id
     * @param request
     * @param response
     * @param base64
     * @param originalFilename
     * @return
     * {"success":true,"errorCode":null,"errorMsg":null,"data":
     *  {"filename":"[TTG] The.Great.Wall.2016.720p.BluRay.x264-WiKi.torrent","filepath":"oss://tenants/1/20170525/77b8a987ab255760872ca5a5d81da6f8","url":"http://xlxq-dev-open.oss-cn-shanghai.aliyuncs.com/tenants/1/20170525/77b8a987ab255760872ca5a5d81da6f8","fileid":"77b8a987ab255760872ca5a5d81da6f8"}
     * }
     */
    @RequestMapping(value = "/upload_video_base64",method = RequestMethod.POST)
    public RestResponse uploadVideoFile(HttpServletRequest request, HttpServletResponse response,
                                        @RequestParam("base64") String base64, @RequestParam("originalFilename") String originalFilename) throws UploadFileException {
        String fileid = UUIDGenerator.uuid();
        String tenantId = getCurrentUser(request).getTenantId();
        String dt = DateUtils.getDate("yyyyMMdd");
        String fileKey = String.format("tenants/%s/%s/%s",tenantId,dt,fileid);
        String ossBucket = SystemConfig.getProperty("global.oss.aliyun.bucket");
        boolean uploadResult = false;
        try {
            if (base64 != null) {
                byte[] fileData = Image2Base64Util.base642bytes(base64);
                InputStream inputStream = new ByteArrayInputStream(fileData);
                uploadResult = ossService.uploadFileStream(inputStream, fileData.length, originalFilename, ossBucket, fileKey);
                if(!uploadResult){
                    throw new UploadFileException("上传文件时出现异常");
                }
            }
        }catch (Exception ex){
            throw new UploadFileException(ex);
        }
        if(logger.isDebugEnabled()){
            logger.debug("文件上传成功 {},{}",originalFilename,fileKey);
        }

        Map resultMap = new HashMap<>();
        resultMap.put("fileid",fileid);
        resultMap.put("filepath","oss://" + fileKey);
        resultMap.put("url",SystemConfig.getProperty("global.oss.aliyun.endpoint.internet") + "/" + fileKey);
        resultMap.put("filename",originalFilename);
        RestResponse responsex = RestResponse.success(resultMap);
        return responsex;
    }

}
