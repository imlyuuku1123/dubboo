package com.oseasy.xlxq.console.comm;

import com.baidu.ueditor.MyActionEnter;
import com.oseasy.xlxq.service.config.SystemConfig;
import org.springframework.context.ApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

@Controller
@RequestMapping("/open/ue")
public class UEditorController {

    @RequestMapping(value="/ctrl")
    public @ResponseBody String ueExc(HttpServletRequest request, HttpServletResponse response) throws IOException {

        request.setCharacterEncoding("utf-8");
        response.setHeader("Content-Type", "text/html");
        @SuppressWarnings("resource")
        ApplicationContext appContext = new ClassPathXmlApplicationContext();
        // response.getWriter().write(baseState);
        return new MyActionEnter(request, appContext.getResource(SystemConfig.getProperty("ueditor.config.json.path")).getInputStream()).exec();
    }
}
