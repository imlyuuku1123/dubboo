package com.oseasy.xlxq.console.account;

import com.oseasy.xlxq.console.base.AbstractPortalController;
import com.oseasy.xlxq.service.api.tenant.service.TenantAccountService;
import com.oseasy.xlxq.service.mq.api.MQService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.servlet.ModelAndView;

import javax.servlet.http.HttpServletRequest;

/**
 * 基本资料
 * Created by Tandy on 2016/6/8.
 */
@Controller
@RequestMapping("/console/account")
public class AccountController  extends AbstractPortalController {
    private static final Logger logger = LoggerFactory.getLogger(AccountController.class);
    @Autowired
    private TenantAccountService tenantAccountService;

    @Autowired
    private MQService mqService;


    /**8
     * 基本资料首页入口
     * @param request
     * @return
     */
    @RequestMapping("/index" )
    public ModelAndView index(HttpServletRequest request){

        ModelAndView mav = new ModelAndView();
        mav.addObject("account",getCurrentUser(request));
        mav.setViewName("/console/account/information/index");
        return mav;
    }

//    /**
//     * 基本资料修改入口
//     * @param request
//     * @return
//     */
//    @RequestMapping(value="/update" ,method = RequestMethod.POST)
//    @ResponseBody
//    public Map update(HttpServletRequest request){
//        Map<String,Object> paramsMap = WebUtils.getRequestParams(request);
//        updateAccont(request,paramsMap);
//        Map map = new HashMap();
//        map.put("msg","修改资料成功");
//
//        return map;
//    }


}
