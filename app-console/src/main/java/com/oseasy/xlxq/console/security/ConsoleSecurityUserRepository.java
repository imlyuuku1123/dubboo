package com.oseasy.xlxq.console.security;

import com.oseasy.xlxq.service.api.tenant.model.TenantAccount;
import com.oseasy.xlxq.service.api.tenant.service.TenantAccountService;
import com.oseasy.xlxq.service.core.security.SecurityUser;
import com.oseasy.xlxq.service.web.security.SecurityUserRepository;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

/**
 * Created by Tandy on 2016/6/8.
 */
@Component
public class ConsoleSecurityUserRepository implements SecurityUserRepository {

    private static  final Log logger = LogFactory.getLog(ConsoleSecurityUserRepository.class);

    @Autowired
    private TenantAccountService tenantAccountService;

    @Override
    public SecurityUser loadSecurityUser(String username) {
        if(logger.isDebugEnabled()){
            logger.debug(String.format("load security user"));
        }
        return getUser(username);
    }

    /**
     * 根据用户名获取用户信息
     * @return
     */
    public SecurityUser getUser(String username){
        TenantAccount tenantAccount = tenantAccountService.findAccountByUserName(username);
        if(null != tenantAccount){
            return new SecurityUser(tenantAccount.getId(), tenantAccount.getUserName(), tenantAccount.getTenant().getCode(), tenantAccount.getTenant().getId());
        }
        return null;
    }

}
