package com.oseasy.xlxq.console.sys;

import com.oseasy.xlxq.console.base.AbstractPortalController;
import com.oseasy.xlxq.console.comm.ConsoleConstants;
import com.oseasy.xlxq.service.api.sys.model.Module;
import com.oseasy.xlxq.service.api.sys.model.ModuleRole;
import com.oseasy.xlxq.service.api.sys.service.ModuleRoleService;
import com.oseasy.xlxq.service.api.sys.service.ModuleService;

import com.oseasy.xlxq.service.core.utils.*;
import com.oseasy.xlxq.service.web.rest.RestResponse;
import net.sf.json.JSONObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 *界面模块管理
 */
@RestController
@RequestMapping("/console/module")
public class ModuleController extends AbstractPortalController {

    @Autowired
    private ModuleService moduleService;

    @Autowired
    private ModuleRoleService moduleRoleService;

    /**
     * 获取实体信息
     * @param id
     * @return
     */
    @RequestMapping("/{id}")
    public RestResponse getEntityById(@PathVariable("id") String id) {
        RestResponse restResponse = new RestResponse();
        try {
            Module module = moduleService.findById(id);
            restResponse.setData(module);
            restResponse.setSuccess(true);
            restResponse.setErrorCode("1001");
            restResponse.setErrorMsg("操作成功");
        } catch (Exception e) {
            logger.error(e.getMessage(),e);
            restResponse.setSuccess(false);
            restResponse.setErrorCode("0000");
            restResponse.setErrorMsg("操作异常");
        }
        return restResponse;
    }


    /**
     * 保存实体
     * @param
     * @return
     */
    @RequestMapping("/save")
    public RestResponse save(@RequestBody Module module) {
        RestResponse restResponse = new RestResponse();
        try {
            if(StringUtil.isNotBlank(module.getId())){
                moduleService.update(module.getId(),module);
            }else {
                moduleService.save(module);
            }
            restResponse.setSuccess(true);
            restResponse.setErrorCode("1001");
            restResponse.setErrorMsg("操作成功");
        } catch (Exception e) {
            logger.error(e.getMessage(),e);
            restResponse.setSuccess(false);
            restResponse.setErrorCode("0000");
            restResponse.setErrorMsg("操作异常");
        }
        return restResponse;
    }


    /**
     * 删除
     * @param id
     * @return
     */
    @RequestMapping("/delete/{id}")
    public RestResponse delete(@PathVariable("id") String id) {
        RestResponse restResponse = new RestResponse();
        try {
            moduleService.delete(id);
            restResponse.setSuccess(true);
            restResponse.setErrorCode("1001");
            restResponse.setErrorMsg("操作成功");
        } catch (Exception e) {
            logger.error(e.getMessage(),e);
            restResponse.setSuccess(false);
            restResponse.setErrorCode("0000");
            restResponse.setErrorMsg("操作异常");
        }
        return restResponse;
    }


    /**
     * 查询列表
     * @param
     * @param pageNo
     * @param pageSize
     * @return
     */
    @RequestMapping("/list")
    public RestResponse getPageList(@RequestParam(defaultValue = "1") Integer pageNo,
                                    @RequestParam(defaultValue = "20") Integer pageSize) {
        RestResponse restResponse = new RestResponse();
        try {
            String hql = "from Module obj";
           /* String moduleName = module.getModuleName();
            if (StringUtil.isNotBlank(moduleName)) {
                hql = HqlUtil.addCondition(hql, "moduleName", moduleName,null,"Like",null);
            }*/
            Page page = moduleService.pageList(hql,pageNo,pageSize);
            restResponse.setData(page);
            restResponse.setSuccess(true);
            restResponse.setErrorCode("1001");
            restResponse.setErrorMsg("操作成功");
        } catch (Exception e) {
            logger.error(e.getMessage(),e);
            restResponse.setSuccess(false);
            restResponse.setErrorCode("0000");
            restResponse.setErrorMsg("操作异常");
        }
        return restResponse;
    }

    /**
     * 批量操作
     * @param
     * @return
     */
    @RequestMapping(value = "/batchoperate",method = RequestMethod.POST)
    public RestResponse batchoperate(@RequestBody JSONObject jsonObj) {
        RestResponse restResponse = new RestResponse();
        String ids =  jsonObj.getString("ids");
        String type =  jsonObj.getString("type");
        try {
            if (StringUtil.isNotBlank(ids)&&StringUtil.isNotBlank(type)) {
                String [] idArray = ids.split(",");
                if (ConsoleConstants.OPERATE_TYPE_DELETE.equals(type)) {
                    moduleService.updateByArray("deleted", ConsoleConstants.DELETE_FLAG, "id", idArray);
                }
            }else{
                return RestResponse.failed("0000","请求参数错误");
            }
            restResponse.setSuccess(true);
            restResponse.setErrorCode("1001");
            restResponse.setErrorMsg("操作成功");
        } catch (Exception e) {
            logger.error(e.getMessage(),e);
            restResponse.setSuccess(false);
            restResponse.setErrorCode("0000");
            restResponse.setErrorMsg("操作异常");
        }
        return restResponse;
    }

    /**
     * 查询菜单列表
     * @param
     * @param
     * @param
     * @return
     */
    @RequestMapping("/menu")
    public RestResponse getMenuTree() {
        RestResponse restResponse = new RestResponse();
        Map<String,Object> rMap = new HashMap<String,Object>();
        Map<String,Object> reMap = new HashMap<String,Object>();
        try {
            List<String> pList = new ArrayList<String>();
            pList.add("1");
            Map<String,Object> mapLevel1 = moduleService.getMenuListByParentIdList(pList,1);
            List<String> idListLevel1 = (List<String>)mapLevel1.get("idListLevel1");
            rMap.putAll(mapLevel1);
            reMap.put("level1",mapLevel1.get("level1"));
            if(idListLevel1!=null&&idListLevel1.size()>0){
                int i=2;
                boolean isContinue = true;
                do {
                  //  logger.info("i==="+i);
                   List<List<String>> xList = (List<List<String>>)rMap.get("idListLevel"  + (i-1));
                   if(xList!=null&&xList.size()>0){
                 // logger.info("xList.size="+xList.size());
                    int k = 0;
                    List<List<String>> currListId = new ArrayList<List<String>>();
                   // List<List<String>> currListName = new ArrayList<List<String>>();
                    List<List<Map<String,Object>>> currListMap = new ArrayList<List<Map<String,Object>>>();
                    for(List<String> ptList:xList){
                        Map<String,Object> tMap = moduleService.getMenuListByParentIdList(ptList,i);
                      //  logger.info("tMap.get(\"idListLevel\"+i)======"+tMap.get("idListLevel"+i));
                        List<List<String>> sIdList = ( List<List<String>>)tMap.get("idListLevel"+i);
                        List<List<Map<String,Object>>> sMapList = ( List<List<Map<String,Object>>>)tMap.get("level"+i);
                 //       List<List<String>> sNameList = ( List<List<String>>)tMap.get("nameListLevel"+i);
                     //  logger.info("sIdList.size:"+sIdList.size());
                        currListId.addAll(sIdList);
                        currListMap.addAll(sMapList);
                  //      currListName.addAll(sNameList);
                    //    logger.info("循环中的currListId.size:"+currListId.size());
                    }
                   // logger.info("currListId.size:"+currListId.size());
                    rMap.put("idListLevel"+i,currListId);
                    //rMap.put("nameListLevel"+i,currListName);
                       reMap.put("level"+i,currListMap);
                    for(List<String> eList:currListId){
                            if(eList.size()==0){
                                k++;
                            }
                    }

                    //logger.info("k=="+k);
                    if (k==currListId.size()){
                        rMap.remove("idListLevel"+i);
                        //rMap.remove("nameListLevel"+i);
                        reMap.remove("level"+i);
                       isContinue=false;
                    }
                    i++;
                   }else {
                       isContinue = false;
                   }
                } while (isContinue);
            }
            //restResponse.setData(rMap);
            restResponse.setData(reMap);
            restResponse.setSuccess(true);
            restResponse.setErrorCode("1001");
            restResponse.setErrorMsg("操作成功");
        } catch (Exception e) {
            logger.error(e.getMessage(),e);
            restResponse.setSuccess(false);
            restResponse.setErrorCode("0000");
            restResponse.setErrorMsg("操作异常");
        }
        return restResponse;
    }



    /**
     * 获取菜单jstree数据,创建时
     * @return
     */
    @RequestMapping("/jstree")
    public RestResponse getJsTree() {
        RestResponse restResponse = new RestResponse();
        List<Map<String,Object>> rList =  new ArrayList<Map<String,Object>>();
        try {
           /* String hql = "from Module obj where obj.deleted = 0";
            List<Module> allMenuList =  (List<Module>) moduleService.list(hql);*/
            List<Module> allModuleList = moduleService.getAllModuleList();
           // logger.info("allMenuList.size()"+allMenuList.size());
            List<Module> firstModuleList = moduleService.getFirstMouleList(allModuleList);
           /*if(allMenuList!=null&&allMenuList.size()>0){
               for (Module xm:allMenuList){
                    if("1".equals(xm.getParentId())){
                        firstModuleList.add(xm);
                    }
               }*/

              // logger.info("firstModuleList.size="+firstModuleList.size());
               if(firstModuleList.size()>0) {
                   rList = moduleService.getModuleByParent(firstModuleList, allModuleList);
               }

            restResponse.setData(rList);
            restResponse.setSuccess(true);
            restResponse.setErrorCode("1001");
            restResponse.setErrorMsg("操作成功");
        } catch (Exception e) {
            logger.error(e.getMessage(),e);
            restResponse.setSuccess(false);
            restResponse.setErrorCode("0000");
            restResponse.setErrorMsg("操作异常");
        }
        return restResponse;
    }


    /**
     * 获取菜单jstree数据,编辑时
     * @return
     */
    @RequestMapping("/jstree2/{roleId}")
    public RestResponse getJsTree2(@PathVariable("roleId") String roleId) {
        logger.info("===============roleId:"+roleId);
        RestResponse restResponse = new RestResponse();
        List<Map<String,Object>> rList =  new ArrayList<Map<String,Object>>();
        try {
            List<Module> allModuleList = moduleService.getAllModuleList();

            List<ModuleRole> mrList = new ArrayList();
            //   logger.info("根据roleId查询已存在角色菜单集合");
            String hql = "from ModuleRole obj where obj.deleted = 0 and obj.tenantRole.id = " + "'" + roleId + "'";
            mrList = moduleRoleService.getList(hql);
            List<String> mIdList = new ArrayList<String>();
            if(mrList!=null&&mrList.size()>0){
                for(ModuleRole mr:mrList){
                    if(mr.getModule()!=null) {
                        mIdList.add(mr.getModule().getId());
                    }
                }
            }
            logger.info("mIdList=========:"+mIdList.toString());
            List<Module> firstModuleList = moduleService.getFirstMouleList(allModuleList);

            if(firstModuleList.size()>0) {
                rList = moduleService.getModuleByParent2(firstModuleList, allModuleList,mIdList);
            }

            restResponse.setData(rList);
            restResponse.setSuccess(true);
            restResponse.setErrorCode("1001");
            restResponse.setErrorMsg("操作成功");
        } catch (Exception e) {
            logger.error(e.getMessage(),e);
            restResponse.setSuccess(false);
            restResponse.setErrorCode("0000");
            restResponse.setErrorMsg("操作异常");
        }
        return restResponse;
    }



}
