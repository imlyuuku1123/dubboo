package com.oseasy.xlxq.console.statistics;

import com.alibaba.fastjson.JSONObject;
import com.oseasy.xlxq.console.base.AbstractPortalController;
import com.oseasy.xlxq.service.api.exceptions.ErrorConstant;
import com.oseasy.xlxq.service.api.statistic.model.StatisticSellReport;
import com.oseasy.xlxq.service.api.statistic.model.StatisticSpecialtyDay;
import com.oseasy.xlxq.service.api.statistic.model.StatisticSpecialtyDayExcel;
import com.oseasy.xlxq.service.api.statistic.model.StatisticSpecialtySellExcel;
import com.oseasy.xlxq.service.api.statistic.service.StatisticSpecialtyDayService;
import com.oseasy.xlxq.service.core.utils.HqlUtil;
import com.oseasy.xlxq.service.core.utils.Page;
import com.oseasy.xlxq.service.core.utils.StringUtil;
import com.oseasy.xlxq.service.web.rest.RestResponse;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import javax.servlet.http.HttpServletResponse;
import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by heckman on 2017/6/12.
 */

@RestController
@RequestMapping("/console/specialty_statistic")
public class SpecialtyDailyStisController extends AbstractPortalController {

    public static final Logger logger = LoggerFactory.getLogger(SpecialtyDailyStisController.class);

    @Autowired
    private StatisticSpecialtyDayService service;

    @RequestMapping("/daily_statistic")
    public RestResponse dailyStatistic(String merchantId, String fClass,
                                       String sClass, String productId,
                                       String startTime, String endTime,
                                       @RequestParam(defaultValue = "1") Integer pageNo,
                                       @RequestParam(defaultValue = "20") Integer pageSize) {
        RestResponse restResponse = new RestResponse();
        try {
            String hql = "from StatisticSpecialtyDay obj ";
            if (StringUtil.isNotBlank(merchantId)) {
                hql = HqlUtil.addCondition(hql, "tenantMerchant.id", merchantId);
            }
            if (StringUtil.isNotBlank(fClass)) {
                hql = HqlUtil.addCondition(hql, "tenantSpecialty.catalogRoot.id", fClass);
            }
            if (StringUtil.isNotBlank(sClass)) {
                hql = HqlUtil.addCondition(hql, "tenantSpecialty.catalog.id", sClass);
            }
            if (StringUtil.isNotBlank(productId)) {
                hql = HqlUtil.addCondition(hql, "tenantSpecialty.id", productId);
            }
            if (StringUtil.isNotBlank(startTime)) {
                hql = HqlUtil.addCondition(hql, "stasticsDay", startTime, HqlUtil.LOGIC_AND, HqlUtil.TYPE_STRING, HqlUtil.COMPARECHAR_GREAT_EQ);
            }
            if (StringUtil.isNotBlank(endTime)) {
                hql = HqlUtil.addCondition(hql, "stasticsDay", endTime, HqlUtil.LOGIC_AND, HqlUtil.TYPE_STRING, HqlUtil.COMPARECHAR_LESS_EQ);
            }
            hql = HqlUtil.addOrder(hql, "stasticsDay", "");

            Page<StatisticSpecialtyDay> page = service.pageList(hql, pageNo, pageSize);
            restResponse.setData(page);
            restResponse.setSuccess(true);
        } catch (Exception ex) {
            logger.error("系统异常：", ex);
            restResponse.setSuccess(false);
            restResponse.setErrorCode(ErrorConstant.INNER_ERROR_CODE);
            restResponse.setErrorMsg(ErrorConstant.getErrMsg(ErrorConstant.INNER_ERROR_CODE));
        }
        return restResponse;
    }

    /**
     * 日销量导出excel接口
     *
     * @param response
     * @param merchantId
     * @param fClass
     * @param sClass
     * @param productId
     * @param startTime
     * @param endTime
     */
    @RequestMapping("/excel")
    public void export(HttpServletResponse response,
                       String merchantId, String fClass,
                       String sClass, String productId,
                       String startTime, String endTime) {
        try {
            String hql = "from StatisticSpecialtyDay obj ";
            if (StringUtil.isNotBlank(merchantId)) {
                hql = HqlUtil.addCondition(hql, "tenantMerchant.id", merchantId);
            }
            if (StringUtil.isNotBlank(fClass)) {
                hql = HqlUtil.addCondition(hql, "tenantSpecialty.catalogRoot.id", fClass);
            }
            if (StringUtil.isNotBlank(sClass)) {
                hql = HqlUtil.addCondition(hql, "tenantSpecialty.catalog.id", sClass);
            }
            if (StringUtil.isNotBlank(productId)) {
                hql = HqlUtil.addCondition(hql, "tenantSpecialty.id", productId);
            }
            if (StringUtil.isNotBlank(startTime)) {
                hql = HqlUtil.addCondition(hql, "stasticsDay", startTime, HqlUtil.LOGIC_AND, HqlUtil.TYPE_STRING, HqlUtil.COMPARECHAR_GREAT_EQ);
            }
            if (StringUtil.isNotBlank(endTime)) {
                hql = HqlUtil.addCondition(hql, "stasticsDay", endTime, HqlUtil.LOGIC_AND, HqlUtil.TYPE_STRING, HqlUtil.COMPARECHAR_LESS_EQ);
            }
            hql = HqlUtil.addOrder(hql, "stasticsDay", "");
            List<StatisticSpecialtyDay> ds = service.getList(hql);
            List<StatisticSpecialtyDayExcel> list = new ArrayList<StatisticSpecialtyDayExcel>();
            StatisticSpecialtyDayExcel sde = null;
            for (StatisticSpecialtyDay ssd : ds) {
                sde=new StatisticSpecialtyDayExcel();
                sde.setRootCatlog(ssd.getTenantSpecialty()==null?"":ssd.getTenantSpecialty().getCatalogRoot().getProductName());
                sde.setMerchantName(ssd.getTenantSpecialty()==null?"":ssd.getTenantSpecialty().getTenantMerchant().getMerchantName());
                sde.setName(ssd.getTenantSpecialty()==null?"":ssd.getTenantSpecialty().getProductName());
                sde.setCatlog(ssd.getTenantSpecialty()==null?"":ssd.getTenantSpecialty().getCatalog().getProductName());
                sde.setSellNum(ssd.getSaleNum() + "");
                sde.setSellSum(ssd.getSaleMoney().setScale(2, BigDecimal.ROUND_HALF_UP) + "");
                list.add(sde);
            }
            String title = "土特产销量统计";
            String[] headers = new String[]{"景区", "商户", "一级分类", "二级分类", "销售数量", "销售金额"};
            String[] values = new String[]{"name", "merchantName", "rootCatlog", "catlog", "sellNum", "sellSum"};
            downloadExcel(title, null, headers, values, list, null, null, response);
        } catch (Exception ex) {
            logger.error("系统异常：", ex);
        }
    }

    @RequestMapping("/report")
    public RestResponse report(@RequestParam(defaultValue = "1")Integer pageNo,
                               @RequestParam(defaultValue = "20")Integer pageSize,
                               String merchantId, String fClass,
                               String sClass, String productId,
                               String startTime, String endTime){
        RestResponse restResponse=new RestResponse();
        try{
            StringBuffer sqlWhere = new StringBuffer("");
            if(StringUtil.isNotBlank(merchantId)){
                sqlWhere.append(" and tt.merchant_id ='"+merchantId+"'");
            }
            if(StringUtil.isNotBlank(fClass)){
                sqlWhere.append(" and ts.catalog_root_id='"+fClass+"'");
            }
            if(StringUtil.isNotBlank(sClass)){
                sqlWhere.append(" and ts.catalog_id='"+sClass+"'");
            }
            if(StringUtil.isNotBlank(productId)){
                sqlWhere.append(" and tp.id='"+productId+"'");
            }
            if(StringUtil.isNotBlank(startTime)){
                sqlWhere.append(" and STR_TO_DATE(tt.dateStr,'%Y-%m-%d') >= '"+startTime+"' ");
            }
            if(StringUtil.isNotBlank(endTime)){
                sqlWhere.append(" and STR_TO_DATE(tt.dateStr,'%Y-%m-%d') <= '"+endTime+"' ");
            }
            String sql = "SELECT " +
                    "  ttt.dateStr, "+
                    "  ttt.productName, " +
                    "  ttt.merchantName, " +
                    "  ttt.sellNum, " +
                    "  ttt.sellSum " +
                    "FROM " +
                    "  ( " +
                    "    SELECT " +
                    "      tt.dateStr, " +
                    "      tp.product_name AS productName, " +
                    "      tm.merchant_name AS merchantName, " +
                    "      sum(tt.sale_num) AS sellNum, " +
                    "      sum(tt.sale_money) AS sellSum, " +
                    "      tp.id AS productId, " +
                    "      tm.id AS merchantId, " +
                    "      ts.catalog_root_id AS fClass, " +
                    "      ts.catalog_id AS sClass " +
                    "    FROM " +
                    "      ( " +
                    "        SELECT " +
                    "          CONCAT( " +
                    "            obj.`year`, " +
                    "            '-', " +
                    "            obj.`month`, " +
                    "            '-', " +
                    "            obj.`day` " +
                    "          ) AS dateStr, " +
                    "          specialty_id, " +
                    "          merchant_id, " +
                    "          sale_num, " +
                    "          sale_money " +
                    "        FROM " +
                    "          `tb_statistic_specialty_day` obj " +
                    "      ) AS tt " +
                    "    LEFT OUTER JOIN tb_tenant_product tp ON tt.specialty_id = tp.id " +
                    "    LEFT OUTER JOIN tb_tenant_merchant tm ON tt.merchant_id = tm.id " +
                    "    LEFT OUTER JOIN tb_tenant_specialty ts ON tt.specialty_id = ts.id where 1=1 " + sqlWhere.toString() +
                    "    GROUP BY " +
                    "      tt.merchant_id, " +
                    "      tt.dateStr " +
                    "  ) AS ttt " +
                    "WHERE " +
                    "  1 = 1";

            String total_sql = "select count(1) from ( "+ sql +" ) as tt";
            String list_sql = "select * from ( "+ sql +" ) as tt limit "+(pageNo-1)*pageSize+","+pageSize;
            Long total = (service.executNativeTotalQuery(total_sql)).longValue();
            List list = service.executNativeQuery(list_sql, StatisticSellReport.class);

            Page page=new Page((pageNo-1)*pageSize,total,pageSize,null);
            page.setResult(list);
            System.out.println(JSONObject.toJSONString(page));
            restResponse.setData(page);
            restResponse.setSuccess(true);

        }catch (Exception ex){
            logger.error("系统异常：", ex);
            restResponse.setSuccess(false);
            restResponse.setErrorCode(ErrorConstant.INNER_ERROR_CODE);
            restResponse.setErrorMsg(ErrorConstant.getErrMsg(ErrorConstant.INNER_ERROR_CODE));
        }
        return  restResponse;
    }

    @RequestMapping("/report_excel")
    public void reportExcel(HttpServletResponse response,String merchantId, String fClass,
                            String sClass, String productId,
                            String startTime, String endTime){
        try{
            StringBuffer sqlWhere = new StringBuffer("");
            if(StringUtil.isNotBlank(merchantId)){
                sqlWhere.append(" and tt.merchant_id ='"+merchantId+"'");
            }
            if(StringUtil.isNotBlank(fClass)){
                sqlWhere.append(" and ts.catalog_root_id='"+fClass+"'");
            }
            if(StringUtil.isNotBlank(sClass)){
                sqlWhere.append(" and ts.catalog_id='"+sClass+"'");
            }
            if(StringUtil.isNotBlank(productId)){
                sqlWhere.append(" and tp.id='"+productId+"'");
            }
            if(StringUtil.isNotBlank(startTime)){
                sqlWhere.append(" and STR_TO_DATE(tt.dateStr,'%Y-%m-%d') >= '"+startTime+"' ");
            }
            if(StringUtil.isNotBlank(endTime)){
                sqlWhere.append(" and STR_TO_DATE(tt.dateStr,'%Y-%m-%d') <= '"+endTime+"' ");
            }
            String sql = "SELECT " +
                    "  ttt.dateStr, "+
                    "  ttt.productName, " +
                    "  ttt.merchantName, " +
                    "  ttt.sellNum, " +
                    "  ttt.sellSum " +
                    "FROM " +
                    "  ( " +
                    "    SELECT " +
                    "      tt.dateStr, " +
                    "      tp.product_name AS productName, " +
                    "      tm.merchant_name AS merchantName, " +
                    "      sum(tt.sale_num) AS sellNum, " +
                    "      sum(tt.sale_money) AS sellSum, " +
                    "      tp.id AS productId, " +
                    "      tm.id AS merchantId, " +
                    "      ts.catalog_root_id AS fClass, " +
                    "      ts.catalog_id AS sClass " +
                    "    FROM " +
                    "      ( " +
                    "        SELECT " +
                    "          CONCAT( " +
                    "            obj.`year`, " +
                    "            '-', " +
                    "            obj.`month`, " +
                    "            '-', " +
                    "            obj.`day` " +
                    "          ) AS dateStr, " +
                    "          specialty_id, " +
                    "          merchant_id, " +
                    "          sale_num, " +
                    "          sale_money " +
                    "        FROM " +
                    "          `tb_statistic_specialty_day` obj " +
                    "      ) AS tt " +
                    "    LEFT OUTER JOIN tb_tenant_product tp ON tt.specialty_id = tp.id " +
                    "    LEFT OUTER JOIN tb_tenant_merchant tm ON tt.merchant_id = tm.id " +
                    "    LEFT OUTER JOIN tb_tenant_specialty ts ON tt.specialty_id = ts.id where 1=1 " + sqlWhere.toString() +
                    "    GROUP BY " +
                    "      tt.merchant_id, " +
                    "      tt.dateStr " +
                    "  ) AS ttt " +
                    "WHERE " +
                    "  1 = 1";




            String list_sql = "select * from ( "+ sql +" ) as tt";
            List list = service.executNativeQuery(list_sql, StatisticSellReport.class);

            List<StatisticSpecialtySellExcel> list2=new ArrayList<StatisticSpecialtySellExcel>();
            StatisticSpecialtySellExcel ssse=null;
            StatisticSellReport sst=null;
            for(int i=0;i<list.size();i++){
                ssse=new StatisticSpecialtySellExcel();
                sst=(StatisticSellReport)list.get(i);
                ssse.setDateStr(sst.getDateStr());
                ssse.setMerchantName(sst.getMerchantName());
                ssse.setProductName(sst.getProductName());
                ssse.setSellNum(sst.getSellNum()+"");
                ssse.setSellSum(sst.getSellSum().setScale(2,BigDecimal.ROUND_HALF_UP)+"");
                list2.add(ssse);
            }

            String title = "土特产销售明细表";
            String[] headers = new String[]{"时间", "土特产名称", "商户", "销售数量", "销售金额"};
            String[] values = new String[]{"dateStr", "productName", "merchantName", "sellNum", "sellSum"};
            downloadExcel(title, null, headers, values, list2, null, null, response);

        }catch (Exception ex){
            logger.error("系统异常：", ex);
        }
    }

}
