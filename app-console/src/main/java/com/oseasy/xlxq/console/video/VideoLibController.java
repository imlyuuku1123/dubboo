package com.oseasy.xlxq.console.video;

import com.oseasy.xlxq.console.base.AbstractPortalController;
import com.oseasy.xlxq.service.api.exceptions.ErrorConstant;
import com.oseasy.xlxq.service.api.tenant.model.Tenant;
import com.oseasy.xlxq.service.api.video.model.VideoLib;
import com.oseasy.xlxq.service.api.video.service.VideoLibService;
import com.oseasy.xlxq.service.core.security.SecurityUser;
import com.oseasy.xlxq.service.core.utils.HqlUtil;
import com.oseasy.xlxq.service.core.utils.Page;
import com.oseasy.xlxq.service.core.utils.StringUtil;
import com.oseasy.xlxq.service.web.rest.RestResponse;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.util.StringUtils;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpServletRequest;
import java.util.Date;

/**
 * Created by zhouyifan on 2017/6/7.
 */

@RestController
@RequestMapping("/console/video_lib")
@SuppressWarnings("all")
public class VideoLibController extends AbstractPortalController{

    private static final Logger logger = LoggerFactory.getLogger(VideoLibController.class);

    @Autowired
    private VideoLibService videoLibService;

    /**
     * 展示视频素材列表接口
     *
     * @param request
     * @param param
     * @return
     */
    @RequestMapping(value = "/list" )
    public RestResponse<Page<VideoLib>> list(
            @RequestParam(defaultValue = "6") Integer pageSize,
            @RequestParam(defaultValue = "1") Integer pageNo,
            String name) {
        RestResponse<Page<VideoLib>> restResponse = new RestResponse<Page<VideoLib>>();
        try {

            String hql = " from VideoLib obj ";

            //设置过滤条件
            if (StringUtil.isNotBlank(name)) {
                hql = HqlUtil.addCondition(hql, "name", name);
            }

            Page<VideoLib> page = videoLibService.pageList(hql, pageNo, pageSize);

            restResponse.setSuccess(true);
            restResponse.setData(page);
        } catch (Exception ex) {
            logger.error("系统异常：", ex);
            restResponse.setSuccess(false);
            restResponse.setErrorCode(ErrorConstant.INNER_ERROR_CODE);
            restResponse.setErrorMsg(ErrorConstant.getErrMsg(ErrorConstant.INNER_ERROR_CODE));
        }
        return restResponse;
    }

    /**
     * 新增或修改视频素材接口
     *
     * @param request
     * @param videoRoom
     * @return
     */
    @RequestMapping(value = "/save", method = RequestMethod.POST)
    public RestResponse<VideoLib> save(HttpServletRequest request, VideoLib videoLib) {
        logger.debug("==============" + videoLib);
        RestResponse<VideoLib> restResponse = new RestResponse<VideoLib>();
        try {
            //做更新操作
            if (!StringUtils.isEmpty(videoLib.getId())) {
                videoLib.setLastTime(new Date());
                videoLibService.update(videoLib.getId(), videoLib);
            } else {
                //新增操作
                SecurityUser securityUser = getCurrentUser(request);
                Tenant tenant = new Tenant();
                tenant.setId(securityUser.getTenantId());
                videoLib.setTenant(tenant);
                videoLibService.save(videoLib);
                //返回videoRoom数据
                restResponse.setData(videoLib);
            }
            restResponse.setSuccess(true);
        } catch (Exception ex) {
            logger.error("系统异常：", ex);
            restResponse.setSuccess(false);
            restResponse.setErrorCode(ErrorConstant.INNER_ERROR_CODE);
            restResponse.setErrorMsg(ErrorConstant.getErrMsg(ErrorConstant.INNER_ERROR_CODE));
        }
        return restResponse;
    }


    /**
     * 删除视频素材接口(支持单个删除和批量删除)
     *
     * @param request
     * @param id
     * @return
     */
    @RequestMapping(value = "/delete")
    public RestResponse delete(HttpServletRequest request, @RequestParam String ids) {
        RestResponse restResponse = new RestResponse();
        try {
            if (ids.contains(",")) {
                //批量删除
                String[] idArray = ids.split(",");
                for (String id : idArray) {
                    videoLibService.delete(id);
                }
            } else {
                //单个删除
                    videoLibService.delete(ids);
            }
            restResponse.setSuccess(true);
        } catch (Exception ex) {
            logger.error("系统异常：", ex);
            restResponse.setSuccess(false);
            restResponse.setErrorCode(ErrorConstant.INNER_ERROR_CODE);
            restResponse.setErrorMsg(ErrorConstant.getErrMsg(ErrorConstant.INNER_ERROR_CODE));
        }
        return restResponse;
    }

    /**
     * 根据id查询单个视频素材接口
     *
     * @param request
     * @param id
     * @return
     */
    @RequestMapping("/find/{id}")
    public RestResponse<VideoLib> find(HttpServletRequest request, @PathVariable String id) {
        RestResponse<VideoLib> restResponse = new RestResponse<VideoLib>();
        try {
            if (!StringUtils.isEmpty(id)) {
                VideoLib videoRoom = videoLibService.findById(id);
                restResponse.setData(videoRoom);
                restResponse.setSuccess(true);
            } else {
                restResponse.setSuccess(false);
                restResponse.setErrorCode(ErrorConstant.PARAM_NULL_EXPT_CODE);
                restResponse.setErrorMsg(ErrorConstant.getErrMsg(ErrorConstant.PARAM_NULL_EXPT_CODE));
            }
        } catch (Exception ex) {
            logger.error("系统异常：", ex);
            restResponse.setSuccess(false);
            restResponse.setErrorCode(ErrorConstant.INNER_ERROR_CODE);
            restResponse.setErrorMsg(ErrorConstant.getErrMsg(ErrorConstant.INNER_ERROR_CODE));
        }
        return restResponse;
    }


}
