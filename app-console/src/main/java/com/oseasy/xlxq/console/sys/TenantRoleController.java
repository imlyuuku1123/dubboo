package com.oseasy.xlxq.console.sys;

import com.oseasy.xlxq.console.base.AbstractPortalController;
import com.oseasy.xlxq.console.comm.ConsoleConstants;
import com.oseasy.xlxq.service.api.exceptions.ErrorConstant;
import com.oseasy.xlxq.service.api.sys.model.Module;
import com.oseasy.xlxq.service.api.sys.model.ModuleRole;
import com.oseasy.xlxq.service.api.sys.model.TenantRole;
import com.oseasy.xlxq.service.api.sys.service.ModuleRoleService;
import com.oseasy.xlxq.service.api.sys.service.TenantRoleService;
import com.oseasy.xlxq.service.core.utils.Page;
import com.oseasy.xlxq.service.core.utils.StringUtil;
import com.oseasy.xlxq.service.web.rest.RestResponse;
import net.sf.json.JSONObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.ArrayList;
import java.util.List;

/**
 *运营账号角色管理
 */
@RestController
@RequestMapping("/console/role")
public class TenantRoleController extends AbstractPortalController {

    @Autowired
    private TenantRoleService tenantRoleService;

    @Autowired
    private ModuleRoleService moduleRoleService;

    /**
     * 获取实体信息
     * @param id
     * @return
     */
    @RequestMapping("/{id}")
    public RestResponse getEntityById(@PathVariable("id") String id) {
        RestResponse restResponse = new RestResponse();
        try {
            TenantRole tenantRole = tenantRoleService.findById(id);
            logger.info("查询角色菜单集合");
            String hql = "from ModuleRole obj where obj.deleted = 0 and obj.tenantRole.id = "+"'"+tenantRole.getId()+"'";
            List<ModuleRole> mrList = (List<ModuleRole>)moduleRoleService.list(hql);
            if(mrList!=null&&mrList.size()>0) {
               StringBuffer sbuff = new StringBuffer();
                for (ModuleRole mr : mrList) {
                   sbuff.append(mr.getId());
                   sbuff.append(",");
                }
                String moduleIds = sbuff.toString();
                moduleIds = moduleIds.substring(0, moduleIds.lastIndexOf(","));
                tenantRole.setModuleIds(moduleIds);
            }

            restResponse.setData(tenantRole);
            restResponse.setSuccess(true);
            restResponse.setErrorCode("1001");
            restResponse.setErrorMsg("操作成功");
        } catch (Exception e) {
            logger.error(e.getMessage(),e);
            restResponse.setSuccess(false);
            restResponse.setErrorCode("0000");
            restResponse.setErrorMsg("操作异常");
        }
        return restResponse;
    }


    /**
     * 保存实体
     * @param
     * @return
     */
    @RequestMapping("/save")
    public RestResponse save(@RequestBody TenantRole tenantRole) {
        RestResponse restResponse = new RestResponse();
        try {
            String hql_1 = "from TenantRole obj where obj.deleted = 0 and obj.roleName = "+"'"+tenantRole.getRoleName()+"'";
            TenantRole tt = (TenantRole)tenantRoleService.getByHql(hql_1);
            if(StringUtil.isBlank(tenantRole.getId())&&tt!=null ||
                    StringUtil.isNotBlank(tenantRole.getId())&&tt!=null&&(!tenantRole.getId().equals(tt.getId()))){
                logger.error("角色名已存在!");
                restResponse.setSuccess(false);
                restResponse.setErrorCode("0000");
                restResponse.setErrorMsg("角色名已存在!");
                return  restResponse;
            }

            if(StringUtil.isNotBlank(tenantRole.getId())){
                tenantRoleService.update(tenantRole.getId(),tenantRole);
            }else {
                tenantRoleService.save(tenantRole);
            }
           // logger.info("保存角色菜单");
            String moduleIds = tenantRole.getModuleIds();
            List<ModuleRole> mrList = new ArrayList();
             //   logger.info("查询已存在角色菜单集合");
                String hql = "from ModuleRole obj where obj.deleted = 0 and obj.tenantRole.id = " + "'" + tenantRole.getId() + "'";
                mrList = moduleRoleService.getList(hql);
          // logger.info("=============mrList.size()=:"+mrList==null?"null":mrList.size()+"");
            if(mrList!=null&&mrList.size()>0){
                String [] updateIdsArray =  new String[mrList.size()];
                int i = 0;
                for(ModuleRole mr:mrList){
                    updateIdsArray[i] = mr.getId();
                    i++;
                }
               // logger.info("删除已存在角色菜单");
                moduleRoleService.updateByArray("deleted", ConsoleConstants.DELETE_FLAG, "id", updateIdsArray);
            }

            if(StringUtil.isNotBlank(moduleIds)){
                String [] moduleIdsArray = moduleIds.split(",");
                for (String moduleId:moduleIdsArray){
                    ModuleRole mrt = new ModuleRole();
                    Module mt = new Module();
                    mt.setId(moduleId);
                    mrt.setModule(mt);
                    mrt.setTenantRole(tenantRole);
                    logger.info("roleId:"+tenantRole.getId());
                    logger.info("保存新角色菜单："+moduleId);
                    moduleRoleService.save(mrt);
                }

            }
            restResponse.setSuccess(true);
            restResponse.setErrorCode("1001");
            restResponse.setErrorMsg("操作成功");
        } catch (Exception e) {
            logger.error(e.getMessage(),e);
            restResponse.setSuccess(false);
            restResponse.setErrorCode("0000");
            restResponse.setErrorMsg("操作异常");
        }
        return restResponse;
    }


    /**
     * 删除
     * @param id
     * @return
     */
    @RequestMapping("/delete/{id}")
    public RestResponse delete(@PathVariable("id") String id) {
        RestResponse restResponse = new RestResponse();
        try {
            tenantRoleService.delete(id);
            String sql = "update  tb_module_role obj set obj.deleted = 1 where obj.role_id = " + "'" + id + "'";
            moduleRoleService.executeSql(sql);///删除模块角色表里的记录
           //logger.info("删除菜单数为："+r);
            restResponse.setSuccess(true);
            restResponse.setErrorCode("1001");
            restResponse.setErrorMsg("操作成功");
        } catch (Exception e) {
            logger.error(e.getMessage(),e);
            restResponse.setSuccess(false);
            restResponse.setErrorCode("0000");
            restResponse.setErrorMsg("操作异常");
        }
        return restResponse;
    }


    /**
     * 查询列表
     * @param
     * @param pageNo
     * @param pageSize
     * @return
     */
    @RequestMapping("/list")
    public RestResponse getPageList(@RequestParam(defaultValue = "1") Integer pageNo,
                                    @RequestParam(defaultValue = "20") Integer pageSize) {
        RestResponse restResponse = new RestResponse();
        try {
            String hql = "from TenantRole obj";
           /* String roleName = tenantRole.getRoleName();
            if (StringUtil.isNotBlank(roleName)) {
                hql = HqlUtil.addCondition(hql, "roleName", roleName,null,"Like",null);
            }*/
            Page page = tenantRoleService.pageList(hql,pageNo,pageSize);
            List<TenantRole> resultList = page.getResult();
            for(TenantRole tr:resultList){
                String roleId = tr.getId();
                String sql = "select count(1) from tb_tenant_account a where a.deleted = 0 and a.role_id ="+ "'" + roleId +"'";
                List<Object> rList = tenantRoleService.getListBySql(sql);
                Object countObj = rList.get(0);
                //logger.info("========countObj s class type name"+countObj.getClass().getTypeName());
                int userNumber = Integer.valueOf(String.valueOf(countObj));
                tr.setUserNumber(userNumber);
            }
            page.setResult(resultList);
            restResponse.setData(page);
            restResponse.setSuccess(true);
            restResponse.setErrorCode("1001");
            restResponse.setErrorMsg("操作成功");
        } catch (Exception e) {
            logger.error(e.getMessage(),e);
            restResponse.setSuccess(false);
            restResponse.setErrorCode("0000");
            restResponse.setErrorMsg("操作异常");
        }
        return restResponse;
    }

    /**
     * 批量操作
     * @param
     * @return
     */
    @RequestMapping(value = "/batchoperate",method = RequestMethod.POST)
    public RestResponse batchoperate(@RequestBody JSONObject jsonObj) {
        RestResponse restResponse = new RestResponse();
        String ids =  jsonObj.getString("ids");
        String type =  jsonObj.getString("type");
        try {
            if (StringUtil.isNotBlank(ids)&&StringUtil.isNotBlank(type)) {
                String [] idArray = ids.split(",");
                if (ConsoleConstants.OPERATE_TYPE_DELETE.equals(type)) {
                    tenantRoleService.updateByArray("deleted", ConsoleConstants.DELETE_FLAG, "id", idArray);
                }
            }else{
                return RestResponse.failed("0000","请求参数错误");
            }
            restResponse.setSuccess(true);
            restResponse.setErrorCode("1001");
            restResponse.setErrorMsg("操作成功");
        } catch (Exception e) {
            logger.error(e.getMessage(),e);
            restResponse.setSuccess(false);
            restResponse.setErrorCode("0000");
            restResponse.setErrorMsg("操作异常");
        }
        return restResponse;
    }

    /**
     * 角色下拉框查询列表
     * @return
     */
    @RequestMapping("/list2")
    public RestResponse<List<TenantRole>> list(){
        RestResponse<List<TenantRole>> restResponse=new RestResponse<List<TenantRole>>();
        try{
            String hql="from TenantRole obj ";
            List<TenantRole> list=tenantRoleService.getList(hql);
            restResponse.setData(list);
            restResponse.setSuccess(true);
        }catch (Exception ex){
            logger.error("系统异常：", ex);
            restResponse.setSuccess(false);
            restResponse.setErrorCode(ErrorConstant.INNER_ERROR_CODE);
            restResponse.setErrorMsg(ErrorConstant.getErrMsg(ErrorConstant.INNER_ERROR_CODE));
        }
        return restResponse;
    }





}
