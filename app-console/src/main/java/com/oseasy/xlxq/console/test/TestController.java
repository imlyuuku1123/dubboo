package com.oseasy.xlxq.console.test;

import com.oseasy.xlxq.console.base.AbstractPortalController;
import com.oseasy.xlxq.console.comm.ConsoleConstants;
import com.oseasy.xlxq.console.index.IndexVO;
import com.oseasy.xlxq.service.api.statistic.model.StatisticHomeStayDay;
import com.oseasy.xlxq.service.api.statistic.model.StatisticSpecialtyDay;
import com.oseasy.xlxq.service.api.statistic.model.StatisticTenantUserDay;
import com.oseasy.xlxq.service.api.statistic.service.StatisticHomeStayDayService;
import com.oseasy.xlxq.service.api.statistic.service.StatisticSpecialtyDayService;
import com.oseasy.xlxq.service.api.statistic.service.StatisticTenantUserDayService;
import com.oseasy.xlxq.service.api.statistic.service.StatisticTicketDayService;
import com.oseasy.xlxq.service.api.tenant.model.TenantHomeStayRoom;
import com.oseasy.xlxq.service.api.tenant.model.TenantMerchant;
import com.oseasy.xlxq.service.api.tenant.model.TenantProduct;
import com.oseasy.xlxq.service.api.tenant.model.TenantScenic;
import com.oseasy.xlxq.service.core.utils.DateUtils;
import com.oseasy.xlxq.service.core.utils.HqlUtil;
import com.oseasy.xlxq.service.core.utils.TreeNode;
import com.oseasy.xlxq.service.web.rest.RestRequest;
import com.oseasy.xlxq.service.web.rest.RestResponse;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;
import scala.App;

import javax.servlet.http.HttpServletRequest;
import java.util.HashMap;
import java.util.Map;
//
///**
// * 测试控制器
// *
// */
//@RestController
//@RequestMapping("/console/test")
//public class TestController extends AbstractPortalController {
//
//    private static final Logger logger = LoggerFactory.getLogger(TestController.class);
//
//    @Autowired
//    private StatisticHomeStayDayService statisticHomeStayDayService;
//
//    @Autowired
//    private StatisticSpecialtyDayService statisticSpecialtyDayService;
//
//    @Autowired
//    private StatisticTenantUserDayService statisticTenantUserDayService;
//
//    @Autowired
//    private StatisticTicketDayService statisticTicketDayService;
//
//    @RequestMapping(value = "", method = RequestMethod.GET)
//    public RestResponse index(HttpServletRequest request) {
//        RestResponse response = new RestResponse();
//        try {
//
//            for (int i = 0; i < 9; i++) {
//                logger.info("保存第" + i + "个");
//
//                StatisticHomeStayDay statisticHomeStayDay = new StatisticHomeStayDay();
//                statisticHomeStayDay.setDay(18);
//                statisticHomeStayDay.setMonth(5);
//                statisticHomeStayDay.setYear(2017);
//                statisticHomeStayDay.setSaleMoney(1000.00);
//                statisticHomeStayDay.setSaleNum(100);
//
//                TenantHomeStayRoom tenantHomeStayRoom = new TenantHomeStayRoom();
//                tenantHomeStayRoom.setId(i + "");
//                statisticHomeStayDay.setTenantHomeStayRoom(tenantHomeStayRoom);
//
//                TenantMerchant tenantMerchant = new TenantMerchant();
//                tenantMerchant.setId(i + "");
//                statisticHomeStayDay.setTenantMerchant(tenantMerchant);
//
//                TenantScenic tenantScenic = new TenantScenic();
//                tenantScenic.setId(i + "");
//                statisticHomeStayDay.setTenantScenic(tenantScenic);
//                statisticHomeStayDayService.save(statisticHomeStayDay);
//
//            }
//            response.setErrorCode("1001");
//            response.setErrorMsg("操作成功");
//            response.setSuccess(true);
//        } catch (Exception e) {
//            logger.error("操作异常！");
//            logger.error(e.getMessage(), e);
//            response.setErrorCode("0000");
//            response.setErrorMsg("操作异常");
//            response.setSuccess(false);
//            return response;
//        }
//            return response;
//    }
//}