<%@ page import="com.oseasy.xlxq.console.comm.ErrorConstant" %>
<%@ page import="com.oseasy.xlxq.service.api.exceptions.XlxqRuntimeException" %>
<%@ page import="com.oseasy.xlxq.service.core.utils.JSONUtil" %>
<%@ page import="com.oseasy.xlxq.service.core.utils.StringUtil" %>
<%@ page import="com.oseasy.xlxq.service.web.rest.RestResponse" %>
<%@ page import="com.oseasy.xlxq.service.web.utils.WebUtils" %>
<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8" isELIgnored="false" isErrorPage="true"%>
<%
	String header = request.getHeader("X-Requested-With");
	if(StringUtil.isNotEmpty(header) && header.equals("XMLHttpRequest")){
		RestResponse responsex = null;
		if(exception instanceof XlxqRuntimeException){
			XlxqRuntimeException xlxqEx = (XlxqRuntimeException) exception;
			responsex = RestResponse.failed(xlxqEx.getCode(),xlxqEx.getMsg());
		}else{
			responsex = RestResponse.failed(ErrorConstant.INNER_ERROR_CODE,exception.getMessage());
		}
		WebUtils.outputJson(request,response, JSONUtil.objectToJson(responsex));

	}else{
%>
			出现异常
			<H1>错误：</H1><%=exception%>
			<H2>错误内容：</H2> <%
		exception.printStackTrace(response.getWriter());
	}

%>
