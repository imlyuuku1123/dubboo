package com.oseasy.xlxq.service.sms.clients;

import com.oseasy.xlxq.service.api.sms.model.SMSSendLog;

import java.util.Map;

/**
 * Created by Tandy on 2016/7/7.
 */
public interface SMSClient {
    /**
     * 发送短信给制定手机号
     * @param to  目标手机号
     * @param msg 消息类型
     * @return
     *      成功  true
     *      失败  false
     */
    public SMSSendLog sendsms(String to, String msg);

    /**
     * 采用模板进行短信发送
     * @param to
     * @param tempNo
     * @param params
     * @return
     */
    public SMSSendLog sendsmsByTemplate(String to,String tempNo,Map<String,String> params);


    /**
     * 提供商名称
     * @return
     */
    public String getClientName();
}
