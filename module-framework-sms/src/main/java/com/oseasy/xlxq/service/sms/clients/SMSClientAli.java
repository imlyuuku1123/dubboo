package com.oseasy.xlxq.service.sms.clients;

import com.aliyun.mns.client.CloudAccount;
import com.aliyun.mns.client.CloudTopic;
import com.aliyun.mns.client.MNSClient;
import com.aliyun.mns.common.ServiceException;
import com.aliyun.mns.model.BatchSmsAttributes;
import com.aliyun.mns.model.MessageAttributes;
import com.aliyun.mns.model.RawTopicMessage;
import com.aliyun.mns.model.TopicMessage;
import com.oseasy.xlxq.service.api.sms.model.SMSSendLog;
import com.oseasy.xlxq.service.config.SystemConfig;
import com.oseasy.xlxq.service.core.utils.JSONUtil;
import com.oseasy.xlxq.service.core.utils.UUIDGenerator;
import com.sun.scenario.effect.impl.sw.sse.SSEBlend_SRC_OUTPeer;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.List;
import java.util.Map;

/**
 * Created by tandy on 2017/5/17.
 */
public class SMSClientAli implements SMSClient{

    private static final Logger logger = LoggerFactory.getLogger(SMSClientAli.class);


    private String aliKey= SystemConfig.getProperty("global.aliyun.key");
    private String aliSecret= SystemConfig.getProperty("global.aliyun.secret");
    private String endpoint = SystemConfig.getProperty("global.sms.aliyun.endpoint");
    private String topic = SystemConfig.getProperty("global.sms.aliyun.topic");
    private String smsSign = SystemConfig.getProperty("global.sms.aliyun.smssign");

    private CloudAccount account;
    private MNSClient client;
    private CloudTopic cloudTopic;

    public SMSClientAli(){
        /**
         * Step 1. 获取主题引用
         */
        account = new CloudAccount(aliKey, aliSecret, endpoint);
        client = account.getMNSClient();
        cloudTopic = client.getTopicRef(topic);
    }

    @Override
    public SMSSendLog sendsms(String to, String msg) {
        throw new RuntimeException("阿里云不支持直接发送内容的短信服务");
    }

    @Override
    public SMSSendLog sendsmsByTemplate(String to, String tempNo, Map<String,String> params) {
        SMSSendLog sendLog = new SMSSendLog();
        sendLog.setMobile(to);
        sendLog.setSendType(SMSSendLog.TYPE_BYTEMPATE);
        sendLog.setProvider("ali");
        sendLog.setParams(JSONUtil.mapToJson(params));
        sendLog.setTemplateNo(tempNo);
        sendLog.setStatus(SMSSendLog.STATUS_NOSEND);

        /**
         * Step 2. 设置SMS消息体（必须）
         *
         * 注：目前暂时不支持消息内容为空，需要指定消息内容，不为空即可。
         */
        RawTopicMessage msg = new RawTopicMessage();

        msg.setMessageBody("sms-message");
        /**
         * Step 3. 生成SMS消息属性
         */
        MessageAttributes messageAttributes = new MessageAttributes();
        BatchSmsAttributes batchSmsAttributes = new BatchSmsAttributes();
        // 3.1 设置发送短信的签名（SMSSignName）
        batchSmsAttributes.setFreeSignName(smsSign);
        // 3.2 设置发送短信使用的模板（SMSTempateCode）
//        batchSmsAttributes.setTemplateCode("SMS_65070087");
        batchSmsAttributes.setTemplateCode(tempNo);

        // 3.3 设置发送短信所使用的模板中参数对应的值（在短信模板中定义的，没有可以不用设置）
        BatchSmsAttributes.SmsReceiverParams smsReceiverParams = new BatchSmsAttributes.SmsReceiverParams();

        for(String param:params.keySet()){
            smsReceiverParams.setParam(param,params.get(param));
        }

        // 3.4 增加接收短信的号码
        batchSmsAttributes.addSmsReceiver(to, smsReceiverParams);
//        batchSmsAttributes.addSmsReceiver("$YourReceiverPhoneNumber2", smsReceiverParams);
        messageAttributes.setBatchSmsAttributes(batchSmsAttributes);
        try {
//            String bid = UUIDGenerator.uuid();
//            msg.setMessageTag(bid);
            /**
             * Step 4. 发布SMS消息
             */
            TopicMessage ret = cloudTopic.publishMessage(msg, messageAttributes);
            System.out.println("MessageId: " + ret.getMessageId());
            System.out.println("MessageMD5: " + ret.getMessageBodyMD5());


            sendLog.setBid(ret.getMessageId());
            sendLog.setStatus(SMSSendLog.STATUS_ING);

        } catch (ServiceException se) {
            logger.error("短信发送失败["+to+"]["+tempNo+"]["+JSONUtil.mapToJson(params)+"]",se);
            sendLog.setStatus(SMSSendLog.STATUS_FAILED);
            sendLog.setRemark(se.getErrorCode() + se.getRequestId() + "==>" + se.getMessage());
        } catch (Exception e) {
            logger.error("短信发送失败["+to+"]["+tempNo+"]["+JSONUtil.mapToJson(params)+"]",e);
        }
        return sendLog;
    }

    @Override
    public String getClientName() {
        return "ali";
    }
}
