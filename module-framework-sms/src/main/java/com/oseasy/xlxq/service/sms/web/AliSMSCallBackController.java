package com.oseasy.xlxq.service.sms.web;

import com.oseasy.xlxq.service.web.utils.WebUtils;
import org.apache.commons.io.IOUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.io.InputStream;

/**
 * Created by tandy on 2017/5/17.
 */
@Controller()
public class AliSMSCallBackController {

    private static final Logger logger = LoggerFactory.getLogger(AliSMSCallBackController.class);

    /**
     * 请求包体结构
     * messageID=F9A7D08415A6FD09-1-15C1AD1C7CE-200000009&receiver=13971068693&state=1&biz_id=107573770005^1110187181881&template_code=SMS_65070087&sms_count=1&receive_time=2017-05-18 17:09:39&ver=1.1&extra=&event=SendSuccessfully
     * @param request
     * @param response
     */
    @RequestMapping("/sms/ali/callback")
    public void callback(HttpServletRequest request, HttpServletResponse response){
        String requestBody = WebUtils.getRequestBody(request,"utf-8");
        if(logger.isDebugEnabled()){
            logger.debug("收到阿里短信回调:{}",requestBody);
        }
    }
}
