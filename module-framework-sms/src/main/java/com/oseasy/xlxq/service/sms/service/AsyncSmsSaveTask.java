package com.oseasy.xlxq.service.sms.service;

import com.oseasy.xlxq.service.api.sms.model.SMSSendLog;
import com.oseasy.xlxq.service.api.sms.service.SMSSendLogService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.scheduling.annotation.Async;
import org.springframework.stereotype.Component;

/**
 * Created by Tandy on 2016/7/9.
 */
@Component
public class AsyncSmsSaveTask {

    private static final Logger logger = LoggerFactory.getLogger(AsyncSmsSaveTask.class);

    @Autowired
    private SMSSendLogService smsSendLogService;

    /**
     * 异步入库
     * @param smsSendLog
     */
//    @Async
    public void saveToDB(SMSSendLog smsSendLog){
        if(logger.isDebugEnabled()){
            logger.debug("发送短信异步入库：{}",smsSendLog);
        }
        smsSendLogService.save(smsSendLog);
    }

}
