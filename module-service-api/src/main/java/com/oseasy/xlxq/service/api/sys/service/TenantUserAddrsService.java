package com.oseasy.xlxq.service.api.sys.service;

import com.oseasy.xlxq.service.api.base.BaseService;
import com.oseasy.xlxq.service.api.sys.model.TenantUserAddrs;

import java.util.List;

public interface TenantUserAddrsService extends BaseService<TenantUserAddrs> {


}
