package com.oseasy.xlxq.service.api.statistic.model;

import javax.persistence.Transient;

/**
 * Created by heckman on 2017/6/14.
 */
public class StatisticHomeStaySellExcel {

    private String dateStr;
    private String scenicName;
    private String merchantName;
    private String productName;
    private String sellNum;
    private String sellSum;

    @Transient
    public String getDateStr() {
        return dateStr;
    }
    @Transient
    public String getScenicName() {
        return scenicName;
    }
    @Transient
    public String getMerchantName() {
        return merchantName;
    }
    @Transient
    public String getProductName() {
        return productName;
    }
    @Transient
    public String getSellNum() {
        return sellNum;
    }
    @Transient
    public String getSellSum() {
        return sellSum;
    }

    public void setDateStr(String dateStr) {
        this.dateStr = dateStr;
    }

    public void setScenicName(String scenicName) {
        this.scenicName = scenicName;
    }

    public void setMerchantName(String merchantName) {
        this.merchantName = merchantName;
    }

    public void setProductName(String productName) {
        this.productName = productName;
    }

    public void setSellNum(String sellNum) {
        this.sellNum = sellNum;
    }

    public void setSellSum(String sellSum) {
        this.sellSum = sellSum;
    }
}
