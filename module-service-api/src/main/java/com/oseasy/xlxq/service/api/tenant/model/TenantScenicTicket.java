package com.oseasy.xlxq.service.api.tenant.model;

import com.oseasy.xlxq.service.api.base.IdEntity;
import org.hibernate.annotations.NotFound;
import org.hibernate.annotations.NotFoundAction;
import org.hibernate.annotations.Where;

import javax.persistence.*;
import java.util.Date;


/**
 * 门票
 *
 */
@Entity
@Where(clause = "deleted=0")
@Table(schema = "db_oe_xlxq", name = "tb_tenant_scenic_ticket")
@PrimaryKeyJoinColumn(name = "id")
public class TenantScenicTicket extends TenantProduct {

//    private TenantProduct tenantProduct;//所属产品
    private TenantScenic tenantScenic;//所属景区

    private String ticketName;//门票名称
    private String salesVolume;//销量
    private String purchaseNum;//购买人数
    private String saleStatus;//是否核销
    private Date usertimeStart;//使用时间起
    private Date usertimeEnd;//使用时间止
    private String linkmanName;//联系人姓名
    private String linkmanTel;//联系人电话
    private String purchaseDes;//购买说明
    private String changeRule;//改退规则
    private String importantCla;//重要条款
    private String buyNum;//购买数量虚拟字段

//    @ManyToOne
//    @JoinColumn(name = "product_id")
//    @NotFound(action = NotFoundAction.IGNORE)
//    public TenantProduct getTenantProduct() {
//        return tenantProduct;
//    }
//
//    public void setTenantProduct(TenantProduct tenantProduct) {
//        this.tenantProduct = tenantProduct;
//    }

    @ManyToOne
    @JoinColumn(name = "scenic_id")
    @NotFound(action = NotFoundAction.IGNORE)
    public TenantScenic getTenantScenic() {
        return tenantScenic;
    }

    public void setTenantScenic(TenantScenic tenantScenic) {
        this.tenantScenic = tenantScenic;
    }

    @Column(name = "ticket_name")
    public String getTicketName() {
        return ticketName;
    }

    public void setTicketName(String ticketName) {
        this.ticketName = ticketName;
    }

    @Column(name = "sales_volume")
    public String getSalesVolume() {
        return salesVolume;
    }

    public void setSalesVolume(String salesVolume) {
        this.salesVolume = salesVolume;
    }

    @Column(name = "purchase_num")
    public String getPurchaseNum() {
        return purchaseNum;
    }

    public void setPurchaseNum(String purchaseNum) {
        this.purchaseNum = purchaseNum;
    }

    @Column(name = "sale_status")
    public String getSaleStatus() {
        return saleStatus;
    }

    public void setSaleStatus(String saleStatus) {
        this.saleStatus = saleStatus;
    }

    @Column(name = "usertime_start")
    public Date getUsertimeStart() {
        return usertimeStart;
    }

    public void setUsertimeStart(Date usertimeStart) {
        this.usertimeStart = usertimeStart;
    }

    @Column(name = "usertime_end")
    public Date getUsertimeEnd() {
        return usertimeEnd;
    }

    public void setUsertimeEnd(Date usertimeEnd) {
        this.usertimeEnd = usertimeEnd;
    }

    @Column(name = "linkman_name")
    public String getLinkmanName() {
        return linkmanName;
    }

    public void setLinkmanName(String linkmanName) {
        this.linkmanName = linkmanName;
    }

    @Column(name = "linkman_tel")
    public String getLinkmanTel() {
        return linkmanTel;
    }

    public void setLinkmanTel(String linkmanTel) {
        this.linkmanTel = linkmanTel;
    }

    @Column(name = "purchase_des")
    public String getPurchaseDes() {
        return purchaseDes;
    }

    public void setPurchaseDes(String purchaseDes) {
        this.purchaseDes = purchaseDes;
    }

    @Column(name = "change_rule")
    public String getChangeRule() {
        return changeRule;
    }

    public void setChangeRule(String changeRule) {
        this.changeRule = changeRule;
    }

    @Column(name = "important_cla")
    public String getImportantCla() {
        return importantCla;
    }

    public void setImportantCla(String importantCla) {
        this.importantCla = importantCla;
    }

    @Transient
    public String getBuyNum() {
        return buyNum;
    }

    public void setBuyNum(String buyNum) {
        this.buyNum = buyNum;
    }

}
