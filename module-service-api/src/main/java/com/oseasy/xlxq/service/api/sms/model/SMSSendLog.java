package com.oseasy.xlxq.service.api.sms.model;

import com.oseasy.xlxq.service.api.base.IdEntity;
import org.hibernate.annotations.Where;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Table;

/**
 * Created by Tandy on 2016/7/7.
 */
@Entity
@Where(clause = "deleted=0")
@Table(schema = "db_oe_xlxq", name = "tb_sms_send_log")
public class SMSSendLog  extends IdEntity {


    public static final int TYPE_BYTEMPATE=1;  //使用模板方式发送
    public static final int TYPE_BYCONTENT=2;  //使用内容直接发送  有些提供商不支持该方式

    public static final int STATUS_NOSEND=0;  //默认没有发送
    public static final int STATUS_ING=1;  //发送中
    public static final int STATUS_SUCCESS=2;  //发送成功
    public static final int STATUS_FAILED=3;    //发送失败

    private String bid; //  对账标识，存储提供商发送的标识

    private String mobile;  //发送手机号

    private int status;  //发送状态

    private int sendType;  //发送方式

    private String provider; //短信服务提供者

    private String templateNo;  //模板号

    private String params; //模板参数

    private String smsContent;  //短信内容

    @Column(name="remark")
    public String getRemark() {
        return remark;
    }

    public void setRemark(String remark) {
        this.remark = remark;
    }

    private String remark;//备注


    @Column(name="bid")
    public String getBid() {
        return bid;
    }

    public void setBid(String bid) {
        this.bid = bid;
    }
    @Column(name="mobile")
    public String getMobile() {
        return mobile;
    }

    public void setMobile(String mobile) {
        this.mobile = mobile;
    }

    @Column(name="status")
    public int getStatus() {
        return status;
    }

    public void setStatus(int status) {
        this.status = status;
    }

    @Column(name="send_type")
    public int getSendType() {
        return sendType;
    }

    public void setSendType(int sendType) {
        this.sendType = sendType;
    }

    @Column(name="provider")
    public String getProvider() {
        return provider;
    }

    public void setProvider(String provider) {
        this.provider = provider;
    }

    @Column(name="template_no")
    public String getTemplateNo() {
        return templateNo;
    }


    public void setTemplateNo(String templateNo) {
        this.templateNo = templateNo;
    }

    @Column(name="template_params")
    public String getParams() {
        return params;
    }

    public void setParams(String params) {
        this.params = params;
    }

    @Column(name="sms_content")
    public String getSmsContent() {
        return smsContent;
    }

    public void setSmsContent(String smsContent) {
        this.smsContent = smsContent;
    }

    public SMSSendLog(){

    }


}
