package com.oseasy.xlxq.service.api.tenant.model;

import com.oseasy.xlxq.service.api.base.IdEntity;
import com.oseasy.xlxq.service.api.sys.model.TenantRole;
import org.hibernate.annotations.NotFound;
import org.hibernate.annotations.NotFoundAction;
import org.hibernate.annotations.Where;

import javax.persistence.*;

/**
 * 租户用户
 * @author tandy
 */
@Entity
@Where(clause = "deleted=0")
@Table(schema = "db_oe_xlxq", name = "tb_tenant_account")
public class TenantAccount extends IdEntity {

	public static final int STATUS_NOT_ACTIVE = 0; 	//账号未激活
	public static final int STATUS_LOCK = 1; 		//账号锁定
	public static final int STATUS_NORMAL = 2; 		//账号正常
	public static final int STATUS_ABNORMAL = 3; 	//账号异常
	public static final int STATUS_EXPIRE = 4; 		//账号过期
	public static final String DEFAULT_PASSWORD = "888888";

	private String userName;
	private Tenant tenant;                //所属租户
	private String password;            //密码

	private String mm;

	private Integer status;                    //账号状态			租户用户状态：0-未激活 1-LOCK 2-正常  3-异常
	private String displayName;		//显示名称

	private String remarks;
	private TenantRole tenantRole;

	@Column(name = "remarks")
	public String getRemarks() {
		return remarks;
	}

	public void setRemarks(String remarks) {
		this.remarks = remarks;
	}

	@ManyToOne
	@JoinColumn(name = "role_id")
	@NotFound(action = NotFoundAction.IGNORE)
	public TenantRole getTenantRole() {
		return tenantRole;
	}

	public void setTenantRole(TenantRole tenantRole) {
		this.tenantRole = tenantRole;
	}

	@Column(name = "mm")
	public String getMm() {
		return mm;
	}

	public void setMm(String mm) {
		this.mm = mm;
	}

	@Column(name = "display_name")
	public String getDisplayName() {
		return displayName;
	}

	public void setDisplayName(String displayName) {
		this.displayName = displayName;
	}

	@Column(name = "username")
	public String getUserName() {
		return userName;
	}

	public void setUserName(String userName) {
		this.userName = userName;
	}

	@Column(name = "password")
	public String getPassword() {
		return password;
	}

	public void setPassword(String password) {
		this.password = password;
	}

	@Column(name = "status")
	public Integer getStatus() {
		return status;
	}

	public void setStatus(Integer status) {
		this.status = status;
	}

	@ManyToOne
	@JoinColumn(name = "tenant_id")
	@NotFound(action = NotFoundAction.IGNORE)
	public Tenant getTenant() {
		return tenant;
	}

	public void setTenant(Tenant tenant) {
		this.tenant = tenant;
	}


	@Override
	public String toString() {
		String account =  "TenantAccount{" +
				"userName='" + userName + '\'' +
				"displayName='" + displayName + '\'' +
				", password='" + password + '\'' +
				", status=" + status;
		if(tenant!=null){
			account+=",tenatn='"+tenant.toString()+"'";
		}
		account+='}';
		return account;
	}
}
