package com.oseasy.xlxq.service.api.sys.model;

import com.oseasy.xlxq.service.api.base.IdEntity;
import com.oseasy.xlxq.service.api.tenant.model.Tenant;
import org.hibernate.annotations.NotFound;
import org.hibernate.annotations.NotFoundAction;
import org.hibernate.annotations.Where;

import javax.persistence.*;

/**
 * 配置公众号
 *
 */
@Entity
@Where(clause = "deleted=0")
@Table(schema = "db_oe_xlxq", name = "tb_tenant_config_wx")
public class TenantConfigWx extends IdEntity {

    private String wxMpAccount;//微信公众号账号
    private String wxMpName;//微信公众号名称
    private String wxMpKey;//秘钥
    private String wxMpAppid;//APPID
    private String wxMpInfokey;//消息秘钥
    private String wxMpUrl;//微信域名
    private String wxMpToken;//微信token
    private Tenant tenant;//所属租户

    @Column(name = "wx_mp_account")
    public String getWxMpAccount() {
        return wxMpAccount;
    }

    public void setWxMpAccount(String wxMpAccount) {
        this.wxMpAccount = wxMpAccount;
    }

    @Column(name = "wx_mp_name")
    public String getWxMpName() {
        return wxMpName;
    }

    public void setWxMpName(String wxMpName) {
        this.wxMpName = wxMpName;
    }

    @Column(name = "wx_mp_key")
    public String getWxMpKey() {
        return wxMpKey;
    }

    public void setWxMpKey(String wxMpKey) {
        this.wxMpKey = wxMpKey;
    }

    @Column(name = "wx_mp_appid")
    public String getWxMpAppid() {
        return wxMpAppid;
    }

    public void setWxMpAppid(String wxMpAppid) {
        this.wxMpAppid = wxMpAppid;
    }

    @Column(name = "wx_mp_infokey")
    public String getWxMpInfokey() {
        return wxMpInfokey;
    }

    public void setWxMpInfokey(String wxMpInfokey) {
        this.wxMpInfokey = wxMpInfokey;
    }

    @Column(name = "wx_mp_url")
    public String getWxMpUrl() {
        return wxMpUrl;
    }

    public void setWxMpUrl(String wxMpUrl) {
        this.wxMpUrl = wxMpUrl;
    }

    @Column(name = "wx_mp_token")
    public String getWxMpToken() {
        return wxMpToken;
    }

    public void setWxMpToken(String wxMpToken) {
        this.wxMpToken = wxMpToken;
    }

    @ManyToOne
    @JoinColumn(name = "tenant_id")
    @NotFound(action = NotFoundAction.IGNORE)
    public Tenant getTenant() {
        return tenant;
    }

    public void setTenant(Tenant tenant) {
        this.tenant = tenant;
    }




}



