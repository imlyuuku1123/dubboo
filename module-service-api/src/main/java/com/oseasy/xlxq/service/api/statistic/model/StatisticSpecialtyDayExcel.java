package com.oseasy.xlxq.service.api.statistic.model;

import javax.persistence.Transient;

/**
 * 土特产日销量统计导出VO类
 * Created by heckman on 2017/6/12.
 */
public class StatisticSpecialtyDayExcel {

    private String name;
    private String merchantName;
    private String rootCatlog;
    private String catlog;
    private String sellNum;
    private String sellSum;

    @Transient
    public String getName() {
        return name;
    }

    @Transient
    public String getMerchantName() {
        return merchantName;
    }

    @Transient
    public String getRootCatlog() {
        return rootCatlog;
    }

    @Transient
    public String getCatlog() {
        return catlog;
    }

    @Transient
    public String getSellNum() {
        return sellNum;
    }

    @Transient
    public String getSellSum() {
        return sellSum;
    }

    public void setName(String name) {
        this.name = name;
    }

    public void setMerchantName(String merchantName) {
        this.merchantName = merchantName;
    }

    public void setRootCatlog(String rootCatlog) {
        this.rootCatlog = rootCatlog;
    }

    public void setCatlog(String catlog) {
        this.catlog = catlog;
    }

    public void setSellNum(String sellNum) {
        this.sellNum = sellNum;
    }

    public void setSellSum(String sellSum) {
        this.sellSum = sellSum;
    }
}
