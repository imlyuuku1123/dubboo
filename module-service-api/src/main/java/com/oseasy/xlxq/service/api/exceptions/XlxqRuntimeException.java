package com.oseasy.xlxq.service.api.exceptions;

/**
 * Created by tandy on 17/5/24.
 */
public abstract class XlxqRuntimeException extends Exception {

    private String code;
    private String msg;

    public XlxqRuntimeException(String code,String msg,Throwable ex){
        super(code +":" + msg,ex);
        this.code = code;
        this.msg = msg;
    }


    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public String getMsg() {
        return msg;
    }

    public void setMsg(String msg) {
        this.msg = msg;
    }
}
