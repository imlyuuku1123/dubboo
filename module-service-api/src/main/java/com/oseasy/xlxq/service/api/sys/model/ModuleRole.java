package com.oseasy.xlxq.service.api.sys.model;

import com.oseasy.xlxq.service.api.base.IdEntity;
import org.hibernate.annotations.NotFound;
import org.hibernate.annotations.NotFoundAction;
import org.hibernate.annotations.Where;

import javax.persistence.*;


/**
 * 模块角色关系
 *
 */
@Entity
@Where(clause = "deleted=0")
@Table(schema = "db_oe_xlxq", name = "tb_module_role")
public class ModuleRole extends IdEntity {

   private TenantRole tenantRole;//角色
   private Module module;//模块


    @ManyToOne
    @JoinColumn(name = "role_id")
    @NotFound(action = NotFoundAction.IGNORE)
    public TenantRole getTenantRole() {
        return tenantRole;
    }

    public void setTenantRole(TenantRole tenantRole) {
        this.tenantRole = tenantRole;
    }

    @ManyToOne
    @JoinColumn(name = "module_id")
    @NotFound(action = NotFoundAction.IGNORE)
    public Module getModule() {
        return module;
    }

    public void setModule(Module module) {
        this.module = module;
    }

}
