package com.oseasy.xlxq.service.api.sys.service;

import com.oseasy.xlxq.service.api.base.BaseService;
import com.oseasy.xlxq.service.api.sys.model.TenantConfigWx;

public interface TenantConfigWxService extends BaseService<TenantConfigWx> {

}
