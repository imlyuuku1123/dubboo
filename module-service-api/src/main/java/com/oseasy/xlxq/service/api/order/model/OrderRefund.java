package com.oseasy.xlxq.service.api.order.model;

import com.oseasy.xlxq.service.api.base.IdEntity;
import org.hibernate.annotations.NotFound;
import org.hibernate.annotations.NotFoundAction;
import org.hibernate.annotations.Where;

import javax.persistence.*;
import java.math.BigDecimal;
import java.util.Date;

/*

/**
 * 产品退货退款退单
 *
 */
@Entity
@Where(clause = "deleted=0")
@Table(schema = "db_oe_xlxq", name = "tb_order_refund")
public class OrderRefund extends IdEntity {


    //退款状态（11：退款审批中；12：待退款；13：已退款；14：退款中；15：退款失败）
    public static String REFUND_APPROVAL_PENGDING = "11";//11：退款审批中
    public static String REFUND_CONFIRM = "12";          //12：待退款/退款确认
    public static String REFUND_SUCCESS = "13";           //13：已退款/退款成功
    public static String REFUND_PENDING = "14";          //14：退款中
    public static String REFUND_FAILED = "15";           //15：退款失败


    //private String orderNo;//下单订单id

    private OrderInfo orderInfo;//退款订单对象

    private String refundId;//退款订单号

    private String refundStatus;//是否退款

    private String refundType;//退款方式

    private String refundLogisticnum;//退款物流单号

    private Date refundTime;//退款时间

    private BigDecimal refundMoney;//退款金额

    private String addressId;//地址id

    private String refundReason;//退款原因

    @ManyToOne
    @JoinColumn(name = "order_id")
    @NotFound(action = NotFoundAction.IGNORE)
    public OrderInfo getOrderInfo() {
        return orderInfo;
    }

    public void setOrderInfo(OrderInfo orderInfo) {
        this.orderInfo = orderInfo;
    }

    @Column(name = "refund_reason")
    public String getRefundReason() {
        return refundReason;
    }

    public void setRefundReason(String refundReason) {
        this.refundReason = refundReason;
    }


    @Column(name = "refund_id")
    public String getRefundId() {
        return refundId;
    }

    public void setRefundId(String refundId) {
        this.refundId = refundId;
    }

    @Column(name = "refund_status")
    public String getRefundStatus() {
        return refundStatus;
    }


    public void setRefundStatus(String refundStatus) {
        this.refundStatus = refundStatus;
    }

    @Column(name = "refund_type")
    public String getRefundType() {
        return refundType;
    }

    public void setRefundType(String refundType) {
        this.refundType = refundType;
    }

    @Column(name = "refund_logisticnum")
    public String getRefundLogisticnum() {
        return refundLogisticnum;
    }

    public void setRefundLogisticnum(String refundLogisticnum) {
        this.refundLogisticnum = refundLogisticnum;
    }

    @Column(name = "refund_time")
    public Date getRefundTime() {
        return refundTime;
    }

    public void setRefundTime(Date refundTime) {
        this.refundTime = refundTime;
    }

    @Column(name = "refund_money")
    public BigDecimal getRefundMoney() {
        return refundMoney;
    }

    public void setRefundMoney(BigDecimal refundMoney) {
        this.refundMoney = refundMoney;
    }

    @Column(name = "address_id")
    public String getAddressId() {
        return addressId;
    }

    public void setAddressId(String addressId) {
        this.addressId = addressId;
    }

}
