package com.oseasy.xlxq.service.api.sys.model;

import com.oseasy.xlxq.service.api.base.IdEntity;
import org.hibernate.annotations.Where;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Table;
import java.util.Date;

/**
 * 微信广告
 *
 */
@Entity
@Where(clause = "deleted=0")
@Table(schema = "db_oe_xlxq", name = "tb_wxadvertis")
public class WxAdvertis extends IdEntity {

	private String  fileUrl;//文件地址

	private int  iscut;//是否有效



	@Column(name = "file_url")
	public String getFileUrl() {
		return fileUrl;
	}

	public void setFileUrl(String fileUrl) {
		this.fileUrl = fileUrl;
	}

	@Column(name = "iscut")
	public int getIscut() {
		return iscut;
	}

	public void setIscut(int iscut) {
		this.iscut = iscut;
	}
}
