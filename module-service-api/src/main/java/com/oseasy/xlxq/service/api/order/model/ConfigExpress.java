package com.oseasy.xlxq.service.api.order.model;

import com.oseasy.xlxq.service.api.base.IdEntity;
import org.hibernate.annotations.Where;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Table;

/**
 * 物流公司对应物流公司CODE
 * Created by heckman on 2017/5/26.
 */

@Entity
@Where(clause = "deleted=0")
@Table(schema = "db_oe_xlxq", name = "tb_config_express")
public class ConfigExpress extends IdEntity {

    private String name;//快递公司名称
    private String code;//快递公司代号

    @Column(name = "name")
    public String getName() {
        return name;
    }

    @Column(name = "code")
    public String getCode() {
        return code;
    }

    public void setName(String name) {
        this.name = name;
    }

    public void setCode(String code) {
        this.code = code;
    }
}
