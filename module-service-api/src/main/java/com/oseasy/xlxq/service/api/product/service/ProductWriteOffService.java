package com.oseasy.xlxq.service.api.product.service;

import com.oseasy.xlxq.service.api.base.BaseService;
import com.oseasy.xlxq.service.api.product.model.ProductWriteOff;

public interface ProductWriteOffService extends BaseService<ProductWriteOff> {

}
