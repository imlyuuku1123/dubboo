package com.oseasy.xlxq.service.api.video.model;

import com.oseasy.xlxq.service.api.base.IdEntity;
import com.oseasy.xlxq.service.api.tenant.model.Tenant;
import org.hibernate.annotations.NotFound;
import org.hibernate.annotations.NotFoundAction;
import org.hibernate.annotations.Where;

import javax.persistence.*;

/**
 * 直播房间
 */
@Entity
@Where(clause = "deleted=0")
@Table(schema = "db_oe_xlxq", name = "tb_video_lib")
public class VideoLib extends IdEntity {

    private Tenant tenant;//租户表示

    private String name;//素材名称

    private String type;//类型

    private String fileUrl;//FileURL

    private String titleImgUrl;//封面相对URL


    @ManyToOne
    @JoinColumn(name = "tenant_id")
    @NotFound(action = NotFoundAction.IGNORE)
    public Tenant getTenant() {
        return tenant;
    }

    public void setTenant(Tenant tenant) {
        this.tenant = tenant;
    }

    @Column(name = "name")
    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    @Column(name = "type")
    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    @Column(name = "file_url")
    public String getFileUrl() {
        return fileUrl;
    }

    public void setFileUrl(String fileUrl) {
        this.fileUrl = fileUrl;
    }

    @Column(name = "titleImgUrl")
    public String getTitleImgUrl() {
        return titleImgUrl;
    }

    public void setTitleImgUrl(String titleImgUrl) {
        this.titleImgUrl = titleImgUrl;
    }

    @Override
    public String toString() {
        return "VideoLib{" +
                "tenant=" + tenant +
                ", name=" + name +
                ", type='" + type + '\'' +
                ", fileUrl='" + fileUrl + '\'' +
                ", titleImgUrl=" + titleImgUrl +
                '}';
    }
}
