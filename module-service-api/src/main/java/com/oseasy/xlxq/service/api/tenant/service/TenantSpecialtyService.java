package com.oseasy.xlxq.service.api.tenant.service;

import com.oseasy.xlxq.service.api.base.BaseService;
import com.oseasy.xlxq.service.api.tenant.model.TenantSpecialty;
import com.oseasy.xlxq.service.api.tenant.model.TenantSpecialtyCatalog;
import com.oseasy.xlxq.service.core.utils.Page;


public interface TenantSpecialtyService extends BaseService<TenantSpecialty> {


}
