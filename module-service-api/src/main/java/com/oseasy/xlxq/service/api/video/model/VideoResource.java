package com.oseasy.xlxq.service.api.video.model;

import com.oseasy.xlxq.service.api.base.IdEntity;
import com.oseasy.xlxq.service.api.tenant.model.Tenant;
import org.hibernate.annotations.Where;

import javax.persistence.Entity;
import javax.persistence.Table;

/**
 * 直播频道
 */
@Entity
@Where(clause = "deleted=0")
@Table(schema = "db_oe_xlxq", name = "tb_video_lib")
public class VideoResource extends IdEntity {

    private Tenant tenant;

    private String name;

    private String type;

    private String fileUrl;

    private String titleImgUrl;


//
//    private String channelName;//频道名称
//
//    private String channelPic;//频道图片
//
//    private int chaRmNum;//直播间数量
//
//    @Column(name = "channel_name")
//    public String getChannelName() {
//        return channelName;
//    }
//
//    public void setChannelName(String channelName) {
//        this.channelName = channelName;
//    }
//
//    @Column(name = "channel_pic")
//    public String getChannelPic() {
//        return channelPic;
//    }
//
//    public void setChannelPic(String channelPic) {
//        this.channelPic = channelPic;
//    }
//
//    @Column(name = "cha_rm_num")
//    public int getChaRmNum() {
//        return chaRmNum;
//    }
//
//    public void setChaRmNum(int chaRmNum) {
//        this.chaRmNum = chaRmNum;
//    }
}
