package com.oseasy.xlxq.service.api.sys.model;

import com.oseasy.xlxq.service.api.base.IdEntity;
import com.oseasy.xlxq.service.api.tenant.model.TenantUser;
import org.hibernate.annotations.NotFound;
import org.hibernate.annotations.NotFoundAction;
import org.hibernate.annotations.Where;

import javax.persistence.*;



/**
 * 用户收货地址
 *
 */
@Entity
@Where(clause = "deleted=0")
@Table(schema = "db_oe_xlxq", name = "tb_tenant_user_addrs")
public class TenantUserAddrs extends IdEntity {

    private TenantUser tenantUser;//所属用户
    private String consignee;//收货人
    private String receivingAddress;//收货地址
    private String receivingTel;//收货号码
    private int isDefault;//默认收货地址标识


    @ManyToOne
    @JoinColumn(name = "user_id")
    @NotFound(action = NotFoundAction.IGNORE)
    public TenantUser getTenantUser() {
        return tenantUser;
    }

    public void setTenantUser(TenantUser tenantUser) {
        this.tenantUser = tenantUser;
    }

    @Column(name = "consignee")
    public String getConsignee() {
        return consignee;
    }

    public void setConsignee(String consignee) {
        this.consignee = consignee;
    }

    @Column(name = "receiving_address")
    public String getReceivingAddress() {
        return receivingAddress;
    }

    public void setReceivingAddress(String receivingAddress) {
        this.receivingAddress = receivingAddress;
    }

    @Column(name = "receiving_tel")
    public String getReceivingTel() {
        return receivingTel;
    }

    public void setReceivingTel(String receivingTel) {
        this.receivingTel = receivingTel;
    }

    @Column(name = "is_default")
    public int getIsDefault() {
        return isDefault;
    }

    public void setIsDefault(int isDefault) {
        this.isDefault = isDefault;
    }
}



