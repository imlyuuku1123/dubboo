package com.oseasy.xlxq.service.api.exceptions;

/**
 * Created by liups on 2016/11/18.
 */
public class PageSizeTooLargeException extends XlxqRuntimeException{
    public PageSizeTooLargeException(String message) {
        super("00002222",message,null);
    }
}
