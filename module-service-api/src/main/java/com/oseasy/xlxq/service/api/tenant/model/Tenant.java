package com.oseasy.xlxq.service.api.tenant.model;

import com.oseasy.xlxq.service.api.base.IdEntity;
import org.hibernate.annotations.Where;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Table;

/**
 * 
 * 租户
 * 
 *
 * 
 */
@Entity
@Where(clause = "deleted=0")
@Table(schema="db_oe_xlxq",name = "tb_tenant")
public class Tenant extends IdEntity {
	private static final long serialVersionUID = 1L;

	private String code;//租户代码

	private String name;//租户名称

	@Column(name = "tenant_code")
	public String getCode() {
		return code;
	}

	public void setCode(String code) {
		this.code = code;
	}

	@Column(name = "tenant_name")
	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}
}
