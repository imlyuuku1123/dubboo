package com.oseasy.xlxq.service.api.tenant.model;

import com.oseasy.xlxq.service.api.base.IdEntity;
import com.oseasy.xlxq.service.api.sys.model.TenantUserWxExt;
import org.hibernate.annotations.NotFound;
import org.hibernate.annotations.NotFoundAction;
import org.hibernate.annotations.Where;

import javax.persistence.*;
import java.util.Date;

/**
 * 平台用户
 *
 */
@Entity
@Where(clause = "deleted=0")
@Table(schema = "db_oe_xlxq", name = "tb_tenant_user")
public class TenantUser extends IdEntity {

	private Tenant tenant;//所属租户

//	private TenantUserWxExt tenantUserWxExt;//用户微信ID

	private String name;//姓名

	private  Integer  sexFlag;//性别

	private Date birthday;//生日

	private String telephone;//手机号

	private  Date registrationDate;//注册日期

	@ManyToOne
	@JoinColumn(name = "tenant_id")
	@NotFound(action = NotFoundAction.IGNORE)
	public Tenant getTenant() {
		return tenant;
	}

	public void setTenant(Tenant tenant) {
		this.tenant = tenant;
	}

//	@OneToOne
//	@JoinColumn(name = "user_wxid")
//	@NotFound(action = NotFoundAction.IGNORE)
//	public TenantUserWxExt getTenantUserWxExt() {
//		return tenantUserWxExt;
//	}
//
//	public void setTenantUserWxExt(TenantUserWxExt tenantUserWxExt) {
//		this.tenantUserWxExt = tenantUserWxExt;
//	}

	@Column(name = "name")
	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	@Column(name = "sex_flag")
	public Integer getSexFlag() {return sexFlag;}

	public void setSexFlag(Integer sexFlag) {this.sexFlag = sexFlag;}

	@Column(name = "birthday")
	public Date getBirthday() {
		return birthday;
	}

	public void setBirthday(Date birthday) {
		this.birthday = birthday;
	}

	@Column(name = "telephone")
	public String getTelephone() {return telephone;	}

	public void setTelephone(String telephone) {this.telephone = telephone;	}

	@Column(name = "registration_date")
	public Date getRegistrationDate() {	return registrationDate;}

	public void setRegistrationDate(Date registrationDate) {this.registrationDate = registrationDate;}
}
