package com.oseasy.xlxq.service.api.tenant.model;

import com.oseasy.xlxq.service.api.base.IdEntity;
import org.hibernate.annotations.NotFound;
import org.hibernate.annotations.NotFoundAction;
import org.hibernate.annotations.Where;


import javax.persistence.*;

/**
 * 民宿
 *
 */
@Entity
@Where(clause = "deleted=0")
@Table(schema = "db_oe_xlxq", name = "tb_tenant_homestay")
public class TenantHomeStay extends IdEntity {

	private String homestayName; //民宿名称
	private TenantScenic tenantScenic;//所属景区
	private TenantMerchant tenantMerchant;//所属商户
	private String salesVolume;//销量
	private String purchaseNum;//购买人数
	private String fullAddress;//详细地址
	private String linkmanName;//联系人姓名
	private String linkmanTel;//联系人电话
	private String hsService;//民宿服务
	private String startPrice;//起售价
	private String titleImgUrl;//封面图片地址
	private String homestayDetails;//民宿详情描述


	@Column(name = "homestay_name")
	public String getHomestayName() {
		return homestayName;
	}

	public void setHomestayName(String homestayName) {
		this.homestayName = homestayName;
	}

	@ManyToOne
	@JoinColumn(name = "merchant_id")
	@NotFound(action = NotFoundAction.IGNORE)
	public TenantMerchant getTenantMerchant() {
		return tenantMerchant;
	}

	public void setTenantMerchant(TenantMerchant tenantMerchant) {
		this.tenantMerchant = tenantMerchant;
	}

	@ManyToOne
	@JoinColumn(name = "scenic_id")
	@NotFound(action = NotFoundAction.IGNORE)
	public TenantScenic getTenantScenic() {
		return tenantScenic;
	}

	public void setTenantScenic(TenantScenic tenantScenic) {
		this.tenantScenic = tenantScenic;
	}

	@Column(name = "sales_volume")
	public String getSalesVolume() {
		return salesVolume;
	}

	public void setSalesVolume(String salesVolume) {
		this.salesVolume = salesVolume;
	}

	@Column(name = "purchase_num")
	public String getPurchaseNum() {
		return purchaseNum;
	}

	public void setPurchaseNum(String purchaseNum) {
		this.purchaseNum = purchaseNum;
	}

	@Column(name = "full_address")
	public String getFullAddress() {
		return fullAddress;
	}

	public void setFullAddress(String fullAddress) {
		this.fullAddress = fullAddress;
	}

	@Column(name = "linkman_name")
	public String getLinkmanName() {
		return linkmanName;
	}

	public void setLinkmanName(String linkmanName) {
		this.linkmanName = linkmanName;
	}

	@Column(name = "linkman_tel")
	public String getLinkmanTel() {
		return linkmanTel;
	}

	public void setLinkmanTel(String linkmanTel) {
		this.linkmanTel = linkmanTel;
	}

	@Column(name = "hs_service")
	public String getHsService() {
		return hsService;
	}

	public void setHsService(String hsService) {
		this.hsService = hsService;
	}

	@Column(name = "start_price")
	public String getStartPrice() {
		return startPrice;
	}

	public void setStartPrice(String startPrice) {
		this.startPrice = startPrice;
	}

	@Column(name = "title_img_url")
	public String getTitleImgUrl() {
		return titleImgUrl;
	}

	public void setTitleImgUrl(String titleImgUrl) {
		this.titleImgUrl = titleImgUrl;
	}

	@Column(name = "homestay_details")
	public String getHomestayDetails() {
		return homestayDetails;
	}

	public void setHomestayDetails(String homestayDetails) {
		this.homestayDetails = homestayDetails;
	}
}
