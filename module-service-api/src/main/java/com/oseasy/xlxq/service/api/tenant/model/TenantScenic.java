package com.oseasy.xlxq.service.api.tenant.model;

import com.oseasy.xlxq.service.api.base.IdEntity;
import org.hibernate.annotations.NotFound;
import org.hibernate.annotations.NotFoundAction;
import org.hibernate.annotations.Where;

import javax.persistence.*;

/**
 * 景区
 *
 */
@Entity
@Where(clause = "deleted=0")
@Table(schema = "db_oe_xlxq", name = "tb_tenant_scenic")
public class TenantScenic extends IdEntity {

    private Tenant tenant;//所属租户

    private String scenicName;//景区名称

    private String positionPro;//位置(省)

    private String positionCity;//位置(市)

    private String postionArea;//位置(区)

    private String positionDetails;//位置(详情)

    @ManyToOne
    @JoinColumn(name = "tenant_id")
    @NotFound(action = NotFoundAction.IGNORE)
    public Tenant getTenant() {
        return tenant;
    }

    public void setTenant(Tenant tenant) {
        this.tenant = tenant;
    }

    @Column(name = "scenic_name")
    public String getScenicName() {
        return scenicName;
    }

    public void setScenicName(String scenicName) {
        this.scenicName = scenicName;
    }

    @Column(name = "position_pro")
    public String getPositionPro() {
        return positionPro;
    }

    public void setPositionPro(String positionPro) {
        this.positionPro = positionPro;
    }

    @Column(name = "position_city")
    public String getPositionCity() {
        return positionCity;
    }

    public void setPositionCity(String positionCity) {
        this.positionCity = positionCity;
    }

    @Column(name = "postion_area")
    public String getPostionArea() {
        return postionArea;
    }

    public void setPostionArea(String postionArea) {
        this.postionArea = postionArea;
    }

    @Column(name = "position_details")
    public String getPositionDetails() {
        return positionDetails;
    }

    public void setPositionDetails(String positionDetails) {
        this.positionDetails = positionDetails;
    }
}
