package com.oseasy.xlxq.service.api.sys.service;

import com.oseasy.xlxq.service.api.base.BaseService;
import com.oseasy.xlxq.service.api.sys.model.Module;

import java.util.List;
import java.util.Map;

public interface ModuleService extends BaseService<Module> {

    /**
     * 根据父菜单整合jstree数据 -创建时
     * @param parentsList 父菜单的集合
     * @param allModuleList 所有菜单的集合
     * @return
     */
    public List<Map<String,Object>> getModuleByParent(List<Module> parentsList, List<Module> allModuleList);

    /**
     * 查询所有菜单列表
     * @return
     */
    public List<Module> getAllModuleList();

    /**
     * 查询第1级菜单-创建时
     * @param allModuleList
     * @return
     */
    public List<Module> getFirstMouleList(List<Module> allModuleList);

    /**
     * 根据父id集合查询子id集合
     * @param parentIdList
     * @param i
     */
    public Map<String,Object> getMenuListByParentIdList(List<String> parentIdList,int i);

    /**
     * 根据父菜单整合jstree数据 -编辑时
     * @param parentsList 父菜单的集合
     * @param allModuleList 所有菜单的集合
     * @return
     */
    public List<Map<String,Object>> getModuleByParent2(List<Module> parentsList, List<Module> allModuleList,List<String> mIdList);


}
