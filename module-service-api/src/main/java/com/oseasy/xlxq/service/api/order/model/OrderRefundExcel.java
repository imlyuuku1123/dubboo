package com.oseasy.xlxq.service.api.order.model;

import javax.persistence.Transient;
import java.util.Date;

/**
 * Created by heckman on 2017/6/9.
 */
public class OrderRefundExcel {

    /**
     * String[] headers = new String[]{"姓名", "商户", "付款订单号",
     * "退款订单号", "订单金额", "申请退款时间", "退款金额", "退款时间", "退款状态"};
     */

    private String name;
    private String telephone;
    private String merchantName;
    private String orderNum;
    private String refundId;
    private String orderSum;
    private Date createTime;
    private String refundMoney;
    private Date refundTime;
    private String refundStatus;

    @Transient
    public String getTelephone() {
        return telephone;
    }

    public void setTelephone(String telephone) {
        this.telephone = telephone;
    }

    @Transient
    public String getName() {
        return name;
    }

    @Transient
    public String getMerchantName() {
        return merchantName;
    }

    @Transient
    public String getOrderNum() {
        return orderNum;
    }

    @Transient
    public String getRefundId() {
        return refundId;
    }

    @Transient
    public String getOrderSum() {
        return orderSum;
    }

    @Transient
    public Date getCreateTime() {
        return createTime;
    }

    @Transient
    public String getRefundMoney() {
        return refundMoney;
    }

    @Transient
    public Date getRefundTime() {
        return refundTime;
    }

    @Transient
    public String getRefundStatus() {
        return refundStatus;
    }

    public void setName(String name) {
        this.name = name;
    }

    public void setMerchantName(String merchantName) {
        this.merchantName = merchantName;
    }

    public void setOrderNum(String orderNum) {
        this.orderNum = orderNum;
    }

    public void setRefundId(String refundId) {
        this.refundId = refundId;
    }

    public void setOrderSum(String orderSum) {
        this.orderSum = orderSum;
    }

    public void setCreateTime(Date createTime) {
        this.createTime = createTime;
    }

    public void setRefundMoney(String refundMoney) {
        this.refundMoney = refundMoney;
    }

    public void setRefundTime(Date refundTime) {
        this.refundTime = refundTime;
    }

    public void setRefundStatus(String refundStatus) {
        this.refundStatus = refundStatus;
    }
}
