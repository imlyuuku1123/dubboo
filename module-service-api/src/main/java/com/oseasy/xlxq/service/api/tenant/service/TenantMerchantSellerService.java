package com.oseasy.xlxq.service.api.tenant.service;

import com.oseasy.xlxq.service.api.base.BaseService;
import com.oseasy.xlxq.service.api.tenant.model.TenantMerchantSeller;


public interface TenantMerchantSellerService extends BaseService<TenantMerchantSeller> {

}
