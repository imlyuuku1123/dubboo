package com.oseasy.xlxq.service.api.tenant.model;

import com.oseasy.xlxq.service.api.base.IdEntity;

import org.hibernate.annotations.NotFound;
import org.hibernate.annotations.NotFoundAction;
import org.hibernate.annotations.Where;


import javax.persistence.*;
import java.math.BigDecimal;

/**
 * 房型管理
 *
 */
@Entity
@Where(clause = "deleted=0")
@Table(schema = "db_oe_xlxq", name = "tb_tenant_homestay_room")
@PrimaryKeyJoinColumn(name = "id")
public class TenantHomeStayRoom extends TenantProduct {

	private TenantHomeStay tenantHomestay;//所属民宿

//	private TenantProduct tenantProduct;//所属产品

	private String roomName;//房型名称

	private String peopleNum;//可住人数

	private BigDecimal roomPrice;//房型原价

	private BigDecimal roomSalprice;//房型优惠价

	private String roomNum;//房间数

	private String roomArea;//房间面积

	private String bedType;//床型

	private String bedNum;//床位数

	private String breakfastFlag;//含早

	private String unsubscribeFlag;//退订说明

	private String unsubscribeInfo;//退订信息

	private String roomExplain;//房型说明

	private String iforder;//虚拟字段 是否可预订 0是；1否

	private String buyNum;//虚拟字段，购买数量

	private String startTime;//预定起始时间

	private String endTime;//预定结束时间

	@ManyToOne
	@JoinColumn(name = "homestay_id")
	@NotFound(action = NotFoundAction.IGNORE)
	public TenantHomeStay getTenantHomestay() {
		return tenantHomestay;
	}

	public void setTenantHomestay(TenantHomeStay tenantHomestay) {
		this.tenantHomestay = tenantHomestay;
	}

//	@ManyToOne
//	@JoinColumn(name = "product_id")
//	@NotFound(action = NotFoundAction.IGNORE)
//	public TenantProduct getTenantProduct() {
//		return tenantProduct;
//	}

//	public void setTenantProduct(TenantProduct tenantProduct) {
//		this.tenantProduct = tenantProduct;
//	}

	@Column(name = "room_name")
	public String getRoomName() {
		return roomName;
	}

	public void setRoomName(String roomName) {
		this.roomName = roomName;
	}

	@Column(name = "room_price")
	public BigDecimal getRoomPrice() {
		return roomPrice;
	}

	public void setRoomPrice(BigDecimal roomPrice) {
		this.roomPrice = roomPrice;
	}

	@Column(name = "room_salprice")
	public BigDecimal getRoomSalprice() {
		return roomSalprice;
	}

	public void setRoomSalprice(BigDecimal roomSalprice) {
		this.roomSalprice = roomSalprice;
	}


	@Column(name = "room_num")
	public String getRoomNum() {
		return roomNum;
	}

	public void setRoomNum(String roomNum) {
		this.roomNum = roomNum;
	}

	@Column(name = "room_area")
	public String getRoomArea() {
		return roomArea;
	}

	public void setRoomArea(String roomArea) {
		this.roomArea = roomArea;
	}


	@Column(name = "bed_type")
	public String getBedType() {
		return bedType;
	}

	public void setBedType(String bedType) {
		this.bedType = bedType;
	}

	@Column(name = "bed_num")
	public String getBedNum() {
		return bedNum;
	}

	public void setBedNum(String bedNum) {
		this.bedNum = bedNum;
	}

	@Column(name = "breakfast_flag")
	public String getBreakfastFlag() {
		return breakfastFlag;
	}

	public void setBreakfastFlag(String breakfastFlag) {
		this.breakfastFlag = breakfastFlag;
	}

	@Column(name = "unsubscribe_flag")
	public String getUnsubscribeFlag() {
		return unsubscribeFlag;
	}

	public void setUnsubscribeFlag(String unsubscribeFlag) {
		this.unsubscribeFlag = unsubscribeFlag;
	}

	@Column(name = "unsubscribe_info")
	public String getUnsubscribeInfo() {
		return unsubscribeInfo;
	}

	public void setUnsubscribeInfo(String unsubscribeInfo) {
		this.unsubscribeInfo = unsubscribeInfo;
	}

	@Column(name = "room_explain")
	public String getRoomExplain() {
		return roomExplain;
	}

	public void setRoomExplain(String roomExplain) {
		this.roomExplain = roomExplain;
	}

	@Transient
	public String getIforder() {
		return iforder;
	}

	public void setIforder(String iforder) {
		this.iforder = iforder;
	}

	@Column(name = "people_num")
	public String getPeopleNum() {
		return peopleNum;
	}

	public void setPeopleNum(String peopleNum) {
		this.peopleNum = peopleNum;
	}

	@Transient
	public String getBuyNum() {
		return buyNum;
	}

	public void setBuyNum(String buyNum) {
		this.buyNum = buyNum;
	}

	@Transient
	public String getStartTime() {
		return startTime;
	}

	public void setStartTime(String startTime) {
		this.startTime = startTime;
	}

	@Transient
	public String getEndTime() {
		return endTime;
	}

	public void setEndTime(String endTime) {
		this.endTime = endTime;
	}
}
