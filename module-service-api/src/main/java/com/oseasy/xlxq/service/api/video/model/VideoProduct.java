package com.oseasy.xlxq.service.api.video.model;

import com.oseasy.xlxq.service.api.base.IdEntity;
import com.oseasy.xlxq.service.api.tenant.model.TenantProduct;
import org.hibernate.annotations.NotFound;
import org.hibernate.annotations.NotFoundAction;
import org.hibernate.annotations.Where;

import javax.persistence.*;

/**
 * 直播房间与产品关系
 *
 */
@Entity
@Where(clause = "deleted=0")
@Table(schema = "db_oe_xlxq", name = "tb_video_product")
public class VideoProduct extends IdEntity {

	private VideoRoom  videoRoom;//直播房间id

	private TenantProduct tenantProduct;//产品id

	private String productType;

	@Column(name="product_type")
	public String getProductType() {
		return productType;
	}

	public void setProductType(String productType) {
		this.productType = productType;
	}

	@OneToOne
	@JoinColumn(name = "charoom_id")
	@NotFound(action = NotFoundAction.IGNORE)
	public VideoRoom getVideoRoom() {
		return videoRoom;
	}

	public void setVideoRoom(VideoRoom videoRoom) {
		this.videoRoom = videoRoom;
	}

	@OneToOne
	@JoinColumn(name = "product_id")
	@NotFound(action = NotFoundAction.IGNORE)
	public TenantProduct getTenantProduct() {
		return tenantProduct;
	}

	public void setTenantProduct(TenantProduct tenantProduct) {
		this.tenantProduct = tenantProduct;
	}

}
