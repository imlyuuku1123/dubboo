package com.oseasy.xlxq.service.api.product.model;

import com.oseasy.xlxq.service.api.base.IdEntity;
import com.oseasy.xlxq.service.api.tenant.model.TenantProduct;
import org.hibernate.annotations.NotFound;
import org.hibernate.annotations.NotFoundAction;
import org.hibernate.annotations.Where;

import javax.persistence.*;



/**
 * 库存变更记录
 */
@Entity
@Where(clause = "deleted=0")
@Table(schema = "db_oe_xlxq", name = "tb_product_stock_change")
public class ProductStockChange extends IdEntity {

    private TenantProduct tenantProduct;//产品
    private String changeReason;//变更原因
    private String changeType;//变更方式
    private Integer beforeNum;//变更前数量
    private Integer changeNum;//变更数量
    private Integer afterNum;//变更后数量

    @ManyToOne
    @JoinColumn(name = "product_id")
    @NotFound(action = NotFoundAction.IGNORE)
    public TenantProduct getTenantProduct() {
        return tenantProduct;
    }

    public void setTenantProduct(TenantProduct tenantProduct) {
        this.tenantProduct = tenantProduct;
    }

    @Column(name="change_reason")
    public String getChangeReason() {
        return changeReason;
    }

    public void setChangeReason(String changeReason) {
        this.changeReason = changeReason;
    }

    @Column(name="change_type")
    public String getChangeType() {
        return changeType;
    }

    public void setChangeType(String changeType) {
        this.changeType = changeType;
    }

    @Column(name="before_num")
    public Integer getBeforeNum() {
        return beforeNum;
    }

    public void setBeforeNum(Integer beforeNum) {
        this.beforeNum = beforeNum;
    }

    @Column(name="change_num")
    public Integer getChangeNum() {
        return changeNum;
    }

    public void setChangeNum(Integer changeNum) {
        this.changeNum = changeNum;
    }

    @Column(name="after_num")
    public Integer getAfterNum() {
        return afterNum;
    }

    public void setAfterNum(Integer afterNum) {
        this.afterNum = afterNum;
    }







}
