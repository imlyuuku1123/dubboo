package com.oseasy.xlxq.service.api.order.model;

import com.oseasy.xlxq.service.api.base.IdEntity;
import com.oseasy.xlxq.service.api.tenant.model.TenantHomeStayRoom;
import org.hibernate.annotations.NotFound;
import org.hibernate.annotations.NotFoundAction;
import org.hibernate.annotations.Where;

import javax.persistence.*;
import java.util.Date;


/**
 * 房型实时库存
 *
 */
@Entity
@Where(clause = "deleted=0")
@Table(schema = "db_oe_xlxq", name = "tb_homestay_room_stock")
public class HomeStayRoomStock extends IdEntity {

	private TenantHomeStayRoom tenantHomeStayRoom;//所属房型

	private String leftNum;//剩余数量
	private Date orderTime;//预定时间


	@ManyToOne
	@JoinColumn(name = "room_id")
	@NotFound(action = NotFoundAction.IGNORE)
	public TenantHomeStayRoom getTenantHomeStayRoom() {
		return tenantHomeStayRoom;
	}

	public void setTenantHomeStayRoom(TenantHomeStayRoom tenantHomeStayRoom) {
		this.tenantHomeStayRoom = tenantHomeStayRoom;
	}

	@Column(name = "left_num")
	public String getLeftNum() {
		return leftNum;
	}

	public void setLeftNum(String leftNum) {
		this.leftNum = leftNum;
	}

	@Column(name = "order_time")
	public Date getOrderTime() {
		return orderTime;
	}

	public void setOrderTime(Date orderTime) {
		this.orderTime = orderTime;
	}



}
