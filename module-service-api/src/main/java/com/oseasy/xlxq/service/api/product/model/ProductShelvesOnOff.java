package com.oseasy.xlxq.service.api.product.model;

import com.oseasy.xlxq.service.api.base.IdEntity;
import com.oseasy.xlxq.service.api.tenant.model.TenantProduct;

import org.hibernate.annotations.NotFound;
import org.hibernate.annotations.NotFoundAction;
import org.hibernate.annotations.Where;

import javax.persistence.*;



/**
 * 上下架记录
 */
@Entity
@Where(clause = "deleted=0")
@Table(schema = "db_oe_xlxq", name = "tb_product_shelves_on_off")
public class ProductShelvesOnOff extends IdEntity {

    private TenantProduct tenantProduct;//产品
    private String changeType;//上架下架类型

    @ManyToOne
    @JoinColumn(name = "product_id")
    @NotFound(action = NotFoundAction.IGNORE)
    public TenantProduct getTenantProduct() {
        return tenantProduct;
    }

    public void setTenantProduct(TenantProduct tenantProduct) {
        this.tenantProduct = tenantProduct;
    }

    @Column(name="change_type")
    public String getChangeType() {
        return changeType;
    }

    public void setChangeType(String changeType) {
        this.changeType = changeType;
    }





}
