package com.oseasy.xlxq.service.api.video.model;

import com.oseasy.xlxq.service.api.base.IdEntity;
import com.oseasy.xlxq.service.api.tenant.model.TenantProduct;
import org.hibernate.annotations.NotFound;
import org.hibernate.annotations.NotFoundAction;
import org.hibernate.annotations.Where;

import javax.persistence.*;

/**
 * 直播房间
 */
@Entity
@Where(clause = "deleted=0")
@Table(schema = "db_oe_xlxq", name = "tb_video_room")
public class VideoRoom extends IdEntity {

    public static int ON_LINE_STATUS = 1;
    public static int OFF_LINE_STATUS = 0;


    private VideoChannel videoChannel;//所属频道

    private int roomType;//房间类型

    private String roomname;//房间名称

    private String anchorname;//主播姓名

    private int anchorsex;//主播性别

    private String anchorhead;//主播头像

    private String anchorprofile;//主播简介

    private String livecover;//直播间封面

    private String directsource;//直播源

    private String sourceaddress;//源地址（URL）

    private int productCount; //关联产品数量

    private int status;//上下线状态（1：上线；0：下线）

    private int ifrecommend;//是否被推荐（0否  ； 1是；）

    @Column(name = "product_count")
    public int getProductCount() {
        return productCount;
    }

    public void setProductCount(int productCount) {
        this.productCount = productCount;
    }

    @OneToOne
    @JoinColumn(name = "channel_id")
    @NotFound(action = NotFoundAction.IGNORE)
    public VideoChannel getVideoChannel() {
        return videoChannel;
    }

    public void setVideoChannel(VideoChannel videoChannel) {
        this.videoChannel = videoChannel;
    }

    @Column(name = "room_type")
    public int getRoomType() {
        return roomType;
    }

    public void setRoomType(int roomType) {
        this.roomType = roomType;
    }

    @Column(name = "roomname")
    public String getRoomname() {
        return roomname;
    }

    public void setRoomname(String roomname) {
        this.roomname = roomname;
    }

    @Column(name = "anchorname")
    public String getAnchorname() {
        return anchorname;
    }

    public void setAnchorname(String anchorname) {
        this.anchorname = anchorname;
    }

    @Column(name = "anchorsex")
    public int getAnchorsex() {
        return anchorsex;
    }

    public void setAnchorsex(int anchorsex) {
        this.anchorsex = anchorsex;
    }

    @Column(name = "anchorprofile")
    public String getAnchorprofile() {
        return anchorprofile;
    }

    public void setAnchorprofile(String anchorprofile) {
        this.anchorprofile = anchorprofile;
    }

    @Column(name = "livecover")
    public String getLivecover() {
        return livecover;
    }

    public void setLivecover(String livecover) {
        this.livecover = livecover;
    }

    @Column(name = "directsource")
    public String getDirectsource() {
        return directsource;
    }

    public void setDirectsource(String directsource) {
        this.directsource = directsource;
    }

    @Column(name = "sourceaddress")
    public String getSourceaddress() {
        return sourceaddress;
    }

    public void setSourceaddress(String sourceaddress) {
        this.sourceaddress = sourceaddress;
    }

    @Column(name = "status")
    public int getStatus() {
        return status;
    }

    public void setStatus(int status) {
        this.status = status;
    }


    @Override
    public String toString() {
        return "VideoRoom{" +
                "videoChannel=" + videoChannel +
                ", roomType=" + roomType +
                ", roomname='" + roomname + '\'' +
                ", anchorname='" + anchorname + '\'' +
                ", anchorsex=" + anchorsex +
                ", anchorprofile='" + anchorprofile + '\'' +
                ", livecover='" + livecover + '\'' +
                ", directsource='" + directsource + '\'' +
                ", sourceaddress='" + sourceaddress + '\'' +
                ", status=" + status +
                '}';
    }

    @Column(name = "anchorhead")
    public String getAnchorhead() {
        return anchorhead;
    }

    public void setAnchorhead(String anchorhead) {
        this.anchorhead = anchorhead;
    }

    @Column(name = "ifrecommend")
    public int getIfrecommend() {
        return ifrecommend;
    }

    public void setIfrecommend(int ifrecommend) {
        this.ifrecommend = ifrecommend;
    }
}
