package com.oseasy.xlxq.service.api.statistic.model;

import com.oseasy.xlxq.service.api.base.IdEntity;
import com.oseasy.xlxq.service.api.tenant.model.TenantMerchant;
import com.oseasy.xlxq.service.api.tenant.model.TenantScenic;
import com.oseasy.xlxq.service.api.tenant.model.TenantScenicTicket;
import org.hibernate.annotations.NotFound;
import org.hibernate.annotations.NotFoundAction;
import org.hibernate.annotations.Where;

import javax.persistence.*;
import java.math.BigDecimal;
import java.util.Date;

/**
 * 门票日统计
 */
@Entity
@Where(clause = "deleted=0")
@Table(schema = "db_oe_xlxq", name = "tb_statistic_ticket_day")
public class StatisticTicketDay extends IdEntity {

    private TenantScenicTicket tenantScenicTicket;//所属门票
    private TenantMerchant tenantMerchant;//所属商户
    private TenantScenic tenantScenic;//所属景区
    private Integer saleNum;//销售数量
    private BigDecimal saleMoney;//销售金额
    private Integer year;//年
    private Integer month;//月
    private Integer day;//日
    private Date stasticsDay;//统计时间

    private BigDecimal refundMoney;//退款金额

    @Column(name = "refund_money")
    public BigDecimal getRefundMoney() {
        return refundMoney;
    }

    public void setRefundMoney(BigDecimal refundMoney) {
        this.refundMoney = refundMoney;
    }

    @ManyToOne
    @JoinColumn(name = "ticket_id")
    @NotFound(action = NotFoundAction.IGNORE)
    public TenantScenicTicket getTenantScenicTicket() {
        return tenantScenicTicket;
    }

    public void setTenantScenicTicket(TenantScenicTicket tenantScenicTicket) {
        this.tenantScenicTicket = tenantScenicTicket;
    }

    @ManyToOne
    @JoinColumn(name = "merchant_id")
    @NotFound(action = NotFoundAction.IGNORE)
    public TenantMerchant getTenantMerchant() {
        return tenantMerchant;
    }

    public void setTenantMerchant(TenantMerchant tenantMerchant) {
        this.tenantMerchant = tenantMerchant;
    }

    @ManyToOne
    @JoinColumn(name = "scenicspots_id")
    @NotFound(action = NotFoundAction.IGNORE)
    public TenantScenic getTenantScenic() {
        return tenantScenic;
    }

    public void setTenantScenic(TenantScenic tenantScenic) {
        this.tenantScenic = tenantScenic;
    }

    @Column(name = "sale_num")
    public Integer getSaleNum() {
        return saleNum;
    }

    public void setSaleNum(Integer saleNum) {
        this.saleNum = saleNum;
    }

    @Column(name = "sale_money")
    public BigDecimal getSaleMoney() {
        return saleMoney;
    }

    public void setSaleMoney(BigDecimal saleMoney) {
        this.saleMoney = saleMoney;
    }

    @Column(name = "year")
    public Integer getYear() {
        return year;
    }

    public void setYear(Integer year) {
        this.year = year;
    }

    @Column(name = "month")
    public Integer getMonth() {
        return month;
    }

    public void setMonth(Integer month) {
        this.month = month;
    }

    @Column(name = "day")
    public Integer getDay() {
        return day;
    }

    public void setDay(Integer day) {
        this.day = day;
    }

    @Column(name = "stastics_day")
    public Date getStasticsDay() {
        return stasticsDay;
    }

    public void setStasticsDay(Date stasticsDay) {
        this.stasticsDay = stasticsDay;
    }
}
