package com.oseasy.xlxq.service.api.order.model;

import com.oseasy.xlxq.service.api.base.IdEntity;
import com.oseasy.xlxq.service.api.sys.model.TenantUserAddrs;
import com.oseasy.xlxq.service.api.tenant.model.TenantMerchant;
import com.oseasy.xlxq.service.api.tenant.model.TenantProduct;
import com.oseasy.xlxq.service.api.tenant.model.TenantUser;
import org.hibernate.annotations.NotFound;
import org.hibernate.annotations.NotFoundAction;
import org.hibernate.annotations.Where;
import org.springframework.stereotype.Component;

import javax.persistence.*;
import java.io.Serializable;
import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;


/**
 * 订单信息
 *
 */
@Entity
@Where(clause = "deleted=0")
@Table(schema = "db_oe_xlxq", name = "tb_order_info")
public class OrderInfo extends IdEntity implements Serializable {
    public static String TYPE_SPECIALTY = "1";//土特产
    public static String TYPE_HOMESTATY = "2";//民宿
    public static String TYPE_TICKET = "3";//门票


    private String orderNum;//订单号
    private TenantMerchant tenantMerchant;//所属商户
    private TenantUserAddrs tenantUserAddrs;//收货地址
    private TenantUser tenantUser;//购买人
    private String payType;//支付方式
    private Date payTime;//付款时间
    private String orderStatus;//订单状态
    private String orderType;//订单类型 1：土特产 ；2：民宿 ；3：门票
    private BigDecimal orderSum;//订单总额

    //扩展属性
    private String extSpecialtyPop1;//土特产扩展属性1
    private String extSpecialtyPop2;//土特产扩展属性2
    private String extTicketPop1;//门票扩展属性1(门票所属景区名称)
    private String extTicketPop2;//门票扩展属性2
    private String extHomestayPop1;//民宿扩展属性1（民宿所属景区名称）
    private String extHomestayPop2;//民宿扩展属性2

    private String orderUserName;//订单填写姓名
    private String orderUserPhone;//订单填写手机号

    private String pendpaynum;//虚拟字段 待支付订单数量
    private String shippendnum;//虚拟字段 待发货订单数量
    private String waitrecnum;//虚拟字段 待收货订单数量
    private String pendevalunum;//虚拟字段 待评价订单数量

    @Column(name = "ext_specialty_pop1")
    public String getExtSpecialtyPop1() {
        return extSpecialtyPop1;
    }

    @Column(name = "ext_specialty_pop2")
    public String getExtSpecialtyPop2() {
        return extSpecialtyPop2;
    }

    @Column(name = "ext_ticket_pop1")
    public String getExtTicketPop1() {
        return extTicketPop1;
    }

    @Column(name = "ext_ticket_pop2")
    public String getExtTicketPop2() {
        return extTicketPop2;
    }

    @Column(name = "ext_homestay_pop1")
    public String getExtHomestayPop1() {
        return extHomestayPop1;
    }

    @Column(name = "ext_homestay_pop2")
    public String getExtHomestayPop2() {
        return extHomestayPop2;
    }

    public void setExtSpecialtyPop1(String extSpecialtyPop1) {
        this.extSpecialtyPop1 = extSpecialtyPop1;
    }

    public void setExtSpecialtyPop2(String extSpecialtyPop2) {
        this.extSpecialtyPop2 = extSpecialtyPop2;
    }

    public void setExtTicketPop1(String extTicketPop1) {
        this.extTicketPop1 = extTicketPop1;
    }

    public void setExtTicketPop2(String extTicketPop2) {
        this.extTicketPop2 = extTicketPop2;
    }

    public void setExtHomestayPop1(String extHomestayPop1) {
        this.extHomestayPop1 = extHomestayPop1;
    }

    public void setExtHomestayPop2(String extHomestayPop2) {
        this.extHomestayPop2 = extHomestayPop2;
    }

    @Column(name = "order_sum")
    public BigDecimal getOrderSum() {
        return orderSum;
    }

    public void setOrderSum(BigDecimal orderSum) {
        this.orderSum = orderSum;
    }


    @Column(name = "order_bi_type")
    public String getOrderType() {
        return orderType;
    }

    public void setOrderType(String orderType) {
        this.orderType = orderType;
    }

    @Column(name = "order_num")
    public String getOrderNum() {
        return orderNum;
    }

    public void setOrderNum(String orderNum) {
        this.orderNum = orderNum;
    }


	@ManyToOne
	@JoinColumn(name = "merchant_id")
	@NotFound(action = NotFoundAction.IGNORE)
	public TenantMerchant getTenantMerchant() {
		return tenantMerchant;
	}

	public void setTenantMerchant(TenantMerchant tenantMerchant) {
		this.tenantMerchant = tenantMerchant;
	}

	@ManyToOne
	@JoinColumn(name = "purchaser_id")
	@NotFound(action = NotFoundAction.IGNORE)
	public TenantUser getTenantUser() {
		return tenantUser;
	}

	public void setTenantUser(TenantUser tenantUser) {
		this.tenantUser = tenantUser;
	}


	@Column(name = "pay_type")
	public String getPayType() {
		return payType;
	}

	public void setPayType(String payType) {
		this.payType = payType;
	}

	@Column(name = "pay_time")
	public Date getPayTime() {
		return payTime;
	}

	public void setPayTime(Date payTime) {
		this.payTime = payTime;
	}

	@Column(name = "order_status")
	public String getOrderStatus() {
		return orderStatus;
	}

	public void setOrderStatus(String orderStatus) {
		this.orderStatus = orderStatus;
	}

	@OneToOne
	@JoinColumn(name = "address_id")
	@NotFound(action = NotFoundAction.IGNORE)
	public TenantUserAddrs getTenantUserAddrs() {
		return tenantUserAddrs;
	}

	public void setTenantUserAddrs(TenantUserAddrs tenantUserAddrs) {
		this.tenantUserAddrs = tenantUserAddrs;
	}

    @Column(name = "order_user_name")
    public String getOrderUserName() {
        return orderUserName;
    }

    public void setOrderUserName(String orderUserName) {
        this.orderUserName = orderUserName;
    }

    @Column(name = "order_user_phone")
    public String getOrderUserPhone() {
        return orderUserPhone;
    }

    public void setOrderUserPhone(String orderUserPhone) {
        this.orderUserPhone = orderUserPhone;
    }

    @Transient
    public String getPendpaynum() {
        return pendpaynum;
    }

    public void setPendpaynum(String pendpaynum) {
        this.pendpaynum = pendpaynum;
    }

    @Transient
    public String getShippendnum() {
        return shippendnum;
    }

    public void setShippendnum(String shippendnum) {
        this.shippendnum = shippendnum;
    }

    @Transient
    public String getWaitrecnum() {
        return waitrecnum;
    }

    public void setWaitrecnum(String waitrecnum) {
        this.waitrecnum = waitrecnum;
    }

    @Transient
    public String getPendevalunum() {
        return pendevalunum;
    }

    public void setPendevalunum(String pendevalunum) {
        this.pendevalunum = pendevalunum;
    }
}
