package com.oseasy.xlxq.service.api.order.model;

import com.oseasy.xlxq.service.api.base.IdEntity;

import com.oseasy.xlxq.service.api.tenant.model.TenantHomeStayRoom;
import com.oseasy.xlxq.service.api.tenant.model.TenantUser;
import org.hibernate.annotations.NotFound;
import org.hibernate.annotations.NotFoundAction;
import org.hibernate.annotations.Where;

import javax.persistence.*;
import java.util.Date;


/**
 * 房型预定记录
 */
@Entity
@Where(clause = "deleted=0")
@Table(schema = "db_oe_xlxq", name = "tb_homestay_room_reserve")
public class HomeStayRoomReserve extends IdEntity {

    private TenantHomeStayRoom tenantHomeStayRoom;//所属房型
    private TenantUser tenantUser;//购买用户
    private Date reserveTimeStart;//预定时间(起)
    private Date reserveTimeEnd;//预定时间(止)
    private OrderInfo orderInfo;

    /*后续添加字段*/
    private String reserveConfirmStatus;
    private Date reserveConfirmTime;
    private String reserveCheckinStatus;
    private Date reserveCheckinTime;

    @Column(name = "reserve_confirm_status")
    public String getReserveConfirmStatus() {
        return reserveConfirmStatus;
    }

    @Column(name = "reserve_confirm_time")
    public Date getReserveConfirmTime() {
        return reserveConfirmTime;
    }

    @Column(name = "reserve_checkin_status")
    public String getReserveCheckinStatus() {
        return reserveCheckinStatus;
    }

    @Column(name = "reserve_checkin_time")
    public Date getReserveCheckinTime() {
        return reserveCheckinTime;
    }

    public void setReserveConfirmStatus(String reserveConfirmStatus) {
        this.reserveConfirmStatus = reserveConfirmStatus;
    }

    public void setReserveConfirmTime(Date reserveConfirmTime) {
        this.reserveConfirmTime = reserveConfirmTime;
    }

    public void setReserveCheckinStatus(String reserveCheckinStatus) {
        this.reserveCheckinStatus = reserveCheckinStatus;
    }

    public void setReserveCheckinTime(Date reserveCheckinTime) {
        this.reserveCheckinTime = reserveCheckinTime;
    }

    @ManyToOne
    @JoinColumn(name = "order_id")
    @NotFound(action = NotFoundAction.IGNORE)
    public OrderInfo getOrderInfo() {
        return orderInfo;
    }

    public void setOrderInfo(OrderInfo orderInfo) {
        this.orderInfo = orderInfo;
    }

    @ManyToOne
    @JoinColumn(name = "room_id")
    @NotFound(action = NotFoundAction.IGNORE)
    public TenantHomeStayRoom getTenantHomeStayRoom() {
        return tenantHomeStayRoom;
    }

    public void setTenantHomeStayRoom(TenantHomeStayRoom tenantHomeStayRoom) {
        this.tenantHomeStayRoom = tenantHomeStayRoom;
    }

    @ManyToOne
    @JoinColumn(name = "user_id")
    @NotFound(action = NotFoundAction.IGNORE)
    public TenantUser getTenantUser() {
        return tenantUser;
    }

    public void setTenantUser(TenantUser tenantUser) {
        this.tenantUser = tenantUser;
    }

    @Column(name = "reserve_time_start")
    public Date getReserveTimeStart() {
        return reserveTimeStart;
    }

    public void setReserveTimeStart(Date reserveTimeStart) {
        this.reserveTimeStart = reserveTimeStart;
    }

    @Column(name = "reserve_time_end")
    public Date getReserveTimeEnd() {
        return reserveTimeEnd;
    }

    public void setReserveTimeEnd(Date reserveTimeEnd) {
        this.reserveTimeEnd = reserveTimeEnd;
    }
}
