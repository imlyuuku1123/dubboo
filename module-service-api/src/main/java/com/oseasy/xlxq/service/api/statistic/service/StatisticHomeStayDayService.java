package com.oseasy.xlxq.service.api.statistic.service;

import com.oseasy.xlxq.service.api.base.BaseService;
import com.oseasy.xlxq.service.api.statistic.model.StatisticHomeStayDay;

import java.util.List;


public interface StatisticHomeStayDayService extends BaseService<StatisticHomeStayDay> {

}
