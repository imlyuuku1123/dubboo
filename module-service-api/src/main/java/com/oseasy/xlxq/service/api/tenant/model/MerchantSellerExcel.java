package com.oseasy.xlxq.service.api.tenant.model;

import javax.persistence.Transient;
import java.util.Date;

/**
 * 导出excel，VO实体类
 * Created by heckman on 2017/6/9.
 */

public class MerchantSellerExcel {

    private String username;
    private String displayName;
    private String merchantName;
    private Date createTime;
    private String isUse;

    @Transient
    public String getUsername() {
        return username;
    }

    @Transient
    public String getDisplayName() {
        return displayName;
    }

    @Transient
    public String getMerchantName() {
        return merchantName;
    }

    @Transient
    public Date getCreateTime() {
        return createTime;
    }

    @Transient
    public String getIsUse() {
        return isUse;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public void setDisplayName(String displayName) {
        this.displayName = displayName;
    }

    public void setMerchantName(String merchantName) {
        this.merchantName = merchantName;
    }

    public void setCreateTime(Date createTime) {
        this.createTime = createTime;
    }

    public void setIsUse(String isUse) {
        this.isUse = isUse;
    }
}
