package com.oseasy.xlxq.service.api.tenant.model;

import org.hibernate.annotations.NotFound;
import org.hibernate.annotations.NotFoundAction;
import org.hibernate.annotations.Where;

import javax.persistence.*;
import java.math.BigDecimal;


/**
 * 土特产
 *
 */
@Entity
@Where(clause = "deleted=0")
@Table(schema = "db_oe_xlxq", name = "tb_tenant_specialty")
@PrimaryKeyJoinColumn(name = "id")
public class TenantSpecialty extends TenantProduct {

//    private TenantProduct tenantProduct;//
    //private String catalogRootId;//
    //private String catalogId;//
    private String distributionType;//
    private String buyNum;//购买数量虚拟字段
    private BigDecimal freight;//运费


    private TenantSpecialtyCatalog catalogRoot;
    private TenantSpecialtyCatalog catalog;

    @ManyToOne
    @JoinColumn(name = "catalog_root_id")
    @NotFound(action = NotFoundAction.IGNORE)
    public TenantSpecialtyCatalog getCatalogRoot() {
        return catalogRoot;
    }

    public void setCatalogRoot(TenantSpecialtyCatalog catalogRoot) {
        this.catalogRoot = catalogRoot;
    }

    @ManyToOne
    @JoinColumn(name = "catalog_id")
    @NotFound(action = NotFoundAction.IGNORE)
    public TenantSpecialtyCatalog getCatalog() {
        return catalog;
    }

    public void setCatalog(TenantSpecialtyCatalog catalog) {
        this.catalog = catalog;
    }

    //    @ManyToOne
//    @JoinColumn(name = "product_id")
//    @NotFound(action = NotFoundAction.IGNORE)
//    public TenantProduct getTenantProduct() {
//        return tenantProduct;
//    }

//    public void setTenantProduct(TenantProduct tenantProduct) {
//        this.tenantProduct = tenantProduct;
//    }

    /*@Column(name = "catalog_root_id")
    public String getCatalogRootId() {
        return catalogRootId;
    }

    public void setCatalogRootId(String catalogRootId) {
        this.catalogRootId = catalogRootId;
    }


    @Column(name = "catalog_id")
    public String getCatalogId() {
        return catalogId;
    }

    public void setCatalogId(String catalogId) {
        this.catalogId = catalogId;
    }*/

    @Column(name = "distribution_type")
    public String getDistributionType() {
        return distributionType;
    }

    public void setDistributionType(String distributionType) {
        this.distributionType = distributionType;
    }

    @Transient
    public String getBuyNum() {
        return buyNum;
    }

    public void setBuyNum(String buyNum) {
        this.buyNum = buyNum;
    }

    @Column(name = "freight")
    public BigDecimal getFreight() {
        return freight;
    }

    public void setFreight(BigDecimal freight) {
        this.freight = freight;
    }
}
