package com.oseasy.xlxq.service.api.statistic.model;

import javax.persistence.Transient;

/**
 * Created by heckman on 2017/6/14.
 */
public class StatisticTicketSellExcel {

    private String dateStr;
    private String scenicName;
    private String merchantName;
    private String ticketName;
    private String sellNum;
    private String sellSum;

    @Transient
    public String getDateStr() {
        return dateStr;
    }
    @Transient
    public String getScenicName() {
        return scenicName;
    }
    @Transient
    public String getMerchantName() {
        return merchantName;
    }
    @Transient
    public String getTicketName() {
        return ticketName;
    }
    @Transient
    public String getSellNum() {
        return sellNum;
    }
    @Transient
    public String getSellSum() {
        return sellSum;
    }

    public void setDateStr(String dateStr) {
        this.dateStr = dateStr;
    }

    public void setScenicName(String scenicName) {
        this.scenicName = scenicName;
    }

    public void setMerchantName(String merchantName) {
        this.merchantName = merchantName;
    }

    public void setTicketName(String ticketName) {
        this.ticketName = ticketName;
    }

    public void setSellNum(String sellNum) {
        this.sellNum = sellNum;
    }

    public void setSellSum(String sellSum) {
        this.sellSum = sellSum;
    }
}
