package com.oseasy.xlxq.service.api.tenant.model;

import com.oseasy.xlxq.service.api.base.IdEntity;
import org.hibernate.annotations.NotFound;
import org.hibernate.annotations.NotFoundAction;
import org.hibernate.annotations.Where;

import javax.persistence.*;



/**
 * 
 * 商户
 *
 */
@Entity
@Where(clause = "deleted=0")
@Table(schema="db_oe_xlxq",name = "tb_tenant_merchant")
public class TenantMerchant extends IdEntity {

	private Tenant tenant;//所属租户

	private String merchantName;//商户名称
	private String merchantFullName;//商户姓名
	private String merchantUserName;//商户用户名
	private String merchantCode;//商户密码
	private String remarks;//备注

	private String businessScope;//经营范围
	private String contactAddress;//联系地址
	private String contactPerson;//联系人
	private String contactTelephone;//联系电话
	private String companyName;//公司名称
	private String isUse;//启用禁用

	private String contactProvince;//地址：省份
	private String contactCity;//地址：市
	private String contactCounty;//地址：区


	@Column(name = "contact_province")
	public String getContactProvince() {
		return contactProvince;
	}
	@Column(name = "contact_city")
	public String getContactCity() {
		return contactCity;
	}
	@Column(name = "contact_county")
	public String getContactCounty() {
		return contactCounty;
	}

	public void setContactProvince(String contactProvince) {
		this.contactProvince = contactProvince;
	}

	public void setContactCity(String contactCity) {
		this.contactCity = contactCity;
	}

	public void setContactCounty(String contactCounty) {
		this.contactCounty = contactCounty;
	}

	@ManyToOne
	@JoinColumn(name = "tenant_id")
	@NotFound(action = NotFoundAction.IGNORE)
	public Tenant getTenant() {
		return tenant;
	}

	public void setTenant(Tenant tenant) {
		this.tenant = tenant;
	}

	@Column(name = "merchant_name")
	public String getMerchantName() {return merchantName;}

	public void setMerchantName(String merchantName) {this.merchantName = merchantName;}

	@Column(name = "merchant_fullname")
	public String getMerchantFullName() {
		return merchantFullName;
	}

	public void setMerchantFullName(String merchantFullName) {
		this.merchantFullName = merchantFullName;
	}

	@Column(name = "merchant_username")
	public String getMerchantUserName() {
		return merchantUserName;
	}

	public void setMerchantUserName(String merchantUserName) {
		this.merchantUserName = merchantUserName;
	}

	@Column(name = "merchant_code")
	public String getMerchantCode() {
		return merchantCode;
	}

	public void setMerchantCode(String merchantCode) {
		this.merchantCode = merchantCode;
	}

	@Column(name = "remarks")
	public String getRemarks() {
		return remarks;
	}

	public void setRemarks(String remarks) {
		this.remarks = remarks;
	}

	@Column(name = "business_scope")
	public String getBusinessScope() {
		return businessScope;
	}

	public void setBusinessScope(String businessScope) {
		this.businessScope = businessScope;
	}

	@Column(name = "contact_address")
	public String getContactAddress() {
		return contactAddress;
	}

	public void setContactAddress(String contactAddress) {
		this.contactAddress = contactAddress;
	}

	@Column(name = "contact_person")
	public String getContactPerson() {
		return contactPerson;
	}

	public void setContactPerson(String contactPerson) {
		this.contactPerson = contactPerson;
	}

	@Column(name = "contact_telephone")
	public String getContactTelephone() {
		return contactTelephone;
	}

	public void setContactTelephone(String contactTelephone) {
		this.contactTelephone = contactTelephone;
	}

	@Column(name = "company_name")
	public String getCompanyName() {
		return companyName;
	}

	public void setCompanyName(String companyName) {
		this.companyName = companyName;
	}

	@Column(name = "is_use")
	public String getIsUse() {
		return isUse;
	}

	public void setIsUse(String isUse) {
		this.isUse = isUse;
	}
}


