package com.oseasy.xlxq.service.api.statistic.model;

import com.oseasy.xlxq.service.api.base.IdEntity;
import org.hibernate.annotations.Where;

import javax.persistence.*;
import java.util.Date;

/**
 * 平台用户日统计
 */
@Entity
@Where(clause = "deleted=0")
@Table(schema = "db_oe_xlxq", name = "tb_statistic_tenant_user_day")
public class StatisticTenantUserDay extends IdEntity {

    private Integer personDayNum;//人数(每日增量)
    private Long personSumNum;//人数（总数）
    private String channel;//来源渠道
    private String sex;//性别
    private Integer year;//年
    private Integer month;//月
    private Integer day;//日
    private Date stasticsDay;//统计时间

    @Column(name = "person_day_num")
    public Integer getPersonDayNum() {
        return personDayNum;
    }

    public void setPersonDayNum(Integer personDayNum) {
        this.personDayNum = personDayNum;
    }


    @Column(name = "person_sum_num")
    public Long getPersonSumNum() {
        return personSumNum;
    }

    public void setPersonSumNum(Long personSumNum) {
        this.personSumNum = personSumNum;
    }

    @Column(name = "channel")
    public String getChannel() {
        return channel;
    }

    public void setChannel(String channel) {
        this.channel = channel;
    }

    @Column(name = "sex")
    public String getSex() {
        return sex;
    }

    public void setSex(String sex) {
        this.sex = sex;
    }

    @Column(name = "year")
    public Integer getYear() {
        return year;
    }

    public void setYear(Integer year) {
        this.year = year;
    }

    @Column(name = "month")
    public Integer getMonth() {
        return month;
    }

    public void setMonth(Integer month) {
        this.month = month;
    }

    @Column(name = "day")
    public Integer getDay() {
        return day;
    }

    public void setDay(Integer day) {
        this.day = day;
    }

    @Column(name = "stastics_day")
    public Date getStasticsDay() {
        return stasticsDay;
    }

    public void setStasticsDay(Date stasticsDay) {
        this.stasticsDay = stasticsDay;
    }
}
