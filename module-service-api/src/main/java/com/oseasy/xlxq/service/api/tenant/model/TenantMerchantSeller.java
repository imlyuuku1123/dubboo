package com.oseasy.xlxq.service.api.tenant.model;

import com.oseasy.xlxq.service.api.base.IdEntity;
import org.hibernate.annotations.NotFound;
import org.hibernate.annotations.NotFoundAction;
import org.hibernate.annotations.Where;

import javax.persistence.*;


/**
 * 商户卖家
 */
@Entity
@Where(clause = "deleted=0")
@Table(schema = "db_oe_xlxq", name = "tb_tenant_merchant_seller")
public class TenantMerchantSeller extends IdEntity {

    public static final int DISABLE = 0;
    public static final int ENABLE = 1;

    private TenantMerchant tenantMerchant;//所属商户
    private TenantUser tenantUser;//关联用户
    private String username;//用户名
    private String password;//密码
    private int isUse;//禁用；启用
    private String displayName;//用户名
    private String remark;

    @Column(name = "remark")
    public String getRemark() {
        return remark;
    }

    public void setRemark(String remark) {
        this.remark = remark;
    }

    @Column(name = "display_name")
    public String getDisplayName() {
        return displayName;
    }

    public void setDisplayName(String displayName) {
        this.displayName = displayName;
    }

    @Column(name = "is_use")
    public int getIsUse() {
        return isUse;
    }

    public void setIsUse(int isUse) {
        this.isUse = isUse;
    }

    @ManyToOne
    @JoinColumn(name = "merchant_id")
    @NotFound(action = NotFoundAction.IGNORE)
    public TenantMerchant getTenantMerchant() {
        return tenantMerchant;
    }

    public void setTenantMerchant(TenantMerchant tenantMerchant) {
        this.tenantMerchant = tenantMerchant;
    }


    @ManyToOne
    @JoinColumn(name = "user_id")
    @NotFound(action = NotFoundAction.IGNORE)
    public TenantUser getTenantUser() {
        return tenantUser;
    }

    public void setTenantUser(TenantUser tenantUser) {
        this.tenantUser = tenantUser;
    }

    @Column(name = "username")
    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    @Column(name = "password")
    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }
}


