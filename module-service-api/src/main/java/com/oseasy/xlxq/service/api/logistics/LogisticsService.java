package com.oseasy.xlxq.service.api.logistics;

/**
 * Created by tandy on 2017/5/22.
 */
public interface LogisticsService {

    /**
     * 物流信息查询
     * @param shipperCode
     * @param logisticCode
     * @return
     * {  "EBusinessID": "1289047",  "ShipperCode": "ANE",  "Success": true,  "LogisticCode": "210001633605",  "State": "2",  "Traces": [    {      "AcceptTime": "2016-04-01 13:44:12",      "AcceptStation": "【2016-04-01 13:44:12】【和平滨江道】派送中,派件人【和平滨江道】,查件电话【null】"    },    {      "AcceptTime": "2016-04-01 15:14:31",      "AcceptStation": ""    },    {      "AcceptTime": "2016-04-03 09:42:48",      "AcceptStation": "【2016-04-03 09:42:48】【和平滨江道】派送中,派件人【和平滨江道】,查件电话【null】"    }  ]}
     */
    public String logistics(String shipperCode,String logisticCode);

}
