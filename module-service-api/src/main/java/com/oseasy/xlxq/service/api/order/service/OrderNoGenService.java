package com.oseasy.xlxq.service.api.order.service;

/**
 * Created by heckman on 2017/5/25.
 * 订单号生成服务
 */
public interface OrderNoGenService {
    public static final int ORDER_TYPE_FK = 1;//下单/付款
    public static final int ORDER_TYPE_TK = 2;//退款

    /**
     * 订单号生成
     * 规则：
     * 订单分为付款订单和退款订单
     * 付款订单F开头 yyMMdd 序号
     * 退款订单T开头 yyMMdd 序号
     * F-170523-No
     * T-170523-No
     * <p>
     * 订单号生成服务
     * public String OrderNoService.genOrderNo();
     * No生成规则：
     * 按天生成序号，该需要在redis中保留一个计数 key为 order_no_20170523  该值有效期为25小时 25小时后过期
     * redis计数原子性，不会重复
     *
     * @return
     */
    public String genOrderNo(int orderType);
}
