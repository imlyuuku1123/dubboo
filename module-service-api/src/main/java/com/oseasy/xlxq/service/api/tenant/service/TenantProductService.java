package com.oseasy.xlxq.service.api.tenant.service;

import com.oseasy.xlxq.service.api.base.BaseService;
import com.oseasy.xlxq.service.api.tenant.model.TenantProduct;

import java.util.List;


public interface TenantProductService extends BaseService<TenantProduct> {

    List<TenantProduct> findRecommendedProductList();
}
