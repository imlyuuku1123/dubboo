package com.oseasy.xlxq.service.api.product.service;

import com.oseasy.xlxq.service.api.base.BaseService;
import com.oseasy.xlxq.service.api.product.model.ProductStockChange;

public interface ProductStockChangeService extends BaseService<ProductStockChange> {

}
