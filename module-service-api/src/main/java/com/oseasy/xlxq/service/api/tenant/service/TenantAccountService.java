package com.oseasy.xlxq.service.api.tenant.service;

import com.oseasy.xlxq.service.api.base.BaseService;
import com.oseasy.xlxq.service.api.exceptions.AccountNotFoundException;
import com.oseasy.xlxq.service.api.tenant.model.TenantAccount;
import com.oseasy.xlxq.service.core.exceptions.MatchMutiEntitiesException;

public interface TenantAccountService extends BaseService<TenantAccount> {

    TenantAccount findAccountByUserName(String userName);

    TenantAccount findAccountByUserNameAndPassword(String username, String s) throws AccountNotFoundException, MatchMutiEntitiesException;
}
