package com.oseasy.xlxq.service.api.sys.service;

import com.oseasy.xlxq.service.api.base.BaseService;
import com.oseasy.xlxq.service.api.sys.model.TenantRole;

public interface TenantRoleService extends BaseService<TenantRole> {

}
