package com.oseasy.xlxq.service.api.order.model;

/**
 * Created by heckman on 2017/5/23.
 */
public enum OrderStatus {

    PENDING_PAY("1", "待支付"),
    PENDING_SHIP("2", "待发货"),
    PENDING_RECEIVE("3", "待收货"),
    TRADE_SUCCESS("4", "交易成功"),
    TRADE_CLOSE("5", "交易关闭"),
    PENDING_SURE("6", "待确认"),
    REFUNDED_APPROVAL("11", "退款审批"),
    PENDING_REFUNED("12", "待退款"),
    REFUNDED("13", "已退款"),
    REFUNED_STAND_BY("14", "退款中"),
    REFUNED_FAILED("15", "退款失败");

    public static String PENDING_PAY_CODE = "1";
    public static String PENDING_SHIP_CODE = "2";
    public static String PENDING_RECEIVE_CODE = "3";
    public static String TRADE_SUCCESS_CODE = "4";
    public static String TRADE_CLOSE_CODE = "5";
    public static String PENDING_SURE_CODE = "6";
    public static String REFUNDED_APPROVAL_CODE = "11";
    public static String PENDING_REFUNED_CODE = "12";
    public static String REFUNDED_CODE = "13";
    public static String REFUNED_STAND_BY_CODE = "14";
    public static String REFUNED_FAILED_CODE = "15";


    private String orderCode;
    private String orderDescription;

    OrderStatus(String orderCode, String orderDescription) {
        this.orderCode = orderCode;
        this.orderDescription = orderDescription;
    }

    public String getOrderCode() {
        return orderCode;
    }

    public void setOrderCode(String orderCode) {
        this.orderCode = orderCode;
    }

    public String getOrderDescription() {
        return orderDescription;
    }

    public void setOrderDescription(String orderDescription) {
        this.orderDescription = orderDescription;
    }

    public static String getOrderDescription(String errCode) {
        for (OrderStatus orderStatus : OrderStatus.values()) {
            if (errCode.equals(orderStatus.getOrderCode())) {
                return orderStatus.getOrderDescription();
            }
        }
        return PENDING_PAY.getOrderDescription();
    }
}
