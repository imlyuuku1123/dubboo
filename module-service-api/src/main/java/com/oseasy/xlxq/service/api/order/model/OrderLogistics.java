package com.oseasy.xlxq.service.api.order.model;

import com.oseasy.xlxq.service.api.base.IdEntity;
import com.oseasy.xlxq.service.api.tenant.model.TenantUser;
import org.hibernate.annotations.NotFound;
import org.hibernate.annotations.NotFoundAction;
import org.hibernate.annotations.Where;

import javax.persistence.*;

/**
 * 订单物流信息
 */
@Entity
@Where(clause = "deleted=0")
@Table(schema = "db_oe_xlxq", name = "tb_order_logistics")
public class OrderLogistics extends IdEntity {

    private OrderInfo orderInfo;//所属订单

    private TenantUser tenantUser;//购买人

    private String purchaserName;//购买人姓名

    private String purchaserTel;//购买人电话

    private String orderAddre;//详细收货地址

    private String deliverStatus;//发货状态

    private String deliverNum;//发货物流号

    private String deliverCompany;//发货物流公司

    @Column(name = "deliver_company")
    public String getDeliverCompany() {
        return deliverCompany;
    }

    public void setDeliverCompany(String deliverCompany) {
        this.deliverCompany = deliverCompany;
    }

    @ManyToOne
    @JoinColumn(name = "order_id")
    @NotFound(action = NotFoundAction.IGNORE)
    public OrderInfo getOrderInfo() {
        return orderInfo;
    }

    public void setOrderInfo(OrderInfo orderInfo) {
        this.orderInfo = orderInfo;
    }

    @ManyToOne
    @JoinColumn(name = "purchaser_id")
    @NotFound(action = NotFoundAction.IGNORE)
    public TenantUser getTenantUser() {
        return tenantUser;
    }

    public void setTenantUser(TenantUser tenantUser) {
        this.tenantUser = tenantUser;
    }

    @Column(name = "purchaser_name")
    public String getPurchaserName() {
        return purchaserName;
    }

    public void setPurchaserName(String purchaserName) {
        this.purchaserName = purchaserName;
    }

    @Column(name = "purchaser_tel")
    public String getPurchaserTel() {
        return purchaserTel;
    }

    public void setPurchaserTel(String purchaserTel) {
        this.purchaserTel = purchaserTel;
    }

    @Column(name = "order_addre")
    public String getOrderAddre() {
        return orderAddre;
    }

    public void setOrderAddre(String orderAddre) {
        this.orderAddre = orderAddre;
    }

    @Column(name = "deliver_status")
    public String getDeliverStatus() {
        return deliverStatus;
    }

    public void setDeliverStatus(String deliverStatus) {
        this.deliverStatus = deliverStatus;
    }

    @Column(name = "deliver_num")
    public String getDeliverNum() {
        return deliverNum;
    }

    public void setDeliverNum(String deliverNum) {
        this.deliverNum = deliverNum;
    }

}
