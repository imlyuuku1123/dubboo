package com.oseasy.xlxq.service.api.order.service;

import com.oseasy.xlxq.service.api.base.BaseService;
import com.oseasy.xlxq.service.api.order.model.HomeStayRoomReserve;

public interface HomeStayRoomReserveService extends BaseService<HomeStayRoomReserve> {

}
