package com.oseasy.xlxq.service.api.sys.model;

import com.oseasy.xlxq.service.api.base.IdEntity;
import org.hibernate.annotations.Where;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Table;
import java.util.Date;

/**
 * pvuv日统计
 *
 */
@Entity
@Where(clause = "deleted=0")
@Table(schema = "db_oe_xlxq", name = "td_pvuv_daily")
public class PvUvDaily extends IdEntity {

	private String  year;//年

	private String  month;//月

	private String  day;//日

	private Date stasticsDay;//统计日期

	private Date pvNum;//pv访问量

	private Date uvNum;//uv访问量


	@Column(name = "year")
	public String getYear() {
		return year;
	}

	public void setYear(String year) {
		this.year = year;
	}

	@Column(name = "month")
	public String getMonth() {
		return month;
	}

	public void setMonth(String month) {
		this.month = month;
	}

	@Column(name = "day")
	public String getDay() {
		return day;
	}

	public void setDay(String day) {
		this.day = day;
	}

	@Column(name = "stastics_day")
	public Date getStasticsDay() {
		return stasticsDay;
	}

	public void setStasticsDay(Date stasticsDay) {
		this.stasticsDay = stasticsDay;
	}

	@Column(name = "pv_num")
	public Date getPvNum() {
		return pvNum;
	}

	public void setPvNum(Date pvNum) {
		this.pvNum = pvNum;
	}

	@Column(name = "uv_num")
	public Date getUvNum() {
		return uvNum;
	}

	public void setUvNum(Date uvNum) {
		this.uvNum = uvNum;
	}
}
