package com.oseasy.xlxq.service.api.sys.model;

import com.oseasy.xlxq.service.api.base.IdEntity;
import com.oseasy.xlxq.service.api.tenant.model.TenantUser;
import org.hibernate.annotations.NotFound;
import org.hibernate.annotations.NotFoundAction;
import org.hibernate.annotations.Where;

import javax.persistence.*;
import java.util.Date;

/**
 * 用户微信属性
 *
 */
@Entity
@Where(clause = "deleted=0")
@Table(schema = "db_oe_xlxq", name = "tb_tenant_user_wxext")
public class TenantUserWxExt extends IdEntity {

    //private String userId;//用户ID
    private TenantUser tenantUser;//用户
    private String wxId;//来源微信渠道
    private String wxNickName;//微信昵称
    private String wxOpenid;//微信openid
    private String wxheadUrl;//微信用户头像地址
    private Integer sexFlag;//性别
    private Date registrationDate;//注册日期
    private Integer subscribe;//微信关注状态  0:未关注  1：已关注

   /* @Column(name = "user_id")
    public String getUserId() {
        return userId;
    }

    public void setUserId(String userId) {
        this.userId = userId;
    }*/

    @ManyToOne
    @JoinColumn(name = "user_id")
    @NotFound(action = NotFoundAction.IGNORE)
    public TenantUser getTenantUser() {
        return tenantUser;
    }

    public void setTenantUser(TenantUser tenantUser) {
        this.tenantUser = tenantUser;
    }

    @Column(name = "wx_id")
    public String getWxId() {
        return wxId;
    }

    public void setWxId(String wxId) {
        this.wxId = wxId;
    }

    @Column(name = "wx_nick_name")
    public String getWxNickName() {
        return wxNickName;
    }


    public void setWxNickName(String wxNickName) {
        this.wxNickName = wxNickName;
    }

    @Column(name = "wx_openid")
    public String getWxOpenid() {
        return wxOpenid;
    }


    public void setWxOpenid(String wxOpenid) {
        this.wxOpenid = wxOpenid;
    }

    @Column(name = "wxhead_url")
    public String getWxheadUrl() {
        return wxheadUrl;
    }

    public void setWxheadUrl(String wxheadUrl) {
        this.wxheadUrl = wxheadUrl;
    }

    @Column(name = "sex_flag")
    public Integer getSexFlag() {
        return sexFlag;
    }

    public void setSexFlag(Integer sexFlag) {
        this.sexFlag = sexFlag;
    }

    @Column(name = "registration_date")
    public Date getRegistrationDate() {
        return registrationDate;
    }

    public void setRegistrationDate(Date registrationDate) {
        this.registrationDate = registrationDate;
    }

    @Column(name = "subscribe")
    public Integer getSubscribe() {
        return subscribe;
    }

    public void setSubscribe(Integer subscribe) {
        this.subscribe = subscribe;
    }
}



