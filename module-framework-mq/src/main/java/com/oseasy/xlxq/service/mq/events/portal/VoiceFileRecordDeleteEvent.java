package com.oseasy.xlxq.service.mq.events.portal;

import com.oseasy.xlxq.service.mq.api.AbstractMQEvent;
import com.oseasy.xlxq.service.mq.topic.MQTopicConstants;

/**
 * 删除录音文件事件
 * Created by zhangxb on 2016/9/12.
 */
public class VoiceFileRecordDeleteEvent extends AbstractMQEvent {
    @Override
    public String getTopicName() {
        return MQTopicConstants.TOPIC_APP_PORTAL;
    }

    public VoiceFileRecordDeleteEvent() {
    }

}
