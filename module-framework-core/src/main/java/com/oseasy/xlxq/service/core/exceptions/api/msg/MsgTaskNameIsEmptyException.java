package com.oseasy.xlxq.service.core.exceptions.api.msg;

import com.oseasy.xlxq.service.core.exceptions.api.ApiReturnCodeEnum;
import com.oseasy.xlxq.service.core.exceptions.api.YunhuniApiException;

/**
 * Created by liups on 2017/3/16.
 */
public class MsgTaskNameIsEmptyException extends YunhuniApiException {
    @Override
    public ApiReturnCodeEnum getApiExceptionEnum() {
        return ApiReturnCodeEnum.MsgTaskNameIsEmpty;
    }
}
