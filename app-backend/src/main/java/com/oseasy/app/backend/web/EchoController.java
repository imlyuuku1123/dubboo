package com.oseasy.app.backend.web;

import com.oseasy.xlxq.service.web.rest.RestResponse;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 * Created by tandy on 17/5/17.
 */
@RestController
public class EchoController {

    @RequestMapping("/open/echo")
    public RestResponse echo(String name){
        return RestResponse.success("你好："+name);
    }
}
