package com.oseasy.app.backend.security;

import com.oseasy.xlxq.service.api.tenant.model.TenantAccount;
import com.oseasy.xlxq.service.api.tenant.service.TenantAccountService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.userdetails.User;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by Tandy on 2016/6/7.
 */
@Service("userDetailsService")
public class APIUserDetailsService implements UserDetailsService{
    @Autowired
    TenantAccountService accountService;

    @Override
    public UserDetails loadUserByUsername(String username) throws UsernameNotFoundException {
        TenantAccount account;
            //根据登录用户名来查找用户
        account = accountService.findAccountByUserName(username);
        if(account == null){
            throw new UsernameNotFoundException("用户不存在");
        }else{
            User user = new User(account.getUserName(),account.getPassword(),true,true,true,true,roles("ROLE_TENANT_USER"));
            return user;
        }
    }



    private List<GrantedAuthority> roles(String... roles) {
        List<GrantedAuthority> authorities = new ArrayList<GrantedAuthority>(
                roles.length);
        for (String role : roles) {
            authorities.add(new SimpleGrantedAuthority( role));
        }
        return authorities;
    }
}
